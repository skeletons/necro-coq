(** * Auxiliary functions **)
let give_version () =
	Printf.printf "Necro Coq, version: %s\n"
	(match Build_info.V1.version () with
	 | None -> "n/a"
	 | Some v -> Build_info.V1.Version.to_string v);
	 exit 0


(** Wrap a function call to make the whole program fail with the right error code if an exception happens. **)
let wrap f x =
	let () = Printexc.record_backtrace true in
	try f x
	with e ->
		let stack = Printexc.get_backtrace () in
		let msg = Printexc.to_string e in
		Printf.eprintf "Fatal error: %s%s\n" msg stack ;
		exit 1

(* Print usage in stderr and exit *)
let usage () =
	Printf.eprintf
	("Usage: %s [OPTIONS] <file.sk>\n\n\
	Generate a coq formalization from the given file.\n\n\
	Options:\n\
	\t-o / --output <File.v>\tOutputs the result in File.v instead of stdout.\n\
	\t-v / --version\tPrint version and exit.\n\
	")
	Sys.argv.(0) ;
	exit 1

let parse_options l =
	let rec parse_output l infile outfile =
		begin match l with
		| [] ->
				begin match infile with
				| None -> usage ()
				| Some f -> (f, outfile)
				end
		| "-v" :: _ | "--version" :: _ ->
				give_version ()
		| ("-o" as s) :: q | ("--output" as s) :: q ->
				begin match q with
				| [] ->
						failwith (s ^ " expects the name of an output file")
				| _ :: _ when outfile <> None ->
						failwith "Multiple output files given"
				| out :: q -> parse_output q infile (Some out)
				end
		| _ :: _ when infile <> None -> failwith "Multiple input files given"
		| f :: q -> parse_output q (Some f) outfile
		end
	in
	begin match l with
	| [] -> usage ()
	| opt -> parse_output opt None None
	end

(** MAIN FUNCTION **)
let () =
	let filename, output_file =
		begin match Array.to_list Sys.argv with
		| [] -> assert false
		| _ :: q -> parse_options q
		end
	in
	let sem = Necro.parse_and_type [] filename in
	let result = wrap Coqgenerator.gen sem in
	begin match output_file with
	| None -> print_endline result
	| Some f ->
			let oc = open_out f in
			let () = output_string oc result in
			close_out oc
	end

