Require Import List Ascii String.
From Necro Require Import Dict.
Import ListNotations DictNotations.
From Necro Require Import Skeleton.
Open Scope string_scope.
Set Decidable Equality Schemes.

Module CoqNotations.
  Notation "'Let' a 'Be' b 'In' c" :=
    ((skel_letin a b c)) (left associativity, at level 50).
  Notation "'λ' p '-->' b" :=
    ((term_func p b)) (right associativity, at level 49, p at level 50).
  Notation "'Branch' < ty > 'End'" :=
    (skel_branch ty []) (no associativity, at level 20, ty at level 21).
  Notation "'Branch' < ty > x 'End'" :=
    (skel_branch ty [x]) (no associativity, at level 20, ty at level 21).
  Notation "'Branch' < ty > x 'Or' y 'Or' .. 'Or' z 'End'" :=
    (skel_branch ty (cons x (cons y .. (cons z nil) ..)))
 (no associativity, at level 20, ty at level 21).
End CoqNotations.
Import CoqNotations.

Definition base_type: list string :=
  [{# base_type #}].
{# types #}
Definition constructor: list string :=
  [{# constructor #}].

Definition field: list string :=
  [{# field #}].

Definition unspec_term: list string :=
  [{# unspec_term #}].

Definition spec_term: list string :=
  [{# spec_term #}].

Definition unspec_term_decl: dict (list string * type) :=
  ∅{# unspec_term_decl #}.
{# sigs #}{# defs #}
Definition spec_term_decl: dict (list string * type * term) :=
  ∅{# spec_term_decl #}.

Definition ctype: dict (list string * type * string) :=
  ∅{# ctype #}.

Definition ftype: dict (list string * string * type) :=
  ∅{# ftype #}.

Definition sem := mk_sem
  base_type constructor field unspec_term spec_term
  ctype ftype unspec_term_decl spec_term_decl.
