open Printf
open Necro
open TypedAST

(* Useful auxiliaries functions *)
let monad_fail () =
  failwith "Necrocoq doesn't handle monadic binds for now"
let include_fail () =
  failwith "Coq doesn't support modularity yet"

let rec print_list print ?(sep="; ") = function
| [] -> ""
| x :: [] -> print x
| x :: l -> print x ^ sep ^ print_list print ~sep l

let print_smap print smap =
  print_list (fun (x,y) -> print x y) (Necro.Util.SMap.bindings smap)

let print_type_name (t: typ): string =
  begin match t.tydesc with
  | UserType ((s, None), _) -> s
  | _ -> assert false
  end

let add_par = Printer.add_parentheses

let rec unalias ss (ty: typ): typ =
  begin match ty.tydesc with
  | UserType ((n, _), ta_got) ->
        begin match Util.SMap.find_opt n ss.ss_types with
        | None -> assert false
        | Some (_, TDAlias (ta, ty)) ->
            unalias ss (Types.subst_mult ta ta_got ty)
        | _ -> ty
        end
  | _ -> ty
  end

let rec unalias_rec ss (ty: typ): typ =
  {ty with tydesc=
    begin match ty.tydesc with
    | Variable x -> Variable x
    | UserType ((n, _), ta) ->
        begin match Util.SMap.find_opt n ss.ss_types with
        | Some (_, TDAlias _) ->
            (unalias_rec ss (unalias ss ty)).tydesc
        | _ -> UserType ((n, None), List.map (unalias_rec ss) ta)
        end
    | Arrow (i, o) ->
        Arrow (unalias_rec ss i, unalias_rec ss o)
    | Product tyl ->
        Product (List.map (unalias_rec ss) tyl)
    end}


(* Basic printing functions *)
let rec print_type ss (ty: typ): string =
  begin match (unalias_rec ss ty).tydesc with
  | Variable s -> sprintf "Var \"%s\"" s
  | Product l -> sprintf "Prod [%s]" (print_list (print_type ss) l)
  | UserType ((s, None), []) -> sprintf "τ_%s" s
  | UserType ((s, None), ta) ->
      sprintf "τ_%s %s" s
      (print_list ~sep:" " (fun x -> print_type ss x |> add_par) ta)
  | UserType _ -> include_fail ()
  | Arrow (s1, s2) -> sprintf "Arrow %s %s"
      (print_type ss s1 |> add_par) (print_type ss s2 |> add_par)
  end

let print_option (print:'a -> string) : 'a option -> string = function
  | None -> "None"
  | Some x -> sprintf "(Some %s)" (print x |> add_par)

let print_var (v:string): string =
  "\"" ^ v ^ "\""

let print_typed_var ss (v:variable): string =
  begin match v with
  | LetBound (n, ty) ->
      sprintf "TVLet %s %s"
      (print_var n)
      (print_type ss ty |> add_par)
  | TopLevel (Specified, (n, None), [], _) -> sprintf "σ_%s" n
  | TopLevel (Specified, (n, None), ta, ty) -> sprintf "TVSpec \"%s\" %s [%s]"
      n (print_type ss ty |> add_par) (print_list (print_type ss) ta)
  | TopLevel (Nonspecified, (n, None), [], _) -> sprintf "σ_%s" n
  | TopLevel (Nonspecified, (n, None), ta, ty) ->
      sprintf "TVUnspec \"%s\" %s [%s]"
      n (print_type ss ty |> add_par) (print_list (print_type ss) ta)
  | TopLevel (_, (_, Some _), _, _) -> include_fail ()
  end

let rec print_pattern ss p =
  begin match p.pdesc with
  | PTuple l ->
      sprintf "pattern_tuple [%s]" (print_list (print_pattern ss) l)
  | PVar x ->
      sprintf "pattern_var %s %s" (print_var x) (print_type ss p.ptyp |> add_par)
  | PWild ->
      sprintf "pattern_wildcard %s"
        (print_type ss p.ptyp |> add_par)
  | PConstr (((c, _), (_, ta)), p) ->
      sprintf "pattern_constr \"%s\" [%s] %s"
      c (print_list (print_type ss) ta) (print_pattern ss p |> add_par)
  | PRecord pl ->
      let print_one (((f, _),(_, ta)), p) =
        sprintf "(\"%s\", [%s], %s)"
        f (print_list (print_type ss) ta) (print_pattern ss p)
      in
      sprintf "pattern_rec [%s]" (print_list print_one pl)
  | POr (p1, p2) ->
      sprintf "pattern_or %s %s" (print_pattern ss p1 |> add_par) (print_pattern ss p2 |> add_par)
  | PType (p, _) -> print_pattern ss p
  end

let nl indent = "\n" ^ String.make indent ' '

let rec print_term ss indent t: string =
  begin match t.tdesc with
  | TVar tv ->
      sprintf "term_var %s" (print_typed_var ss tv |> add_par) ;
  | TConstr (((c, _), (_, args)), t) ->
      sprintf "term_constructor \"%s\" [%s] %s"
      c (print_list (print_type ss) args)
      (print_term ss indent t |> add_par)
  | TTuple tl ->
      sprintf "term_tuple [%s]" (print_list (fun f -> print_term ss indent f) tl)
  | TFunc (p, sk) ->
      sprintf "λ %s -->%s%s"
      (print_pattern ss p)
      (nl (indent+2))
      (print_skeleton ss (indent+2) sk);
  | TField (t, ((f, _), (_, ta))) ->
      begin match int_of_string_opt f with
      | None ->
          sprintf "term_field %s [%s] \"%s\""
          (print_term ss indent t |> add_par)
          (print_list (print_type ss) ta) f
      | Some n -> 
          sprintf "term_nth %s [%s] %d"
          (print_term ss indent t |> add_par)
          (print_list (print_type ss) ta) (n - 1)
      end
  | TNth _ -> assert false (* TODO *)
  | TRecMake tl ->
      let print_one (((f, _), (_, ta)), t) =
        sprintf "(\"%s\", [%s], %s)"
        f (print_list (print_type ss) ta) (print_term ss indent t)
      in
      sprintf "term_rec_make [%s]" (print_list print_one tl)
  | TRecSet (t, tl) ->
      let print_one (((f, _), (_, ta)), t) =
        sprintf "(\"%s\", [%s], %s)"
        f (print_list (print_type ss) ta)
        (print_term ss indent t)
      in
      sprintf "term_rec_set %s [%s]"
      (print_term ss indent t |> add_par)
      (print_list print_one tl)
  end
and print_skeleton ss indent sk: string =
  begin match sk.sdesc with
  | LetIn (b, p, s1, s2) ->
      let () = match b with | NoBind -> () | _ -> monad_fail () in
      let pattern = print_pattern ss p in
      "Let"  ^ " " ^ pattern ^ " Be" ^
        nl (indent + 2) ^ print_skeleton ss (indent + 2) s1 ^
      " In" ^ nl indent ^ print_skeleton ss indent s2
  | Exists ty ->
      sprintf "skel_exists %s"
      (print_type ss ty |> add_par)
  | Branching (ty, sl) ->
      let sep = nl indent ^ "Or" ^ nl (indent + 2) in
      sprintf "Branch <%s>%s%s%sEnd"
      (print_type ss ty) (nl (indent+2))
      (print_list (fun s -> print_skeleton ss (indent + 2) s) ~sep sl)
      (nl indent)
  | Match (s, psl) ->
      sprintf "skel_match %s %s [%s]"
      (print_skeleton ss indent s |> add_par)
      (print_type ss sk.styp |> add_par)
      (print_list (fun (p, s) ->
        sprintf "(%s, %s)" (print_pattern ss p) (print_skeleton ss indent s)) psl)
  | Apply (t1, t2) ->
      sprintf "skel_apply %s %s"
      (print_term ss indent t1 |> add_par)
      (print_term ss indent t2 |> add_par)
  | Return t ->
      sprintf "skel_return %s" (print_term ss indent t |> add_par)
  end


(* Main printing functions *)
let def_base_type base_types: string =
    print_smap (fun n _ -> print_var n) base_types

let def_constructor constructors: string =
  print_list (fun c -> print_var c.cs_name) constructors

let def_field fields: string =
  print_list (fun f -> print_var f.fs_name) fields

let def_ctype ss constructors: string =
  let print_one_constr c: string = 
      let name = c.cs_name in
      let args = print_type ss c.cs_input_type in
      let out = c.cs_variant_type in
      let ta = print_list print_var c.cs_type_args in
      sprintf "  + { \"%s\" → ([%s], %s, \"%s\") }" name ta args out
  in
    if constructors = [] then "" else
    "\n" ^ print_list print_one_constr ~sep:"\n" constructors

let def_ftype ss fields: string =
  let print_one_field f: string = 
      let name = f.fs_name in
      let rtype = f.fs_record_type in
      let ftype = print_type ss f.fs_field_type in
      let ta = print_list print_var f.fs_type_args in
      sprintf "  + { \"%s\" → ([%s], \"%s\", %s) }" name ta rtype ftype
  in
    if fields = [] then "" else
    "\n" ^ print_list print_one_field ~sep:"\n" fields

let unspec_term_decl ss term_decls: string =
  let print_one_term_decl (s, (_, (l, ty, _))) =
    sprintf "  + { \"%s\" → ([%s], %s) }" s (print_list print_var l) (print_type ss ty)
  in
  if Necro.Util.SMap.is_empty term_decls then "" else
    "\n" ^ print_list print_one_term_decl ~sep:"\n" (Necro.Util.SMap.bindings
    term_decls)

let spec_term_decl ss term_decls: string =
  let print_one_term_decl (s, (_, (l, ty, _))) =
    sprintf "  + { \"%s\" → ([%s], %s, δ_%s)}" s (print_list print_var l) (print_type ss
    ty) s
  in
  if Necro.Util.SMap.is_empty term_decls then "" else
    "\n" ^ print_list print_one_term_decl ~sep:"\n" (Necro.Util.SMap.bindings
    term_decls)

let spec_term_sig ss term_decls: string =
  let print_one_term_sig (s, (_, (ta, ty, t))) =
    begin match ta, t with
    | [], None ->
        sprintf "Definition σ_%s := TVUnspec \"%s\" %s [].\n" s s
        (print_type ss ty |> add_par)
    | [], Some _ ->
        sprintf "Definition σ_%s := TVSpec \"%s\" %s [].\n" s s
        (print_type ss ty |> add_par)
    | _ -> ""
    end
  in
  if Necro.Util.SMap.is_empty term_decls then "" else
    "\n" ^ print_list print_one_term_sig ~sep:"" (Necro.Util.SMap.bindings
    term_decls)

let spec_term_def ss term_decls: string =
  let print_one_term_def (s, (_, (_, _, t))) =
    sprintf "Definition δ_%s: term :=\n  %s." s (print_term ss 2 (Option.get t))
  in
  if Necro.Util.SMap.is_empty term_decls then "" else
    "\n" ^ print_list print_one_term_def ~sep:"\n" (Necro.Util.SMap.bindings
    term_decls) ^ "\n"

let unspec_term term_decls =
  print_list (fun (s, _) -> print_var s)
    (Necro.Util.SMap.bindings term_decls)

let spec_term term_decls =
  print_list (fun (s, _) -> print_var s)
    (Necro.Util.SMap.bindings term_decls)

let def_types types =
  let print_one (name, (_, td)) =
    let typeargslength =
      begin match td with
      | TDUnspec i -> i
      | TDVariant (ta, _) | TDRecord (ta, _) | TDAlias (ta, _) ->
          List.length ta
      end
    in
    begin match typeargslength with
    | 0 -> sprintf "Definition τ_%s := Base \"%s\" []." name name
    | n ->
        let ta = List.init n (fun i -> sprintf "τ%d" (i+1)) in
        sprintf "Definition τ_%s %s := Base \"%s\" [%s]."
        name (print_list ~sep:" " Fun.id ta)
        name (print_list Fun.id ta)
    end
  in
  if Necro.Util.SMap.is_empty types then "" else
  let types = Necro.Util.SMap.bindings types in
  "\n" ^ print_list ~sep:"\n" print_one types ^ "\n"
