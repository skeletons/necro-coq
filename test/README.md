This folder provides some examples of use for necro-coq

# `certif`

The [`certif`](certif) folder provides a proof of correction for a factorial
function, written in IMP. 

# `compil`

The [`compil`](compil) folder is not up to date. It used to provide a proof of a
compiler between an arithmetic language and a stack manipulation language. It
~~might~~ will be brought up to date some day

# `WF`
The [`WF`](WF) folder contains a proof of the well-formedness of a skeletal
semantics. It is extremely generic, and it can be used to prove the
well-formedness of any skeletal semantics, just by replacing the `Lists.sem` in
the body of the lemma.

# `test_all_files`

The [`test_all_files`](test_all_files) folder contains a script `test.sh`, which
tests `necrocoq` on all files in [`necro-test`](../necro-test)

# `lambda`

The [`lambda`](lambda) folder contains the file
[`Certif_lambda.v`](lambda/Certif_lambda.v), initially done by Olivier Idir, which
proves the equivalence of an extension of lambda calculus in big step, and the
small step version. The two versions are defined in
[`lambda_calculus.sk`](lambda/lambda_calculus.sk)
