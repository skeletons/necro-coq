Require Import Lists.
From Necro Require Import WellFormed.

Lemma wf_sem:
  well_formed_semantics Lists.sem.
Proof.
  well_formed.
Qed.

(** Yes it is that simple *)
