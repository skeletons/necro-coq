#!/bin/bash

for f in $(ls ../../necro-test/*.sk); do
	echo "Testing necrocoq on $(basename $f)"
	../../necrocoq $f -o Test_$(basename -s .sk $f).v &&
	coqc -R ../../files Test_$(basename -s .sk $f).v
done
