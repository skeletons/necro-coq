Require Arithmetic Stack.
Require Import List Ascii String.
Require Import Skeleton Concrete.
Require Import Prelim.
Import ListNotations.
Open Scope string_scope.
Set Default Goal Selector "all".


Notation "x <- y ; z" := (option_bind y (fun x => z))
    (right associativity, at level 55, y at level 53).

Fixpoint cast_binop (e : A.constructor A.s_binop) : S.constructor S.s_binop :=
  match e with
  | A.C_Add => S.C_Add
  | A.C_Sub => S.C_Sub
  | A.C_Mult => S.C_Mult
  | A.C_Div => S.C_Div
  end.

Fixpoint uncast_binop (e : S.constructor S.s_binop) : A.constructor A.s_binop :=
  match e with
  | S.C_Add => A.C_Add
  | S.C_Sub => A.C_Sub
  | S.C_Mult => A.C_Mult
  | S.C_Div => A.C_Div
  end.

(* Définition du compilateur *)
Fixpoint compile (e : cvalue ar bia) : option(cvalue st bis):=
  match e with
  (* Const n ⇒ Push n *)
  | cval_constructor _ _ _ A.C_Const (cval_base _ _ A.s_literal n) =>
      Some (cval_constructor st bis _ S.C_Push
        (cval_base st bis S.s_literal (castLiteral n)))
  (* Binop e1 e2 ⇒ compile e1 ; compile e2 ; Binop *)
  | cval_constructor _ _ _ A.C_Binop (cval_tuple _ _ [cval_constructor _ _ A.s_binop op (cval_tuple _ _ []) ; e1 ; e2]) =>
          e'1 <- (compile e1);
          e'2 <- (compile e2);
          Some (cval_constructor st bis _ S.C_Seq
            (cval_tuple _ _
            [ cval_constructor st bis _ S.C_Seq (cval_tuple st bis [e'1; e'2]);
              cval_constructor st bis _ S.C_Binop 
                (cval_constructor st bis _ (cast_binop op) (cval_tuple st bis [])) ] ))
    | _ => None
  end.


Notation stack_semantics := (hook_semantics st bis Sfilter_interp).
Notation arith_semantics := (hook_semantics ar bia Afilter_interp).
Notation cval_tuple := (cval_tuple _ _).
Notation cval_base := (cval_base _ _ _).


Lemma compile_binop_sort :
  forall op n1 n2 n,
  arith_semantics (A.h_apply, cval_tuple [op; n1; n2], n) ->
  exists n'1 n'2 n' op',
    n1 = Concrete.cval_base ar bia A.s_value n'1 /\
    n2 = Concrete.cval_base ar bia A.s_value n'2 /\
    n = Concrete.cval_base ar bia A.s_value n' /\
    op = Concrete.cval_constructor ar bia A.s_binop op' (cval_tuple []).
Proof.
  intros op n1 n2 n [[|p] H]; [contradiction H|].
  apply inv_hook_consequence in H.
  destruct H as (e & v & sk & Heq & Hasn & H).
  repeat destruct Heq as [Heq|Heq].
  try solve [inversion Heq].
  injection Heq as <- <-.
  unfold make_asn in Hasn.
  destruct op as [|[] c [| |[|? ?]]|]; simpl in Hasn.
  try solve [inversion Hasn].
  try (destruct A.constructor_eq_dec in Hasn).
  try solve [inversion Hasn]; subst.
  injection Hasn as <-.
  apply invF in H.
  destruct H as (Ht & e & v & Heq1 & Heq2 & H).
  injection Heq1 as <- <-.
  simpl in Heq2. injection Heq2 as <-.
  1:apply AddArithmeticSort in H.
  2:apply DivArithmeticSort in H.
  4:apply SubArithmeticSort in H.
  3:apply MultArithmeticSort in H.
  destruct H as (n'1 & n'2 & n' & eq1 & eq2).
  injection eq1 as -> ->. subst n.
  exists n'1, n'2, n'; eexists.
  repeat split; reflexivity.
Qed.

Lemma compile_binop_complete :
  forall op v1 v2 v,
    (* apply (op, v1, v2) ⇓a v *)
    arith_semantics (A.h_apply, cval_tuple [cval_constructor _ _ _ op (cval_tuple []);
      cval_base v1; cval_base v2], cval_base v) -> 
    (* apply(op, v1, v2) ⇓s v *)
    consequence st bis Sfilter_interp 1 (S.h_apply, cval_tuple
      [cval_constructor st bis _ (cast_binop op) (cval_tuple []);
       cval_base (castValue v1); cval_base (castValue v2)], cval_base (castValue v)).
Proof.
  intros op v1 v2 v.
  (* We extract the hypothesis that v1 op v2 = v *)
  intros [[] H]; [inversion H| apply inv_hook_consequence in H].
  destruct H as (e & v0 & sk & Hin & Hmakeasn & H).
  unfold make_asn in Hmakeasn.
  repeat( destruct Hin as [Heq|Hin];
      try (injection Heq as <- <-; simpl in Hmakeasn; try congruence) ).
  try contradiction Hin. try inversion Heq.
  destruct Arithmetic.constructor_eq_dec as [eq|neq]; [subst|inversion Hmakeasn].
  injection Hmakeasn as <-.
  apply invF in H.
  destruct H as (? & e & v0 & Heq1 & Heq2 & H).
  injection Heq1 as <- <-.
  injection Heq2 as <-.
  (* Now we conclude by using the hypothesis, and the equivalence of the operations *)
  econstructor.
  1:do 0 right; left; reflexivity.
  3:do 1 right; left; reflexivity.
  5:do 2 right; left; reflexivity.
  7:do 3 right; left; reflexivity.
  try reflexivity.
  econstructor; [reflexivity|].
  1: apply sameAdd.
  2: apply sameDiv.
  3: apply sameMult.
  4: apply sameSub.
  assumption.
Qed.

Lemma compile_k_complete :
  forall k e n s (s2 ns2: cvalue st bis),
    compile e = Some s ->
    Sfilter_interp Stack.h_push (cval_tuple [cval_base (castValue n); s2]) ns2 ->
    consequence ar bia Afilter_interp k (A.h_expr, e, cval_base n) ->
    hook_semantics st bis Sfilter_interp (Stack.h_eval, cval_tuple [s2; s], ns2).
Proof.
  induction k as [k IHk] using Wf_nat.lt_wf_rec.
  intros e n s s2 ns2 Hc Hp Hs.
  destruct e as [|v1 [] arg|]; simpl in Hc; try congruence.
  (* Constant *)
  2:{ clear IHk.
  (* arg matches cval_base Arithmetic.s_literal n *)
    destruct arg as [[]| |]; simpl in Hc; try congruence.
    injection Hc as <-.
    (* destruct Hs *)
    destruct k as [|]; inversion Hs; subst.
    repeat (destruct H3 as [Heq|H3]); try destruct H3;
        try inversion Heq;
        unfold make_asn in H4; simpl in H4; try discriminate H4.
    injection Heq as <- <-. inversion H4.
    injection H4 as <-. inversion H5. subst.
    clear H5 H0 H1 H2 Hs.
    simpl in H8; injection H8 as <-.
    rename H9 into HlitToVal.
    (* Now do the proof *)
    exists 1. simpl.
    econstructor; [do 5 right; left; reflexivity|reflexivity|].
    econstructor.
    + econstructor; [reflexivity|].
      apply sameLitToVal; exact HlitToVal.
    + reflexivity.
    + econstructor; [reflexivity|exact Hp].
  }
  (* Addition, Division, Multiplication, Substraction *)
    (* first, k is a successor *)
    destruct k as [|k]; [destruct Hs|].
    (* arg matches (cval_tuple [op; e1; e2]) *)
    destruct arg as [| |[|[|[] c [| |[|? ?]]|] [|e1 [|e2 [|? ?]]]]]; simpl in Hc; try congruence.
    (* compile e1 matches Some c1 *)
    destruct (compile e1) as [c1|] eqn:Hc1; try (simpl in Hc; congruence).
    (* compile e2 matches Some c2 *)
    destruct (compile e2) as [c2|] eqn:Hc2; try (simpl in Hc; congruence).
    (* Now we know the result s *)
    injection Hc as <-.
    (* Now we unfold the skeleton for the evaluation of op e1 e2 *)
    simpl in Hs. apply inv_hook_consequence in Hs.
    destruct Hs as (e & v & S & Hhook_interp & Hmake_asn & Hinterp).
    (* v matches op e1 e2 *)
    destruct v as [v' [] []| |];
        try (unfold make_asn in Hmake_asn; simpl in Hmake_asn; congruence).
    (* Get the rule for op e1 e2 *)
    (repeat destruct Hhook_interp as [Heq|Hhook_interp]; try congruence);
    try contradiction Hhook_interp.
    (* Break Heq, deduce skeleton *)
    injection Heq as Heq1 Heq2; subst.
    try discriminate.
    (* Destruct skeleton into pieces *)
    apply invLetIn in Hinterp.
    destruct Hinterp as (?e & env & ? & n1 & eq & Henv & Hbone & Hskel); subst.
    unfold make_asn in Hmake_asn. simpl in Hmake_asn.
    injection Hmake_asn as <-.
    injection eq as <- <-.
    apply invH in Hbone.
    destruct Hbone as (? & ? & ? & eq & Heval & IH1).
    injection eq as <- <-. injection Heval as <-.
    simpl in Henv; injection Henv as <-.
    apply invLetIn in Hskel.
    destruct Hskel as (?e & env & ? & n2 & eq & Henv & Hbone1 & Hbone2); subst.
    injection eq as <- <-.
    apply invH in Hbone1.
    destruct Hbone1 as (? & ? & ? & eq & Heval & IH2).
    injection eq as <- <-. injection Heval as <-.
    simpl in Henv; injection Henv as <-.
    apply invH in Hbone2.
    destruct Hbone2 as (Ht & e & v & eq & Heval & IHop).
    injection eq as <- <-. injection Heval as <-.
    (* Time to apply induction hypothesis *)
    specialize(IHk k (le_n _)).
    apply (ex_intro (fun k => consequence ar bia _ k _)) in IHop.
    destruct(compile_binop_sort _ n1 n2 (cval_base n) IHop)
      as (n'1&n'2&_&_&eq1&eq2&_&_); subst.
    assert(Hcompile_binop:=compile_binop_complete c n'1 n'2 n).
    destruct(PushStackSort _ _ Hp) as (? & s & ns & eq1 & eq2).
    injection eq1 as <- ->. subst.
    rename n'1 into n1, n'2 into n2.
    destruct(StackPushDef (castValue n1) s) as (n1s & Hn1s).
    destruct(StackPushDef (castValue n2) n1s) as (n2n1s & Hn2n1s).
    assert(IHk1 := IHk e1 n1 c1 _ _ Hc1 Hn1s IH1).
    assert(IHk2 := IHk e2 n2 c2 _ _ Hc2 Hn2n1s IH2).
    clear IHk IH1 IH2.
    destruct IHk1 as [p1 IH1]. destruct IHk2 as [p2 IH2].
    (* The max of both p is enough *)
    exists (S (S (S (max p1 p2)))).
    assert(Hk1:p1<=S (max p1 p2)) by (constructor; apply Max.le_max_l).
    assert(Hk2:p2<=S (max p1 p2)) by (constructor; apply Max.le_max_r).
    apply (Shcons_inc_str _ _ _ Hk1) in IH1.
    apply (Shcons_inc_str _ _ _ Hk2) in IH2.
    set(p := max p1 p2) in *; clearbody p; clear p1 p2 Hk1 Hk2.
    (* Now let's do the proof *)
    simpl; econstructor; [do 7 right; left; f_equal | reflexivity| ].
    econstructor; [econstructor; [reflexivity|]|..].
    1:simpl; econstructor; [do 7 right; left; f_equal | | ].
    1:unfold make_asn; simpl; reflexivity.
    1:econstructor;
      [ econstructor; [reflexivity|exact IH1]
      | reflexivity
      | econstructor; [reflexivity|exact IH2]].
    1:reflexivity.
    clear IH1 IH2.
    econstructor; [reflexivity|].
    1:econstructor; [do 6 right; left; f_equal | reflexivity| ].
    econstructor.
    {
      econstructor; [reflexivity |].
      apply StackPushPopDef; exact Hn2n1s.
    }
    1:reflexivity.
    econstructor.
    {
      econstructor; [reflexivity |].
      apply StackPushPopDef; exact Hn1s.
    }
    1:reflexivity.
    econstructor.
    - econstructor; [reflexivity |].
      apply (Shcons_inc_str 1 (S p)).
      + apply le_n_S, le_0_n.
      + apply Hcompile_binop. assumption.
    - reflexivity.
    - econstructor; [reflexivity |]. exact Hp.
Qed.


Lemma cast_uncast:
  forall o, uncast_binop (cast_binop o) = o.
Proof.
  refine(fun o => match o with
  | A.C_Add => _
  | A.C_Sub => _
  | A.C_Mult => _
  | A.C_Div => _
  end). reflexivity.
Qed.

Lemma compile_binop_correct :
  forall op v1 v2 v,
    (* apply (op, v1, v2) ⇓s v *)
    stack_semantics (S.h_apply, cval_tuple [cval_constructor st bis _ (cast_binop op) (cval_tuple []);
    cval_base (castValue v1); cval_base (castValue v2)], cval_base (castValue v)) -> 
    (* apply(op, v1, v2) ⇓a v *)
    consequence ar bia Afilter_interp 1 (A.h_apply, cval_tuple [cval_constructor
    ar bia _ op (cval_tuple []); cval_base v1; cval_base v2], cval_base v).
Proof.
  intros op v1 v2 v.
  (* We extract the hypothesis that v1 op v2 = v *)
  intros [[] H]; [inversion H| apply inv_hook_consequence in H].
  destruct H as (e & v0 & sk & Hin & Hmakeasn & H).
  unfold make_asn in Hmakeasn.
  repeat( destruct Hin as [Heq|Hin];
      try (injection Heq as <- <-; simpl in Hmakeasn; try congruence) ).
  try contradiction Hin. try inversion Heq.
  destruct Stack.constructor_eq_dec as [eq|]; [subst|inversion Hmakeasn].
  injection Hmakeasn as <-.
  apply invF in H.
  destruct H as (? & e & v0 & Heq1 & Heq2 & H).
  injection Heq1 as <- <-.
  injection Heq2 as <-.
  (* Now we conclude by using the hypothesis, and the equivalence of the operations *)
  econstructor.
  1:do 0 right; left; reflexivity.
  3:do 1 right; left; reflexivity.
  5:do 2 right; left; reflexivity.
  7:do 3 right; left; reflexivity.
  unfold make_asn. simpl.
  apply (f_equal uncast_binop) in eq; simpl in eq; rewrite cast_uncast in eq.
  subst; simpl; try reflexivity.
  econstructor; [reflexivity|].
  1: apply sameAdd.
  2: apply sameDiv.
  3: apply sameMult.
  4: apply sameSub.
  assumption.
Qed.

Lemma compile_k_correct :
  forall k e s s2 ns2,
    compile e = Some s ->
    consequence st bis Sfilter_interp k (S.h_eval, cval_tuple [s2; s], ns2) ->
    exists n, 
      Sfilter_interp Stack.h_push
        (cval_tuple [cval_base (castValue n); s2]) ns2 /\
      hook_semantics ar bia Afilter_interp
        (A.h_expr, e, cval_base n).
Proof.
  induction k as [k IHk] using Wf_nat.lt_wf_rec.
  destruct e as [|v1 [] arg|]; intros s s2 ns2 Hc Hp;
      try (simpl in Hc; congruence).
  (* Constant *)
  2:{clear IHk.
    (* arg matches cval_base Arithmetic.s_literal n *)
    destruct arg as [[]| |]; simpl in Hc; try congruence.
    injection Hc as <-.
    (* unfold the meaning of Hp *)
    destruct k; [destruct Hp|simpl in Hp].
    apply inv_hook_consequence in Hp.
    destruct Hp as (e & v & sk & Heq & Hmakeasn & Hinterp).
    (* v is (s, Push) *)
    unfold make_asn in Hmakeasn.
    repeat destruct Heq as [Heq|Heq].
    try (inversion Heq; fail).
    try injection Heq as <- <-.
    simpl in Hmakeasn.
    try congruence.
    injection Hmakeasn as <-.
    (* Destruct Hinterp into pieces *)
    apply invLetIn in Hinterp.
    destruct Hinterp as (? & ? & ? & v & Heq & addasn & is1 & is2).
    injection Heq as <- <-. injection addasn as <-.
    apply invF in is1. destruct is1 as (? & ? & ? & Heq & Heval & fi1).
    injection Heq as <- <-. injection Heval as <-. 
    apply invF in is2. destruct is2 as (? & ? & ? & Heq & Heval & fi2).
    injection Heq as <- <-. injection Heval as <-.
    destruct (LitToValStackSort _ _ fi1) as [_ [cast_n [_ H]]]; subst.
    destruct(proj2 sameValue cast_n) as [n Hn]; subst.
    apply sameLitToVal in fi1.
    (* Now just give the results *)
    exists n. split; [assumption|].
    exists 1. econstructor; [do 4 right; left; reflexivity|reflexivity|].
    econstructor; [reflexivity|assumption].
  }
  (* Binary operands *)
  (* arg is a tuple [op, e1, e2] *)
  destruct arg as [| |[|[|[] c [| |[|? ?]]|] [|e1 [|e2 [|? ?]]]]]; simpl in Hc; try congruence.
  destruct (compile e1) as [c1|] eqn:Hc1; try (simpl in Hc; congruence).
  destruct (compile e2) as [c2|] eqn:Hc2; try (simpl in Hc; congruence).
  injection Hc as <-.
  (* We break Hp into pieces *)
  destruct k as [|k]; [contradiction Hp|].
  simpl in Hp; apply inv_hook_consequence in Hp.
  destruct Hp as (? & ? & ? & Heq & Hmakeasn & Hinterp).
  unfold make_asn in Hmakeasn.
  repeat destruct Heq as [Heq|Heq].
  try (inversion Heq; fail).
  try injection Heq as <- <-.
  simpl in Hmakeasn.
  try congruence.
  injection Hmakeasn as <-.
  apply invLetIn in Hinterp.
  destruct Hinterp as (? & ? & ? & v & Heq & addasn & is1 & is2).
  injection Heq as <- <-. injection addasn as <-.
  apply invH in is1.
  destruct is1 as (?&?&?&Heq&Heval&IH).
  injection Heq as <- <-. injection Heval as <-.
  apply invH in is2.
  destruct is2 as (?&?&?&Heq&Heval&IHop).
  injection Heq as <- <-. injection Heval as <-.
  destruct k as [|k]; [contradiction IH|].
  simpl in IH; apply inv_hook_consequence in IH.
  destruct IH as (? & ? & ? & Heq & Hmakeasn & Hinterp).
  unfold make_asn in Hmakeasn.
  repeat destruct Heq as [Heq|Heq].
  try (inversion Heq; fail).
  try injection Heq as <- <-.
  simpl in Hmakeasn.
  try congruence.
  injection Hmakeasn as <-.
  apply invLetIn in Hinterp.
  destruct Hinterp as (? & ? & ? & v' & Heq & addasn & is1 & is2).
  injection Heq as <- <-. injection addasn as <-.
  apply invH in is1.
  destruct is1 as (?&?&?&Heq&Heval&IH1).
  injection Heq as <- <-. injection Heval as <-.
  apply invH in is2.
  destruct is2 as (?&?&?&Heq&Heval&IH2).
  injection Heq as <- <-. injection Heval as <-.
  (* Apply induction hypothesis *)
  assert(IHk1:=IHk k (ltac:(repeat econstructor)) e1 c1 s2 v' Hc1 IH1).
  assert(IHk2:=IHk k (ltac:(repeat econstructor)) e2 c2 v' v Hc2 IH2).
  clear IHk IH1 IH2;
    destruct IHk1 as [n1 [pushn1 [p1 IH1]]];
    destruct IHk2 as [n2 [pushn2 [p2 IH2]]].
  destruct(PushStackSort _ _ pushn1) as (?&s&n1s'&H1&H2).
  injection H1 as <- ->; subst.
  destruct(PushStackSort _ _ pushn2) as (?&n1s&n2n1s&H1&H2).
  injection H1 as <- ->; subst.
  (* Destruct Hop *)
  apply inv_hook_consequence in IHop.
  destruct IHop as (? & ? &  ? & Heq & Hmakeasn & is).
  unfold make_asn in Hmakeasn.
  repeat destruct Heq as [Heq|Heq].
  try (inversion Heq; fail).
  try injection Heq as <- <-.
  simpl in Hmakeasn.
  try congruence.
  injection Hmakeasn as <-.
  apply invLetIn in is.
  destruct is as (? & ? & ? & v' & Heq & addasn & is1 & is2).
  injection Heq as <- <-.
  apply invF in is1.
  destruct is1 as (?&?&?&Heq&Heval&popn2).
  injection Heq as <- <-. injection Heval as <-.
  assert(Heq:=StackPushPop _ _ _ pushn2 popn2); subst.
  injection addasn as <-.
  apply invLetIn in is2.
  destruct is2 as (? & ? & ? & v' & Heq & addasn & is1 & is2).
  injection Heq as <- <-.
  apply invF in is1.
  destruct is1 as (?&?&?&Heq&Heval&popn1).
  injection Heq as <- <-. injection Heval as <-.
  assert(Heq:=StackPushPop _ _ _ pushn1 popn1); subst.
  injection addasn as <-.
  apply invLetIn in is2.
  destruct is2 as (? & ? & ? & v' & Heq & addasn & is1 & is2).
  injection Heq as <- <-. injection addasn as <-.
  apply invH in is1. apply invF in is2.
  destruct is1 as (?&?&?&Heq&Heval&IHop).
  injection Heq as <- <-. injection Heval as <-.
  destruct is2 as (?&?&?&Heq&Heval&pushn).
  injection Heq as <- <-. injection Heval as <-.
  destruct(PushStackSort _ _ pushn) as (cast_n&ss&ns&H1&H2).
  injection H1 as -> _; subst; clear ss.
  destruct(proj2 sameValue cast_n) as [n ?]; subst.
  exists n; split; [exact pushn|].
  (* Conclude *)
  set(p := S (max p1 p2)).
  apply (Ahcons_inc_str p1 p) in IH1; [|constructor; apply Max.le_max_l].
  apply (Ahcons_inc_str p2 p) in IH2; [|constructor; apply Max.le_max_r].
  exists (S p).
  econstructor; [do 5 right; left;reflexivity|reflexivity|].
  econstructor; [ econstructor; [reflexivity|exact IH1] | reflexivity |].
  econstructor; [ econstructor; [reflexivity|exact IH2] | reflexivity |].
  econstructor; [reflexivity|].
  apply (Ahcons_inc_str 1); [unfold p; apply le_n_S, le_0_n|].
  apply (compile_binop_correct).
  exists k; exact IHop.
Qed.


(* Main theorems *)
Theorem compile_correct :
  forall e stmt stack newstack,
    compile e = Some stmt ->
      (* eval(stack, stmt) ⇓s newstack *)
      stack_semantics (S.h_eval, cval_tuple [stack; stmt], newstack) ->
    exists n, 
      (* push(n, stack) = newstack *)
      Sfilter_interp Stack.h_push
        (cval_tuple [cval_base (castValue n); stack]) newstack /\
      (* expr(e) ⇓a n *)
      arith_semantics (A.h_expr, e, cval_base n).
Proof.
  intros e stmt stack newstack H [k IH];
  apply (compile_k_correct k e) in IH; assumption.
Qed.

Theorem compile_complete :
  forall e stmt stack newstack n,
    compile e = Some stmt ->
    (* eval(e) ⇓ n *)
    arith_semantics (A.h_expr, e, cval_base n) -> 
    (* push(n, stack) = newstack*)
    Sfilter_interp Stack.h_push (cval_tuple [cval_base (castValue n); stack]) newstack ->
    (* eval(stack, stmt) ⇓ newstack *)
    hook_semantics st bis Sfilter_interp (Stack.h_eval, cval_tuple [stack; stmt], newstack).
Proof.
  intros e stmt stack newstack n H1 [k IH] H2;
  apply(compile_k_complete k e n); assumption.
Qed.

