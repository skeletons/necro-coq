Require Arithmetic Stack.
Require Import List Ascii String.
Require Import Skeleton Concrete.
Import ListNotations.
Open Scope string_scope.

Module A := Arithmetic.
Definition ar := Arithmetic.sem.
Module S := Stack.
Definition st := Stack.sem.


Fixpoint get_sort l bi (c:cvalue l bi) : sort _ _:=
  match c with
  | cval_base _ _ b _ => Abs _ _ b
  | cval_constructor _ _ v c _ => Var _ _ v
  | cval_tuple _ _ t => Prod _ _ (map (get_sort l bi) t)
  end.


(* Base tactics *)
Ltac inj_ext H :=
  match type of H with
  | Some _ = Some _ => injection H as ?
  | (_, _) = (_, _) => injection H as ?
  | _::_ = _::_ => injection H as ?
  | S _ = S _ => injection H as ?
  | ?x = ?y => unify x y; clear H
  | False => destruct H
  end.

Ltac subst_ext :=
  repeat
  (match goal with
  | [ H: _ |- _] => (simpl in H; cbn in H; inj_ext H)
  | _ => discriminate
  | _ => progress subst
  end).


(* Axiomes *)

Axiom bia: s_abs_sort ar -> Type.
Axiom bis: s_abs_sort st -> Type.
Axiom Afilter_interp : A.filter ->
  cvalue ar bia -> cvalue ar bia -> Prop.
Axiom Sfilter_interp : S.filter ->
  cvalue st bis -> cvalue st bis -> Prop.


Definition FilterSort l bi iFC :=
  forall v1 v2 (f: s_filter l),
  iFC f v1 v2 ->
  get_sort l bi v1 = fst (s_fsort l f) /\
  get_sort l bi v2 = snd (s_fsort l f).
Axiom FilterArithmeticSort:
  FilterSort ar bia Afilter_interp.
Axiom FilterStackSort:
  FilterSort st bis Sfilter_interp.


Axiom StackPushDef :
  forall (a :bis Stack.s_value),
  forall (b :bis Stack.s_stack),
  exists (c :bis Stack.s_stack),
  Sfilter_interp Stack.h_push
  (cval_tuple _ _ [cval_base st bis S.s_value a;
  cval_base st bis S.s_stack b])
  (cval_base st bis S.s_stack c).
Axiom StackPushPop :
  forall l1 l2 l3,
  Sfilter_interp Stack.h_push l1 l2 ->
  Sfilter_interp Stack.h_pop l2 l3 -> l1 = l3.
Axiom StackPushPopDef :
  forall l1 l2,
  Sfilter_interp Stack.h_push l1 l2 ->
  Sfilter_interp Stack.h_pop l2 l1.


Definition isInj {A B} (f : A -> B) :=
  forall x y, f x = f y -> x = y.
Definition isSurj {A B} (f : A -> B) :=
  forall y, exists x, y = f x.
Definition isomorphism {A B : Type} (f : A -> B) : Prop :=
  isInj f /\ isSurj f.

Axiom castLiteral: bia A.s_literal -> bis S.s_literal.

Axiom castValue : bia Arithmetic.s_value -> bis Stack.s_value.
Axiom sameValue : isomorphism castValue.

Axiom sameLitToVal :
  forall n b,
  Afilter_interp Arithmetic.h_litToVal
  (cval_base ar bia A.s_literal n)
  (cval_base ar bia A.s_value b) <->
  Sfilter_interp Stack.h_litToVal
  (cval_base st bis S.s_literal (castLiteral n))
  (cval_base st bis S.s_value (castValue b)).
Axiom sameAdd :
  forall a b c,
  Afilter_interp Arithmetic.h_add
  (cval_tuple _ _ [cval_base ar bia A.s_value a;
  cval_base ar bia A.s_value b])
  (cval_base ar bia A.s_value c) <->
  Sfilter_interp Stack.h_add
  (cval_tuple _ _ [cval_base st bis S.s_value (castValue a);
  cval_base st bis S.s_value (castValue b)])
  (cval_base st bis S.s_value (castValue c)).
Axiom sameSub :
  forall a b c,
  Afilter_interp Arithmetic.h_sub
  (cval_tuple _ _ [cval_base ar bia A.s_value a;
  cval_base ar bia A.s_value b])
  (cval_base ar bia A.s_value c) <->
  Sfilter_interp Stack.h_sub
  (cval_tuple _ _ [cval_base st bis S.s_value (castValue a);
  cval_base st bis S.s_value (castValue b)])
  (cval_base st bis S.s_value (castValue c)).
Axiom sameMult :
  forall a b c,
  Afilter_interp Arithmetic.h_mult
  (cval_tuple _ _ [cval_base ar bia A.s_value a;
  cval_base ar bia A.s_value b])
  (cval_base ar bia A.s_value c) <->
  Sfilter_interp Stack.h_mult
  (cval_tuple _ _ [cval_base st bis S.s_value (castValue a);
  cval_base st bis S.s_value (castValue b)])
  (cval_base st bis S.s_value (castValue c)).
Axiom sameDiv :
  forall a b c,
  Afilter_interp Arithmetic.h_div
  (cval_tuple _ _ [cval_base ar bia A.s_value a;
  cval_base ar bia A.s_value b])
  (cval_base ar bia A.s_value c) <->
  Sfilter_interp Stack.h_div
  (cval_tuple _ _ [cval_base st bis S.s_value (castValue a);
  cval_base st bis S.s_value (castValue b)])
  (cval_base st bis S.s_value (castValue c)).



(* Specify lemmas FilterArithmeticSort and FilterStackSort to all filters *)
Lemma AddArithmeticSort:
  forall v1 v2,
  Afilter_interp A.h_add v1 v2 -> exists a b c,
  v1 = cval_tuple _ _ [cval_base ar bia A.s_value a;
  cval_base ar bia A.s_value b] /\
  v2 = cval_base ar bia A.s_value c.
Proof.
  intros. apply FilterArithmeticSort in H; destruct H as [H1 H2].
  destruct v1 as [| |l1]; simpl in *; try congruence; inversion H1 as [A].
  destruct l1 as [|i [|j [|? ?]]]; simpl in A; try congruence.
  destruct v2 as [? w| |]; simpl in *; try congruence; inversion H2 as [B].
  destruct i as [? v |[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  subst_ext. exists v, v', w. tauto.
Qed.
Lemma SubArithmeticSort:
  forall v1 v2,
  Afilter_interp Arithmetic.h_sub v1 v2 -> exists a b c,
  v1 = cval_tuple _ _ [cval_base ar bia A.s_value a;
  cval_base ar bia A.s_value b] /\
  v2 = cval_base ar bia A.s_value c.
Proof.
  intros. apply FilterArithmeticSort in H; destruct H as [H1 H2].
  destruct v1 as [| |l1]; simpl in *; try congruence; inversion H1 as [A].
  destruct l1 as [|i [|j [|? ?]]]; simpl in A; try congruence.
  destruct v2 as [? w| |]; simpl in *; try congruence; inversion H2 as [B].
  destruct i as [? v |[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  subst_ext. exists v, v', w. tauto.
Qed.
Lemma MultArithmeticSort:
  forall v1 v2,
  Afilter_interp Arithmetic.h_mult v1 v2 -> exists a b c,
  v1 = cval_tuple ar bia [cval_base ar bia A.s_value a;
  cval_base ar bia A.s_value b] /\
  v2 = cval_base ar bia A.s_value c.
Proof.
  intros. apply FilterArithmeticSort in H; destruct H as [H1 H2].
  destruct v1 as [| |l1]; simpl in *; try congruence; inversion H1 as [A].
  destruct l1 as [|i [|j [|? ?]]]; simpl in A; try congruence.
  destruct v2 as [? w| |]; simpl in *; try congruence; inversion H2 as [B].
  destruct i as [? v |[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  subst_ext. exists v, v', w. tauto.
Qed.
Lemma DivArithmeticSort:
  forall v1 v2,
  Afilter_interp Arithmetic.h_div v1 v2 -> exists a b c,
  v1 = cval_tuple ar bia [cval_base ar bia A.s_value a;
  cval_base ar bia A.s_value b] /\
  v2 = cval_base ar bia A.s_value c.
Proof.
  intros. apply FilterArithmeticSort in H; destruct H as [H1 H2].
  destruct v1 as [| |l1]; simpl in *; try congruence; inversion H1 as [A].
  destruct l1 as [|i [|j [|? ?]]]; simpl in A; try congruence.
  destruct v2 as [? w| |]; simpl in *; try congruence; inversion H2 as [B].
  destruct i as [? v |[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  subst_ext. exists v, v', w. tauto.
Qed.
Lemma LitToValArithmeticSort:
  forall v1 v2,
  Afilter_interp Arithmetic.h_litToVal v1 v2 -> exists a b,
  v1 = cval_base ar bia A.s_literal a /\
  v2 = cval_base ar bia A.s_value b.
Proof.
  intros. apply FilterArithmeticSort in H; destruct H as [H1 H2].
  destruct v1 as [? v| |]; simpl in *; try congruence; inversion H1 as [A].
  destruct v2 as [? w| |]; simpl in *; try congruence; inversion H2 as [B].
  subst_ext. exists v, w. tauto.
Qed.
Lemma AddStackSort:
  forall v1 v2,
  Sfilter_interp Stack.h_add v1 v2 -> exists a b c,
  v1 = cval_tuple st bis [cval_base st bis S.s_value a;
  cval_base st bis Stack.s_value b] /\
  v2 = cval_base st bis Stack.s_value c.
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct v1 as [| |l1]; simpl in *; try congruence; inversion H1 as [A].
  destruct l1 as [|i [|j [|? ?]]]; simpl in A; try congruence.
  destruct v2 as [? w| |]; simpl in *; try congruence; inversion H2 as [B].
  destruct i as [? v |[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  subst_ext. exists v, v', w. tauto.
Qed.
Lemma SubStackSort:
  forall v1 v2,
  Sfilter_interp Stack.h_sub v1 v2 -> exists a b c,
  v1 = cval_tuple st bis [cval_base st bis S.s_value a;
  cval_base st bis S.s_value b] /\
  v2 = cval_base st bis S.s_value c.
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct v1 as [| |l1]; simpl in *; try congruence; inversion H1 as [A].
  destruct l1 as [|i [|j [|? ?]]]; simpl in A; try congruence.
  destruct v2 as [? w| |]; simpl in *; try congruence; inversion H2 as [B].
  destruct i as [? v |[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  subst_ext. exists v, v', w. tauto.
Qed.
Lemma MultStackSort:
  forall v1 v2,
  Sfilter_interp Stack.h_mult v1 v2 -> exists a b c,
  v1 = cval_tuple st bis [cval_base st bis S.s_value a;
  cval_base st bis S.s_value b] /\
  v2 = cval_base st bis S.s_value c.
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct v1 as [| |l1]; simpl in *; try congruence; inversion H1 as [A].
  destruct l1 as [|i [|j [|? ?]]]; simpl in A; try congruence.
  destruct v2 as [? w| |]; simpl in *; try congruence; inversion H2 as [B].
  destruct i as [? v |[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  subst_ext. exists v, v', w. tauto.
Qed.
Lemma DivStackSort:
  forall v1 v2,
  Sfilter_interp Stack.h_div v1 v2 -> exists a b c,
  v1 = cval_tuple st bis [cval_base st bis S.s_value a;
  cval_base st bis S.s_value b] /\
  v2 = cval_base st bis S.s_value c.
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct v1 as [| |l1]; simpl in *; try congruence; inversion H1 as [A].
  destruct l1 as [|i [|j [|? ?]]]; simpl in A; try congruence.
  destruct v2 as [? w| |]; simpl in *; try congruence; inversion H2 as [B].
  destruct i as [? v |[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  subst_ext. exists v, v', w. tauto.
Qed.
Lemma LitToValStackSort:
  forall v1 v2,
  Sfilter_interp Stack.h_litToVal v1 v2 -> exists a b,
  v1 = cval_base st bis S.s_literal a /\
  v2 = cval_base st bis S.s_value b.
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct v1 as [? v| |]; simpl in *; try congruence; inversion H1 as [A].
  destruct v2 as [? w| |]; simpl in *; try congruence; inversion H2 as [B].
  subst_ext. exists v, w. tauto.
Qed.
Lemma PushStackSort:
  forall v1 v2,
  Sfilter_interp Stack.h_push v1 v2 -> exists a b c,
  v1 = cval_tuple _ _ [cval_base st bis S.s_value a;
  cval_base st bis S.s_stack b] /\
  v2 = cval_base st bis S.s_stack c.
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct v1 as [| |l1]; simpl in *; try congruence; inversion H1 as [A].
  destruct l1 as [|i [|j [|? ?]]]; simpl in A; try congruence.
  destruct v2 as [? w| |]; simpl in *; try congruence; inversion H2 as [B].
  destruct i as [? v |[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  subst_ext. exists v, v', w. tauto.
Qed.
Lemma PopStackSort:
  forall v1 v2,
  Sfilter_interp Stack.h_pop v1 v2 -> exists a b c,
  v1 = cval_base st bis S.s_stack a /\
  v2 = cval_tuple _ _ [cval_base st bis S.s_value b;
  cval_base st bis S.s_stack c].
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct v2 as [| |l1]; simpl in *; try congruence; inversion H2 as [A].
  destruct l1 as [|i [|j [|? ?]]]; simpl in A; try congruence.
  destruct v1 as [? w| |]; simpl in *; try congruence; inversion H1 as [B].
  destruct i as [? v |[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[|] ?|]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  subst_ext. exists w, v, v'. tauto.
Qed.





(* Induction on skeletons *)
Section Recursivity.
  Variable bs ps f h: Set.
  Variable c: ps -> Set.
  Variable (P: skeleton bs ps c f h -> Prop).
  Variable (branchs: forall Ss, (Forall P Ss) -> P (B _ _ _ _ _ Ss)).
  Variable (filters: forall f x, P (F _ _ _ _ _ f x)).
  Variable (hooks: forall h x, P (H _ _ _ _ _ h x)).
  Variable (ret: forall x, P (R _ _ _ _ _ x)).
  Variable (letin: forall x b s, P b -> P s -> P (LetIn _ _ _ _ _ x b s)).

  Fixpoint skel_rec sk {struct sk} : P sk :=
    match sk with
    | B _ _ _ _ _ Ss =>
        branchs Ss ((fix l_ind1 (l : list (skeleton bs ps c f h)) :
        (Forall P l) :=
        match l with
        | [] => Forall_nil P
        | sk1 :: skr => Forall_cons sk1 (skel_rec sk1)
         (l_ind1 skr)
        end) Ss)
    | F _ _ _ _ _ f x => filters f x
    | H _ _ _ _ _ h x => hooks h x
    | R _ _ _ _ _ x => ret x
    | LetIn _ _ _ _ _ x b1 br => letin x b1 br (skel_rec b1) (skel_rec br)
    end.
End Recursivity.



(** Useful lemmas *)

Lemma interp_skel_inc l bi iFC:
  forall s s1 s2,
  forall (Ht Hu: hook_triple l bi -> Prop),
  (forall x, Ht x -> Hu x) ->
  interp_skel l bi iFC s (s1, Ht) s2 ->
    interp_skel l bi iFC s (s1, Hu) s2.
Proof.
  induction s as [Ss H|f x|h x|x|x b s Hb Hs] using skel_rec.
  - intros s1 s2 Ht Hu Htu Hint.
     inversion Hint; subst; simpl in *.
     econstructor; try eassumption. eapply Forall_forall in H; [eapply H|]; eassumption.
  - intros s1 s2 Ht Hu Htu Hint.
    inversion Hint; subst; econstructor; eauto.
  - intros s1 s2 Ht Hu Htu Hint.
    inversion Hint; subst; econstructor; eauto.
  - intros s1 s2 Ht Hu Htu Hint.
    inversion Hint; subst; econstructor; auto.
  - intros e1 r1 Ht Hu Htu Hint.
    inversion Hint; subst; econstructor; eauto.
Qed.


Lemma Shcons_inc:
  forall n (x:hook_triple st bis),
  consequence st bis Sfilter_interp n x ->
  consequence st bis Sfilter_interp (S n) x.
Proof.
  induction n.
  - intros ? H; destruct H.
  - intros ht Ht; simpl; destruct ht as [[h x] y]; inversion Ht;
      subst_ext; econstructor; try eassumption; clear H3 H4.
    eapply(interp_skel_inc st bis Sfilter_interp); eassumption.
  Qed.

Lemma Ahcons_inc:
  forall n (x:hook_triple ar bia),
  consequence ar bia Afilter_interp n x ->
  consequence ar bia Afilter_interp (S n) x.
Proof.
  induction n.
  - intros ? H; destruct H.
  - intros ht Ht; simpl; destruct ht as [[h x] y]; inversion Ht;
      subst_ext; econstructor; try eassumption; clear H3 H4.
    eapply(interp_skel_inc ar bia Afilter_interp); eassumption.
  Qed.

Lemma Shcons_inc_str:
  forall m n x, m <= n ->
  consequence st bis Sfilter_interp m x ->
  consequence st bis Sfilter_interp n x.
Proof.
  intros ? ? ? H ?; induction H; [|apply Shcons_inc]; trivial.
Qed.

Lemma Ahcons_inc_str:
  forall m n x, m <= n ->
  consequence ar bia Afilter_interp m x ->
  consequence ar bia Afilter_interp n x.
Proof.
  intros ? ? ? H ?; induction H; [|apply Ahcons_inc]; trivial.
Qed.

Lemma invLetIn :
  forall s bi iF x b sk st r,
  interp_skel s bi iF (LetIn _ _ _ _ _ x b sk) st r ->
  exists e e' Ht v,
  st = (e, Ht) /\
  add_asn s bi e x v = Some e' /\
  interp_skel s bi iF b  (e, Ht) v /\
  interp_skel s bi iF sk (e', Ht) r.
Proof.
  intros ? ? ? ? ? ? ? ? H; inversion H; subst; repeat eexists; eassumption.
Qed.

Lemma invF :
  forall s bi iF f x st r,
  interp_skel s bi iF (F _ _ _ _ _ f x) st r ->
  exists Ht e v,
  st = (e, Ht) /\
  eval_term s bi e x = Some v /\
  iF f v r.
Proof.
  intros ? ? ? ? ? ? ? H; inversion H; subst; repeat eexists;
  eassumption.
Qed.

Lemma invH :
  forall s bi iF h x st r,
  interp_skel s bi iF (H _ _ _ _ _ h x) st r ->
  exists Ht e v,
  st = (e, Ht) /\
  eval_term s bi e x = Some v /\
  Ht (h, v, r).
Proof.
  intros ? ? ? ? ? ? ? H; inversion H; subst; repeat eexists;
  eassumption.
Qed.

Lemma invR :
  forall s bi iF x st r,
  interp_skel s bi iF (R _ _ _ _ _ x) st r ->
  exists Ht e,
  st = (e, Ht) /\
  eval_term s bi e x = Some r.
Proof.
  intros ? ? ? ? ? ? H; inversion H; subst; repeat eexists;
  eassumption.
Qed.

Lemma inv_hook_consequence:
  forall s bi filter_interp Ht h x y, hook_consequence s bi filter_interp Ht (h, x, y) ->
  exists e v S,
    In (h, v, S) (s_hooks_interp s) /\
    make_asn s bi v x = Some e /\
    interp_skel s bi filter_interp S (e, Ht) y.
Proof.
  intros ? ? ? Ht h x y H. inversion H. subst. do 3 eexists.
  repeat (try split; try eassumption).
Qed.
