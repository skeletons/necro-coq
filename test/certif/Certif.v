Require Import List Ascii String.
From Necro Require Import Dict.
Import ListNotations DictNotations.
Require Import Fact.
From Necro Require Import Skeleton Concrete.
Require Import ConcreteLemmas.
Require Import ZArith.
Open Scope string.
Open Scope Z.
Require Import Lia.

(* We first instantiate the non specified types and terms *)

(* The type of terms: either an integer, or a boolean *)
Inductive value :=
| Int : Z -> value
| Bool : bool -> value.

(* The type of states: a map (ordered by increasing variable name)
   every element of the list is a mapping of a variable name to the matching
   term. *)
Definition fstate := list (string * value).

(* Compare two strings vis-à-vis lexicographical order *)
Fixpoint string_lt m n: bool :=
  match m,n with
  | _, EmptyString => false
  | EmptyString, _ => true
  | String a s, String a' s' =>
      orb (nat_of_ascii a <? nat_of_ascii a')
          (andb (nat_of_ascii a =? nat_of_ascii a')
                (string_lt s s'))
  end%nat.

(* read the term of the variable [n] in the state [s] *)
Fixpoint rd_fm_state (s: fstate) (n: string): option value :=
  match s with
  | [] => None
  | (m, v) :: q =>
      if string_dec m n then Some v else
      if string_lt m n then rd_fm_state q n else None
  end.

(* add the mapping (n -> v) to the state s *)
Fixpoint add_to_state (s: fstate) (n: string) (v: value): fstate :=
  match s with
  | [] => (n,v) :: []
  | (m, w) :: q =>
      if string_dec m n then (n,v) :: q else
      if string_lt m n then (m,w) :: (add_to_state q n v) else
      (n,v) :: (m,w) :: q
  end.

Definition unspec :=
  list type -> list cvalue -> cvalue -> Prop.

Inductive litToVal_interp: unspec :=
| litToVal_intro i:
    litToVal_interp [] [cval_base Z i] (cval_base value (Int i)).

Inductive read_interp: unspec :=
| read_intro n s v:
  rd_fm_state s n = Some v ->
  read_interp [] [cval_base string n; cval_base fstate s] (cval_base value v).

Inductive write_interp: unspec :=
| write_intro n s v:
  write_interp []
    [cval_base string n; cval_base fstate s; cval_base value v]
    (cval_base fstate (add_to_state s n v)).

Inductive isInt_interp: unspec :=
| isInt_intro i:
  isInt_interp [] [cval_base value (Int i)] (cval_base Z i).

Inductive isBool_interp: unspec :=
| isBool_intro b:
  isBool_interp [] [cval_base value (Bool b)] (cval_base bool b).

Inductive add_interp: unspec :=
| add_intro i j:
  add_interp [] [cval_base Z i; cval_base Z j] (cval_base value (Int (i+j))).

Inductive eq_interp: unspec :=
| eq_intro i j:
  eq_interp [] [cval_base Z i; cval_base Z j] (cval_base value (Bool (i=?j))).

Inductive neg_interp: unspec :=
| neg_intro b:
  neg_interp [] [cval_base bool b] (cval_base value (Bool (negb b))).

Inductive isTrue_interp: unspec :=
| isTrue_intro:
  isTrue_interp [] [cval_base bool true] (cval_tuple []).

Inductive isFalse_interp: unspec :=
| isFalse_intro:
  isFalse_interp [] [cval_base bool false] (cval_tuple []).

Inductive minus_one_interp: unspec :=
| minus_one_intro:
  minus_one_interp [] [] (cval_base Z (-1)).

Inductive one_interp: unspec :=
| one_intro:
  one_interp [] [] (cval_base Z 1).

Inductive v_a_interp: unspec :=
| v_a_intro:
  v_a_interp [] [] (cval_base string "a").

Inductive v_fact_interp: unspec :=
| v_fact_intro:
  v_fact_interp [] [] (cval_base string "fact").

Inductive v_i_interp: unspec :=
| v_i_intro:
  v_i_interp [] [] (cval_base string "i").

Inductive v_n_interp: unspec :=
| v_n_intro:
  v_n_interp [] [] (cval_base string "n").

Inductive zero_interp: unspec :=
| zero_intro:
  zero_interp [] [] (cval_base Z 0).

(* We specify the unspecified terms *)
Definition unspec_term_decl_interp: dict (nat * unspec) :=
  ∅
  + { "add"       → (2, add_interp) }
  + { "eq"        → (2, eq_interp) }
  + { "isBool"    → (1, isBool_interp) }
  + { "isFalse"   → (1, isFalse_interp) }
  + { "isInt"     → (1, isInt_interp) }
  + { "isTrue"    → (1, isTrue_interp) }
  + { "litToVal"  → (1, litToVal_interp) }
  + { "minus_one" → (0, minus_one_interp) }
  + { "neg"       → (1, neg_interp) }
  + { "one"       → (0, one_interp) }
  + { "read"      → (2, read_interp) }
  + { "v_a"       → (0, v_a_interp) }
  + { "v_fact"    → (0, v_fact_interp) }
  + { "v_i"       → (0, v_i_interp) }
  + { "v_n"       → (0, v_n_interp) }
  + { "write"     → (3, write_interp) }
  + { "zero"      → (0, zero_interp) }%nat.

Notation "a → b" := (Arrow a b) (right associativity, at level 39).


Definition gen (_:type) (_:cvalue) := False.

(* Let's set some notations and definitions to get a readable code *)
Notation "'interp_skel'" := (interp_skel sem unspec_term_decl_interp gen).
Notation "'interp_term'" := (interp_term sem unspec_term_decl_interp).

Definition init_state : fstate := [].

(* order the mapping in lexicographical order *)
Definition state_of_list: list (string * value) -> fstate :=
  fold_right (fun x s =>
    match x with (n,v) =>
    add_to_state s n v end) init_state.

Definition make_state fact n a i :=
  state_of_list
  [ ("fact", Int fact);
    ("n", Int n);
    ("a", Int a);
    ("i", Int i)].

Definition make_state_val fact n a i :=
  cval_base fstate (make_state fact n a i).

(* factorial function. Z is chosen as output type because it uses binary
* reprezentation, and as nat uses unary, it is not suitable for a factorial *)
Fixpoint factorial (n:nat): Z :=
match n with
| 0%nat => 1
| S n => Z.of_nat (S n) * (factorial n)
end.

(* Auxiliary arithmetical lemmas *)
Lemma aux1:
  forall n1 k',
  let k := Z.of_nat (S k') in
    n1 - k =? n1 = false.
Proof.
  lia.
Qed.

Lemma aux1_bis:
  forall n1 k',
    n1 - Z.of_nat (S k') + 1 = n1 - (Z.of_nat k').
Proof.
  lia.
Qed.

Lemma aux1_ter:
  forall f1 a1 k',
    f1 + a1 + Z.of_nat k' * a1 = f1 + (Z.of_nat (S k')) * a1.
Proof.
  lia.
Qed.






(* Auxiliary lemmas to destruct hypotheses *)

Lemma forall_destr:
  forall {A1 A2} P (x1:A1) l1 (x2:A2) l2,
  Forall2 P (x1::l1) (x2::l2) ->
  P x1 x2 /\ Forall2 P l1 l2.
Proof.
  intros ? ? ? ? ? ? ? H; inversion H; subst; split; assumption.
Qed.

Lemma i_var_let:
  forall e x ty v,
  interp_term e (term_var (TVLet x ty)) v ->
  WellFormed.findEnv x e = Some v.
Proof.
  inversion 1; subst; assumption.
Qed.

Lemma i_var_term_destr:
  forall e v ty ta w,
    interp_term e (term_var (TVUnspec v ty ta)) w ->
    match find v unspec_term_decl_interp with
    | Some (O, f) => f ta [] w
    | Some _ => True
    | None => False
    end.
Proof.
  intros.
  inversion H; subst.
  - assert(A:find v unspec_term_decl_interp = Some (O, v0)) by assumption.
    rewrite A; assumption.
  - assert(A:find v unspec_term_decl_interp = Some (S n, v0)) by assumption.
    rewrite A. trivial.
Qed.




(* Auxiliary tactics *)

Ltac all2 :=
  match goal with
  | [|- Forall2 _ ?l _ ] =>
    match l with
    | [] => econstructor
    | _ => econstructor; [|all2]
    end
  end.


(* Crushing hypotheses *)

Ltac crush_term H :=
  match type of H with
  | interp_term _ (term_constructor _ _ _) _ =>
    apply i_constructor_destr in H;
    let eq := fresh "eq" in
    destruct H as [?v [eq H]]; subst;
    crush_term H; try (crush_term eq)
  | interp_term _ (term_tuple []) _ =>
      apply i_tuple_nil_destr in H; crush_term H; subst
  | interp_term _ (term_tuple (_::_)) _ =>
      apply i_tuple_cons_destr in H;
      let H' := fresh "interp" in
      destruct H as (?v & ?vl & ?eq & H & H');
      subst;
      crush_term H;
      crush_term H'
  | interp_term _ (term_var (TVUnspec ?v _ _)) _ =>
      apply i_var_term_destr in H; simpl in H; crush_term H
  | Some _ = Some _ =>
      injection H as ?; subst
  | cval_tuple _ = cval_tuple _ =>
      injection H as ?; subst
  | exists _ : _, _ =>
      destruct H as [? H]; crush_term H
  | _ /\ _ =>
      let H1 := fresh H in
      let H2 := fresh H in
      destruct H as [H1 H2]; crush_term H1; crush_term H2
  | _ => idtac
  end.




(* Main tactic *)

Ltac main_aux :=
match goal with
(* skeletons *)
| [ |- interp_skel _ (skel_return _) _ ] => econstructor
| [ |- interp_skel _ (skel_letin _ _ _) _ ] => econstructor
| [ |- interp_skel _ (skel_match _ _ _) _ ] => econstructor
| [|- interp_skel _ (skel_apply _ _) _ ] => econstructor
(* terms *)
| [ |- interp_term _ (term_func _ _ _) _ ] => econstructor
| [|- interp_term _ (term_var (TVSpec _ _ _)) _ ] =>
  eapply i_var_spec; [reflexivity..|]; main_aux
| [|- interp_term _ _ (term_var (TVLet _ _)) _ ] =>
  eapply i_var_let; reflexivity
| [|- interp_term _ (term_var ?e) _ ] => unfold e
| [|- interp_term _ (term_constructor _ _ _) _ ] => econstructor
| [|- interp_term _ (term_tuple _) _ ] => econstructor
(* apply *)
| [|- apply _ _ _ (cval_closure _ _ _) _ _] => eapply i_apply_clos; main_aux
| [|- apply _ _ _ (cval_unspec ?n _ _ _) ?args _] =>
    match eval compute in (S n - List.length args)%nat with
    | O => eapply i_apply_unspec_done; main_aux
    | S _ => eapply i_apply_unspec_cont; main_aux
    end
(* other *)
| [|- add_asn _ _ _ _ ] => repeat econstructor
| [|- Forall2 _ [] _ ] => econstructor
| [|- Forall2 _ _ _ ] => econstructor; [|main_aux]
| _ => instantiate (2:= make_state _ _ _ _); reflexivity
| _ => econstructor; reflexivity
| _ => unfold WellFormed.subst_term'; simpl
| _ => idtac
end.

Ltac main := repeat main_aux.







(* Inner loop invariant *)
Lemma loop1:
  forall k',
  let k := Z.of_nat k' in
  forall a1 f1 n1,
  interp_skel
    [("σ", make_state_val f1 n1 a1 (n1-k))]
  (skel_apply
    (term_var σ_eval_stmt)
    [term_var (TVLet "σ" τ_state); term_var σ_loop1]
  )
  (make_state_val (f1+k*a1) n1 a1 n1).
Proof.
  intros k' k. induction k'; intros a1 f1 n1.
  - simpl in k. rewrite Z.add_0_r, Z.sub_0_r.
    main.
    rewrite Z.eqb_refl.
    econstructor; [right;left; reflexivity|].
    main.
  - main.
    unfold k; rewrite aux1.
    econstructor; [left; reflexivity|].
    simpl in IHk'.
    specialize(IHk' a1 (f1 + a1) n1).
    eapply i_apply_destr in IHk'.
    destruct IHk' as (vl & w & all & IHk' & Happly).
    inversion IHk'; subst.
    injection H3 as <- <- <-.
    clear H5.
    unfold WellFormed.subst_term' in H6. simpl in H6.
    inversion H6; subst; clear H6.

    destruct vl as [|v1 vl]; [inversion all|].
    apply forall_destr in all.
    destruct all as [interp1 all].
    destruct vl as [|v2 vl]; [inversion all|].
    apply forall_destr in all.
    destruct all as [interp2 all].
    destruct vl as [|]; [clear all|inversion all].
    apply i_var_let in interp1.
    simpl in interp1. injection interp1 as interp1.
    clear IHk'.

    econstructor; [ main ..| ].
    econstructor; [ main ..| ].
    assert(A:k * a1 =
      match a1 with
      | 0 => 0
      | Z.pos y' => Z.pos (Pos.of_succ_nat k' * y')
      | Z.neg y' => Z.neg (Pos.of_succ_nat k' * y')
      end) by reflexivity.
    rewrite <- A; clear A.
    unfold k.
    rewrite <- aux1_ter.
    econstructor.
    eapply i_var_spec; [reflexivity..|main_aux].
    all2; main_aux.
    unfold WellFormed.subst.
    simpl map. simpl snd.
    rewrite <- (eq_refl: Z.of_nat (S k') = Z.pos (Pos.of_succ_nat k')).
    rewrite aux1_bis.
    inversion interp2; subst. clear interp2.
    injection H3 as <- <- <-.  clear H5.
    unfold WellFormed.subst_term' in H6.
    simpl WellFormed.subst_term in H6.
    crush_term H6.
    repeat match goal with
    | [ H : _ [] _ |- _ ] => inversion H; clear H
           end. subst.
    apply Happly.
Qed.



(* Outer loop invariant *)
Lemma loop2:
  forall n1,
  forall a1 f1 i1,
  exists a2 i2,
  interp_skel
    [("σ", make_state_val f1 (Z.of_nat n1) a1 i1)]
  (skel_apply
    (term_var
      σ_eval_stmt)
  [ term_var (TVLet "σ" τ_state)
  ; term_var σ_loop2
  ])
  (make_state_val (f1*factorial n1) 0 a2 i2).
Proof.
  induction n1; intros a1 f1 i1.
  - exists a1, i1.
  main.
  econstructor; [right;left;reflexivity|].
  rewrite Z.mul_1_r.
  main.
  - destruct (IHn1 f1 (f1 + (Z.of_nat n1 * f1)) (Z.of_nat n1 + 1)) as [a2 [i2 IH]]; clear IHn1.
    exists a2, i2.
    main.
    econstructor; [left; reflexivity|].
    econstructor; [main..|].
    econstructor.
    (* do one iteration of the loop*)
    + main_aux; [main..|]. simpl.
      main_aux; [main|].
      main_aux; [main..|].
      main_aux; [main..|].
      main_aux; [main..|].
      main_aux; [main..|].
      main_aux; [main..|].
      main_aux; [main..|].
      main_aux; [main..|].
      main_aux; [main..|].
      main_aux; [main..|].
      econstructor.
      (* Here we execute loop1, so we can use the matching lemma *)
      (* eval_stmt (σ(f -> f1, n -> S n1, a -> f1, i -> 1)) loop1 =
         σ(f -> f1 + n1 * f1, n -> S n1, a -> f1, i -> S n1) *)
      1:{
        instantiate (1 := make_state_val (f1 + Z.of_nat n1 * f1) (Z.pos
        (Pos.of_succ_nat n1)) f1 (Z.pos (Pos.of_succ_nat n1))).
        clear.
        assert (G:=loop1 n1 f1 f1 (Z.pos (Pos.of_succ_nat n1))).
        assert(H: Z.pos (Pos.of_succ_nat n1) - Z.of_nat n1 = 1) by lia.
        rewrite H in G; clear H.
        apply i_apply_destr in G.
        destruct G as (vl & w & all & ievalstmt & app).
        destruct vl as [|v1 vl]; [inversion all|].
        apply forall_destr in all.
        destruct all as (istate & all).
        destruct vl as [|v2 vl]; [inversion all|].
        apply forall_destr in all.
        destruct all as (iloop & all).
        destruct vl as [|]; [|inversion all]. clear all.
        eapply i_var_let in istate. injection istate as <-.
        inversion ievalstmt; subst.
        econstructor.
        (* eval_stmt *)
        * simpl in H3. injection H3 as <- <- <-. inversion H6; subst.
          eapply i_var_spec; [reflexivity..|].
          unfold subst_term'. simpl. constructor.
        (* all2 *)
        * econstructor. econstructor. reflexivity.
          econstructor. econstructor. reflexivity.
          econstructor.
        (* apply *)
        * simpl snd.
        inversion iloop; subst.
        injection H3 as <- <- <-.
        unfold WellFormed.subst_term' in H6.
        simpl in H6.
        crush_term H6.
        repeat match goal with
        | [ H : _ [] _ |- _ ] => inversion H; clear H
               end. subst.
        simpl. unfold make_state_val in app. unfold make_state in app.
        injection H4 as <- <- <-.
        unfold δ_loop1 in H9. unfold subst_term' in H9. simpl in H9.
        itCrush H9.
        simpl in *.
        repeat match goal with
        | [ H : _ [] _ |- _ ] => inversion H; clear H
               end. subst.
        exact app.
      }
      main.
      main.
    + main.
    (* do the rest of the loop by induction *)
    +
      apply i_apply_destr in IH.
      destruct IH as (vl & w & all & ievalstmt & app).
      destruct vl as [|v1 vl]; [inversion all|].
      apply forall_destr in all.
      destruct all as (istate & all).
      destruct vl as [|v2 vl]; [inversion all|].
      apply forall_destr in all.
      destruct all as (iloop & all).
      destruct vl as [|]; [|inversion all]. clear all.
      eapply i_var_let in istate. injection istate as <-.
      econstructor.
      inversion ievalstmt; subst.
      eapply i_var_spec; eassumption.
      main. simpl snd.
      inversion iloop; subst.
      injection H3 as <- <- <-.
      unfold WellFormed.subst_term' in H6.
      simpl in H6.
      crush_term H6.
      repeat match goal with
      | [ H : _ [] _ |- _ ] => inversion H; clear H
             end. subst.
      clear H5 iloop ievalstmt.
      inversion interp5; subst.
      injection H3 as <- <- <-.
      unfold WellFormed.subst_term' in H6.
      simpl in H6.
      crush_term H6.
      repeat match goal with
      | [ H : _ [] _ |- _ ] => inversion H; clear H
             end. subst.
      clear H5 interp5.
      assert(aux:Z.pos (Pos.of_succ_nat n1) - 1 = Z.of_nat n1) by lia.
      simpl in aux; rewrite aux.
      assert(aux2_ter: Z.of_nat n1 + 1 = Z.pos (Pos.of_succ_nat n1)) by now rewrite Zpos_P_of_succ_nat.
      rewrite aux2_ter in app; clear aux2_ter aux.
      assert(aux2_quinter: (f1 + Z.of_nat n1 * f1) * factorial n1 = f1 * (Z.pos (Pos.of_succ_nat n1) * factorial n1)) by lia.
      rewrite aux2_quinter in app.
      exact app.
Qed.



(* eval_fact is the skeleton that evaluates factorial of the "n" in the
   environment *)
Definition eval_fact: skeleton :=
  skel_letin
    (pattern_var "fact" τ_stmt)
    (skel_apply
      (term_var σ_fact)
      [term_var (TVLet "n" τ_lit)]
    )
    (skel_apply
      (term_var σ_eval_stmt)
      [ term_var (TVLet "σ" τ_state)
      ; term_var (TVLet "fact" τ_stmt)]
    )
.

Definition eval_fact_env n: env :=
  [("n",cval_base Z (Z.of_nat n)); ("σ", cval_base fstate [])].




(* Now for the main theorem *)
Theorem fact_while (n:nat):
  exists x,
    interp_skel (eval_fact_env n) eval_fact (cval_base fstate x)
  /\ rd_fm_state x "fact" = Some (Int (factorial n)).
Proof.
  simpl.
  destruct(loop2 n%nat 0 1 0) as [a2 [i2 res]].
  eexists. split.
  -
    unfold eval_fact_env, eval_fact.
    main_aux; [main..|].
    do 16 (main_aux; [main..|]).
    (* We can conclude by destructing res *)
    apply i_apply_destr in res.
    destruct res as (vl & w & all & ievalstmt & app).
    destruct vl as [|v1 vl]; [inversion all|].
    apply forall_destr in all.
    destruct all as (istate & all).
    destruct vl as [|v2 vl]; [inversion all|].
    apply forall_destr in all.
    destruct all as (iloop & all).
    destruct vl as [|]; [|inversion all]. clear all.
    eapply i_var_let in istate. injection istate as <-.
    econstructor.
    (* eval_stmt *)
    inversion ievalstmt; subst.
    eapply i_var_spec; eassumption.
    (* all2 *)
    econstructor. econstructor. reflexivity.
    econstructor. econstructor. reflexivity.
    econstructor.
    (* apply *)
    simpl snd.
    inversion iloop; subst.
    injection H3 as <- <- <-.
    unfold WellFormed.subst_term' in H6.
    simpl in H6.
    crush_term H6.
    repeat match goal with
    | [ H : _ [] _ |- _ ] => inversion H; clear H
           end. subst.
    clear H5 iloop ievalstmt.
    inversion interp5; subst.
    injection H3 as <- <- <-.
    unfold WellFormed.subst_term' in H6.
    simpl in H6.
    crush_term H6.
    repeat match goal with
    | [ H : _ [] _ |- _ ] => inversion H; clear H
           end. subst.
    clear H5 interp5.
    exact app.
  - rewrite Z.mul_1_l; reflexivity.
Qed.

