Require Import String List.
From Necro Require Import Dict.
From Necro Require Import Skeleton Concrete.
Require Import ZArith Lia.
Import ListNotations DictNotations.
Open Scope string_scope.



Section Lemmae.
  Variable sem:
    Skeleton.skeletal_semantics.
  Variable unspec:
    dict (nat * unspec_function).
  Variable g: type -> cvalue -> Prop.
  (* Let's set some notations to get a readable code *)
  Notation "'interp_skel'" := (interp_skel sem unspec g).
  Notation "'interp_term'" := (interp_term sem unspec).


  (* Destructors for Forall2 *)


  Lemma forall2_cons_destr1:
    forall {A1 A2} P (x1:A1) l1 (l:list A2),
    Forall2 P (x1::l1) l ->
    exists x2 l2,
    l = x2 :: l2 /\ P x1 x2 /\ Forall2 P l1 l2.
  Proof.
    inversion 1; eexists; eexists; split; auto.
  Qed.

  Lemma forall2_cons_destr2:
    forall {A1 A2} P (x2:A2) l2 (l:list A1),
    Forall2 P l (x2::l2) ->
    exists x1 l1,
    l = x1 :: l1 /\ P x1 x2 /\ Forall2 P l1 l2.
  Proof.
    inversion 1; eexists; eexists; split; auto.
  Qed.

  Lemma forall2_nil_destr1:
    forall {A B} (P: A -> B -> Prop) l,
    Forall2 P [] l -> l = [].
  Proof.
    inversion 1; auto.
  Qed.

  Lemma forall2_nil_destr2:
    forall {A B} (P: A -> B -> Prop) l,
    Forall2 P l [] -> l = [].
  Proof.
    inversion 1; auto.
  Qed.




  (* Destructors for add_asn *)

  Lemma add_asn_var_destr:
    forall x ty v e e',
      add_asn e (pattern_var x ty) v = e' ->
      e' = Some ((x, v) :: e).
  Proof.
    inversion 1; eauto.
  Qed.

  Lemma add_asn_wildcard_destr:
    forall e ty e' v,
      add_asn e (pattern_wildcard ty) v = e' ->
      e' = Some e.
  Proof.
    inversion 1; eauto.
  Qed.

  Lemma add_asn_constr_destr:
    forall e c p w f ta,
    add_asn e (pattern_constr c ta p) w = Some f ->
    exists v,
      w = cval_constructor c v /\ add_asn e p v = Some f.
  Proof.
    inversion 1. destruct w; try discriminate H1.
    destruct (c=?s) eqn:eq; [|discriminate H1].
    apply eqb_eq in eq. subst.
    eexists; split; eauto.
  Qed.

  Lemma add_asn_tuple_nil_destr:
    forall e v f,
    add_asn e (pattern_tuple []) v = Some f ->
    v = cval_tuple [] /\ e = f.
  Proof.
    inversion 1. destruct v; try discriminate H1.
    destruct l; try discriminate H1.
    injection H1 as ->. eauto. 
  Qed.

  Lemma add_asn_tuple_cons_destr:
    forall e w g p pl,
    add_asn e (pattern_tuple (p::pl)) w = Some g ->
    exists f v vl,
    w = cval_tuple (v :: vl) /\
    add_asn e p v = Some f /\
    add_asn f (pattern_tuple pl) (cval_tuple vl) = Some g.
  Proof.
    inversion 1. destruct w; try discriminate H1.
    destruct l; try discriminate H1.
    destruct add_asn eqn:aa in H1; [|discriminate H1].
    exists e0, c, l.
    split; [reflexivity|].
    split; [assumption|].
    rewrite aa. reflexivity.
  Qed.




  (* Destructors for interp_term and interp_skel *)

  Lemma i_branch_destr:
    forall e ty l v,
    interp_skel e (skel_branch ty l) v ->
    exists sk,
      In sk l /\ interp_skel e sk v.
  Proof.
    inversion 1; eauto.
  Qed.

  Lemma i_letin_destr:
    forall e p s1 s2 w,
      interp_skel e (skel_letin p s1 s2) w ->
      exists v e',
        interp_skel e s1 v /\
        add_asn e p v = Some e' /\
        interp_skel e' s2 w.
  Proof.
    inversion 1; subst;
    repeat eexists; repeat split; eauto.
  Qed.

  Lemma i_constructor_destr:
    forall e c t ta v,
    interp_term e (term_constructor c ta t) v ->
    exists w,
      v = cval_constructor c w /\ interp_term e t w.
  Proof.
    inversion 1; eexists; repeat split; eauto.
  Qed.

  Lemma i_tuple_nil_destr:
    forall e v,
    interp_term e (term_tuple []) v ->
      v = cval_tuple [].
  Proof.
    inversion 1; auto.
  Qed.

  Lemma i_tuple_cons_destr:
    forall e t tl w,
        interp_term e (term_tuple (t::tl)) w ->
        exists v vl, w = cval_tuple (v :: vl) /\
        interp_term e t v /\
        interp_term e (term_tuple tl) (cval_tuple vl).
  Proof.
    inversion 1; eexists; eexists; repeat split; eauto.
  Qed.

  Lemma i_apply_destr:
    forall e t tl v,
    interp_skel e (skel_apply t tl) v ->
    exists vl w,
      List.Forall2 (interp_term e) tl vl /\
      interp_term e t w /\
      apply sem unspec g w vl v.
  Proof.
    inversion 1; subst.
    eexists. eexists.
    split; [|split]; eassumption.
  Qed.

  Lemma i_return_destr:
    forall e t v,
    interp_skel e (skel_return t) v ->
    interp_term e t v.
  Proof.
    inversion 1; eauto.
  Qed.


End Lemmae.



  (* Tactics to crush *)

  Ltac crush_Forall2 H :=
    match type of H with
    | Forall2 _ [] [] =>
        clear H
    | Forall2 _ [] (_::_) =>
        apply forall2_nil_destr1 in H; discriminate H
    | Forall2 _ (_::_) [] =>
        apply forall2_nil_destr2 in H; discriminate H
    | Forall2 _ [] _ =>
        apply forall2_nil_destr1 in H; subst
    | Forall2 _ _ [] =>
        apply forall2_nil_destr2 in H; subst
    | Forall2 _ (?x1::?l1) (?x2::?l2) =>
        apply forall2_cons_destr1 in H;
        let eq := fresh "eq" in
        let H' := fresh H in
        destruct H as (?x & ?l & eq & H & H');
        injection eq as ? ?;
        subst;
        crush_Forall2 H'
    | Forall2 _ (?x1::?l1) _ =>
        apply forall2_cons_destr1 in H;
        let eq := fresh "eq" in
        let H' := fresh H in
        destruct H as (?x & ?l & eq & H & H');
        subst;
        crush_Forall2 H'
    | Forall2 _ _ (?x2::?l2) =>
        apply forall2_cons_destr2 in H;
        let eq := fresh "eq" in
        let H' := fresh H in
        destruct H as (?x & ?l & eq & H & H');
        subst;
        crush_Forall2 H'
    | Forall2 _ _ _ => idtac
    | _ =>
        fail "The hypothesis doesn't match Forall2 _ _ _"
    end.

  Ltac crush_addasn H :=
    match type of H with
    | add_asn _ (pattern_constr _ _ _ _) (cval_constructor _ _ _) = Some _ =>
      apply add_asn_constr_destr in H;
      let v := fresh "v" in
      let eq := fresh "eq" in
      destruct H as [v [eq H]];
      injection eq as <-;
      crush_addasn H
    | add_asn _ (pattern_constr _ _ _ _) _ = Some _ =>
      apply add_asn_constr_destr in H;
      let v := fresh "v" in
      destruct H as [v [-> H]] ;
      crush_addasn H
    | add_asn _ (pattern_tuple _ (?p::?pl)) (cval_tuple _ (?v::?vl)) = Some _ =>
      apply add_asn_tuple_cons_destr in H;
      let f := fresh "e" in
      let eq := fresh "eq" in
      let v := fresh "v" in
      let vl := fresh "vl" in
      let H' := fresh "addasn" in
      destruct H as (f & v & vl & eq & H & H');
      injection eq as <- <-;
      crush_addasn H ;
      crush_addasn H'
    | add_asn _ (pattern_tuple _ (?p::?pl)) _ = Some _ =>
      apply add_asn_tuple_cons_destr in H;
      let f := fresh "e" in
      let eq := fresh "eq" in
      let v := fresh "v" in
      let vl := fresh "vl" in
      let H' := fresh "addasn" in
      destruct H as (f & v & vl & -> & H & H');
      crush_addasn H ;
      crush_addasn H'
    | add_asn _ (pattern_tuple _ []) (cval_tuple _ []) = Some _ =>
      apply add_asn_tuple_nil_destr in H;
      destruct H as (_ & <-)
    | add_asn _ (pattern_tuple _ []) (cval_tuple _ _) = Some _ =>
      apply add_asn_tuple_nil_destr in H;
      let eq := fresh "eq" in
      destruct H as (eq & <-);
      injection eq as ->
    | add_asn _ (pattern_tuple _ []) _ = Some _ =>
      apply add_asn_tuple_nil_destr in H;
      destruct H as (-> & <-)
    | add_asn _ (pattern_var _ _) _ = Some _ =>
        injection H as <-
    | add_asn _ (pattern_wildcard _) _ = Some _ =>
        injection H as <-
    end.

  Ltac crush_term H :=
    match type of H with
    | Concrete.interp_term _ _ _ (term_constructor _ _ _) _ =>
      apply i_constructor_destr in H;
      let eq := fresh "eq" in
      destruct H as [?v [eq H]]; subst;
      crush_term H; try (crush_term eq)
    | Concrete.interp_term _ _ _ (term_tuple []) _ =>
        apply i_tuple_nil_destr in H; crush_term H; subst
    | Concrete.interp_term _ _ _ (term_tuple (_::_)) _ =>
        apply i_tuple_cons_destr in H;
        let H' := fresh "interp" in
        destruct H as (?v & ?vl & ?eq & H & H');
        subst;
        crush_term H;
        crush_term H'
    | Some _ = Some _ =>
        injection H as ?; subst
    | cval_tuple _ = cval_tuple _ =>
        injection H as ?; subst
    | exists _ : _, _ =>
        destruct H as [? H]; crush_term H
    | _ /\ _ =>
        let H1 := fresh H in
        let H2 := fresh H in
        destruct H as [H1 H2]; crush_term H1; crush_term H2
    | _ => idtac
    end.
