Require Import List.
From Necro Require Import Dict.
Import ListNotations DictNotations.
Require Import Ascii String.
Require Import Lambda_calculus ConcreteLemmae.
From Necro Require Import Skeleton Concrete Concrete_rec EqRec.
Require Import Relations.

(* Some useful notations *)

Notation "'Sv'" := (cval_constructor "Sv").
Notation "'Zv'" := (cval_constructor "Zv" (cval_tuple [])).
Notation "'Z'" := (cval_constructor "Z" (cval_tuple [])).
Notation "'S'" := (cval_constructor "S").
Notation "'Num'" := (cval_constructor "Num").
Notation "'Cons' x y l" :=
  (cval_constructor "Cons" (cval_tuple [cval_tuple [x;y]; l]))
  (at level 20, x at level 9, y at level 9).
Notation "'Sc'" := (cval_constructor "Sc").
Notation "'Val'" := (cval_constructor "Val").
Notation "'Etat' a b" :=
  (cval_constructor "Etat" (cval_tuple [a;b]))
  (at level 20, a at level 9).
Notation "'Var'" := (cval_constructor "Var").
Notation "'Match_nat' a b c" :=
  (cval_constructor "Match_nat" (cval_tuple [a;b;c]))
  (at level 20, a at level 9, b at level 9).
Notation "'Clos' x t s" :=
  (cval_constructor "Clos" (cval_tuple [x;t;s]))
  (at level 20, x at level 9, t at level 9).
Notation "'App' a b" :=
  (cval_constructor "App" (cval_tuple [a;b]))
  (at level 20, a at level 9).
Notation "'Fun' x t" :=
  (cval_constructor "Fun" (cval_tuple [x; t]))
  (at level 20, x at level 9).
Notation "'App1' a b c" :=
  (cval_constructor "App1" (cval_tuple [a; b; c]))
  (at level 20, a at level 9, b at level 9).
Notation "'App2' a b" :=
  (cval_constructor "App2" (cval_tuple [a; b]))
  (at level 20, a at level 9).
Notation "'MatchN' a b c d" :=
  (cval_constructor "MatchN" (cval_tuple [a; b; c; d]))
  (at level 20, a at level 9, b at level 9, c at level 9).
Notation "'MatchApp' a b" :=
  (cval_constructor "MatchApp" (cval_tuple [a; b]))
  (at level 20, a at level 9).


(* instantiation of our unspecified term *)

Inductive eq_var_interp: unspec_function :=
|eq_v x:
   eq_var_interp [] [cval_base string x; cval_base string x] (Sv Zv)
|neq_v x y:
   x <> y ->
   eq_var_interp [] [cval_base string x; cval_base string y] Zv.

Definition unspec_term_decl_interp :=
  ∅ + { "eq_var" → (2, eq_var_interp) }.


(* A few notations and definitions *)

Notation "a → b" := (Arrow a b) (right associativity, at level 39).
Definition gen (_:type) (_:cvalue) := False.
Notation "'interp_skel'" := (Concrete.interp_skel sem unspec_term_decl_interp gen).
Notation "'interp_term'" := (Concrete.interp_term sem unspec_term_decl_interp).
Notation "'apply'" := (Concrete.apply sem unspec_term_decl_interp gen).
Notation "'L_env'" := (τ_list (Prod [τ_variable; τ_value])).
Definition eval_lv envir expr v :=
  interp_skel [("envir", envir); ("expr", expr)]
    (skel_apply (
      term_var (TVSpec "eval_lv" (L_env → τ_expression → τ_value) []))
      [ term_var (TVLet "envir" L_env);
        term_var (TVLet "expr" (τ_expression))])
    v.
Definition get_env σ x v :=
  interp_skel [("σ", σ); ("x", x)]
    (skel_apply (
      term_var (TVSpec "get_env" (L_env → τ_variable → τ_value) []))
       [ term_var (TVLet "σ" L_env);
         term_var (TVLet "x" (τ_variable))])
    v.


(* Some tactics *)
Ltac crush_addasn2 :=
   repeat (match goal with
   |[H:add_asn _ _ _ _ |- _] => crush_addasn H
   end).

Lemma apply_empty : forall v w, apply v [] w -> v = w.
Proof.
  intros v w H. inversion H.
  - reflexivity.
  - subst. simpl in H1. unfold ge in H. apply Le.le_n_0_eq in H1. discriminate.
  - subst. simpl. rewrite app_nil_r. rewrite PeanoNat.Nat.sub_0_r. reflexivity.
Qed.
Lemma apply_tr_empty : forall v w P4 P5,
    next_triple_apply unspec_term_decl_interp P4 P5 v [] w -> v = w.
Proof.
  intros v w P4 P5 H. inversion H.
  - reflexivity.
  - subst. simpl in H1. unfold ge in H. apply Le.le_n_0_eq in H1. discriminate.
  - subst. simpl. rewrite app_nil_r. rewrite PeanoNat.Nat.sub_0_r. reflexivity.
Qed.
Ltac apply_empty :=
  match goal with
  | [ H: apply _ [] _ |- _] =>
      apply apply_empty in H; subst
  | [ H: next_triple_apply _ _ _ _ [] _ |- _] =>
      apply apply_tr_empty in H; subst
  end.

(*basically, the poof tactic says that if we have an hypothesis H of the form
"Pi _ _ _", the n associated to iter_n cannot be 0 (else H would imply False),
therefore H (and all the other similar hypotheses) becomes "next_triple_X ___"*)
Ltac poofaux n H1 H2 :=
  let P1 := fresh "P1" in
  let P2 := fresh "P2" in
  let P3 := fresh "P3" in
  destruct n; [solve [inversion H1; subst; exfalso; exact H2]|];
  simpl in H1;
  destruct (iter_n _ _ _ n) as [[P1 P2] P3] eqn:?;
  inversion H1; subst; clear H1.
Ltac poof := match goal with
  | [ Hiter : iter_n _ _ _ ?n = (?P1, ?P2, ?P3), H : ?P1 _ _ _ |- _ ] => poofaux n Hiter H
  | [ Hiter : iter_n _ _ _ ?n = (?P1, ?P2, ?P3), H : ?P2 _ _ _ |- _ ] => poofaux n Hiter H
  | [ Hiter : iter_n _ _ _ ?n = (?P1, ?P2, ?P3), H : ?P3 _ _ _ |- _ ] => poofaux n Hiter H
             end.



(* Auxiliary lemmae *)

Lemma strong_nat_ind : forall P : nat -> Prop,
    (forall n, (forall m, m<n -> P m) -> P n) -> forall n, P n.
Proof.
  intros P Hind n.
  apply Hind. induction n; intros m H. unfold lt in H. apply Le.le_n_0_eq in H.
  discriminate. apply Hind. intros m0 H0. apply IHn. unfold lt in *.
  apply (PeanoNat.Nat.le_trans (1 + m0) m). assumption. apply Peano.le_S_n.
  assumption.
Qed.

Lemma eval_lv_switch_env:
      forall P3 P4 P5 n env σ t σ_name t_name v,
      iter_n sem unspec_term_decl_interp gen n = (P3, P4, P5) ->
      findEnv σ_name env = Some σ ->
      findEnv t_name env = Some t ->
      next_triple_skel gen P3 P4 P5 env
        (skel_apply
          (term_var
             (TVSpec "eval_lv"
                (L_env → τ_expression → τ_value) []))
          [term_var (TVLet σ_name L_env);
          term_var (TVLet t_name (τ_expression))]) v ->
      next_triple_skel gen P3 P4 P5 [("envir", σ); ("expr", t)]
        (skel_apply
          (term_var
             (TVSpec "eval_lv"
                (L_env → τ_expression → τ_value) []))
          [term_var (TVLet "envir" L_env);
          term_var (TVLet "expr" (τ_expression))]) v.
Proof.
  intros until v. intros Heqp env_σ env_t H.
  iRApply H Hterm Hall. poof.
  iRVarSpec Hterm. crush_Forall2 Hall.
  destruct Hall as [A B]. iRVarLet A; iRVarLet B.
  rewrite A in env_σ; rewrite B in env_t.
  injection env_σ as ->; injection env_t as ->; clear A B.
  econstructor.
    * econstructor; [reflexivity..|eassumption].
    * repeat econstructor.
    * assumption.
Qed.


(* Definitions *)

Definition eval_ss : cvalue -> cvalue -> Prop :=
  fun c_expr c_val =>
   interp_skel [ ("expr", c_expr)] (skel_apply (term_var
              (TVSpec "eval_ss" (τ_expression →
              τ_value) []))
              [term_var (TVLet "expr" (τ_expression))]
                                   ) c_val.

Lemma eval_ss':
 forall e x τ τ' v w,
 interp_skel e
   (skel_apply
     (term_var (TVSpec "eval_ss" τ []))
     [term_var (TVLet x τ')]
   ) w ->
 findEnv x e = Some v ->
 eval_ss v w.
Proof.
  intros e x τ τ' v w app find. unfold eval_ss.
  apply i_apply_destr in app.
  destruct app as [? [? [Hall [Hterm app]]]].
  crush_Forall2 Hall.
  apply i_var_let_destr in Hall.
  rewrite Hall in find; clear Hall; injection find as ->.
  apply i_apply with (v:=x1) (vl:=[v]).
  - apply i_var_spec_destr in Hterm.
    let eq := fresh in
    destruct Hterm as [ta [ty [t [eq [_ Hterm]]]]];
    injection eq as <- <- <-.
    eapply i_var_spec; [reflexivity..|]. assumption.
  - repeat constructor.
  - assumption.
Qed.

Definition eval_ss_iter : cvalue -> cvalue -> Prop :=
  fun c_expr c_val => exists k, rel_iter eval_ss k c_expr c_val.

