Require Import List Relations.
Import ListNotations.
Require Import Ascii String.
Require Import Lambda_calculus.
Require Import ConcreteLemmae Prelim EvalInd CuttingLemmae EmbeddingLemmae.
From Necro Require Import Skeleton Concrete Concrete_rec EqRec Induction.
Require Import Coq.micromega.Lia.
Open Scope string.

Theorem bs_to_ss:
  forall envir expr val,
  eval_lv envir expr val -> eval_ss_iter (Etat envir expr) (Val val).
Proof.
  intros envir expr val Hevallv.
  induction Hevallv as
      [e x v getenv
      |e t1 t2 x t σ v2 vf [k1 Ht1] [k2 Ht2] [kapp Happ]
      |e t1 t2 t3 v [k1 Ht1] [k2 Ht2]
      |e t1 t2 t3 nv x t e' vf [k1 Ht1] [k3 Ht3] [kapp Happ]
      |e t x | e | e t v [k H]]
      using eval_lv_ind.
  + exists 1. constructor 2 with (b:=Val v); [|constructor].
    econstructor; [repeat econstructor..|].
    econstructor; [constructor|simpl|constructor].
    econstructor; [repeat constructor|reflexivity|].
    econstructor; [repeat constructor|reflexivity|].
    econstructor; [|reflexivity|repeat constructor].
    unfold get_env in getenv. iApply getenv Hterm Hall.
    crush_Forall2 Hall. destruct Hall as [H1 H2].
    iVarLet H1. iVarLet H2. iVarSpec Hterm.
    econstructor; [|repeat constructor|exact getenv].
    econstructor; [reflexivity..|assumption].
  + exists (1 + k1 + (1 + k2 + (1 + kapp))).
    apply rel_iter_split with (b:=App1 (Val (Clos x t σ)) e t2);
    [| apply rel_iter_split with (b:=App2 (Clos x t σ) (Val v2))].
    - (* computation of t1 *)
      constructor 2 with (b:=App1 (Etat e t1) e t2).
      * repeat econstructor.
      * apply in_out_App1. assumption.
    - (* computation of t2 *)
      constructor 2 with (b:=App2 (Clos x t σ) (Etat e t2)).
      * repeat econstructor.
      * apply in_out_App2. assumption.
    - (* computation of application *)
      constructor 2 with (b:=Etat (Cons x v2 σ) t).
      * repeat econstructor.
      * assumption.
  + exists (1 + k1 + (1 + k2)).
    apply rel_iter_split with (b:=MatchN (Val (Num Zv)) e t2 t3).
    - (* computation of t1 *)
      constructor 2 with (b:=MatchN (Etat e t1) e t2 t3).
      * repeat econstructor.
      * apply in_out_MatchN. assumption.
    - (* computation of t2 *)
      constructor 2 with (b:=(Etat e t2)).
      * repeat econstructor.
      * assumption.
  + exists (1 + k1 + (1 + k3 + (1 + kapp))).
    apply rel_iter_split with (b:=MatchN (Val (Num (Sv nv))) e t2 t3);
    [| apply rel_iter_split with (b:=MatchApp nv (Val (Clos x t e')))].
    - (* computation of t1 *)
      constructor 2 with (b:=MatchN (Etat e t1) e t2 t3).
      * repeat econstructor.
      * apply in_out_MatchN. assumption.
    - (* computation of t3 *)
      constructor 2 with (b:=MatchApp nv (Etat e t3)).
      * repeat econstructor.
      * apply in_out_MatchApp. assumption.
    - (* computation of application *)
      constructor 2 with (b:=Etat (Cons x (Num nv) e') t).
      * repeat econstructor.
      * assumption.
  + repeat econstructor.
  + repeat econstructor.
  + exists (2 + k).
    constructor 2 with (b:=Sc (Etat e t));
    [|apply rel_iter_last with (b:=Sc (Val (Num v)))].
    * repeat econstructor.
    * apply in_out_S. assumption.
    * repeat econstructor.
Qed.

Theorem ss_to_bs:
  forall envir expr val,
  eval_ss_iter (Etat envir expr) (Val val) -> eval_lv envir expr val.
Proof.
  intros envir expr val [k Hk]. revert val expr envir Hk.
  induction k using strong_nat_ind. intros.
  apply rel_iter_destr in Hk. destruct k as [|k]; [inversion Hk|].
  destruct Hk as [x [step1 ksteps]].
  unfold eval_ss in step1. iApply step1 Hterm Hall.
  crush_Forall2 Hall. iVarLet Hall. iVarSpec Hterm. unfold subst_term' in Hterm.
  simpl in Hterm. iFunc Hterm.
  iApplyClos step1 addasn isk. pCrush addasn. iApplyO step1.
  iMatch isk Ht getmatch. iVarLet Ht.
  (* destruct pattern-matching on c *)
  iGetMatch' getmatch addasn; try discriminate. pCrush addasn.
  iMatch isk Ht getmatch. iVarLet Ht.
  (* destruct pattern-matching on t *)
  iGetMatch' getmatch addasn; pCrush addasn.
  + (* Etat sigma (App t1 t2) *)
    iRet isk. itCrush isk.
    apply cut_App1 in ksteps.
    destruct ksteps as [v1 [v2 [k1 [k2 [k3 [<- [Hv1 [Hv2 Happ]]]]]]]].
    apply H in Hv1; [|lia].
    apply H in Hv2; [|lia].
    destruct k3 as [|k3]; [inversion Happ|].
    apply rel_iter_destr in Happ.
    destruct Happ as [x [step1 Happ]].
    unfold eval_ss in step1.
    iApply step1 Hterm Hall.
    crush_Forall2 Hall. iVarLet Hall. iVarSpec Hterm. unfold subst_term' in Hterm.
    simpl in Hterm. iFunc Hterm. iApplyClos step1 addasn isk. iApplyO step1.
    pCrush addasn. iMatch isk Hterm getmatch. iVarLet Hterm.
    iGetMatch' getmatch addasn; try discriminate; pCrush addasn.
    2:{ iLetIn isk addasn bound. clear - bound.
        apply eval_ss' with (v:=Val v2) in bound; [|reflexivity].
        apply eval_ss_val in bound. destruct bound. }
    iRet isk. itCrush isk.
    apply H in Happ; [|lia].
    econstructor; [repeat econstructor..|].
    econstructor; [repeat econstructor..|].
    econstructor; [reflexivity| |constructor].
    econstructor; [repeat econstructor|reflexivity|simpl].
    econstructor; [|repeat econstructor|].
    - unfold eval_lv in Hv2. iApply Hv2 Hterm Hall. iVarSpec Hterm.
      crush_Forall2 Hall. destruct Hall as [A B]. iVarLet A; iVarLet B.
      econstructor; [|repeat econstructor|eassumption].
      econstructor; [reflexivity..|assumption].
    - econstructor; [|repeat econstructor|].
      * unfold eval_lv in Hv1. iApply Hv1 Hterm Hall. iVarSpec Hterm.
        crush_Forall2 Hall. destruct Hall as [A B]. iVarLet A; iVarLet B.
        econstructor; [|repeat econstructor|eassumption].
        econstructor; [reflexivity..|assumption].
      * repeat econstructor.
      * unfold eval_lv in Happ. iApply Happ Hterm Hall. iVarSpec Hterm.
        crush_Forall2 Hall. destruct Hall as [A B]. iVarLet A; iVarLet B.
        econstructor; [|repeat econstructor|eassumption].
        econstructor; [reflexivity..|assumption].
  + (* Etat sigma (Var x) *)
    iLetIn isk addasn getenv. pCrush addasn.
    iRet isk. itCrush isk.
    apply rel_iter_destr in ksteps.
    destruct k as [|k];
        [|destruct ksteps as [? [F _]]; apply eval_ss_val in F; destruct F].
        injection ksteps as ->.
    econstructor; [repeat econstructor..|simpl].
    econstructor; [repeat econstructor..|simpl].
    econstructor; [reflexivity| |constructor].
    econstructor; [repeat econstructor|reflexivity|].
    iApply getenv Hterm Hall. crush_Forall2 Hall. destruct Hall as [A B].
    iVarLet A; iVarLet B.
    econstructor; [|repeat econstructor|eassumption].
    iVarSpec Hterm. econstructor; [reflexivity..|assumption].
  + (* Etat sigma (Match_nat t1 t2 t3) *)
    iRet isk. itCrush isk.
    apply cut_Match_nat in ksteps.
    destruct ksteps as [k1 [k2 [w [-> [Hv0 Hmatch]]]]].
    apply H in Hv0; [|lia].
    destruct k2 as [|k2]; [inversion Hmatch|].
    apply rel_iter_destr in Hmatch.
    destruct Hmatch as [x [step1 Hw]].
    unfold eval_ss in step1. iApply step1 Hterm Hall.
    crush_Forall2 Hall. iVarLet Hall. iVarSpec Hterm. unfold subst_term' in Hterm.
    simpl in Hterm. iFunc Hterm.
    iApplyClos step1 addasn isk. pCrush addasn. iApplyO step1.
    iMatch isk Ht getmatch. iVarLet Ht.
    iGetMatch' getmatch addasn; try discriminate.
    - pCrush addasn. iRet isk. itCrush isk.
      apply H in Hw; [|lia].
      econstructor; [repeat econstructor..|simpl].
      econstructor; [repeat econstructor..|simpl].
      econstructor; [reflexivity| |constructor].
      econstructor; [repeat econstructor|reflexivity|].
      econstructor.
      (* Hv0 *)
      unfold eval_lv in Hv0. iApply Hv0 Hterm Hall.
      crush_Forall2 Hall. destruct Hall as [A B]. iVarLet A. iVarLet B.
      iVarSpec Hterm. econstructor; [|repeat econstructor|eassumption].
      econstructor; [reflexivity..|assumption]. reflexivity.
      econstructor; [repeat econstructor|reflexivity|].
      (* Hw *)
      unfold eval_lv in Hw. iApply Hw Hterm Hall.
      crush_Forall2 Hall. destruct Hall as [A B]. iVarLet A. iVarLet B.
      iVarSpec Hterm. econstructor; [|repeat econstructor|eassumption].
      econstructor; [reflexivity..|assumption].
    - pCrush addasn. iRet isk. itCrush isk.
      apply cut_Match_App in Hw.
      destruct Hw as [k1' [k3 [v2 [-> [Hv1 Hmatch]]]]].
      apply H in Hv1; [|lia].
      destruct k3 as [|k3]; [inversion Hmatch|].
      apply rel_iter_destr in Hmatch.
      destruct Hmatch as [x [step1 Happ]].
      unfold eval_ss in step1. iApply step1 Hterm Hall.
      crush_Forall2 Hall. iVarLet Hall. iVarSpec Hterm. cbn in Hterm. iFunc Hterm.
      iApplyClos step1 addasn isk. pCrush addasn. iApplyO step1.
      iMatch isk Ht getmatch. iVarLet Ht.
      iGetMatch' getmatch addasn; try discriminate; pCrush addasn.
      * iRet isk. itCrush isk.
        apply H in Happ; [|lia].
        econstructor; [repeat econstructor..|simpl].
        econstructor; [repeat econstructor..|simpl].
        econstructor; [reflexivity| |constructor].
        econstructor; [repeat econstructor|reflexivity|].
        econstructor.
        (* Hv0 *)
        unfold eval_lv in Hv0. iApply Hv0 Hterm Hall.
        crush_Forall2 Hall. destruct Hall as [A B]. iVarLet A. iVarLet B.
        iVarSpec Hterm. econstructor; [|repeat econstructor|eassumption].
        econstructor; [reflexivity..|assumption]. reflexivity.
        econstructor; [repeat econstructor|reflexivity|].
        econstructor.
        (* Hw *)
        unfold eval_lv in Hv1. iApply Hv1 Hterm Hall.
        crush_Forall2 Hall. destruct Hall as [A B]. iVarLet A. iVarLet B.
        iVarSpec Hterm. econstructor; [|repeat econstructor|eassumption].
        econstructor; [reflexivity..|assumption]. reflexivity.
        (* Happ *)
        unfold eval_lv in Happ. iApply Happ Hterm Hall.
        crush_Forall2 Hall. destruct Hall as [A B]. iVarLet A. iVarLet B.
        iVarSpec Hterm. econstructor; [|repeat econstructor|eassumption].
        econstructor; [reflexivity..|assumption].
      * iLetIn isk addasn bound. pCrush addasn.
        apply eval_ss' with (v:=Val v2) in bound; [|reflexivity].
        apply eval_ss_val in bound. destruct bound.
    - pCrush addasn.
      iLetIn isk addasn bound. pCrush addasn. iRet isk. itCrush isk.
      apply eval_ss' with (v:=Val w) in bound; [|reflexivity].
      apply eval_ss_val in bound. destruct bound.
  + (* Etat sigma Z *)
    iRet isk. itCrush isk.
    apply rel_iter_destr in ksteps.
    destruct k as [|k].
    - injection ksteps as <-; repeat econstructor.
    - destruct ksteps as [? [F _]]; apply eval_ss_val in F; destruct F.
  + (* Etat sigma (S n) *)
    iRet isk. itCrush isk.
    destruct k as [|k]; [inversion ksteps|].
    apply cut_S in ksteps.
    destruct ksteps as [n [Hv ->]].
    apply H in Hv; [|lia].
        econstructor; [repeat econstructor..|simpl].
        econstructor; [repeat econstructor..|simpl].
        econstructor; [reflexivity| |constructor].
        econstructor; [repeat econstructor|reflexivity|].
        econstructor.
        (* Hv *)
        unfold eval_lv in Hv. iApply Hv Hterm Hall. iVarSpec Hterm.
        crush_Forall2 Hall. destruct Hall as [A B]. iVarLet A. iVarLet B.
        econstructor; [|repeat econstructor|eassumption].
        econstructor; [reflexivity..|assumption].
        reflexivity. repeat constructor.
  + (* Etat sigma (Fun x t1) *)
    iRet isk. itCrush isk.
    apply rel_iter_destr in ksteps.
    destruct k as [|k].
    - injection ksteps as <-. repeat econstructor.
    - destruct ksteps as [? [F ?]]; apply eval_ss_val in F; destruct F.
Qed.




Theorem equivalence_ss_lv :
  forall envir expr val,
    eval_lv envir expr val <-> eval_ss_iter (Etat envir expr) (Val val).
Proof.
  split; [apply bs_to_ss|apply ss_to_bs].
Qed.
