Inductive rel_iter {A} (P: A -> A -> Prop): nat -> A -> A -> Prop :=
|rel_iter_refl c : rel_iter P O c c
|rel_iter_next k a b c:
    P a b ->
    rel_iter P k b c ->
    rel_iter P (S k) a c.

Lemma rel_iter_split:
  forall {A} (P: A -> _) n m a b c,
    rel_iter P n a b ->
    rel_iter P m b c ->
    rel_iter P (n+m) a c.
Proof.
  intros A P n m. induction n; intros c c' c'' Hn Hm.
  - inversion Hn. simpl. assumption.
  - inversion Hn.
    rewrite plus_Sn_m. eapply rel_iter_next. eassumption.
    eapply IHn; eassumption.
Qed.

Lemma rel_iter_destr_S:
  forall {A} (P: A -> _) n v w,
  rel_iter P (1 + n) v w ->
    exists x, P v x /\ rel_iter P n x w.
Proof.
  intros A P n v w Hrel.
  inversion Hrel; subst.
  eexists; split; eassumption.
Qed.

Lemma rel_iter_destr_O:
  forall {A} (P: A -> _) v w,
  rel_iter P 0 v w -> v = w.
Proof.
  now inversion 1.
Qed.

Lemma rel_iter_destr_S':
  forall {A} (P: A -> _) n v w,
  rel_iter P (1 + n) v w ->
    exists x, rel_iter P n v x /\ P x w.
Proof.
  intros A P. induction n; intros v w Hrel.
  - apply rel_iter_destr_S in Hrel.
    destruct Hrel as [x [Pvx H0]].
    apply rel_iter_destr_O in H0; subst.
    exists v. split; [constructor|assumption].
  - apply rel_iter_destr_S in Hrel.
    destruct Hrel as [v' [Pvv' Hrel]].
    specialize (IHn v' w Hrel). clear Hrel.
    destruct IHn as [x [Hrel Pxw]].
    exists x. split; [|assumption].
    apply rel_iter_next with (b:=v'); assumption.
Qed.

Lemma rel_iter_last:
  forall {A} (P: A -> _) n a b c,
  rel_iter P n a b ->
  P b c ->
  rel_iter P (S n) a c.
Proof.
  intros A P n. induction n; intros a b c Hit HP.
  - inversion Hit; subst. econstructor; [eassumption|econstructor].
  - apply rel_iter_destr_S in Hit.
    destruct Hit as [a' [Haa' Ha'b]].
    apply rel_iter_next with (b:=a'); [assumption|].
    now apply (IHn a' b c).
Qed.

Lemma rel_iter_destr:
  forall {A} (P: A -> _) n v w,
  rel_iter P n v w ->
  match n with
  | 0 => v = w
  | S m => exists x, P v x /\ rel_iter P m x w
  end.
Proof.
  destruct n.
  - apply rel_iter_destr_O.
  - apply rel_iter_destr_S.
Qed.

Lemma rel_iter_destr':
  forall {A} (P: A -> _) n v w,
  rel_iter P n v w ->
  match n with
  | 0 => v = w
  | S m => exists x, rel_iter P m v x /\ P x w
  end.
Proof.
  destruct n.
  - apply rel_iter_destr_O.
  - apply rel_iter_destr_S'.
Qed.

