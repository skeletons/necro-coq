Require Import List.
Import ListNotations.
Require Import Relations Ascii String.
Require Import Lambda_calculus ConcreteLemmae.
From Necro Require Import Skeleton Concrete Concrete_rec EqRec.
From Necro Require Import Induction.
Require Import Prelim EvalInd.
Open Scope string.


Section Cut_ss.
  Lemma cut_App2: forall k0 v0 c v,
    rel_iter eval_ss k0 (App2 v0 c) (Val v) ->
    exists k1 k2 v1,
      k0 = k1 + k2 /\
      rel_iter eval_ss k1 c (Val v1) /\
      rel_iter eval_ss k2 (App2 v0 (Val v1)) (Val v).
  Proof.
    intro k. induction k using strong_nat_ind.
    intros ? ? ? Heval. apply rel_iter_destr in Heval.
    destruct k; [discriminate Heval|].
    destruct Heval as [x [step1 Heval]].
    assert(evalss:=step1).
    unfold eval_ss in step1.
    iApply step1 Heval_ss Hall.
    crush_Forall2 Hall.
    iVarLet Hall. iVarSpec Heval_ss.
    unfold WellFormed.subst_term' in Heval_ss. simpl in Heval_ss.
    iFunc Heval_ss. iApplyClos step1 addasn isk. pCrush addasn.
    apply_empty.
    iMatch isk Ht getmatch. iVarLet Ht.
    iGetMatch' getmatch addasn; try discriminate addasn.
    (* First case :
       | App2(Clos(x, t1, sigma), Val v2) → Etat(Cons ((x, v2), sigma), t1) *)
    - pCrush addasn. iRet isk. itCrush isk.
      exists 0, (1+k), v1. split; [reflexivity|]. split; [constructor|].
      constructor 2 with (b:=Etat (Cons v0 v1 v3) v2); assumption.
    (* Second case :
       | App2(v, c1) → let c2 = eval_ss c1 in App2(v, c2) *)
    - pCrush addasn. iLetIn isk Haa bound. pCrush Haa. iRet isk. itCrush isk.
      apply eval_ss' with (v:=c) in bound; [|reflexivity].
      specialize (H k (ltac:(constructor)) v0 _ v Heval).
      destruct H as [k1 [k2 [v1 [-> [Hk1 Hk2]]]]].
      exists (1+k1), k2, v1.
      split; [reflexivity|].
      split; [|exact Hk2].
      constructor 2 with (b:=x0); assumption.
Qed.

Lemma cut_App1: forall k sigma t1 t2 v,
    rel_iter eval_ss k
      (App1 t1 sigma t2) (Val v) ->
    exists v1 v2 k1 k2 k3,
      1 + k1 + k2 + k3 = k /\
      rel_iter eval_ss k1 t1 (Val v1) /\
      rel_iter eval_ss k2
        (Etat sigma t2) (Val v2) /\
      rel_iter eval_ss k3
        (App2 v1 (Val v2)) (Val v).
Proof.
  intro k. induction k as [k IH] using strong_nat_ind.
  intros σ t1 t2 v eval_iter. destruct k as [|k]; [inversion eval_iter|].
  apply rel_iter_destr in eval_iter.
  destruct eval_iter as [x [evalss evaliter]].
  unfold eval_ss in evalss. iApply evalss Hevalss Hall.
  crush_Forall2 Hall. iVarLet Hall. iVarSpec Hevalss.
  unfold subst_term' in Hevalss. simpl in Hevalss.
  iFunc Hevalss. iApplyClos evalss addasn isk. pCrush addasn.
  iApplyO evalss. iMatch isk Ht getmatch. iVarLet Ht.
  iGetMatch' getmatch addasn; try discriminate addasn.
    (* First case :
      | App1(Val v, sigma, t2) → App2(v, Etat(sigma, t2)) *)
    - pCrush addasn. iRet isk. itCrush isk.
      apply cut_App2 in evaliter.
      destruct evaliter as [k1 [k2 [v1 [eq [A B]]]]].
      exists v0, v1, 0, k1, k2. subst k.
      split; [reflexivity|].
      split; [constructor|].
      split; assumption.
    (* Second case :
       | App1(c1, sigma, t2) → let c2 = eval_ss c1 in App1(c2, sigma, t2) *)
    - pCrush addasn. iLetIn isk Haa bound. pCrush Haa. iRet isk. itCrush isk.
      apply eval_ss' with (v:=t1) in bound; [|reflexivity].
      specialize (IH k (ltac:(constructor)) _ _ _ _ evaliter).
      destruct IH as (v1 & v2 & k1 & k2 & k3 & <- & Hk1 & Hk2 & Hk3).
      exists v1, v2, (1+k1), k2, k3.
      split; [reflexivity|].
      split; [|split; assumption].
      constructor 2 with (b:=x0); assumption.
Qed.

Lemma cut_Match_nat: forall k0 e v1 v2 v3 v,
    rel_iter eval_ss k0 (MatchN v1 e v2 v3) (Val v) ->
    exists k1 k2 v1',
      k0 = k1+k2 /\ rel_iter eval_ss k1 v1 (Val v1') /\
      rel_iter eval_ss k2 (MatchN (Val v1') e v2 v3) (Val v).
Proof.
  intro k. induction k as [k IH] using strong_nat_ind.
  intros e v1 v2 v3 v eval_iter. destruct k as [|k]; [inversion eval_iter|].
  apply rel_iter_destr in eval_iter.
  destruct eval_iter as [x [evalss evaliter]]. assert(step1:=evalss).
  unfold eval_ss in evalss. iApply evalss Hevalss Hall.
  crush_Forall2 Hall. iVarLet Hall. iVarSpec Hevalss.
  unfold subst_term' in Hevalss. simpl in Hevalss.
  iFunc Hevalss. iApplyClos evalss addasn isk. pCrush addasn.
  iApplyO evalss. iMatch isk Ht getmatch. iVarLet Ht.
  iGetMatch' getmatch addasn; try discriminate.
    (* First case :
       | MatchN(Val(Num Zv), sigma, t2, _) → Etat(sigma, t2) *)
    - pCrush addasn. iRet isk. itCrush isk.
      exists 0, (1+k), (Num Zv).
      split; [reflexivity|].
      split; [constructor|].
      constructor 2 with (b:=Etat e v2); assumption.
    (* Second case :
       | MatchN(Val(Num (Sv nv)), sigma, _, t3) → MatchApp(nv, Etat(sigma, t3)) *)
    - pCrush addasn. iRet isk. itCrush isk.
      exists 0, (1+k), (Num (Sv v0)).
      split; [reflexivity|].
      split; [constructor|].
      constructor 2 with (b:=MatchApp v0 (Etat e v3)); assumption.
    (* Third case :
       | MatchN(c1, sigma, t2, t3) →
           let c2 = eval_ss c1 in (* bound *)
           MatchN(c2, sigma, t2, t3) *)
    - pCrush addasn. iLetIn isk Haa bound. pCrush Haa. iRet isk. itCrush isk.
      apply eval_ss' with (v:=v1) in bound; [|reflexivity].
      specialize (IH k (ltac:(constructor)) _ _ _ _ _ evaliter).
      destruct IH as (k1 & k2 & w & -> & Hk1 & Hk2).
      exists (1+k1), k2, w.
      split; [reflexivity|].
      split; [|assumption].
      constructor 2 with (b:=x0); assumption.
Qed.

Lemma cut_Match_App: forall k0 n c v,
    rel_iter eval_ss k0 (MatchApp n c) (Val v) ->
    exists k1 k2 v1,
      k0 = k1+k2 /\
      rel_iter eval_ss k1 c (Val v1) /\
      rel_iter eval_ss k2 (MatchApp n (Val v1)) (Val v).
Proof.
  intro k. induction k as [k IH] using strong_nat_ind.
  intros n c v eval_iter. destruct k as [|k]; [inversion eval_iter|].
  apply rel_iter_destr in eval_iter.
  destruct eval_iter as [x [evalss evaliter]]. assert(step1:=evalss).
  unfold eval_ss in evalss. iApply evalss Hevalss Hall.
  crush_Forall2 Hall. iVarLet Hall. iVarSpec Hevalss.
  unfold subst_term' in Hevalss. simpl in Hevalss.
  iFunc Hevalss. iApplyClos evalss addasn isk. pCrush addasn.
  iApplyO evalss. iMatch isk Ht getmatch. iVarLet Ht.
  iGetMatch' getmatch addasn; try discriminate.
    (* First case :
       | MatchApp(nv, Val (Clos(x, t1, sigma))) →
           Etat(Cons<(variable, value)>((x, Num nv), sigma), t1) *)
    - pCrush addasn. iRet isk. itCrush isk.
      exists 0, (1+k), (Clos v0 v1 v2).
      split; [reflexivity|].
      split; [constructor|].
      constructor 2 with (b:=Etat (Cons v0 (Num n) v2) v1); assumption.
    (* Second case :
       | MatchApp(nv, c1) →
           let c2 = eval_ss c1 in (* bound *)
           MatchApp(nv, c2) *)
    - pCrush addasn. iLetIn isk addasn bound.
      pCrush addasn. iRet isk. itCrush isk.
      apply eval_ss' with (v:=c) in bound; [|reflexivity].
      specialize (IH k (ltac:(constructor)) _ _ _ evaliter).
      destruct IH as (k1 & k2 & w & -> & Hk1 & Hk2).
      exists (1+k1), k2, w.
      split; [reflexivity|].
      split; [|assumption].
      constructor 2 with (b:=x0); assumption.
Qed.


Lemma cut_S: forall k0 c v,
    rel_iter eval_ss (1+k0) (Sc c) (Val v) ->
    exists n,
      rel_iter eval_ss k0 c (Val (Num n)) /\
      v = Num (Sv n).
Proof.
  intro k. induction k as [k IH] using strong_nat_ind.
  intros c v eval_iter.
  apply rel_iter_destr in eval_iter.
  destruct eval_iter as [x [evalss evaliter]].
  unfold eval_ss in evalss. iApply evalss Hevalss Hall.
  crush_Forall2 Hall. iVarLet Hall. iVarSpec Hevalss.
  unfold subst_term' in Hevalss. simpl in Hevalss.
  iFunc Hevalss. iApplyClos evalss addasn isk. pCrush addasn.
  iApplyO evalss. iMatch isk Ht getmatch. iVarLet Ht.
  iGetMatch' getmatch addasn; try discriminate.
  pCrush addasn.
  iMatch isk Ht getmatch. itCrush Ht.
  iGetMatch' getmatch addasn; try discriminate.
  - (* first case : | Val (Num n) → Val (Num (Sv n)) *)
  pCrush addasn.
  iRet isk. itCrush isk.
  enough(k = 0).
  { exists v1. subst. apply rel_iter_destr in evaliter.
    injection evaliter as <-. split; [constructor|reflexivity]. }
  destruct k as [|k]; [reflexivity|exfalso].
  apply rel_iter_destr in evaliter.
  destruct evaliter as [x [F _]]. unfold eval_ss in F.
  iApply F Hterm Hall.
  iVarSpec Hterm. unfold subst_term' in Hterm. simpl in Hterm. iFunc Hterm.
  crush_Forall2 Hall. itCrush Hall. iApplyClos F addasn isk. pCrush addasn.
  iApplyO F. iMatch isk Ht getmatch. itCrush Ht.
  iGetMatch' getmatch addasn; try discriminate. simpl in addasn.
  iBranch isk.
  - (* second case : | _ → let c2 = eval_ss c' in Sc c2 *)
    pCrush addasn. iLetIn isk addasn bound.
    pCrush addasn. iRet isk. itCrush isk.
    apply eval_ss' with (v:=c) in bound; [|reflexivity].
    simpl in evaliter.
    destruct k as [|k]; [inversion evaliter|].
    specialize (IH k (ltac:(constructor)) _ _ evaliter).
    destruct IH as (n & Hk & ->).
    exists n. split; [|reflexivity].
    constructor 2 with (b:=x0); assumption.
Qed.

End Cut_ss.
