Require Import List.
Import ListNotations.
Require Import Ascii String.
Require Import Relations.
Require Import Lambda_calculus ConcreteLemmae Prelim EvalInd.
From Necro Require Import Skeleton Concrete Concrete_rec EqRec Induction.
Open Scope string.

(*A bunch of lemmas stating that the small-step evaluation inside and outside
of the different configuration constructors take as many steps*)

Lemma eval_ss_val:
  forall x y,
    ~eval_ss (Val x) y.
Proof.
  intros x y step1.
  unfold eval_ss in step1. iApply step1 Hterm Hall.
  crush_Forall2 Hall. iVarLet Hall. iVarSpec Hterm.
  unfold subst_term' in Hterm. simpl in Hterm. iFunc Hterm.
  iApplyClos step1 addasn isk. iApplyO step1. pCrush addasn.
  iMatch isk Ht getmatch. iVarLet Ht.
  iGetMatch' getmatch addasn; try discriminate.
  iBranch isk.
Qed.


Section In_Out.
Lemma in_out_S :
  forall k c v,
    rel_iter eval_ss k c v ->
    rel_iter eval_ss k (Sc c) (Sc v).
Proof.
  induction k; intros c v Heval.
  - inversion Heval. constructor.
  - apply rel_iter_destr in Heval.
    destruct Heval as [w [step1 ksteps]].
    constructor 2 with (b:=Sc w); [|apply IHk; assumption].
    clear - step1. eapply i_apply with (vl:=[Sc c]).
    eapply i_var_spec; [reflexivity..|eapply i_func].
    repeat constructor.
    eapply i_apply_clos; [reflexivity|..|constructor]. simpl.
    eapply i_match; [constructor|..]; [reflexivity..|].
    destruct (add_asn [("c'",c);("c",Sc c)] (pattern_constr "Val" [] (pattern_constr "Num" [] (pattern_var "n" (Base "nval" [])))) c) as [e'|] eqn:H.
    + pCrush H. apply eval_ss_val in step1. destruct step1.
    + eapply i_match; [constructor|..]; [reflexivity|..].
      simpl. simpl in H. rewrite H. clear H. reflexivity.
      econstructor; [|repeat econstructor..]. clear H.
      unfold eval_ss in step1. iApply step1 Hterm Hall.
      crush_Forall2 Hall. iVarLet Hall.
      econstructor.
      iVarSpec Hterm.
      * econstructor; [reflexivity..|eassumption].
      * repeat constructor.
      * assumption.
Qed.

Lemma in_out_App1 : forall k c v e c', rel_iter eval_ss k c v ->
      rel_iter eval_ss k (App1 c e c') (App1 v e c').
Proof.
  intros k. induction k; intros c v e c' Heval.
  - inversion Heval. constructor.
  - apply rel_iter_destr in Heval.
    destruct Heval as [w [step1 ksteps]].
    constructor 2 with (b:=App1 w e c').
    * econstructor; [|repeat constructor|].
      econstructor; [reflexivity..|]. constructor.
      econstructor; [repeat constructor|..|constructor]. simpl.
      destruct (add_asn [("c", App1 c e c')]
        (pattern_constr "App1" []
          (pattern_tuple
             [pattern_constr "Val" [] (pattern_var "v" (Base "value" []));
             pattern_var "sigma"
               (Base "list" [Prod [Base "variable" []; Base "value" []]]);
             pattern_var "t2" (Base "expression" [])])) (App1 c e c'))
             as [e'|] eqn:aa.
      + pCrush aa. apply eval_ss_val in step1. destruct step1.
      + eapply i_match; [constructor|..]; [reflexivity|..].
        simpl. simpl in aa. rewrite aa. clear aa. reflexivity.
        econstructor; [|repeat econstructor..]. clear aa.
        unfold eval_ss in step1. iApply step1 Hterm Hall.
        crush_Forall2 Hall. iVarLet Hall.
        econstructor; [|repeat econstructor|eassumption].
        iVarSpec Hterm. econstructor; [reflexivity..|]. assumption.
    * now apply IHk.
Qed.

Lemma in_out_App2 : forall k v0 c c', rel_iter eval_ss k c c'->
      rel_iter eval_ss k (App2 v0 c) (App2 v0 c').
Proof.
  intros k. induction k; intros v c c' Heval.
  - inversion Heval. constructor.
  - apply rel_iter_destr in Heval.
    destruct Heval as [w [step1 ksteps]].
    constructor 2 with (b:=App2 v w).
    * econstructor; [|repeat constructor|].
      econstructor; [reflexivity..|]. constructor.
      econstructor; [repeat constructor|..|constructor]. simpl.
      destruct (add_asn [("c", App2 v c)]
        (pattern_constr "App2" []
          (pattern_tuple
             [pattern_constr "Clos" []
                (pattern_tuple
                   [pattern_var "x" (Base "variable" []);
                   pattern_var "t1" (Base "expression" []);
                   pattern_var "sigma"
                     (Base "list" [Prod [Base "variable" []; Base "value" []]])]);
             pattern_constr "Val" [] (pattern_var "v2" (Base "value" []))]))
             (App2 v c))
             as [e'|] eqn:aa.
      + pCrush aa. apply eval_ss_val in step1. destruct step1.
      + eapply i_match; [constructor|..]; [reflexivity|..].
        simpl. simpl in aa. rewrite aa. clear aa. reflexivity.
        econstructor; [|repeat econstructor..]. clear aa.
        unfold eval_ss in step1. iApply step1 Hterm Hall.
        crush_Forall2 Hall. iVarLet Hall.
        econstructor; [|repeat econstructor|eassumption].
        iVarSpec Hterm. econstructor; [reflexivity..|]. assumption.
    * now apply IHk.
Qed.

Lemma in_out_MatchN : forall k t1 t2 t3 v1 e,
  rel_iter eval_ss k t1 v1->
  rel_iter eval_ss k (MatchN t1 e t2 t3) (MatchN v1 e t2 t3).
Proof.
  intros k. induction k; intros t1 t2 t3 v1 e Heval.
  - inversion Heval. constructor.
  - apply rel_iter_destr in Heval.
    destruct Heval as [w [step1 ksteps]].
    constructor 2 with (b:=MatchN w e t2 t3).
    * econstructor; [|repeat constructor|].
      econstructor; [reflexivity..|]. constructor.
      econstructor; [repeat constructor|..|constructor]. simpl.
      destruct (add_asn [("c", MatchN t1 e t2 t3)]
        (pattern_constr "MatchN" []
          (pattern_tuple
             [pattern_constr "Val" []
                (pattern_constr "Num" []
                   (pattern_constr "Zv" [] (pattern_tuple [])));
             pattern_var "sigma"
               (Base "list" [Prod [Base "variable" []; Base "value" []]]);
             pattern_var "t2" (Base "expression" []);
             pattern_wildcard (Base "expression" [])]))
             (MatchN t1 e t2 t3))
             as [e'|] eqn:aa.
      2:destruct (add_asn [("c", MatchN t1 e t2 t3)]
          (pattern_constr "MatchN" []
            (pattern_tuple
               [pattern_constr "Val" []
                  (pattern_constr "Num" []
                     (pattern_constr "Sv" [] (pattern_var "nv" (Base "nval"
                     []))));
               pattern_var "sigma"
                 (Base "list" [Prod [Base "variable" []; Base "value" []]]);
               pattern_wildcard (Base "expression" []);
               pattern_var "t3" (Base "expression" [])]))
             (MatchN t1 e t2 t3))
             as [e'|] eqn:aa'.
      + pCrush aa. apply eval_ss_val in step1. destruct step1.
      + pCrush aa'. apply eval_ss_val in step1. destruct step1.
      + eapply i_match; [constructor|..]; [reflexivity|..].
        simpl. simpl in aa, aa'. rewrite aa, aa'. clear aa aa'. reflexivity.
        econstructor; [|repeat econstructor..]. clear aa aa'.
        unfold eval_ss in step1. iApply step1 Hterm Hall.
        crush_Forall2 Hall. iVarLet Hall.
        econstructor; [|repeat econstructor|eassumption].
        iVarSpec Hterm. econstructor; [reflexivity..|]. assumption.
    * now apply IHk.
Qed.


Lemma in_out_MatchApp : forall k t v v0,
  rel_iter eval_ss k t v ->
  rel_iter eval_ss k (MatchApp v0 t) (MatchApp v0 v).
Proof.
  intros k. induction k; intros t v v0 Heval.
  - inversion Heval. constructor.
  - apply rel_iter_destr in Heval.
    destruct Heval as [w [step1 ksteps]].
    constructor 2 with (b:=MatchApp v0 w).
    * econstructor; [|repeat constructor|].
      econstructor; [reflexivity..|]. constructor.
      econstructor; [repeat constructor|..|constructor]. simpl.
      destruct (add_asn [("c", MatchApp v0 t)]
        (pattern_constr "MatchApp" []
          (pattern_tuple
             [pattern_var "nv" (Base "nval" []);
             pattern_constr "Val" []
               (pattern_constr "Clos" []
                  (pattern_tuple
                     [pattern_var "x" (Base "variable" []);
                     pattern_var "t1" (Base "expression" []);
                     pattern_var "sigma"
                       (Base "list"
                          [Prod [Base "variable" []; Base "value" []]])]))]))
             (MatchApp v0 t))
             as [e'|] eqn:aa.
      + pCrush aa. apply eval_ss_val in step1. destruct step1.
      + eapply i_match; [constructor|..]; [reflexivity|..].
        simpl. simpl in aa. rewrite aa. clear aa. reflexivity.
        econstructor; [|repeat econstructor..]. clear aa.
        unfold eval_ss in step1. iApply step1 Hterm Hall.
        crush_Forall2 Hall. iVarLet Hall.
        econstructor; [|repeat econstructor|eassumption].
        iVarSpec Hterm. econstructor; [reflexivity..|]. assumption.
    * now apply IHk.
Qed.

End In_Out.
