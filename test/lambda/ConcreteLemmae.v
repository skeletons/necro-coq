Require Import String List ZArith Lia.
From Necro Require Import Dict Skeleton Concrete.
Import ListNotations DictNotations.
Open Scope string_scope.



Section Lemmae.
  Variable sem:
    Skeleton.skeletal_semantics.
  Variable unspec:
    dict (nat * unspec_function).
  Variable g: type -> cvalue -> Prop.


  (* Destructors for Forall2 *)


  Lemma forall2_cons_destr1:
    forall {A1 A2} P (x1:A1) l1 (l:list A2),
    Forall2 P (x1::l1) l ->
    exists x2 l2,
    l = x2 :: l2 /\ P x1 x2 /\ Forall2 P l1 l2.
  Proof.
    inversion 1; eexists; eexists; split; auto.
  Qed.

  Lemma forall2_cons_destr2:
    forall {A1 A2} P (x2:A2) l2 (l:list A1),
    Forall2 P l (x2::l2) ->
    exists x1 l1,
    l = x1 :: l1 /\ P x1 x2 /\ Forall2 P l1 l2.
  Proof.
    inversion 1; eexists; eexists; split; auto.
  Qed.

  Lemma forall2_nil_destr1:
    forall {A B} (P: A -> B -> Prop) l,
    Forall2 P [] l -> l = [].
  Proof.
    inversion 1; auto.
  Qed.

  Lemma forall2_nil_destr2:
    forall {A B} (P: A -> B -> Prop) l,
    Forall2 P l [] -> l = [].
  Proof.
    inversion 1; auto.
  Qed.
End Lemmae.

  Ltac crush_Forall2 H :=
    match type of H with
    | Forall2 _ [] [] =>
        clear H
    | Forall2 _ [] (_::_) =>
        apply forall2_nil_destr1 in H; discriminate H
    | Forall2 _ (_::_) [] =>
        apply forall2_nil_destr2 in H; discriminate H
    | Forall2 _ [] _ =>
        apply forall2_nil_destr1 in H; subst
    | Forall2 _ _ [] =>
        apply forall2_nil_destr2 in H; subst
    | Forall2 _ (?x1::[]) (?x2::[]) =>
        apply forall2_cons_destr1 in H;
        let eq := fresh "eq" in
        destruct H as (?x & ?l & eq & H & _);
        injection eq as <- <-
    | Forall2 _ (?x1::[]) _ =>
        apply forall2_cons_destr1 in H;
        let eq := fresh "eq" in
        let H' := fresh H in
        destruct H as (?x & ?l & -> & H & H');
        crush_Forall2 H'
    | Forall2 _ (?x1::?l1) (?x2::?l2) =>
        apply forall2_cons_destr1 in H;
        let eq := fresh "eq" in
        let H' := fresh H in
        destruct H as (?x & ?l & eq & H & H');
        injection eq as ? ?;
        subst;
        crush_Forall2 H';
        let G := fresh in
        assert(G:=conj H H'); clear H H';
        assert(H:=G); clear G
    | Forall2 _ (?x1::?l1) _ =>
        apply forall2_cons_destr1 in H;
        let eq := fresh "eq" in
        let H' := fresh H in
        destruct H as (?x & ?l & eq & H & H');
        subst;
        crush_Forall2 H';
        assert(G:=conj H H'); clear H H';
        assert(H:=G); clear G
    | Forall2 _ _ (?x2::?l2) =>
        apply forall2_cons_destr2 in H;
        let eq := fresh "eq" in
        let H' := fresh H in
        destruct H as (?x & ?l & eq & H & H');
        subst;
        crush_Forall2 H';
        assert(G:=conj H H'); clear H H';
        assert(H:=G); clear G
    | Forall2 _ _ _ => idtac
    | _ =>
        fail "The hypothesis doesn't match Forall2 _ _ _"
    end.


