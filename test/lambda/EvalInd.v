(* This files proves the induction principle on eval_lv *)

Require Import Ascii String List.
From Necro Require Import Dict.
Import ListNotations DictNotations.
Require Import Lambda_calculus ConcreteLemmae.
From Necro Require Import Skeleton Concrete Concrete_rec EqRec.
Require Import Prelim.

Section Eval_lv_ind.

  Variable P:cvalue -> cvalue -> cvalue -> Prop.

  Variable HVar:
    forall e x v,
      get_env e x v ->
      P e (Var x) v.

  Variable HApp:
    forall e t1 t2 x t sigma v2 vf,
      P e t1 (Clos x t sigma) ->
      P e t2 v2 ->
      P (Cons x v2 sigma) t vf->
      P e (App t1 t2) vf.

  Variable HMatchZ:
    forall e t1 t2 t3 v,
      P e t1 (Num Zv) ->
      P e t2 v ->
      P e (Match_nat t1 t2 t3) v.

  Variable HMatchS:
    forall e t1 t2 t3 nv x t e' vf,
      P e t1 (Num (Sv nv)) ->
      P e t3 (Clos x t e') ->
      P (Cons x (Num nv) e') t vf->
      P e (Match_nat t1 t2 t3) vf.

  Variable HFun:
    forall e t x,
      P e (Fun x t) (Clos x t e).

  Variable HZ:
    forall e,
      P e Z (Num Zv).

  Variable HS:
    forall e t v,
      P e t (Num v) ->
      P e (S t) (Num (Sv v)).

  Lemma eval_lv_ind :
    forall e t v,
      eval_lv e t v ->
      P e t v.
  Proof.
    intros t e v Heval.
    apply bs_rec_eq_skel in Heval.
    unfold interp_skel_rec in Heval. destruct Heval as [n Hevaln].
    revert t e v Hevaln.
    induction n as [n IH] using strong_nat_ind.
    intros t e v Hevaln.
    destruct n; [destruct Hevaln|].
    (*we explicit the implications of Hevaln*)
    unfold interp_skel_n in Hevaln. simpl in Hevaln.
    destruct iter_n as [[? ?] ?] eqn:Eeval in Hevaln.
    apply irec_apply_destr in Hevaln.
    destruct Hevaln as [vl [w [all2 [evallv app]]]].
    crush_Forall2 all2; destruct all2 as [A B].
    poof. iRVarLet A. iRVarLet B. iRVarSpec evallv. poof.
    unfold subst_term' in evallv. simpl in evallv.
    iRFunc evallv. iRApplyClos app addasn isk.
    pVar addasn. iRRet isk. poof.
    iRFunc isk. iRApplyClos app addasn isk.
    iRApplyO app. pVar addasn.
    iRMatch isk Ht getmatch. poof.
    iRVarLet Ht.
    apply get_match_destr' in getmatch.
    destruct getmatch as [p [Hin Haddasn]].
    repeat destruct Hin as [eq|Hin]; [..|destruct Hin]; injection eq as <- <-.
    (* | Var x → get_env sigma x *)
    - pConstr Haddasn. pVar Haddasn.
      apply HVar.
      unfold get_env.
      apply irec_apply_destr in isk.
      destruct isk as [? [? [all2 [getenv app]]]]. poof.
      apply bs_rec_eq_skel. exists (2+n). unfold interp_skel_n.
      simpl. rewrite Heqp.
      econstructor.
      + iRVarSpec getenv. econstructor; [reflexivity..|eassumption].
      + repeat econstructor.
      + crush_Forall2 all2. destruct all2 as [A B].
        iRVarLet A. iRVarLet B. eassumption.
    (* | App(t1, t2) →
           let v = eval_lv sigma t2 in (* evallv1 *)
           let Clos(x,t,sigma1) = eval_lv sigma t1 in (* evallv2 *)
           eval_lv (Cons<(variable, value)>((x, v), sigma1)) t *)
    - pConstr Haddasn.
      pTupleCons Haddasn Hhd. pVar Haddasn.
      pTupleCons Hhd Hhdhd. pTupleNil Hhdhd. pVar Hhd.
      iRLetIn isk addasn evallv1. pVar addasn. poof.
      iRLetIn isk addasn evallv2.
      pConstr addasn. pTupleCons addasn Hhd. pVar addasn.
      pTupleCons Hhd Hhdhd. pTupleCons Hhdhd Hhdhdhd.
      pTupleNil Hhdhdhd. pVar Hhdhd. pVar Hhd. poof.
      apply HApp with (x:=v3) (t:=v2) (sigma:=v4) (v2:=x).
      + (* evallv2 *)
        apply IH with (m:=1+n); [repeat constructor|].
        unfold interp_skel_n. simpl. rewrite Heqp0.
        eapply eval_lv_switch_env with (4:=evallv2); eauto.
      + (* evallv1 *)
        apply IH with (m:=2+n); [repeat constructor|].
        unfold interp_skel_n. simpl. rewrite Heqp0.
        eapply eval_lv_switch_env with (n:=1+n) (4:=evallv1); eauto.
        simpl. now rewrite Heqp0.
      + (* isk *)
        clear evallv1 evallv2.
        iRApply isk Hevallv all2.
        crush_Forall2 all2. destruct all2 as [A B].
        poof. iRVarLet B. iRConstr A. poof.
        iRTupleCons A B. poof.
        iRTupleCons A C. poof.
        iRTupleNil A. iRVarLet C.
        iRTupleCons B A. iRVarLet A.
        iRTupleCons B A. poof.
        iRVarLet A. iRTupleNil B.
        apply IH with (m:=6+n); [repeat constructor|].
        unfold interp_skel_n. simpl. rewrite Heqp.
        econstructor.
        * iRVarSpec Hevallv. econstructor; [reflexivity..|eassumption].
        * repeat econstructor.
        * eassumption.
    (* | Match_nat (t1, t2, t3) →
           let Num n = eval_lv sigma t1 in (* evallv1 *)
           match n with
           | Zv →
               eval_lv sigma t2 (* isk *)
           | Sv nv →
               let Clos(x,t,s) = eval_lv sigma t3 in (* evallv2 *)
               eval_lv (Cons<(variable, value)>((x, Num nv), s)) t (* isk *)
           end *)
    - pConstr Haddasn. pTupleCons Haddasn A. pVar Haddasn.
      pTupleCons A B. pVar A. pTupleCons B A. pTupleNil A. pVar B.
      iRLetIn isk addasn evallv1. poof.
      pConstr addasn. pVar addasn.
      iRMatch isk Ht getmatch. poof.
      iRVarLet Ht.
      apply get_match_destr' in getmatch.
      destruct getmatch as [p [[eq|[eq|[]]] addasn]]; injection eq as <- <-.
      + (* n is Zv *)
        pConstr addasn. pTupleNil addasn.
        apply HMatchZ.
        * (* evallv1 *)
          apply IH with (m:=2+n); [repeat constructor|].
          unfold interp_skel_n. simpl. rewrite Heqp0.
          eapply eval_lv_switch_env with (n:=1+n) (4:=evallv1); eauto.
          simpl. now rewrite Heqp0.
        * (* isk *)
          apply IH with (m:=1+n); [repeat constructor|].
          unfold interp_skel_n. simpl. rewrite Heqp0.
          eapply eval_lv_switch_env with (4:=isk); eauto.
      + (* n is Sv _ *)
        pConstr addasn. pVar addasn.
        iRLetIn isk addasn evallv2. poof.
        pConstr addasn. pTupleCons addasn A. pVar addasn.
        pTupleCons A B. pVar A. pTupleCons B A.
        pVar B. pTupleNil A.
        apply HMatchS with (nv:=v4) (x:=v5) (t:=v3) (e':=v6).
        * (* evallv1 *)
          clear evallv2 isk.
          apply IH with (m:=3+n); [repeat constructor|].
          unfold interp_skel_n. simpl. rewrite Heqp.
          eapply eval_lv_switch_env with (n:=2+n)(4:=evallv1); eauto.
          simpl. now rewrite Heqp.
        * (* evallv2 *)
          clear evallv1 isk.
          apply IH with (m:=1+n); [repeat constructor|].
          unfold interp_skel_n. simpl. rewrite Heqp.
          eapply eval_lv_switch_env with (n:=n)(4:=evallv2); eauto.
        * (* isk *)
          clear evallv1 evallv2.
          apply IH with (m:=1+n); [repeat constructor|].
          unfold interp_skel_n. simpl. rewrite Heqp.
          iRApply isk Hevallv all2. poof.
          crush_Forall2 all2.
          destruct all2 as [A B].
          iRConstr A. iRVarLet B. poof. iRTupleCons A B. poof.
          iRTupleCons A C. poof. iRTupleNil A. iRVarLet C. iRTupleCons B A.
          iRVarLet A. iRTupleCons B A. poof. iRTupleNil B.
          iRConstr A. poof. iRVarLet A.
          econstructor.
          -- iRVarSpec Hevallv. econstructor; [reflexivity..|eassumption].
          -- repeat econstructor.
          -- eassumption.
    (* | Fun (x, t1) → Clos(x,t1,sigma) *)
    - pConstr Haddasn. pTupleCons Haddasn B. pVar Haddasn. pTupleCons B A.
      pVar B. pTupleNil A. iRRet isk. poof.
      iRConstr isk. poof. iRTupleCons isk ihd. poof.
      iRVarLet ihd. iRTupleCons isk ihd. poof. iRVarLet ihd.
      iRTupleCons isk ihd. poof. iRVarLet ihd. iRTupleNil isk.
      apply HFun.
    - (* | Z → Num Zv *)
      pConstr Haddasn. pTupleNil Haddasn.
      iRRet isk. poof. iRConstr isk. poof. iRConstr isk. poof.
      iRTupleNil isk.
      apply HZ.
    (* | S n →
           let Num x = eval_lv sigma n in (* bound *)
           Num (Sv x) *)
    - pConstr Haddasn. pVar Haddasn.
      iRLetIn isk addasn bound.
      pConstr addasn. pVar addasn.
      poof. iRRet isk. poof.
      iRConstr isk. poof. iRConstr isk. poof. iRVarLet isk.
      apply HS.
      apply IH with (m:=4+n); [repeat constructor|].
      unfold interp_skel_n. simpl. rewrite Heqp0.
      apply eval_lv_switch_env with (n:=3+n) (4:=bound); eauto.
      simpl. now rewrite Heqp0.
  Qed.

End Eval_lv_ind.
