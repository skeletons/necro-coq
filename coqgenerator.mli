(** This module generates Coq code for the skeletal semnatics of
 * some already typed ast **)

(** This function takes as argument the contents of the ast and
    returns the corresponding Coq code. For now, It does not work if the
    skeletal semantics contains monads or uses includes **)
val gen: Necro.TypedAST.skeletal_semantics -> string

