# How to use Necro Coq (Walkthrough)

The `necrocoq` app produces a gallina file, which is a module, like all gallina
files.

For instance, when running `necrocoq myinput.sk -o Myinput.v`, a `Myinput.v`
file will be created, which can be referred as a `Myinput` module in another
gallina file.

Let us analyze the contents of `Skeleton.v`, `Myinput.v` and `Concrete.v` and
explain what needs to be done to do proofs.

This file contains several pieces.

## Skeleton.v ([files/Skeleton.v](files/Skeleton.v))

The contents of the `Skeleton` section defines the AST for the `Skel` language.
For more information about it, see the
[Skel manual](https://gitlab.inria.fr/skeletons/necro-man).

The `skeletal_semantics` record type is the type of a skeletal semantics. The
contents of a record of this type is exactly the translation of a `.sk` file.

Then, the `Recursors` section gives recursors on `type`, `value` and `skel`. As
of Coq 8.13.2, it is better than the automatically generated one, since there
are lists in the constructors of `type` and `value` that are not well handled by
Coq's default elimination scheme.

## Myinput.v

### `unspec_type`, `spec_type`, `value_name`, `constructor`

These are the list of the names of all defined non-specified types, specified
types, values (specified or non-specified) and constructor (respectively).

### `value_decl`, `ctype` and `sem`

`value_decl` contains the type of all the declared values, and its contents when
the value is specified.

`ctype` gives the expected input type and the output type of all constructors.

`sem` gives a record of type [`Skeleton.skeletal_semantics`](Skeleton.v#L55)

## Concrete.v ([files/Concrete.v](files/Concrete.v))

`Concrete.v` defines the Concrete interpretation for the skeletal semantics.
There are variations on `Concrete.v` defined in [files](files), but their working
is basically the same.

### `Require Import WellFormed`

The file [`WellFormed.v`](files/WellFormed.v) is not explained here, but it
contains basic type manipulations, and definition as to what is a well-formed
skeleton / value / skeletal semantics.

### Variables

There are two variables that must be instantiated.
- `Variable s: skeletal_semantics`
  The `s` provided is the semantics of the considered language.
- `Variable unspec_value_interp:
    s_value_name s -> option (nat * (list type -> option unspec_function)).`
  It gives the contents of the non-specified value. Let `v` be a non-specified
  value declared by `s`. Then, `unspec_value_interp v` must be `Some (n, f)`
  where `n` is the number of argument that must be provided to `v`, and (`f ta`
  is `Some g`) and `g args w` is true iff `v<ta> args` can be evaluated to `w`.
  If `ta` is an invalid set of type arguments, then `f ta` is `None`.

### `interp_value`, `interp_skel` and `apply`

Since `Skel` is non-deterministic, values and skeletons are interpreted as
relations. So for instance, `interp_skel e s v` is true iff the skeleton `s` can
be evaluated to `v` in the environment `e`.

The three interpretation functions are mutually recursive. To reason on them,
one can use the induction functions defined in [files/Induction.v](files/Induction.v).


