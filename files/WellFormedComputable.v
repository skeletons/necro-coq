(** This file provides the definition of a well-formed semantics, in a
   computable way. It is proven equivalent to WellFormed.v in the file EqWF.v *)

Require Import String List.
Import ListNotations.

(** See https://gitlab.inria.fr/skeletons/necro-coq/-/tree/master/files *)
From Necro Require Import Dict Skeleton WellFormed.

Definition option_bind {A B} (x:option A) (f:A -> option B) :=
  match x with
  | None => None
  | Some x => f x
  end.
Notation "x ← y ; z" := (option_bind y (fun x => z)) (at level 49, right associativity).

Section WellFormed.
  (** We suppose a skeletal semantics given *)
  Variable ss: skeletal_semantics.

  Definition mem {A} eq (x:A) (l: list A) :=
    List.existsb (eq x) l.

  Fixpoint no_overlap {A} (eq: A -> A -> bool)
    (l1 l2: list A): bool :=
    match l1 with
    | [] => true
    | a :: q => negb (mem eq a l2) && no_overlap eq q l2
    end.

  Fixpoint no_overlap_list {A} (eq: A -> A -> bool)
    (l: list (list A)): bool :=
    match l with
    | [] => true
    | a :: q =>
        no_overlap eq a (List.concat q)
        && no_overlap_list eq q
    end.

  Fixpoint incl_list {A} (eq: A -> A -> bool) (l1 l2:list A): bool :=
    match l1 with
    | [] => true
    | a1 :: q1 => mem eq a1 l2 && incl_list eq q1 l2
    end.

  Definition same_list {A} eq (l1 l2:list A) : bool :=
    incl_list eq l1 l2 && incl_list eq l2 l1.

  Open Scope bool.
  Fixpoint eq_type (τ1 τ2: type): bool :=
    let eq_type_list := (fix reccall τl1 τl2 :=
      match τl1, τl2 with
      | [], [] => true
      | τ1 :: τq1, τ2 :: τq2 => eq_type τ1 τ2 && reccall τq1 τq2
      | _, _ => false
      end)
    in
    match τ1, τ2 with
    | Var x1, Var x2 => String.eqb x1 x2
    | Base n1 ta1, Base n2 ta2 => String.eqb n1 n2 && eq_type_list ta1 ta2
    | Prod τl1, Prod τl2 => eq_type_list τl1 τl2
    | Arrow ti1 to1, Arrow ti2 to2 => eq_type ti1 ti2 && eq_type to1 to2
    | _, _ => false
    end.

  Fixpoint type_pattern (p:pattern): option type :=
    let type_pattern_list :=
      (fix list (pl: list pattern) :=
        match pl with
        | [] => Some []
        | p :: pq =>
            τ ← type_pattern p ;
            τq ← list pq ;
            Some (τ :: τq)
        end)
    in
    match p with
    | pattern_var tv ty => Some ty
    | pattern_wildcard ty => Some ty
    | pattern_tuple pl =>
        τl ← type_pattern_list pl ;
          if no_overlap_list eqb (map pattern_variables pl)
          then Some (Prod τl)
          else None
    | pattern_constr c tyl p =>
        csig ← find c (s_ctype ss) ;
        let '(c_ta, c_in, c_out) := csig in
        τ ← type_pattern p ;
        if (eq_type τ (subst' c_in c_ta tyl) &&
           Nat.eqb (length tyl) (length c_ta))%bool
        then Some (Base c_out tyl)
        else None
    | pattern_rec [] => None
    | pattern_rec ((f, τl, p) :: pl) =>
        let type_record f_out_fst τl_fst := (fix type_rec pl :=
          match pl with
          | [] => true
          | (f, τl, p) :: q =>
              match find f (s_ftype ss) with
              | Some (f_ta, f_out, f_in) =>
                  eqb f_out f_out_fst &&
                  eq_type (Prod τl) (Prod τl_fst) &&
                  match type_pattern p with
                  | Some τ =>
                      eq_type τ (subst' f_in f_ta τl) &&
                      Nat.eqb (length τl) (length f_ta)
                  | None => false
                  end
              | None => false
              end && type_rec q
          end)
        in
        match find f (s_ftype ss) with
        | Some (f_ta, f_out, f_in) =>
            τ ← type_pattern p ;
            if (eq_type τ (subst' f_in f_ta τl) &&
               Nat.eqb (length τl) (length f_ta))%bool
            then
              if type_record f_out τl pl &&
                 no_overlap_list eqb (map (fun (x:string * list type * pattern) =>
                 let (_, p):= x in pattern_variables p) ((f,τl,p) ::pl))
              then Some (Base f_out τl)
              else None
            else None
        | None => None
        end
    | pattern_or p1 p2 =>
        τ1 ← type_pattern p1 ;
        τ2 ← type_pattern p2 ;
        if eq_type τ1 τ2 &&
        same_list (fun '(x, τ) '(x', τ') => eqb x x' && eq_type τ τ')
          (pattern_variables_with_type p1)
          (pattern_variables_with_type p2)
        then Some τ1 else None
    end.

  Fixpoint ok_fields {A B} f_out (tl:list (string * A * B)) fields :=
    match fields with
    | [] => true
    | f :: q =>
        match find f (s_ftype ss) with
        | Some (_, f_out', _) =>
          negb (String.eqb f_out' f_out) ||
          mem eqb f (map (fun y => match y with (f, _, _) => f end) tl)
        | None => true
        end && ok_fields f_out tl q
    end.

  Fixpoint type_term (e:typing_env) (t:term) {struct t}: option type:=
    let type_record := (fix rec e f_out τl tl {struct tl} :=
    match tl with
    | [] => true
    | (f, τl', t) :: tq =>
        match type_term e t with
        | None => false
        | Some τt =>
            match find f (s_ftype ss) with
            | None => false
            | Some (f_ta, f_out', f_in) =>
              Nat.eqb (length f_ta) (length τl) &&
              eq_type (Prod τl) (Prod τl') &&
              eqb f_out f_out' &&
              eq_type τt (subst' f_in f_ta τl)
            end
        end && rec e f_out τl tq
    end)
    in
    match t with
    | term_var (TVLet x τ) =>
        τ' ← findEnv x e ;
        if eq_type τ τ' then Some τ' else None
    | term_var (TVUnspec x τ args) =>
        tsig ← find x (s_unspec_term_decl ss) ;
        let (ta, ty) := tsig in
        if Nat.eqb (length ta) (length args) &&
           eq_type τ (subst' ty ta args)
        then Some τ else None
    | term_var (TVSpec x τ args) =>
        tsig ← find x (s_spec_term_decl ss) ;
        let '(ta, ty, _) := tsig in
        if Nat.eqb (length ta) (length args) &&
           eq_type τ (subst' ty ta args)
        then Some τ else None
    | term_constructor c τl t =>
        csig ← find c (s_ctype ss) ;
        let '(c_ta, c_in, c_out) := csig in
        τt ← type_term e t ;
        if eq_type τt (subst' c_in c_ta τl) &&
           Nat.eqb (length c_ta) (length τl)
        then Some (Base c_out τl)
        else None
    | term_field t τl f =>
        fsig ← find f (s_ftype ss) ;
        let '(f_ta, f_out, f_in) := fsig in
        τt ← type_term e t ;
        if eq_type τt (Base f_out τl) &&
           Nat.eqb (length f_ta) (length τl)
        then Some (subst' f_in f_ta τl)
        else None
    | term_nth t τl n =>
        τt ← type_term e t ;
        τ_out ← nth_error τl n ;
        if eq_type τt (Prod τl)
        then Some τ_out
        else None
    | term_rec_make [] => None
    | term_rec_make (((f, τl, τ) :: _) as tl) =>
        fsig ← find f (s_ftype ss) ;
        let '(_, f_out, _) := fsig in
          if type_record e f_out τl tl &&
             ok_fields f_out tl (s_field ss)
          then Some (Base f_out τl)
          else None
    | term_rec_set t tl =>
        match type_term e t with
        | Some (Base f_out τl) =>
            if type_record e f_out τl tl
            then Some (Base f_out τl)
            else None
        | _ => None
        end
    | term_tuple tl =>
        τl ← (fix list tl :=
          match tl with
          | [] => Some []
          | th :: tq =>
              τ ← type_term e th ;
              τl ← list tq ;
              Some (τ :: τl)
          end) tl ;
        Some (Prod τl)
    | term_func p s =>
        τp ← type_pattern p ;
        τs ← type_skel (extEnv p e) s ;
        Some (Arrow τp τs)
    end

  with type_skel (e:typing_env) (s:skeleton) {struct s}: option type:=
    match s with
    | skel_branch τ sl =>
        if (fix branch_ok sl :=
          match sl with
          | [] => true
          | sh :: sq =>
              match type_skel e sh with
              | Some τ' => eq_type τ τ'
              | None => false
              end && branch_ok sq
          end) sl
        then Some τ else None
    | skel_return t =>
        type_term e t
    | skel_letin p s1 s2 =>
        τ1 ← type_skel e s1 ;
        τp ← type_pattern p ;
        τ2 ← type_skel (extEnv p e) s2 ;
        if eq_type τ1 τp
        then Some τ2
        else None
    | skel_exists τ => Some τ
    | skel_match t τs psl =>
        let check_match τ_term τ_match :=
          (fix rec psl :=
            match psl with
            | [] => true
            | (p, s) :: psq =>
                match type_pattern p, type_skel (extEnv p e) s with
                | Some τp, Some τs =>
                    eq_type τp τ_term && eq_type τs τ_match
                | _, _ => false
                end && rec psq
            end)
        in
        τt ← type_term e t ;
        if check_match τt τs psl then Some τs else None
    | skel_apply f a =>
        τf ← type_term e f ;
        τa ← type_term e a ;
        match τf with
        | Arrow τi τo => if eq_type τi τa then Some τo else None
        | _ => None
        end
    end.

  Definition is_well_formed_ctype: bool :=
    same_list eqb (keys (s_ctype ss)) (s_constructor ss) &&
    List.forallb (fun c =>
      match Dict.find c (s_ctype ss) with
      | Some (c_ta, c_in, c_out) =>
          incl_list eqb (type_variables c_in) c_ta
      | None => false
      end) (keys (s_ctype ss)) &&
      forallb (fun c =>
        forallb (fun c' =>
          match find c (s_ctype ss), find c' (s_ctype ss) with
          | Some (c_ta, c_ty, ty),  Some (c'_ta, c'_ty, ty') =>
              if eqb ty ty'
              then Nat.eqb (length c_ta) (length c'_ta)
              else true
          | _, _ => false
          end
        ) (keys (s_ctype ss))
      ) (keys (s_ctype ss)).


  Definition is_well_formed_ftype: bool :=
    same_list eqb (keys (s_ftype ss)) (s_field ss) &&
    List.forallb (fun c =>
      match Dict.find c (s_ftype ss) with
      | Some (f_ta, f_out, f_in) =>
          incl_list eqb (type_variables f_in) f_ta
      | None => false
      end) (keys (s_ftype ss)) &&
      forallb (fun f =>
        forallb (fun f' =>
          match find f (s_ftype ss), find f' (s_ftype ss) with
          | Some (f_ta, ty, f_ty),  Some (f'_ta, ty', f'_ty) =>
              if eqb ty ty'
              then Nat.eqb (length f_ta) (length f'_ta)
              else true
          | _, _ => false
          end
        ) (keys (s_ftype ss))
      ) (keys (s_ftype ss)).

  Definition is_well_formed_unspec_terms: bool :=
    same_list eqb (keys (s_unspec_term_decl ss)) (s_unspec_term ss) &&
    forallb (fun x =>
      match find x (s_unspec_term_decl ss) with
      | Some (ta, ty) => incl_list eqb (type_variables ty) ta
      | None => false
      end) (keys (s_unspec_term_decl ss)).

  Definition is_well_formed_spec_terms: bool :=
    same_list eqb (keys (s_spec_term_decl ss)) (s_spec_term ss) &&
    forallb (fun x =>
      match find x (s_spec_term_decl ss) with
      | Some (ta, ty, t) =>
          incl_list eqb (type_variables ty) ta &&
          match type_term [] t with
          | Some ty' => eq_type ty ty'
          | None => false
          end
      | None => false
      end) (keys (s_spec_term_decl ss)).

  Definition is_well_formed_semantics: bool :=
    is_well_formed_ctype &&
    is_well_formed_ftype &&
    is_well_formed_unspec_terms &&
    is_well_formed_spec_terms.

End WellFormed.


