(** This file provides the necessary definitions to represent the deep embedding
    of a Skel file in coq. The tool necrocoq
    (https://gitlab.inria.fr/skeletons/necro-coq/) automatically generates a
    file that provides an instantiation of the `skeletal_semantics` type. *)

Require Import String List.
From Necro Require Import Dict.
Import ListNotations DictNotations.
(* We unset elimination schemes because they don't handle lists well.
   We define our own recursors in the section Recursors. *)
Unset Elimination Schemes.


Section Skeleton.
  (** Representation of necro types *)
  Inductive type :=
  | Var: string -> type
  | Base :  string -> list type -> type
  | Prod : list type -> type
  | Arrow : type -> type -> type.

  (** Typed variables *)
  Inductive typed_var: Type :=
  | TVLet: string -> type -> typed_var
  | TVUnspec: string -> type -> list type -> typed_var
  | TVSpec: string -> type -> list type -> typed_var.

  (** Main AST definitions *)
  Inductive pattern: Type :=
  | pattern_var: string -> type -> pattern
  | pattern_wildcard: type -> pattern
  | pattern_tuple: list pattern -> pattern
  | pattern_constr: string -> list type -> pattern -> pattern
  | pattern_rec: list (string * list type * pattern) -> pattern
  | pattern_or: pattern -> pattern -> pattern.
  Inductive term: Type :=
  | term_constructor : string -> list type -> term -> term
  | term_var: typed_var -> term
  | term_tuple: list term -> term
  | term_func: pattern -> skeleton -> term
  | term_field: term -> list type -> string -> term
  | term_nth: term -> list type -> nat -> term
  | term_rec_make: list (string * list type * term) -> term
  | term_rec_set: term -> list (string * list type * term) -> term
  with skeleton: Type :=
  | skel_branch : type -> list skeleton -> skeleton
  (* In [skel_match t τ [(p1, s1); … (pn, sn)]], τ is the type of the skeletons *)
  | skel_match : term -> type -> list (pattern * skeleton) -> skeleton
  | skel_return : term -> skeleton
  | skel_apply : term -> term -> skeleton
  | skel_letin : pattern -> skeleton -> skeleton -> skeleton
  | skel_exists : type -> skeleton.
End Skeleton.


(** A record type to sum up the definition of a skeletal semantics *)
Record skeletal_semantics :=
  mk_sem {
    (* Base types: specified and non-specified types *)
    s_base_type: list string;
    (* constructors *)
    s_constructor: list string;
    (* record fields *)
    s_field: list string;
    (* Term names *)
    s_unspec_term: list string;
    s_spec_term: list string;
    (* Signature of constructors *)
    s_ctype: dict (list string * type * string) ;
    (* Signature of fields *)
    s_ftype: dict (list string * string * type) ;
    (* Term declarations *)
    s_unspec_term_decl: dict (list string * type) ;
    s_spec_term_decl: dict (list string * type * term) ;
  }.




Section Recursors.
  (* Preliminary definitions *)
  Inductive Forall_type {A} (P:A->Type) : list A -> Type :=
  | Forall_type_nil: Forall_type P []
  | Forall_type_cons x l: P x -> Forall_type P l -> Forall_type P (x :: l).
  Arguments Forall_type_nil {_} {_}.
  Arguments Forall_type_cons {_} {_} _ _ _ _.


  Definition type_rect (P: type -> Type)
      var base prod arrow: forall ty, P ty :=
    fix aux ty :=
      let auxl :=
        (fix auxl tyl :=
          match tyl return Forall_type P tyl with
          | []     => Forall_type_nil
          | ty :: tyq => Forall_type_cons ty tyq (aux ty) (auxl tyq)
          end)
      in
      match ty with
      | Var s => var s
      | Base b l => base b l (auxl l)
      | Prod p => prod p (auxl p)
      | Arrow t1 t2 =>
        arrow t1 t2 (aux t1) (aux t2)
      end.

  Definition pattern_rect (P: pattern -> Type)
      var wild tup constr rec or: forall p, P p :=
    fix aux p :=
      match p with
      | pattern_var x ty => var x ty
      | pattern_wildcard ty => wild ty
      | pattern_tuple pl => tup pl
          ((fix auxl pl :=
            match pl return Forall_type P pl with
            | [] => Forall_type_nil
            | p :: pq => Forall_type_cons p pq (aux p) (auxl pq)
            end) pl)
      | pattern_constr c ta p => constr c ta p (aux p)
      | pattern_rec pl => rec pl
          ((fix auxl pl :=
            match pl return Forall_type (fun x =>
              match x with (x, y, z) => P z end) pl with
            | [] => Forall_type_nil
            | (f, ta, p) :: pq => Forall_type_cons (f,ta,p) pq (aux p) (auxl pq)
            end) pl)
      | pattern_or p1 p2 => or p1 p2 (aux p1) (aux p2)
      end.

  (* Terms and skeletons are nested, so we have to define our own recursion *)
  Section Rec.

    Variable Pt: term -> Type.
    Variable Ps: skeleton -> Type.
    Definition P't: (string * list type * term) -> Type :=
      fun x =>
      match x with
      | (_, _, t) => Pt t
      end.
    Definition P''s: (pattern * skeleton) -> Type :=
      fun x =>
      match x with
      | (_, s) => Ps s
      end.
    Variable var: forall tv, Pt (term_var tv).
    Variable cons: forall c ta t, Pt t -> Pt (term_constructor c ta t).
    Variable tuple: forall lt, Forall_type Pt lt -> Pt (term_tuple lt).
    Variable func: forall p sk, Ps sk -> Pt (term_func p sk).
    Variable field: forall t ta f, Pt t -> Pt (term_field t ta f).
    Variable nth: forall t ta f, Pt t -> Pt (term_nth t ta f).
    Variable rec_make: forall tl,
      Forall_type P't tl -> Pt (term_rec_make tl).
    Variable rec_set: forall t tl,
      Pt t -> Forall_type P't tl -> Pt (term_rec_set t tl).
    Variable branchings: forall s l, Forall_type Ps l -> Ps (skel_branch s l).
    Variable apply: forall v t, Pt v -> Pt t -> Ps (skel_apply v t).
    Variable returns: forall t, Pt t -> (Ps (skel_return t)).
    Variable matches: forall t ty psl, Pt t -> Forall_type P''s psl -> (Ps (skel_match t ty psl)).
    Variable letin: forall p b s, Ps b -> Ps s -> Ps (skel_letin p b s).
    Variable exist: forall ty, Ps (skel_exists ty).

    Fixpoint term_rect t: Pt t :=
      let listterm_rect :=
        (fix listterm_rect lt: Forall_type Pt lt :=
          match lt with
          | [] => Forall_type_nil
          | t :: lt' => Forall_type_cons _ _ (term_rect t) (listterm_rect lt')
          end) in
      let record_rect :=
        (fix aux lt : Forall_type P't lt :=
        match lt with
        | [] => Forall_type_nil
        | (f, ta, t) :: tl => Forall_type_cons (f,ta,t) tl (term_rect t) (aux tl)
        end) in
      match t with
      | term_var tv => var tv
      | term_constructor c ta t => cons c ta t (term_rect t)
      | term_tuple lt => tuple lt (listterm_rect lt)
      | term_func p sk => func p sk (skel_rect sk)
      | term_field t ta f => field t ta f (term_rect t)
      | term_nth t ta f => nth t ta f (term_rect t)
      | term_rec_make tl =>
          rec_make tl (record_rect tl)
      | term_rec_set t tl =>
          rec_set t tl (term_rect t) (record_rect tl)
      end
    with skel_rect sk: Ps sk :=
      match sk with
      | skel_apply v t => apply v t (term_rect v) (term_rect t)
      | skel_branch s Ss => branchings s Ss
          ((fix list_skeleton_rect Ss :Forall_type Ps Ss :=
          match Ss return Forall_type Ps Ss with
          | [] => Forall_type_nil
          | s :: s_rest => Forall_type_cons _ _ (skel_rect s)
              (list_skeleton_rect s_rest) end) Ss)
      | skel_match t ty psl =>
          let match_rect :=
            (fix aux psl : Forall_type P''s psl :=
            match psl with
            | [] => Forall_type_nil
            | (p, s) :: psl => Forall_type_cons (p,s) psl (skel_rect s) (aux psl)
            end) in
          matches t ty psl (term_rect t) (match_rect psl)
      | skel_return t => returns t (term_rect t)
      | skel_letin p b s => letin p b s (skel_rect b) (skel_rect s)
      | skel_exists ty => exist ty
      end.

    Definition skel_and_term_rect := (fun s => skel_rect s, fun t => term_rect t).
  End Rec.

End Recursors.




