Require Import String List.
From Necro Require Import Dict.
Import ListNotations DictNotations.
Open Scope string_scope.

(** See https://gitlab.inria.fr/skeletons/necro-coq/-/tree/master/files *)
From Necro Require Import Skeleton WellFormed Concrete.

Section ConcreteRec.

  Variable s: skeletal_semantics.

  (* find x unspec_term_interp = Some (n, f) means that [x] expects [n] arguments and is
     interpreted as [f] (see unspec_function). *)
  Variable unspec_term_interp:
    dict (nat * unspec_function).

  (* a generator for existentials. [gen_exists ty v] means that [v] is a valid
     term of type [ty]. *)
  Variable gen_exists:
    type -> cvalue -> Prop.

  Section NextTriple.

    Variable (Ht: env -> term -> cvalue -> Prop).
    Variable (Hs: env -> skeleton -> cvalue -> Prop).
    Variable (Happ: cvalue -> list cvalue -> cvalue -> Prop).

    Inductive next_triple_term: env -> term -> cvalue -> Prop :=
    | irec_var_let: forall e x ty v,
        findEnv x e = Some v ->
        next_triple_term e (term_var (TVLet x ty)) v
    | irec_var_unspec_O: forall e x ty ta v w,
        find x unspec_term_interp = Some (0, v) ->
        v ta [] w ->
        next_triple_term e (term_var (TVUnspec x ty ta)) w
    | irec_var_unspec_S: forall e x ty ta v n,
        find x unspec_term_interp = Some (S n, v) ->
        next_triple_term e (term_var (TVUnspec x ty ta)) (cval_unspec n x ta [])
    | irec_var_spec: forall e t x ty ta tyl ty' v,
        find x (s_spec_term_decl s) = Some (ta, ty, t) ->
        length ta = length tyl ->
        Ht [] (subst_term' t ta tyl) v ->
        next_triple_term e (term_var (TVSpec x ty' tyl)) v
    | irec_constr: forall e arg c ta v,
        Ht e arg v ->
        next_triple_term e (term_constructor c ta arg) (cval_constructor c v)
    | irec_tuple_nil: forall e,
        next_triple_term e (term_tuple []) (cval_tuple [])
    | irec_tuple_cons: forall e t tl v vl,
        Ht e t v ->
        Ht e (term_tuple tl) (cval_tuple vl) ->
        next_triple_term e (term_tuple (t::tl)) (cval_tuple (v::vl))
    | irec_func: forall p sk e,
        next_triple_term e (term_func p sk) (cval_closure p sk e)
    | irec_field: forall e t ta f cl v,
        Ht e t (cval_record cl) ->
        findEnv f cl = Some v ->
        next_triple_term e (term_field t ta f) v
    | irec_nth: forall e t ta n cl v,
        Ht e t (cval_tuple cl) ->
        nth_error cl n = Some v ->
        next_triple_term e (term_nth t ta n) v
    | irec_rec_make_nil: forall e,
        next_triple_term e (term_rec_make []) (cval_record [])
    | irec_rec_make_cons: forall e f ta t tl v vl,
        Ht e t v ->
        Ht e (term_rec_make tl) (cval_record vl) ->
        next_triple_term e (term_rec_make ((f,ta,t)::tl)) (cval_record ((f,v)::vl))
    | irec_rec_set: forall e t tl vl1 vl2,
        Ht e t (cval_record vl1) ->
        Ht e (term_rec_make tl) (cval_record vl2) ->
        next_triple_term e (term_rec_set t tl) (cval_record (vl2++vl1)).


    Inductive next_triple_skel: env -> skeleton -> cvalue -> Prop :=
    | irec_branch: forall ty e v sks ski,
        In ski sks ->
        Hs e ski v ->
        next_triple_skel e (skel_branch ty sks) v
    | irec_match: forall tm ty s psl e e' v w,
        Ht e tm v ->
        get_match e psl v = Some (s, e') ->
        Hs e' s w ->
        next_triple_skel e (skel_match tm ty psl) w
    | irec_return: forall e t v,
        Ht e t v ->
        next_triple_skel e (skel_return t) v
    | irec_apply_clos : forall e f p sk e' e'' a v res,
        Ht e f (cval_closure p sk e') ->
        Ht e a v ->
        add_asn e' p v = Some e'' ->
        Hs e'' sk res ->
        next_triple_skel e (skel_apply f a) res
    | irec_apply_unspec_done: forall e f a v w res m x ta vl,
        Ht e f (cval_unspec 0 x ta vl) ->
        Ht e a v ->
        find x unspec_term_interp = Some (m, w) ->
        w ta (app vl [v]) res ->
        next_triple_skel e (skel_apply f a) res
    | irec_apply_unspec_cont: forall e f a n x ta vl v,
        Ht e f (cval_unspec (S n) x ta vl) ->
        Ht e a v ->
        next_triple_skel e (skel_apply f a) (cval_unspec n x ta (vl ++ [v]))
    | irec_letin: forall e e' p s1 s2 v w,
        Hs e s1 v ->
        add_asn e p v = Some e' ->
        Hs e' s2 w ->
        next_triple_skel e (skel_letin p s1 s2) w
    | irec_exists: forall e ty v,
        gen_exists ty v ->
        next_triple_skel e (skel_exists ty) v.
  End NextTriple.

  Fixpoint iter_n n :=
    match n with
    | 0 => (fun e sk v => False, fun e t v => False)
    | S n =>
        let '(Ht, Hs) := iter_n n in
        ( next_triple_term Ht,
          next_triple_skel Ht Hs)
    end.

  Definition interp_term_n n e t v :=
    let '(Ht, _) := iter_n n in
    Ht e t v.
  Definition interp_skel_n n e s v :=
    let '(_, Hs) := iter_n n in
    Hs e s v.

  Definition interp_term_rec e t v :=
    exists n, interp_term_n n e t v.
  Definition interp_skel_rec e sk v :=
    exists n, interp_skel_n n e sk v.
End ConcreteRec.




Section Destructors.
  Variable s: skeletal_semantics.
  Variable u: dict (nat * unspec_function).
  Variable g: type -> cvalue -> Prop.
  Variable Ht: env -> term -> cvalue -> Prop.
  Variable Hs: env -> skeleton -> cvalue -> Prop.

  (***************************************************************************)
  (*                    Destructors for next_triple_term                     *)
  (***************************************************************************)

  Lemma irec_var_let_destr:
    forall e v x ty,
    next_triple_term s u Ht e (term_var (TVLet x ty)) v ->
    findEnv x e = Some v.
  Proof.
    inversion 1; auto.
  Qed.

  Lemma irec_var_unspec_destr:
    forall e w x ty ta,
    next_triple_term s u Ht e (term_var (TVUnspec x ty ta)) w ->
    match find x u with
    | Some (0, v) => v ta [] w
    | Some (S n, _) => w = cval_unspec n x ta []
    | None => False
    end.
  Proof.
    inversion 1; subst; now rewrite H5.
  Qed.

  Lemma irec_var_spec_destr:
    forall e x ty' tyl v,
    next_triple_term s u Ht e (term_var (TVSpec x ty' tyl)) v ->
    exists ta ty t,
      find x (s_spec_term_decl s) = Some (ta, ty, t) /\
      length ta = length tyl /\
      Ht [] (subst_term' t ta tyl) v.
  Proof.
    inversion 1; subst.
    do 3 eexists; split; [|split]; eassumption.
  Qed.

  Lemma irec_constr_destr:
    forall e c t ta v,
    next_triple_term s u Ht e (term_constructor c ta t) v ->
    exists w,
      v = cval_constructor c w /\
      Ht e t w.
  Proof.
    inversion 1; eexists; repeat split; eauto.
  Qed.

  Lemma irec_tuple_nil_destr:
    forall e v,
    next_triple_term s u Ht e (term_tuple []) v ->
      v = cval_tuple [].
  Proof.
    inversion 1; auto.
  Qed.

  Lemma irec_tuple_cons_destr:
    forall e t tl w,
        next_triple_term s u Ht e (term_tuple (t::tl)) w ->
        exists v vl, w = cval_tuple (v :: vl) /\
        Ht e t v /\
        Ht e (term_tuple tl) (cval_tuple vl).
  Proof.
    inversion 1; eexists; eexists; repeat split; eauto.
  Qed.

  Lemma irec_func_destr:
    forall e p sk w,
        next_triple_term s u Ht e (term_func p sk) w ->
        w = cval_closure p sk e.
  Proof.
    now inversion 1.
  Qed.

  Lemma irec_field_destr:
    forall e t ta f w,
        next_triple_term s u Ht e (term_field t ta f) w ->
        exists cl,
          Ht e t (cval_record cl) /\
          findEnv f cl = Some w.
  Proof.
    inversion 1; subst.
    eexists; split; eassumption.
  Qed.

  Lemma irec_nth_destr:
    forall e t ta n w,
        next_triple_term s u Ht e (term_nth t ta n) w ->
        exists cl,
          Ht e t (cval_tuple cl) /\
          nth_error cl n = Some w.
  Proof.
    inversion 1; subst.
    eexists; split; eassumption.
  Qed.

  Lemma irec_rec_make_nil_destr:
    forall e v,
    next_triple_term s u Ht e (term_rec_make []) v ->
    v = cval_record [].
  Proof.
    now inversion 1.
  Qed.

  Lemma irec_rec_make_cons_destr:
    forall e f ta t tl w,
    next_triple_term s u Ht e (term_rec_make ((f, ta, t) :: tl)) w ->
    exists v vl,
      Ht e t v /\
      Ht e (term_rec_make tl) (cval_record vl) /\
    w = cval_record ((f,v)::vl).
    Proof.
      inversion 1; subst.
      do 2 eexists.
      split; [eassumption|].
      split; [eassumption|].
      reflexivity.
    Qed.

  Lemma irec_rec_set_destr:
    forall e t tl w,
      next_triple_term s u Ht e (term_rec_set t tl) w ->
      exists vl1 vl2,
        w = cval_record (vl2 ++ vl1) /\
        Ht e t (cval_record vl1) /\
        Ht e (term_rec_make tl) (cval_record vl2).
  Proof.
    inversion 1; subst.
    do 2 eexists.
    split; [reflexivity|].
    split; assumption.
  Qed.

  (***************************************************************************)
  (*                     Destructors for next_triple_skel                    *)
  (***************************************************************************)

  Lemma irec_branch_destr:
    forall e ty l v,
    next_triple_skel u g Ht Hs e (skel_branch ty l) v ->
    exists sk,
      In sk l /\ Hs e sk v.
  Proof.
    inversion 1; eauto.
  Qed.

  Lemma irec_match_destr:
    forall e tm ty psl w,
    next_triple_skel u g Ht Hs e (skel_match tm ty psl) w ->
    exists v sk e',
      Ht e tm v /\
      get_match e psl v = Some (sk, e') /\
      Hs e' sk w.
  Proof.
    inversion 1; subst.
    do 3 eexists.
    split; [|split]; eassumption.
  Qed.

  Lemma irec_return_destr:
    forall e t v,
    next_triple_skel u g Ht Hs e (skel_return t) v ->
    Ht e t v.
  Proof.
    inversion 1; eauto.
  Qed.

  Lemma irec_apply_destr:
    forall e f a res,
    next_triple_skel u g Ht Hs e (skel_apply f a) res ->
    ( exists p sk e' v e'',
      Ht e f (cval_closure p sk e') /\
        Ht e a v /\
        add_asn e' p v = Some e'' /\
        Hs e'' sk res) \/
    ( exists v w m x ta vl,
      Ht e f (cval_unspec 0 x ta vl) /\
        Ht e a v /\
        find x u = Some (m, w) /\
        w ta (app vl [v]) res) \/
    ( exists n x ta vl v,
      Ht e f (cval_unspec (S n) x ta vl) /\
        Ht e a v /\
        res = cval_unspec n x ta (vl ++ [v])).
  Proof.
    inversion 1; subst; [left|right;left|right;right].
    + do 5 eexists.
    split; [|split]; [..|split]; eassumption.
    + do 6 eexists.
    split; [|split]; [..|split]; eassumption.
    + do 5 eexists.
    split; [|split]; [eassumption..|]; reflexivity.
  Qed.

  Lemma irec_letin_destr:
    forall e p s1 s2 w,
      next_triple_skel u g Ht Hs e (skel_letin p s1 s2) w ->
      exists v e',
        Hs e s1 v /\
        add_asn e p v = Some e' /\
        Hs e' s2 w.
  Proof.
    inversion 1; subst;
    repeat eexists; repeat split; eauto.
  Qed.

  Lemma irec_exists_destr:
    forall e ty w,
      next_triple_skel u g Ht Hs e (skel_exists ty) w ->
        g ty w.
  Proof.
    inversion 1; subst; auto.
  Qed.

End Destructors.







(*****************************************************************************)
(*                                  Tactics                                  *)
(*****************************************************************************)


Ltac iRVarLet H :=
  match type of H with
  | next_triple_term _ _ _ ?e (term_var (TVLet ?s _)) _ =>
      match eval compute in (findEnv s e) with
      | Some ?v => apply irec_var_let_destr in H;
          first [injection H as <-|injection H as ->|injection H as H]
      | _ => apply irec_var_let_destr in H
      end
  end.

Ltac iRVarSpec H :=
  match type of H with
  | next_triple_term _ _ _ _ (term_var (TVSpec _ _ _)) _ =>
      apply irec_var_spec_destr in H;
      let eq := fresh in
      destruct H as [?ta [?ty [?t [eq [_ H]]]]];
      injection eq as <- <- <-
  end.

Ltac iRConstr H :=
  match type of H with
  | next_triple_term _ _ _ ?e (term_constructor _ _ _) _ =>
      apply irec_constr_destr in H;
      destruct H as [? [-> H]]
  end.

Ltac iRTupleNil H :=
  match type of H with
  | next_triple_term _ _ _ ?e (term_tuple []) _ =>
      apply irec_tuple_nil_destr in H;
      first [
        discriminate H
      | injection H as ->
      | subst
        ]
  end.

Ltac iRTupleCons H hd :=
  match type of H with
  | next_triple_term _ _ _ ?e (term_tuple (_::_)) _ =>
      apply irec_tuple_cons_destr in H;
      destruct H as [? [? [-> [hd H]]]]
  | next_triple_term _ _ _ ?e (term_tuple (_::_)) _ =>
      apply irec_tuple_cons_destr in H;
      let eq := fresh in
      destruct H as [? [? [eq [hd H]]]] ;
      injection eq as ->
  end.

Ltac iRFunc H :=
  match type of H with
  | next_triple_term _ _ _ _ (term_func _ _) _ =>
      inversion H; subst; clear H
  end.

Ltac iRBranch H :=
  match type of H with
  | next_triple_skel _ _ _ _ ?e (skel_branch _ _) _ =>
      let Hin:=fresh in let sk := fresh in
      apply irec_branch_destr in H; destruct H as [sk [Hin H]];
      repeat destruct Hin as [<-|Hin]; [..|destruct Hin]
  end.

Ltac iRRet H :=
  match type of H with
  | next_triple_skel _ _ _ _ ?e (skel_return _) _ =>
      apply irec_return_destr in H
  end.

Ltac iRApply H Hterm Hall :=
  match type of H with
  | next_triple_skel _ _ _ _ ?e (skel_apply _ _) _ =>
      apply irec_apply_destr in H
  end.

Ltac iRLetIn H addasn bound :=
  match type of H with
  | next_triple_skel _ _ _ _ ?e (skel_letin _ _ _) _ =>
      let Hin:=fresh in let sk := fresh in
      apply irec_letin_destr in H;
      destruct H as [? [? [bound [addasn H]]]]
  end.

Ltac iRMatch H Ht getmatch:=
  match type of H with
  | next_triple_skel _ _ _ _?e (skel_match _ _ _) _ =>
      apply irec_match_destr in H;
      destruct H as [?v [?sk [?e' [Ht [getmatch H]]]]]
  end.

