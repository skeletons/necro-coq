Require Import String List.
From Necro Require Import Dict.
Import ListNotations DictNotations.
Open Scope string_scope.

(** See https://gitlab.inria.fr/skeletons/necro-coq/-/tree/master/files *)
From Necro Require Import Skeleton.
From Necro Require Export WellFormed.

Section Concrete.

  Variable s: skeletal_semantics.

  Inductive cvalue : Type :=
  (* for unspecified terms, use any term of any type *)
  | cval_base : forall A, A -> cvalue
  | cval_constructor : string -> cvalue -> cvalue
  | cval_tuple: list cvalue -> cvalue
  (* [cval_closure p sk e] is the closure for [λ p → sk] in the environment [e] *)
  | cval_closure: pattern -> skeleton -> list (string * cvalue) -> cvalue
  | cval_record: list (string * cvalue) -> cvalue
  (* cval_unspec n x ta vl means it is the unspecified term x with type
     arguments ta, applied to arguments vl, and there are still (S n) arguments
     missing. *)
  | cval_unspec: nat -> string -> list type -> list cvalue -> cvalue.

  (* Environment is a list of associations. As there is no way to remove a term
     from an environment, it is handled like a stack *)
  Definition env := list (string * cvalue).

  (* [f ta vl w] means that [f] applied to type arguments [ta] and to terms
     [vl] may yield the result [w]. *)
  Definition unspec_function: Type :=
    list type -> list cvalue -> cvalue -> Prop.

  (* unspec_term_interp x = (n, f) means that [x] expects [n] arguments and is
     interpreted as [f] (see unspec_function). *)
  Variable unspec_term_interp:
    dict (nat * unspec_function).

  (* a generator for existentials. [gen_exists ty v] means that [v] is a valid
     term of type [ty]. *)
  Variable gen_exists:
    type -> cvalue -> Prop.

  Fixpoint add_asn (e:env) (p:pattern) (v:cvalue): option env :=
    match p, v with
    | pattern_wildcard _, _ =>
        Some e
    | pattern_var x _, v =>
        Some ((x, v) :: e)
    | pattern_constr c _ p, cval_constructor c' v =>
        if eqb c c'
        then add_asn e p v
        else None
    | pattern_tuple pl, cval_tuple vl =>
        (fix add_asnlist e pl vl :=
        match pl, vl with
        | [], [] => Some e
        | p::pl, v::vl =>
            match add_asn e p v with
            | None => None
            | Some e => add_asnlist e pl vl
            end
        | _, _ => None
        end) e pl vl
    | pattern_rec pl, cval_record cl =>
        (fix add_asnrec e pl :=
          match pl with
          | [] => Some e
          | (f, _, p) :: pl =>
              match findEnv f cl with
              | None => None
              | Some c =>
                  match add_asn e p c with
                  | None => None
                  | Some e => add_asnrec e pl
                  end
              end
          end) e pl
    | pattern_or p1 p2, v =>
        match add_asn e p1 v with
        | Some e' => Some e'
        | None => add_asn e p2 v
        end
    | _, _ => None
    end.

  Fixpoint get_match {A} (e:env) (p:list (pattern * A)) (v:cvalue):
    option (A * env) :=
    match p with
    | [] => None
    | (p1,sk) :: pq =>
        match add_asn e p1 v with
        | None => get_match e pq v
        | Some e => Some (sk, e)
        end
    end.

  Inductive interp_term: env -> term -> cvalue -> Prop :=
  | i_var_let: forall e x ty v,
      findEnv x e = Some v ->
      interp_term e (term_var (TVLet x ty)) v
  | i_var_unspec_O: forall e x ty ta v w,
      find x unspec_term_interp = Some (0, v) ->
      v ta [] w ->
      interp_term e (term_var (TVUnspec x ty ta)) w
  | i_var_unspec_S: forall e x ty ta v n,
      find x unspec_term_interp = Some (S n, v) ->
      interp_term e (term_var (TVUnspec x ty ta)) (cval_unspec n x ta [])
  | i_var_spec: forall e t x ty ta tyl ty' v,
      find x (s_spec_term_decl s) = Some (ta, ty, t) ->
      length ta = length tyl ->
      interp_term [] (subst_term' t ta tyl) v ->
      interp_term e (term_var (TVSpec x ty' tyl)) v
  | i_constr: forall e arg c ta v,
      interp_term e arg v ->
      interp_term e (term_constructor c ta arg) (cval_constructor c v)
  | i_tuple_nil: forall e,
      interp_term e (term_tuple []) (cval_tuple [])
  | i_tuple_cons: forall e t tl v vl,
      interp_term e t v ->
      interp_term e (term_tuple tl) (cval_tuple vl) ->
      interp_term e (term_tuple (t::tl)) (cval_tuple (v::vl))
  | i_func: forall p sk e,
      interp_term e (term_func p sk) (cval_closure p sk e)
  | i_field: forall e t ta f cl v,
      interp_term e t (cval_record cl) ->
      findEnv f cl = Some v ->
      interp_term e (term_field t ta f) v
  | i_nth: forall e t ta n cl v,
      interp_term e t (cval_tuple cl) ->
      nth_error cl n = Some v ->
      interp_term e (term_nth t ta n) v
  | i_rec_make_nil: forall e,
      interp_term e (term_rec_make []) (cval_record [])
  | i_rec_make_cons: forall e f ta t tl v vl,
      interp_term e t v ->
      interp_term e (term_rec_make tl) (cval_record vl) ->
      interp_term e (term_rec_make ((f,ta,t)::tl)) (cval_record ((f,v)::vl))
  | i_rec_set: forall e t tl vl1 vl2,
      interp_term e t (cval_record vl1) ->
      interp_term e (term_rec_make tl) (cval_record vl2) ->
      interp_term e (term_rec_set t tl) (cval_record (vl2++vl1)).

  Inductive interp_skel: env -> skeleton -> cvalue -> Prop :=
  | i_branch: forall ty e v sks ski,
      In ski sks ->
      interp_skel e ski v ->
      interp_skel e (skel_branch ty sks) v
  | i_match: forall tm ty s psl e e' v w,
      interp_term e tm v ->
      get_match e psl v = Some (s, e') ->
      interp_skel e' s w ->
      interp_skel e (skel_match tm ty psl) w
  | i_return: forall e t v,
      interp_term e t v ->
      interp_skel e (skel_return t) v
  | i_apply_clos : forall e f p sk e' e'' a v res,
      interp_term e f (cval_closure p sk e') ->
      interp_term e a v ->
      add_asn e' p v = Some e'' ->
      interp_skel e'' sk res ->
      interp_skel e (skel_apply f a) res
  | i_apply_unspec_done: forall e f a v w res m x ta vl,
      interp_term e f (cval_unspec 0 x ta vl) ->
      interp_term e a v ->
      find x unspec_term_interp = Some (m, w) ->
      w ta (app vl [v]) res ->
      interp_skel e (skel_apply f a) res
  | i_apply_unspec_cont: forall e f a n x ta vl v,
      interp_term e f (cval_unspec (S n) x ta vl) ->
      interp_term e a v ->
      interp_skel e (skel_apply f a) (cval_unspec n x ta (vl ++ [v]))
  | i_letin: forall e e' p s1 s2 v w,
      interp_skel e s1 v ->
      add_asn e p v = Some e' ->
      interp_skel e' s2 w ->
      interp_skel e (skel_letin p s1 s2) w
  | i_exists: forall e ty v,
      gen_exists ty v ->
      interp_skel e (skel_exists ty) v.
End Concrete.












Section Destructors.
  Variable s: skeletal_semantics.
  Variable u: dict (nat * unspec_function).
  Variable g: type -> cvalue -> Prop.


  (***************************************************************************)
  (*                         Destructors for add_asn                         *)
  (***************************************************************************)

  Lemma add_asn_wildcard_destr:
    forall e ty e' v,
      add_asn e (pattern_wildcard ty) v = Some e' ->
      e' = e.
  Proof.
    inversion 1; eauto.
  Qed.

  Lemma add_asn_var_destr:
    forall x ty v e e',
      add_asn e (pattern_var x ty) v = Some e' ->
      e' = ((x, v) :: e).
  Proof.
    inversion 1; eauto.
  Qed.

  Lemma add_asn_constr_destr:
    forall e c p w f ta,
    add_asn e (pattern_constr c ta p) w = Some f ->
    exists v,
      w = cval_constructor c v /\ add_asn e p v = Some f.
  Proof.
    inversion 1. destruct w; try discriminate H1.
    destruct (c=?s0) eqn:eq; [|discriminate H1].
    apply eqb_eq in eq. subst.
    eexists; split; eauto.
  Qed.

  Lemma add_asn_tuple_nil_destr:
    forall e v f,
    add_asn e (pattern_tuple []) v = Some f ->
    v = cval_tuple [] /\ e = f.
  Proof.
    inversion 1. destruct v; try discriminate H1.
    destruct l; try discriminate H1.
    injection H1 as ->. eauto. 
  Qed.

  Lemma add_asn_tuple_cons_destr:
    forall e w g p pl,
    add_asn e (pattern_tuple (p::pl)) w = Some g ->
    exists f v vl,
    w = cval_tuple (v :: vl) /\
    add_asn e p v = Some f /\
    add_asn f (pattern_tuple pl) (cval_tuple vl) = Some g.
  Proof.
    inversion 1. destruct w; try discriminate H1.
    destruct l; try discriminate H1.
    destruct add_asn eqn:aa in H1; [|discriminate H1].
    exists e0, c, l.
    split; [reflexivity|].
    split; [assumption|].
    rewrite aa. reflexivity.
  Qed.

  Lemma add_asn_rec_nil_destr:
    forall e v f,
    add_asn e (pattern_rec []) v = Some f ->
    exists vl, v = cval_record vl /\ e = f.
  Proof.
    inversion 1. destruct v; try discriminate H1.
    injection H1 as ->. eexists; split; auto.
  Qed.

  Lemma add_asn_rec_cons_destr:
    forall e w f ta g p pl,
    add_asn e (pattern_rec ((f,ta,p)::pl)) w = Some g ->
    exists e' v vl,
    w = cval_record vl /\
    findEnv f vl = Some v /\
    add_asn e p v = Some e' /\
    add_asn e' (pattern_rec pl) (cval_record vl) = Some g.
  Proof.
    inversion 1. destruct w; try discriminate H1.
    destruct (findEnv f l) as [v|] eqn:fE; [|discriminate H1].
    destruct (add_asn e p v) as [e'|] eqn:epc; [|discriminate H1].
    exists e', v, l.
    split; [reflexivity|].
    split; [assumption|].
    split; [assumption|].
    reflexivity.
  Qed.


  (***************************************************************************)
  (*                         Destructor for get_match                        *)
  (***************************************************************************)

  Lemma get_match_destr:
    forall {A} e e' p pl v (a a':A),
      get_match e ((p,a)::pl) v = Some (a', e') <->
        (add_asn e p v = Some e' /\ a = a') \/
        (add_asn e p v = None /\ get_match e pl v = Some (a', e')).
  Proof.
    intros ? e e' p pl v a a'.
    split.
    + inversion 1.
      destruct (add_asn e p v) as [epv|] eqn:Hepv in *; [left|right].
      - injection H1 as -> ->. split; reflexivity.
      - split; reflexivity.
    + intros [[B1 <-]|[B1 B2]].
      - simpl. rewrite B1. reflexivity.
      - simpl. rewrite B1. exact B2.
  Qed.


  (* Only useful if patterns are disjoint *)
  Lemma get_match_destr':
    forall {A} e e' pl v (a:A),
      get_match e pl v = Some (a, e') ->
      exists p,
        In (p, a) pl /\
        add_asn e p v = Some e'.
  Proof.
    intros A e e' pl v a.
    induction pl as [|[p b] pl IH]; [discriminate 1|].
    simpl.
    destruct (add_asn e p v) as [epv|] eqn:Hepv.
    - injection 1 as -> ->. exists p. split; [now left|assumption].
    - intro get. specialize(IH get).
      destruct IH as [p' [Hp' add]].
      exists p'. split; [right|]; assumption.
  Qed.


  (***************************************************************************)
  (*                       Destructors for interp_term                       *)
  (***************************************************************************)

  Lemma i_var_let_destr:
    forall e v x ty,
    interp_term s u e (term_var (TVLet x ty)) v ->
    findEnv x e = Some v.
  Proof.
    inversion 1; auto.
  Qed.

  Lemma i_var_unspec_destr:
    forall e w x ty ta,
    interp_term s u e (term_var (TVUnspec x ty ta)) w ->
    match find x u with
    | Some (0, v) => v ta [] w
    | Some (S n, _) => w = cval_unspec n x ta []
    | None => False
    end.
  Proof.
    inversion 1; subst; now rewrite H5.
  Qed.

  Lemma i_var_spec_destr:
    forall e x ty' tyl v,
    interp_term s u e (term_var (TVSpec x ty' tyl)) v ->
    exists ta ty t,
      find x (s_spec_term_decl s) = Some (ta, ty, t) /\
      length ta = length tyl /\
      interp_term s u [] (subst_term' t ta tyl) v.
  Proof.
    inversion 1; subst.
    do 3 eexists; split; [|split]; eassumption.
  Qed.

  Lemma i_constr_destr:
    forall e c t ta v,
    interp_term s u e (term_constructor c ta t) v ->
    exists w,
      v = cval_constructor c w /\
      interp_term s u e t w.
  Proof.
    inversion 1; eexists; repeat split; eauto.
  Qed.

  Lemma i_tuple_nil_destr:
    forall e v,
    interp_term s u e (term_tuple []) v ->
      v = cval_tuple [].
  Proof.
    inversion 1; auto.
  Qed.

  Lemma i_tuple_cons_destr:
    forall e t tl w,
        interp_term s u e (term_tuple (t::tl)) w ->
        exists v vl, w = cval_tuple (v :: vl) /\
        interp_term s u e t v /\
        interp_term s u e (term_tuple tl) (cval_tuple vl).
  Proof.
    inversion 1; eexists; eexists; repeat split; eauto.
  Qed.

  Lemma i_tuple_destr:
    forall e tl w,
        interp_term s u e (term_tuple tl) w ->
        exists vl, w = cval_tuple vl /\
        Forall2 (interp_term s u e) tl vl.
  Proof.
    intro e; induction tl; intros w A.
    - apply i_tuple_nil_destr in A.
      exists []. split; [assumption|constructor].
    - apply i_tuple_cons_destr in A.
      destruct A as [v [vl [-> [ithd ittl]]]].
      specialize (IHtl (cval_tuple vl) ittl).
      destruct IHtl as [vl' [eq italltl]]; injection eq as <-.
      exists (v::vl).
      split; [reflexivity|].
      constructor; assumption.
  Qed.

  Lemma i_func_destr:
    forall e p sk w,
        interp_term s u e (term_func p sk) w ->
        w = cval_closure p sk e.
  Proof.
    now inversion 1.
  Qed.

  Lemma i_field_destr:
    forall e t ta f w,
        interp_term s u e (term_field t ta f) w ->
        exists cl,
          interp_term s u e t (cval_record cl) /\
          findEnv f cl = Some w.
  Proof.
    inversion 1; subst.
    eexists; split; eassumption.
  Qed.

  Lemma i_nth_destr:
    forall e t ta n w,
        interp_term s u e (term_nth t ta n) w ->
        exists cl,
          interp_term s u e t (cval_tuple cl) /\
          nth_error cl n = Some w.
  Proof.
    inversion 1; subst.
    eexists; split; eassumption.
  Qed.

  Lemma i_rec_make_nil_destr:
    forall e v,
    interp_term s u e (term_rec_make []) v ->
    v = cval_record [].
  Proof.
    now inversion 1.
  Qed.

  Lemma i_rec_make_cons_destr:
    forall e f ta t tl w,
    interp_term s u e (term_rec_make ((f, ta, t) :: tl)) w ->
    exists v vl,
      interp_term s u e t v /\
      interp_term s u e (term_rec_make tl) (cval_record vl) /\
    w = cval_record ((f,v)::vl).
    Proof.
      inversion 1; subst.
      do 2 eexists.
      split; [eassumption|].
      split; [eassumption|].
      reflexivity.
    Qed.

  Lemma i_rec_make_destr:
    forall e tl w,
    interp_term s u e (term_rec_make tl) w ->
    exists vl,
      w = cval_record vl /\
      Forall2 (fun (a:string*list type*term) (b:string * cvalue) =>
        let '(f, ta, t) := a in
        let (f', v) := b in
        f = f' /\ interp_term s u e t v) tl vl.
  Proof.
    induction tl as [|[[f ta] t] tl IHtl].
    - inversion 1; subst.
      exists []. split; [reflexivity|constructor].
    - intros w A.
      apply i_rec_make_cons_destr in A.
      destruct A as [v [vl [ithd [ittl ->]]]].
      specialize (IHtl (cval_record vl) ittl).
      destruct IHtl as [? [eq all2]]; injection eq as <-.
      exists ((f,v)::vl).
      split; [reflexivity|].
      constructor; [|exact all2].
      split; [reflexivity|assumption].
  Qed.

  Lemma i_rec_set_destr:
    forall e t tl w,
      interp_term s u e (term_rec_set t tl) w ->
      exists vl1 vl2,
        w = cval_record (vl2 ++ vl1) /\
        interp_term s u e t (cval_record vl1) /\
        interp_term s u e (term_rec_make tl) (cval_record vl2).
  Proof.
    inversion 1; subst.
    do 2 eexists.
    split; [reflexivity|].
    split; assumption.
  Qed.

  (***************************************************************************)
  (*                       Destructors for interp_skel                       *)
  (***************************************************************************)

  Lemma i_branch_destr:
    forall e ty l v,
    interp_skel s u g e (skel_branch ty l) v ->
    exists sk,
      In sk l /\ interp_skel s u g e sk v.
  Proof.
    inversion 1; eauto.
  Qed.

  Lemma i_match_destr:
    forall e tm ty psl w,
    interp_skel s u g e (skel_match tm ty psl) w ->
    exists v sk e',
      interp_term s u e tm v /\
      get_match e psl v = Some (sk, e') /\
      interp_skel s u g e' sk w.
  Proof.
    inversion 1; subst.
    do 3 eexists.
    split; [|split]; eassumption.
  Qed.

  Lemma i_return_destr:
    forall e t v,
    interp_skel s u g e (skel_return t) v ->
    interp_term s u e t v.
  Proof.
    inversion 1; eauto.
  Qed.

  Lemma i_apply_destr:
    forall e f a res,
    interp_skel s u g e (skel_apply f a) res ->
    ( exists p sk e' v e'',
      interp_term s u e f (cval_closure p sk e') /\
        interp_term s u e a v /\
        add_asn e' p v = Some e'' /\
        interp_skel s u g e'' sk res) \/
    ( exists v w m x ta vl,
      interp_term s u e f (cval_unspec 0 x ta vl) /\
        interp_term s u e a v /\
        find x u = Some (m, w) /\
        w ta (app vl [v]) res) \/
    ( exists n x ta vl v,
      interp_term s u e f (cval_unspec (S n) x ta vl) /\
        interp_term s u e a v /\
        res = cval_unspec n x ta (vl ++ [v])).
  Proof.
    inversion 1; subst; [left|right;left|right;right].
    + do 5 eexists.
    split; [|split]; [..|split]; eassumption.
    + do 6 eexists.
    split; [|split]; [..|split]; eassumption.
    + do 5 eexists.
    split; [|split]; [eassumption..|]; reflexivity.
  Qed.

  Lemma i_letin_destr:
    forall e p s1 s2 w,
      interp_skel s u g e (skel_letin p s1 s2) w ->
      exists v e',
        interp_skel s u g e s1 v /\
        add_asn e p v = Some e' /\
        interp_skel s u g e' s2 w.
  Proof.
    inversion 1; subst;
    repeat eexists; repeat split; eauto.
  Qed.

  Lemma i_exists_destr:
    forall e ty v,
      interp_skel s u g e (skel_exists ty) v ->
        g ty v.
  Proof.
    inversion 1; subst;
    repeat eexists; repeat split; eauto.
  Qed.



End Destructors.







(*****************************************************************************)
(*                                  Tactics                                  *)
(*****************************************************************************)

Ltac pWild H:=
  match type of H with
  | add_asn _ (pattern_wildcard _) _ = Some _ =>
      apply add_asn_wildcard_destr in H;
      subst
  | add_asn _ (pattern_wildcard _) _ = None =>
      simpl in H; discriminate H
  end.

Ltac pVar H:=
  match type of H with
  | add_asn _ (pattern_var _ _) _ = Some _ =>
      apply add_asn_var_destr in H;
      subst
  | add_asn _ (pattern_var _ _) _ = None =>
      simpl in H; discriminate H
  end.

Ltac pConstr H :=
  match type of H with
  | add_asn _ (pattern_constr _ _ _) (cval_constructor _ _) = Some _ =>
    apply add_asn_constr_destr in H;
    let v := fresh "v" in
    let eq := fresh "eq" in
    destruct H as [v [eq H]];
    injection eq as <-
  | add_asn _ (pattern_constr _ _ _) _ = Some _ =>
    apply add_asn_constr_destr in H;
    let v := fresh "v" in
    destruct H as [v [-> H]]
  | add_asn _ (pattern_constr ?c _ _) (cval_constructor ?c' _) = None =>
      match eval compute in (c=?c') with
      | false => clear H
      | true => simpl in H
      end
  end.

Ltac pTupleCons H H':=
  match type of H with
  | add_asn _ (pattern_tuple (?p::?pl)) (cval_tuple (?v::?vl)) = Some _ =>
    apply add_asn_tuple_cons_destr in H;
    let f := fresh "e" in
    let eq := fresh "eq" in
    let v := fresh "v" in
    let vl := fresh "vl" in
    destruct H as (f & v & vl & eq & H & H');
    injection eq as <- <-
  | add_asn _ (pattern_tuple (?p::?pl)) (cval_tuple ?vl) = Some _ =>
    apply add_asn_tuple_cons_destr in H;
    let f := fresh "e" in
    let eq := fresh "eq" in
    let v := fresh "v" in
    let vl := fresh "vl" in
    destruct H as (f & v & vl & eq & H & H');
    injection eq as ->
  | add_asn _ (pattern_tuple (?p::?pl)) _ = Some _ =>
    apply add_asn_tuple_cons_destr in H;
    let f := fresh "e" in
    let eq := fresh "eq" in
    let v := fresh "v" in
    let vl := fresh "vl" in
    destruct H as (f & v & vl & -> & H & H')
  end.

Ltac pTupleNil H:=
  match type of H with
  | add_asn _ (pattern_tuple []) (cval_tuple []) = Some _ =>
    apply add_asn_tuple_nil_destr in H;
    destruct H as (_ & <-)
  | add_asn _ (pattern_tuple []) (cval_tuple _) = Some _ =>
    apply add_asn_tuple_nil_destr in H;
    let eq := fresh "eq" in
    destruct H as (eq & <-);
    injection eq as ->
  | add_asn _ (pattern_tuple []) _ = Some _ =>
    apply add_asn_tuple_nil_destr in H;
    destruct H as (-> & <-)
  end.

Ltac iGetMatch H addasn :=
  match type of H with
  | get_match ?e [] _ = Some _ => discriminate H
  | get_match ?e ((_, _) :: _) _ = Some (_, _) =>
      apply get_match_destr in H;
      let addasn := fresh addasn in
      destruct H as [[addasn H]|[addasn H]]; [|iGetMatch H addasn]
  end.

Ltac iGetMatch' H addasn :=
  match type of H with
  | get_match ?e [] _ = Some _ => discriminate H
  | get_match ?e ((_, _) :: _) _ = Some (_, _) =>
      apply get_match_destr' in H;
      let Hin := fresh in
      let eq := fresh in
      destruct H as [p [Hin addasn]];
      repeat destruct Hin as [eq|Hin]; [..|destruct Hin];
      injection eq as <- <-
  end.

Ltac iVarLet H :=
  match type of H with
  | interp_term _ _ ?e (term_var (TVLet ?s _)) _ =>
      match eval compute in (findEnv s e) with
      | Some ?v => apply i_var_let_destr in H;
          first [injection H as <-|injection H as ->|injection H as H]
      | _ => apply i_var_let_destr in H
      end
  end.

Ltac iVarSpec H :=
  match type of H with
  | interp_term _ _ _ (term_var (TVSpec _ _ _)) _ =>
      apply i_var_spec_destr in H;
      let eq := fresh in
      destruct H as [?ta [?ty [?t [eq [_ H]]]]];
      injection eq as <- <- <-
  end.

Ltac iVarUnspec H :=
  match type of H with
  | interp_term _ _ _ (term_var (TVUnspec _ _ _)) _ =>
      apply i_var_unspec_destr in H
  end.

Ltac iConstr H :=
  match type of H with
  | interp_term _ _ ?e (term_constructor _ _ _) _ =>
      apply i_constr_destr in H;
      destruct H as [? [-> H]]
  end.

Ltac iTupleNil H :=
  match type of H with
  | interp_term _ _ ?e (term_tuple []) _ =>
      apply i_tuple_nil_destr in H;
      first [
        discriminate H
      | injection H as ->
      | subst
        ]
  end.

Ltac iTupleCons H hd :=
  match type of H with
  | interp_term _ _ ?e (term_tuple (_::_)) _ =>
      apply i_tuple_cons_destr in H;
      destruct H as [? [? [-> [hd H]]]]
  | interp_term _ _ ?e (term_tuple (_::_)) _ =>
      apply i_tuple_cons_destr in H;
      let eq := fresh in
      destruct H as [? [? [eq [hd H]]]] ;
      injection eq as ->
  end.

Ltac iFunc H :=
  match type of H with
  | interp_term _ _ _ (term_func _ _) _ =>
      inversion H; subst; clear H
  end.

Ltac iBranch H :=
  match type of H with
  | interp_skel _ _ _ ?e (skel_branch _ _) _ =>
      let Hin:=fresh in let sk := fresh in
      apply i_branch_destr in H; destruct H as [sk [Hin H]];
      repeat destruct Hin as [<-|Hin]; [..|destruct Hin]
  end.

Ltac iRet H :=
  match type of H with
  | interp_skel _ _ _ ?e (skel_return _) _ =>
      apply i_return_destr in H
  end.

Ltac iApply H Hterm Hall :=
  match type of H with
  | interp_skel _ _ _ ?e (skel_apply _ _) _ =>
      apply i_apply_destr in H
  end.

Ltac iLetIn H addasn bound :=
  match type of H with
  | interp_skel _ _ _ ?e (skel_letin _ _ _) _ =>
      let Hin:=fresh in let sk := fresh in
      apply i_letin_destr in H;
      destruct H as [? [? [bound [addasn H]]]]
  end.

Ltac iMatch Happ Ht getmatch:=
  match type of Happ with
  | interp_skel _ _ _?e (skel_match _ _ _) _ =>
      apply i_match_destr in Happ;
      destruct Happ as [?v [?sk [?e' [Ht [getmatch Happ]]]]]
  end.

Ltac pCrush H :=
  match type of H with
  | add_asn _ ?p _ = Some _ =>
      match p with
      | pattern_wildcard _ => pWild H
      | pattern_var _ _ => pVar H
      | pattern_constr _ _ _ => pConstr H; pCrush H
      | pattern_tuple [] => pTupleNil H
      | pattern_tuple (_ :: _) =>
          let H' := fresh in
          pTupleCons H H'; pCrush H'; pCrush H
      | _ => idtac
      end
  | _ => idtac
  end.

  Ltac itCrush H :=
    match type of H with
    | interp_term _ _ _ ?t _ =>
        match t with
        | term_var (TVLet _ _) => iVarLet H
        | term_var (TVSpec _ _ _) => iVarSpec H; itCrush H
        | term_var (TVUnspec _ _ _) => iVarUnspec H
        | term_constructor _ _ _ => iConstr H; itCrush H
        | term_tuple [] => iTupleNil H
        | term_tuple (_ :: _) =>
            let H' := fresh in iTupleCons H H'; itCrush H'; itCrush H
        | term_func _ _ => iFunc H
        end
    end.

  (* Tactics to crush *)

  Ltac crush_addasn H :=
    match type of H with
    | add_asn _ (pattern_constr _ _ _) (cval_constructor _ _) = Some _ =>
      apply add_asn_constr_destr in H;
      let v := fresh "v" in
      let eq := fresh "eq" in
      destruct H as [v [eq H]];
      injection eq as <-;
      crush_addasn H
    | add_asn _ (pattern_constr _ _ _) _ = Some _ =>
      apply add_asn_constr_destr in H;
      let v := fresh "v" in
      destruct H as [v [-> H]] ;
      crush_addasn H
    | add_asn _ (pattern_tuple (?p::?pl)) (cval_tuple (?v::?vl)) = Some _ =>
      apply add_asn_tuple_cons_destr in H;
      let f := fresh "e" in
      let eq := fresh "eq" in
      let v := fresh "v" in
      let vl := fresh "vl" in
      let H' := fresh "addasn" in
      destruct H as (f & v & vl & eq & H & H');
      injection eq as <- <-;
      crush_addasn H ;
      crush_addasn H'
    | add_asn _ (pattern_tuple (?p::?pl)) (cval_tuple _) = Some _ =>
      apply add_asn_tuple_cons_destr in H;
      let f := fresh "e" in
      let eq := fresh "eq" in
      let v := fresh "v" in
      let vl := fresh "vl" in
      let H' := fresh "addasn" in
      destruct H as (f & v & vl & eq & H & H');
      injection eq as ->;
      crush_addasn H ;
      crush_addasn H'
    | add_asn _ (pattern_tuple (?p::?pl)) _ = Some _ =>
      apply add_asn_tuple_cons_destr in H;
      let f := fresh "e" in
      let eq := fresh "eq" in
      let v := fresh "v" in
      let vl := fresh "vl" in
      let H' := fresh "addasn" in
      destruct H as (f & v & vl & -> & H & H');
      crush_addasn H ;
      crush_addasn H'
    | add_asn _ (pattern_tuple []) (cval_tuple []) = Some _ =>
      apply add_asn_tuple_nil_destr in H;
      destruct H as (_ & <-)
    | add_asn _ (pattern_tuple []) (cval_tuple _) = Some _ =>
      apply add_asn_tuple_nil_destr in H;
      let eq := fresh "eq" in
      destruct H as (eq & <-);
      injection eq as ->
    | add_asn _ (pattern_tuple []) _ = Some _ =>
      apply add_asn_tuple_nil_destr in H;
      destruct H as (-> & <-)
    | add_asn _ (pattern_var _ _) _ = Some _ =>
        injection H as  <-
    | add_asn _ (pattern_wildcard _) _ = Some _ =>
        injection H as <-
    | add_asn _ _ _ = _ => inversion H; fail
    end.

  Ltac crush_term H :=
    match type of H with
    | interp_term _ _ _ (term_constructor _ _ _) _ =>
      apply i_constr_destr in H;
      destruct H as [? [-> H]]; subst;
      crush_term H
    | interp_term _ _ _ (term_constructor _ _ _) _ =>
      apply i_constr_destr in H;
      let eq := fresh "eq" in
      destruct H as [?v [eq H]]; subst;
      crush_term H; try (crush_term eq)
    | interp_term _ _ _ (term_tuple []) _ =>
        apply i_tuple_nil_destr in H; discriminate H
    | interp_term _ _ _ (term_tuple []) _ =>
        apply i_tuple_nil_destr in H; injection H as ->
    | interp_term _ _ _ (term_tuple (_::_)) _ =>
        apply i_tuple_cons_destr in H;
        let H' := fresh "interp" in
        destruct H as (?v & ?vl & -> & H & H');
        crush_term H; crush_term H'
    | interp_term _ _ _ (term_tuple (_::_)) _ =>
        apply i_tuple_cons_destr in H;
        let H' := fresh "interp" in
        destruct H as (?v & ?vl & ?eq & H & H');
        subst; crush_term H; crush_term H'
    | Some _ = Some _ =>
        injection H as ?; subst
    | cval_tuple _ = cval_tuple _ =>
        injection H as ?; subst
    | exists _ : _, _ =>
        destruct H as [? H]; crush_term H
    | _ /\ _ =>
        let H1 := fresh H in
        let H2 := fresh H in
        destruct H as [H1 H2]; crush_term H1; crush_term H2
    | interp_term _ _ ?e (term_var (TVLet ?x _)) _ =>
        apply i_var_let_destr in H;
        match eval compute in (findEnv x e) with
        | None => discriminate H
        | Some _ => injection H as ->
        | _ => idtac
        end
    end.
