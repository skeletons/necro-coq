Require Import String List.
From Necro Require Import Dict.
Require Import ZArith.
Import ListNotations DictNotations.
Unset Elimination Schemes.

(** See https://gitlab.inria.fr/skeletons/necro-coq/-/tree/master/files *)
From Necro Require Import Skeleton WellFormed Concrete.

Section Concrete_ss.

  Variable s: skeletal_semantics.

  Definition cvalue := Concrete.cvalue.

  Definition unspec_function: Type :=
    list type -> list cvalue -> cvalue -> Prop.

  Variable unspec_term_interp:
    dict (nat * unspec_function).

  Variable gen_exists:
    type -> cvalue -> Prop.

  Definition add_asn := Concrete.add_asn.

  Inductive ext_term:Type :=
  | ret_term: cvalue -> ext_term
  | cont_term: env -> term -> ext_term
  | ext_term_constr: string -> list type -> ext_term -> ext_term
  | ext_term_tuple: list ext_term -> ext_term
  | ext_term_field: ext_term -> string -> ext_term
  | ext_term_nth: ext_term -> nat -> ext_term
  | ext_term_record: list (string * ext_term) -> ext_term
  | ext_term_rec_set: ext_term -> list (string * ext_term) -> ext_term
  with ext_skel:Type :=
  | ret_skel: cvalue -> ext_skel
  | cont_skel: env -> skeleton -> ext_skel
  | ext_skel_match: env -> ext_term -> list (pattern * skeleton) -> ext_skel
  | ext_skel_return: ext_term -> ext_skel
  | ext_skel_letin: env -> pattern -> ext_skel -> skeleton -> ext_skel
  | ext_skel_apply: ext_term -> ext_term -> ext_skel.

  Inductive interp_term_ss: ext_term -> ext_term -> Prop :=
  | itss_var_val: forall e x ty v,
      findEnv x e = Some v ->
      interp_term_ss
        (cont_term e (term_var (TVLet x ty)))
        (ret_term v)
  | itss_var_spec: forall e t x τ ta tyl ty',
      find x (s_spec_term_decl s) = Some (ta, τ, t) ->
      length ta = length tyl ->
      interp_term_ss
        (cont_term e (term_var (TVSpec x ty' tyl)))
        (cont_term [] (subst_term' t ta tyl))
  | itss_var_unspec_O: forall e x ty ta v w,
      find x unspec_term_interp = Some (0, v) ->
      v ta [] w ->
      interp_term_ss
        (cont_term e (term_var (TVUnspec x ty ta)))
        (ret_term w)
  | itss_var_unspec_S: forall e x ty ta v n,
      find x unspec_term_interp = Some (S n, v) ->
      interp_term_ss
        (cont_term e (term_var (TVUnspec x ty ta)))
        (ret_term (cval_unspec n x ta []))
  | itss_constr_embed: forall e arg c ta,
      interp_term_ss
        (cont_term e (term_constructor c ta arg))
        (ext_term_constr c ta (cont_term e arg))
  | itss_constr: forall c ta t t',
      interp_term_ss t t' ->
      interp_term_ss
        (ext_term_constr c ta t)
        (ext_term_constr c ta t')
  | itss_constr_extract: forall c ta v,
      interp_term_ss
        (ext_term_constr c ta (ret_term v))
        (ret_term (cval_constructor c v))
  | itss_tuple_embed: forall e tl,
      interp_term_ss
        (cont_term e (term_tuple tl))
        (ext_term_tuple (map (cont_term e) tl))
  | itss_tuple_l: forall tl t t',
      interp_term_ss t t' ->
      interp_term_ss
        (ext_term_tuple (t :: tl))
        (ext_term_tuple (t' :: tl))
  | itss_tuple_r: forall tl tl' v,
      interp_term_ss
        (ext_term_tuple tl)
        (ext_term_tuple tl') ->
      interp_term_ss
        (ext_term_tuple (ret_term v :: tl))
        (ext_term_tuple (ret_term v :: tl'))
  | itss_tuple_extract: forall vl,
      interp_term_ss
        (ext_term_tuple (map ret_term vl))
        (ret_term (cval_tuple vl))
  | itss_func: forall p sk e,
      interp_term_ss
        (cont_term e (term_func p sk))
        (ret_term (cval_closure p sk e))
  | itss_field_embed: forall e t ta f,
      interp_term_ss
        (cont_term e (term_field t ta f))
        (ext_term_field (cont_term e t) f)
  | itss_field: forall t t' f,
      interp_term_ss t t' ->
      interp_term_ss
        (ext_term_field t f)
        (ext_term_field t' f)
  | itss_field_extract: forall r f w,
      findEnv f r = Some w ->
      interp_term_ss
        (ext_term_field (ret_term (cval_record r)) f)
        (ret_term w)
  | itss_nth_embed: forall e t ta n,
      interp_term_ss
        (cont_term e (term_nth t ta n))
        (ext_term_nth (cont_term e t) n)
  | itss_nth: forall t t' n,
      interp_term_ss t t' ->
      interp_term_ss
        (ext_term_nth t n)
        (ext_term_nth t' n)
  | itss_nth_extract: forall tl n w,
      nth_error tl n = Some w ->
      interp_term_ss
        (ext_term_nth (ret_term (cval_tuple tl)) n)
        (ret_term w)
  | itss_rec_embed: forall e ftl,
      interp_term_ss
        (cont_term e (term_rec_make ftl))
        (ext_term_record
          (map (fun '(f,_,t) => (f,cont_term e t)) ftl))
  | itss_set_embed: forall e t ftl,
      interp_term_ss
        (cont_term e (term_rec_set t ftl))
        (ext_term_rec_set (cont_term e t)
          (map (fun '(f,_,t) => (f,cont_term e t)) ftl))
  | itss_set_l: forall t t' ftl,
      interp_term_ss t t' ->
      interp_term_ss
        (ext_term_rec_set t ftl)
        (ext_term_rec_set t' ftl)
  | itss_rec: forall tl' ftl,
      interp_term_ss (ext_term_tuple (map snd ftl)) (ext_term_tuple tl') ->
      interp_term_ss
        (ext_term_record ftl)
        (ext_term_record (combine (map fst ftl) tl'))
  | itss_set_r: forall v tl' ftl,
      interp_term_ss (ext_term_tuple (map snd ftl)) (ext_term_tuple tl') ->
      interp_term_ss
        (ext_term_rec_set (ret_term v) ftl)
        (ext_term_rec_set (ret_term v) (combine (map fst ftl) tl'))
  | itss_rec_unfold: forall ftl,
      interp_term_ss
        (ext_term_record (map (fun '(f, t) => (f, ret_term t)) ftl))
        (ret_term (cval_record ftl))
  | itss_set_unfold: forall ftl1 ftl2,
      interp_term_ss
        (ext_term_rec_set
          (ret_term (cval_record ftl1))
          (map (fun '(f, t) => (f, ret_term t)) ftl2))
        (ret_term (cval_record (ftl2 ++ ftl1))).

  Inductive interp_skel_ss: ext_skel -> ext_skel -> Prop :=
  | isss_branch: forall ty e sks ski,
      In ski sks ->
      interp_skel_ss
        (cont_skel e (skel_branch ty sks))
        (cont_skel e ski)
  | i_match_embed: forall e t ty psl,
      interp_skel_ss
        (cont_skel e (skel_match t ty psl))
        (ext_skel_match e (cont_term e t) psl)
  | i_match_l: forall e a b psl,
      interp_term_ss a b ->
      interp_skel_ss
        (ext_skel_match e a psl)
        (ext_skel_match e b psl)
  | i_match_extract: forall e psl v sk e',
      get_match e psl v = Some (sk, e') ->
      interp_skel_ss
        (ext_skel_match e (ret_term v) psl)
        (cont_skel e' sk)
  | isss_return_embed: forall e t,
      interp_skel_ss
        (cont_skel e (skel_return t))
        (ext_skel_return (cont_term e t))
  | isss_return: forall t t',
      interp_term_ss t t' ->
      interp_skel_ss
        (ext_skel_return t)
        (ext_skel_return t')
  | isss_return_extract: forall v,
      interp_skel_ss
        (ext_skel_return (ret_term v))
        (ret_skel v)
  | isss_apply_embed: forall e f a,
      interp_skel_ss
        (cont_skel e (skel_apply f a))
        (ext_skel_apply (cont_term e f) (cont_term e a))
  | isss_apply_l: forall f f' a,
      interp_term_ss f f' ->
      interp_skel_ss
        (ext_skel_apply f  a)
        (ext_skel_apply f' a)
  | isss_apply_r: forall v a a',
      interp_term_ss a a' ->
      interp_skel_ss
        (ext_skel_apply (ret_term v) a)
        (ext_skel_apply (ret_term v) a')
  | isss_apply_clos: forall p sk e a e',
      add_asn e p a = Some e' ->
      interp_skel_ss
        (ext_skel_apply (ret_term (cval_closure p sk e)) (ret_term a))
        (cont_skel e' sk)
  | isss_apply_unspec_done: forall x ta vl a m w res,
      find x unspec_term_interp = Some (m, w) ->
      w ta (app vl [a]) res ->
      interp_skel_ss
        (ext_skel_apply (ret_term (cval_unspec 0 x ta vl)) (ret_term a))
        (ret_skel res)
  | isss_apply_unspec_cont: forall x ta vl a n,
      interp_skel_ss
        (ext_skel_apply (ret_term (cval_unspec (S n) x ta vl)) (ret_term a))
        (ret_skel (cval_unspec n x ta (vl++[a])))
  | isss_letin_embed: forall e p s1 s2,
      interp_skel_ss
        (cont_skel e (skel_letin p s1 s2))
        (ext_skel_letin e p (cont_skel e s1) s2)
  | isss_letin_l: forall e p s s' s2,
      interp_skel_ss s s' ->
      interp_skel_ss
        (ext_skel_letin e p s s2)
        (ext_skel_letin e p s' s2)
  | isss_letin_extract: forall e p v e' s2,
      add_asn e p v = Some e' ->
      interp_skel_ss
        (ext_skel_letin e p (ret_skel v) s2)
        (cont_skel e' s2)
  | isss_exists: forall e τ v,
      gen_exists τ v ->
      interp_skel_ss
        (cont_skel e (skel_exists τ))
        (ret_skel v).
End Concrete_ss.




Section Rec.
  Variable s: skeletal_semantics.

  Definition ext_term_rec
      (P: ext_term -> Prop)
      (ret: forall v, P (ret_term v))
      (cont: forall e v, P (cont_term e v))
      (constr: forall c ta v, P v -> P (ext_term_constr c ta v))
      (tuple: forall vl, Forall P vl -> P (ext_term_tuple vl))
      (field: forall t f, P t -> P (ext_term_field t f))
      (nth: forall t n, P t -> P (ext_term_nth t n))
      (record: forall tl,
        Forall (fun '(f, t) => P t) tl -> P (ext_term_record tl))
      (rec_set: forall t tl,
        P t ->
        Forall (fun '(f, t) => P t) tl -> P (ext_term_rec_set t tl)):
      forall v, P v :=
    fix aux v :=
    match v return P v with
    | ret_term w => ret w
    | cont_term e w => cont e w
    | ext_term_constr c ta w => constr c ta w (aux w)
    | ext_term_tuple vl =>
        tuple vl
        ((fix aux_list vl :=
          match vl return Forall P vl with
          | [] => Forall_nil P
          | a :: q => Forall_cons a (aux a) (aux_list q)
          end) vl)
    | ext_term_field t f => field t f (aux t)
    | ext_term_nth t n => nth t n (aux t)
    | ext_term_record tl => record tl
        ((fix auxl tl :=
          match tl return Forall (fun '(f, t) => P t) tl with
          | [] => Forall_nil _
          | (f, t) :: q => Forall_cons (f,t) (aux t) (auxl q)
          end) tl)
    | ext_term_rec_set t tl => rec_set t tl (aux t)
        ((fix auxl tl :=
          match tl return Forall (fun '(f, t) => P t) tl with
          | [] => Forall_nil _
          | (f, t) :: q => Forall_cons (f,t) (aux t) (auxl q)
          end) tl)
    end.

  Definition ext_skel_rec
      (P: ext_skel -> Prop)
      (ret: forall v, P (ret_skel v))
      (cont: forall e v, P (cont_skel e v))
      (skel_ret: forall v, P (ext_skel_return v))
      (skel_match: forall e tm psl, P (ext_skel_match e tm psl))
      (letin: forall e p s1 s2, P s1 -> P (ext_skel_letin e p s1 s2))
      (skel_app: forall f a, P (ext_skel_apply f a)):
      forall sk, P sk :=
    fix aux sk :=
    match sk return P sk with
    | ret_skel w => ret w
    | cont_skel e w => cont e w
    | ext_skel_return v => skel_ret v
    | ext_skel_letin e p s1 s2 => letin e p s1 s2 (aux s1)
    | ext_skel_match e tm psl => skel_match e tm psl
    | ext_skel_apply f a => skel_app f a
    end.
End Rec.
