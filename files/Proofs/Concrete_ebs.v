Require Import String List.
From Necro Require Import Dict Skeleton WellFormed Concrete Concrete_ss.
Import ListNotations DictNotations.
Open Scope string_scope.

Section Concrete_ebs.

  Variable s: skeletal_semantics.

  Definition option_bind {A B} (x : option A) (f : A -> option B) :=
    match x with
    | None => None
    | Some x' => f x'
    end.
  Notation "x <- y ; z" := (option_bind y (fun x => z))
    (right associativity, at level 55, y at level 53).

  Definition env := list (string * cvalue).
  Notation "'void_env'" := [] (only parsing).

  Definition unspec_function: Type :=
    list type -> list cvalue -> cvalue -> Prop.

  Variable u: dict (nat * unspec_function).
  Variable g: type -> cvalue -> Prop.


  Inductive interp_term_ebs: ext_term -> cvalue -> Prop :=
  | iebs_ret_term: forall v,
      interp_term_ebs (ret_term v) v
  | iebs_cont_term: forall e v w,
      interp_term s u e v w ->
      interp_term_ebs (cont_term e v) w
  | iebs_ext_term_constr: forall c ta v w,
      interp_term_ebs v w ->
      interp_term_ebs (ext_term_constr c ta v) (cval_constructor c w)
  | iebs_ext_term_tuple: forall tl vl,
      Forall2 interp_term_ebs tl vl ->
      interp_term_ebs (ext_term_tuple tl) (cval_tuple vl)
  | iebs_ext_term_field: forall t v f fvl,
      interp_term_ebs t (cval_record fvl) ->
      findEnv f fvl = Some v ->
      interp_term_ebs (ext_term_field t f) v
  | iebs_ext_term_nth: forall t v n vl,
      interp_term_ebs t (cval_tuple vl) ->
      nth_error vl n = Some v ->
      interp_term_ebs (ext_term_nth t n) v
  | iebs_record_none_nil:
      interp_term_ebs (ext_term_record []) (cval_record [])
  | iebs_record_none_cons: forall f t tl v vl,
      interp_term_ebs t v ->
      interp_term_ebs (ext_term_record tl) (cval_record vl) ->
      interp_term_ebs
        (ext_term_record ((f,t)::tl))
        (cval_record ((f,v)::vl))
  | iebs_record_some: forall t tl vl1 vl2,
      interp_term_ebs t (cval_record vl1) ->
      interp_term_ebs (ext_term_record tl) (cval_record vl2) ->
      interp_term_ebs (ext_term_rec_set t tl) (cval_record (vl2++vl1)).

  Inductive interp_skel_ebs: ext_skel -> cvalue -> Prop :=
  | iebs_ret_skel: forall v,
      interp_skel_ebs (ret_skel v) v
  | iebs_cont_skel: forall e v w,
      interp_skel s u g e v w ->
      interp_skel_ebs (cont_skel e v) w
  | iebs_ext_skel_return: forall v w,
      interp_term_ebs v w ->
      interp_skel_ebs (ext_skel_return v) w
  | iebs_ext_skel_match: forall tm s psl e e' v w,
      interp_term_ebs tm v ->
      get_match e psl v = Some (s, e') ->
      interp_skel_ebs (cont_skel e' s) w ->
      interp_skel_ebs (ext_skel_match e tm psl) w
  | iebs_ext_skel_letin: forall e e' p s1 s2 v1 w,
      interp_skel_ebs s1 v1 ->
      Concrete.add_asn e p v1 = Some e' ->
      interp_skel s u g e' s2 w ->
      interp_skel_ebs (ext_skel_letin e p s1 s2) w
  | iebs_ext_skel_apply_clos: forall t p sk e a v e' w,
      interp_term_ebs t (cval_closure p sk e) ->
      interp_term_ebs a v ->
      Concrete.add_asn e p v = Some e' ->
      interp_skel s u g e' sk w ->
      interp_skel_ebs (ext_skel_apply t a) w
  | iebs_ext_skel_apply_unspec_done: forall t x ta vl a v m w res,
      interp_term_ebs t (cval_unspec 0 x ta vl) ->
      interp_term_ebs a v ->
      find x u = Some (m, w) ->
      w ta (app vl [v]) res ->
      interp_skel_ebs (ext_skel_apply t a) res
  | iebs_ext_skel_apply_unspec_cont: forall t n x ta vl a v,
      interp_term_ebs t (cval_unspec (S n) x ta vl) ->
      interp_term_ebs a v ->
      interp_skel_ebs (ext_skel_apply t a) (cval_unspec n x ta (app vl [v])).
End Concrete_ebs.

