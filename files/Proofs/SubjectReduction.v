Require Import String List.
Import ListNotations.

From Necro Require Import Dict.
From Necro Require Import Skeleton WellFormed Concrete Concrete_ss.







(* Assume a well-formed skeletal semantics s *)
Parameter s: skeletal_semantics.
Parameter wf_s: well_formed_semantics s.



(* Assume an increasing way to type base cvalues. *)
Parameter type_base_cvalue:
  (cvalue -> type -> Prop) ->
  forall A, A -> type -> Prop.
Parameter type_base_cvalue_inc:
  forall (P Q:cvalue -> type -> Prop),
    (forall v ty, P v ty -> Q v ty) ->
    (forall A a ty, type_base_cvalue P A a ty -> type_base_cvalue Q A a ty).




Definition env_ok (type_cvalue: cvalue -> type -> Prop) g e :=
  forall x,
  match findEnv x g, findEnv x e with
  | None, None => True
  | Some τ, Some v => type_cvalue v τ
  | _, _ => False
  end.

Definition well_formed_term' type_cvalue e sk ty :=
  exists g, env_ok type_cvalue g e /\ well_formed_term s g sk ty.
Definition well_formed_skel' type_cvalue e sk ty :=
  exists g, env_ok type_cvalue g e /\ well_formed_skel s g sk ty.

Inductive well_formed_apply: type -> list type -> type -> Prop :=
| wf_nil τ:
    well_formed_apply τ [] τ
| wf_cons τ ν τl μ:
    well_formed_apply ν τl μ ->
    well_formed_apply (Arrow τ ν) (τ :: τl) μ.

Inductive type_cvalue_S type_cvalue_n: cvalue -> type -> Prop :=
| type_cval_base A a ty ta:
  type_base_cvalue type_cvalue_n A a (Base ty ta) ->
  type_cvalue_S type_cvalue_n (cval_base A a) (Base ty ta)
| type_cval_constr c v ta c_ta c_in c_out:
  find c (s_ctype s) = Some (c_ta, c_in, c_out) ->
  type_cvalue_S type_cvalue_n v (subst' c_in c_ta ta) ->
  type_cvalue_S type_cvalue_n (cval_constructor c v) (Base c_out ta)
| type_cval_tuple vl tyl:
  Forall2 type_cvalue_n vl tyl ->
  type_cvalue_S type_cvalue_n (cval_tuple vl) (Prod tyl)
| type_cval_closure p ty ty_out sk e g:
  env_ok type_cvalue_n g e ->
  well_formed_pattern s p ty ->
  well_formed_skel s (extEnv p g) sk ty_out ->
  type_cvalue_S type_cvalue_n (cval_closure p sk e) (Arrow ty ty_out)
| type_cval_unspec m x ty vl tyl tyo ta:
  well_formed_term s [] (term_var (TVUnspec x ty ta)) ty ->
  Forall2 type_cvalue_n vl tyl ->
  well_formed_apply ty tyl tyo ->
  type_cvalue_S type_cvalue_n (cval_unspec m x ta vl) tyo
| type_cval_record tl tyl f_out:
  (forall x, In x (s_field s) ->
    forall f_ta f_in,
    find x (s_ftype s) = Some (f_ta, f_out, f_in) ->
    In x (map fst tl)) ->
  Forall (fun '(f, v) =>
    match find f (s_ftype s) with
    | None => False
    | Some (f_ta, f'_out, f_in) =>
        f_out = f'_out /\ type_cvalue_n v (subst' f_in f_ta tyl)
    end) tl ->
  type_cvalue_S type_cvalue_n (cval_record tl) (Base f_out tyl).

Fixpoint type_cvalue_n n v ty: Prop :=
  match n with
  | 0 => False
  | S m => type_cvalue_S (type_cvalue_n m) v ty
  end.
Definition type_cvalue v ty :=
  exists n, type_cvalue_n n v ty.

Inductive type_record:
  list (string * ext_term) -> type -> Prop :=
| type_rec_nil ty:
    type_record [] ty
| type_rec_cons t tyl f f_ta f_out f_in tl:
    find f (s_ftype s) = Some (f_ta, f_out, f_in) ->
    length f_ta = length tyl ->
    type_ext_term t (subst' f_in f_ta tyl) ->
    type_record tl (Base f_out tyl) ->
    type_record ((f, t) :: tl) (Base f_out tyl)

with type_ext_term: ext_term -> type -> Prop :=
| type_ret_term: forall c ty,
    type_cvalue c ty ->
    type_ext_term (ret_term c) ty
| type_cont_term: forall e w ty,
    well_formed_term' type_cvalue e w ty ->
    type_ext_term (cont_term e w) ty
| type_ext_term_constr: forall c c_ta c_in c_out w ta,
    find c (s_ctype s) = Some (c_ta, c_in, c_out) ->
    type_ext_term w (subst' c_in c_ta ta) ->
    type_ext_term (ext_term_constr c ta w) (Base c_out ta)
| type_ext_term_tuple: forall vl tyl,
    Forall2 type_ext_term vl tyl ->
    type_ext_term (ext_term_tuple vl) (Prod tyl)
| type_ext_term_field: forall t f f_in f_ta f_out ta,
    find f (s_ftype s) = Some (f_ta, f_out, f_in) ->
    type_ext_term t (Base f_out ta) ->
    type_ext_term (ext_term_field t f) (subst' f_in f_ta ta)
| type_ext_term_nth: forall t n tyl ty,
    nth_error tyl n = Some ty ->
    type_ext_term t (Prod tyl) ->
    type_ext_term (ext_term_nth t n) ty
| type_ext_term_record: forall ftl tyl f_out,
    (forall x, In x (s_field s) ->
      forall f_ta f_in,
      find x (s_ftype s) = Some (f_ta, f_out, f_in) ->
      In x (map fst ftl)) ->
    type_record ftl (Base f_out tyl) ->
    type_ext_term (ext_term_record ftl) (Base f_out tyl)
| type_ext_term_rec_set: forall t ftl ty,
    type_ext_term t ty ->
    type_record ftl ty ->
    type_ext_term (ext_term_rec_set t ftl) ty.

Inductive type_ext_skel: ext_skel -> type -> Prop :=
| type_ret_skel: forall c ty,
    type_cvalue c ty ->
    type_ext_skel (ret_skel c) ty
| type_cont_skel: forall e w ty,
    well_formed_skel' type_cvalue e w ty ->
    type_ext_skel (cont_skel e w) ty
| type_ext_skel_return: forall v ty,
    type_ext_term v ty ->
    type_ext_skel (ext_skel_return v) ty
| type_ext_skel_letin: forall e g p s1 s2 ty' ty,
    env_ok type_cvalue g e ->
    well_formed_skel s (extEnv p g) s2 ty ->
    well_formed_pattern s p ty' ->
    type_ext_skel s1 ty' ->
    type_ext_skel (ext_skel_letin e p s1 s2) ty
| type_ext_skel_apply: forall f a tyi tyo,
    type_ext_term f (Arrow tyi tyo) ->
    type_ext_term a tyi ->
    type_ext_skel (ext_skel_apply f a) tyo
| type_ext_skel_match: forall t ty g e psl ty_out,
    env_ok type_cvalue g e ->
    type_ext_term t ty ->
    Forall (fun '(p,sk) =>
      well_formed_pattern s p ty /\
      well_formed_skel s (extEnv p g) sk ty_out) psl ->
    type_ext_skel (ext_skel_match e t psl) ty_out.



(* take_args n ty tyl tyo means that ty = tyl -> tyo (curried) *)
Inductive take_args: nat -> type -> list type -> type -> Prop :=
| take_args_0: forall ty,
    take_args 0 ty [] ty
| take_args_s: forall n o ty1 tyq tyo,
    take_args n o tyq tyo ->
    take_args (S n) (Arrow ty1 o) (ty1 :: tyq) tyo.
Fact take_args_length:
  forall n ty tyl tyo,
  take_args n ty tyl tyo ->
  length tyl = n.
Proof.
   induction 1.
   - reflexivity.
   - simpl; f_equal. assumption.
Qed.

(* Assume a correct specification of unspecified terms *)
Parameter u: dict (nat * unspec_function).
Parameter u_ok:
  forall (v: string),
    match find v u with
    | None => True
    | Some (n, f) => (* v's specification is (n, f) *)
        match find v (s_unspec_term_decl s) with
        | None => False
        | Some (ta, ty) => (* val v<ta>: ty *)
            (* ty is an arrow type with at least n arguments *)
            (exists tyl tyo, take_args n ty tyl tyo)
            /\
            (* if f [tyl] [vl] w then |tyl| = |ta| and |vl| = n *)
            (forall tyl vl w, f tyl vl w ->
              length tyl = length ta /\ length vl = n)
            /\
            (* if f [tyl] [vl] w, and if ty<ta←tyl> = [ty'l]→ty'o
               then [vl] has type [ty'l] and w has type ty'o *)
            (forall tyl vl w, f tyl vl w ->
              exists ty'l ty'o,
              take_args n (subst' ty ta tyl) ty'l ty'o /\
              Forall2 type_cvalue vl ty'l /\
              type_cvalue w ty'o)
        end
    end.


(* Assume a correct generator *)
Parameter gen: type -> cvalue -> Prop.
Parameter gen_ok:
  forall ty c, gen ty c -> type_cvalue c ty.


(* We prove some useful lemmae *)

Ltac inverse H := inversion H; subst; clear H.

Lemma forall2_imp:
  forall {A B} (P Q: A -> B -> Prop) l1 l2,
  Forall2 P l1 l2 ->
  (forall x y, P x y -> Q x y) ->
  Forall2 Q l1 l2.
Proof.
  intros A B P Q l1 l2 all H.
  revert l2 all; induction l1; destruct l2; [constructor|inversion 1..|].
  inversion 1; subst.
  constructor; firstorder.
Qed.

Lemma forall2_length:
  forall {A B} (P: A -> B -> Prop) l1 l2,
  Forall2 P l1 l2 -> length l1 = length l2.
Proof.
  induction l1; destruct l2; [reflexivity|inversion 1..|].
  inversion 1; simpl; f_equal; firstorder.
Qed.

Lemma forall_imp2:
  forall {A} (P Q R: A -> Prop) l,
  Forall P l ->
  Forall Q l ->
  (forall x, P x -> Q x -> R x) ->
  Forall R l.
Proof.
  intros A P Q R l HP HQ H.
  induction l; [constructor|].
  inversion HP; inversion HQ; subst.
  constructor; firstorder.
Qed.

Lemma Forall2_cons_iff:
  forall {A B} (P:A -> B -> Prop) v1 l1 v2 l2,
  Forall2 P (v1 :: l1) (v2 :: l2) <->
  P v1 v2 /\ Forall2 P l1 l2.
Proof.
  intros A B P v1 l1 v2 l2; split.
  - inversion 1; split; assumption.
  - intros [? ?]; constructor; assumption.
Qed.

Lemma incl_map:
  forall X Y (f:X -> Y) l1 l2,
    incl l1 l2 ->
    incl (map f l1) (map f l2).
Proof.
  intros X Y f l1 l2 H y Hy.
  apply in_map_iff in Hy.
  destruct Hy as [x [Hy Hx]].
  apply in_map_iff.
  exists x.
  split; [exact Hy|].
  now apply H.
Qed.

Lemma sameList_map:
  forall X Y (f:X -> Y) l1 l2,
    sameList l1 l2 ->
    sameList (map f l1) (map f l2).
Proof.
  intros X Y f l1 l2 [incl1 incl2].
  split; apply incl_map; assumption.
Qed.

Lemma take_args_wf_apply:
  forall n tyi tyl tyo,
  take_args n tyi tyl tyo ->
  well_formed_apply tyi tyl tyo.
Proof.
  induction n.
  - inversion_clear 1. constructor.
  - inversion_clear 1. constructor.
    firstorder.
Qed.

Fact extEnv_pattern_tuple:
  forall p pl e,
      extEnv (pattern_tuple (p :: pl)) e =
      extEnv (pattern_tuple pl) (extEnv p e).
Proof. reflexivity. Qed.

Fact add_asn_pattern_tuple:
  forall p pl v e e',
    add_asn e (pattern_tuple (p :: pl)) v = Some e' ->
    exists e'' w wl,
      v = cval_tuple (w :: wl) /\
      add_asn e p w = Some e'' /\
      add_asn e'' (pattern_tuple pl) (cval_tuple wl) = Some e'.
Proof.
  intros p pl v e e' H.
  destruct v as [| |[|w wl]| | |]; simpl in H; try congruence.
  destruct (add_asn e p w) as [e''|] eqn:He'' in H; simpl in H; [|congruence].
  exists e'', w, wl.
  split; [reflexivity|].
  split; assumption.
Qed.

Fact pattern_variables_pattern_tuple:
  forall p pl,
      pattern_variables (pattern_tuple (p::pl)) =
      (pattern_variables p ++
      pattern_variables (pattern_tuple pl))%list.
Proof. reflexivity. Qed.

Fact pattern_variables_with_type_pattern_tuple:
  forall p pl,
      pattern_variables_with_type (pattern_tuple (p::pl)) =
      (pattern_variables_with_type p ++
      pattern_variables_with_type (pattern_tuple pl))%list.
Proof. reflexivity. Qed.

Fact extEnv_pattern_rec:
  forall f τl p pq e,
      extEnv (pattern_rec ((f,τl,p) :: pq)) e =
      extEnv (pattern_rec pq) (extEnv p e).
Proof. reflexivity. Qed.

Fact add_asn_pattern_rec:
  forall f τl p pl v e e',
    add_asn e (pattern_rec ((f, τl, p) :: pl)) v = Some e' ->
    exists e'' w vl,
      v = cval_record vl /\
      findEnv f vl = Some w /\
      add_asn e p w = Some e'' /\
      add_asn e'' (pattern_rec pl) v = Some e'.
Proof.
  intros f τl p pl v e e' H.
  destruct v as [| | | |vl|]; simpl in H; try congruence.
  destruct (findEnv f vl) as [w|] eqn:Hf; [|simpl in *; congruence].
  destruct (add_asn e p w) as [e''|] eqn:He''; [|simpl in *; congruence].
  exists e'', w, vl.
  split; [reflexivity|].
  split; [assumption|].
  split; assumption.
Qed.

Fact pattern_variables_pattern_rec:
  forall f τl p pq,
      pattern_variables (pattern_rec ((f,τl,p)::pq)) =
      (pattern_variables p ++
      pattern_variables (pattern_rec pq))%list.
Proof. reflexivity. Qed.

Fact pattern_variables_with_type_pattern_rec:
  forall f τl p pq,
      pattern_variables_with_type (pattern_rec ((f,τl,p)::pq)) =
      (pattern_variables_with_type p ++
      pattern_variables_with_type (pattern_rec pq))%list.
Proof. reflexivity. Qed.



Lemma env_ok_destr g e:
  env_ok type_cvalue g e ->
  forall x, (
    (findEnv x e = None <-> findEnv x g = None)
      /\
    forall tau v,
    findEnv x g = Some tau -> findEnv x e = Some v -> type_cvalue v tau)
.
Proof.
  intros envok x. specialize (envok x).
  destruct (findEnv x e); destruct (findEnv x g);
  repeat split; try congruence; contradiction.
Qed.

Lemma type_cvalue_inc n v ty:
  type_cvalue_n n v ty ->
  type_cvalue_n (S n) v ty.
Proof.
revert v ty; induction n as [|n IH]; [destruct 1|].
induction 1.
- constructor. apply type_base_cvalue_inc with (1:=IH). assumption.
- econstructor; eauto.
- econstructor.
  now apply forall2_imp with (2:=IH).
- constructor 4 with (g:=g); [|assumption..].
  clear - H IH.
  revert g H.
  induction e as [|[x τ] e]; [destruct g as [|[s ty] g]; auto|].
  intros g H y. specialize(H y).
  destruct (findEnv y g) eqn:Hyg; simpl in *;
  (destruct (x =? y) eqn:Hxy;
  [|destruct (findEnv y e) eqn:Hye; simpl in *]);
      try (now apply IH); try contradiction; trivial.
- econstructor; try eassumption.
  now apply forall2_imp with (2:=IH).
- constructor; [assumption|].
  revert H0. clear H.
  induction tl; [constructor|].
  intros A. inverse A.
  specialize(IHtl H2). clear H2. constructor; [|assumption].
  destruct a as [f v]. destruct (find f (s_ftype s)); [|inversion H1].
  destruct p as [[? ?] ?]. destruct H1.
  split; [assumption|now apply IH].
Qed.

Lemma type_cvalue_inc' m n v ty:
  m <= n ->
  type_cvalue_n m v ty ->
  type_cvalue_n n v ty.
Proof.
  remember type_cvalue_inc.
  induction 1; firstorder.
Qed.

Lemma type_cval_tuple_forall:
  forall vl tyl,
  type_cvalue (cval_tuple vl) (Prod tyl) <->
  Forall2 type_cvalue vl tyl.
Proof.
  split; revert tyl; induction vl; destruct tyl as [|ty tyl]; intros.
  - constructor.
  - destruct H as [n H].
    destruct n as [|n]; inversion H; subst.
    inversion H2.
  - destruct H as [n H].
    destruct n as [|n]; inversion H; subst.
    inversion H2.
  - destruct H as [[|n] H]; inversion H; subst.
    inversion H2; subst.
    constructor; [exists n; assumption|].
    apply forall2_imp with (1:=H6).
    intros. now exists n.
  - exists 1. do 2 constructor.
  - inversion H.
  - inversion H.
  - inversion H; subst.
    specialize (IHvl tyl H5).
    destruct IHvl as [[|n] Hvl]; [inversion Hvl|].
    destruct H3 as [m Hv].
    clear H5 H.
    exists (S (max m n)).
    constructor.
    inversion Hvl; subst.
    constructor.
    + apply type_cvalue_inc' with (m:=m); [|assumption].
      apply PeanoNat.Nat.le_max_l.
    + apply forall2_imp with (1:=H1).
      intros ? ?. apply type_cvalue_inc'.
      apply PeanoNat.Nat.le_max_r.
Qed.

Lemma type_cval_constr_destr:
  forall c τl f_out v,
  type_cvalue (cval_constructor c v) (Base f_out τl) ->
  exists c_τl c_in c_out,
  find c (s_ctype s) = Some (c_τl, c_in, c_out) /\
  type_cvalue v (subst' c_in c_τl τl).
Proof.
  intros.
  destruct H as [[|n] H]; [destruct H|].
  inversion H; subst; clear H.
  do 3 eexists. split; [eassumption|].
  now exists (S n).
Qed.

Definition subst_env e tae :=
  map (fun (y:string*type) =>
    let (x, tau) := y in (x, subst tau tae)) e.
Definition subst_env' e ta tyl :=
  let tae := combine ta tyl in
  subst_env e tae.

Lemma findEnv_subst_env_some:
  forall s e t tae,
  findEnv s e = Some t ->
  findEnv s (subst_env e tae) = Some (subst t tae).
Proof.
  intros s e t tae H.
  induction e as [|[a ty] e IHe].
  - simpl in H; congruence.
  - simpl in H; simpl. destruct (eqb a s) eqn:eqas.
    + injection H as <-. reflexivity.
    + exact (IHe H).
Qed.

Lemma findEnv_subst_env_some_rev:
  forall s e t tae,
  findEnv s (subst_env e tae) = Some t ->
  exists t', t = (subst t' tae) /\ findEnv s e = Some t'.
Proof.
  intros s e t tae H.
  induction e as [|[a ty] e IHe].
  - simpl in H; congruence.
  - simpl in H; simpl. destruct (eqb a s) eqn:eqas.
    + injection H as <-. eexists. split; reflexivity.
    + exact (IHe H).
Qed.

Lemma findEnv_subst_env_none:
  forall s e tae,
  findEnv s e = None <->
  findEnv s (subst_env e tae) = None.
Proof.
  intros s e tae; split; intro H.
  - destruct (findEnv s (subst_env e tae)) eqn:A; [|reflexivity].
    apply findEnv_subst_env_some_rev in A.
    destruct A as [? [_ A]]; congruence.
  - destruct (findEnv s e) eqn:A; [|reflexivity].
    apply findEnv_subst_env_some with (tae:=tae) in A. congruence.
Qed.

Lemma subst_list_type_length:
  forall tl tae,
    length tl =
    length (subst_list_type tl tae).
Proof.
  induction tl.
  - reflexivity.
  - intros. simpl. rewrite IHtl with (tae := tae). reflexivity.
Qed.

Lemma subst_comm:
  forall tau tyl ta tae,
    length ta = length tyl ->
    incl (type_variables tau) ta ->
    subst' tau ta (subst_list_type tyl tae) =
    subst (subst' tau ta tyl) tae.
Proof.
  induction tau using type_rect; unfold subst' in *.
  - intros tyl ta tae Hlen Hincl.
    simpl in Hincl.
    revert tyl Hlen; induction ta; intros.
    + destruct (Hincl x); now left.
    + simpl.
      destruct tyl as [|ty tyl]; inversion Hlen.
      destruct (string_dec x a).
      * subst. unfold subst'. simpl. rewrite eqb_refl. reflexivity.
      * destruct (Hincl x (or_introl eq_refl)); [congruence|].
        unfold subst'. unfold subst' in IHta. simpl.
        assert(G:eqb a x = false) by (apply eqb_neq; congruence).
        rewrite G.
        apply IHta; [intros y [->|[]]|]; assumption.
  - intros.
    enough (
      let subst_list tyl tae := map (fun ty => subst ty tae) tyl in
      subst_list x0 (combine ta (subst_list_type tyl tae)) =
      subst_list (subst_list x0 (combine ta tyl)) tae).
    simpl. simpl in H1. rewrite H1. reflexivity.
    induction x0; simpl; [reflexivity|].
    inversion x1; subst. f_equal.
    apply H3; [assumption|].
    simpl in H0. apply incl_app_inv in H0. destruct H0 as [H0 _]. assumption.
    apply IHx0. assumption.
    apply incl_tran with (m:=type_variables (Base x (a::x0)));[|assumption].
    clear. simpl. apply incl_appr. apply incl_refl.
  - intros.
    enough (
      let subst_list tyl tae := map (fun ty => subst ty tae) tyl in
      subst_list x (combine ta (subst_list_type tyl tae)) =
      subst_list (subst_list x (combine ta tyl)) tae).
    simpl. simpl in H1. rewrite H1. reflexivity.
    induction x; simpl; [reflexivity|].
    inversion x0; subst. f_equal. apply H3; [assumption|].
    simpl in H0. apply incl_app_inv in H0. destruct H0 as [H0 _]. assumption.
    simpl in IHx. apply IHx. assumption.
    apply incl_tran with (m:=type_variables (Prod (a::x)));[|assumption].
    clear. simpl. apply incl_appr. apply incl_refl.
  - intros. simpl. simpl in H0. apply incl_app_inv in H0. destruct H0.
  f_equal; firstorder.
Qed.

Lemma extEnvVar_subst_env:
  forall e x tau tae,
    extEnvVar (x, subst tau tae) (subst_env e tae) =
    subst_env (extEnvVar (x, tau) e) tae.
Proof.
  reflexivity.
Qed.


Definition pattern_ind (P: pattern -> Prop)
    var wild nil cons constr rec or: forall p, P p :=
  fix aux p :=
    match p with
    | pattern_var x ty => var x ty
    | pattern_wildcard ty => wild ty
    | pattern_tuple pl =>
      (fix plist pl :=
      match pl return P (pattern_tuple pl) with
      | [] => nil
      | p :: pl => cons p pl (aux p) (plist pl)
      end) pl
    | pattern_constr c ta p => constr c ta p (aux p)
    | pattern_rec pl => rec pl
        ((fix auxl pl :=
          match pl return Forall (fun x =>
            match x with (x, y, z) => P z end) pl with
          | [] => Forall_nil _
          | (f, ta, p) :: pq => Forall_cons (f, ta, p) (aux p) (auxl pq)
          end) pl)
    | pattern_or p1 p2 => or p1 p2 (aux p1) (aux p2)
    end.

Lemma pattern_variables_record:
  forall x,
  concat (map (fun x : string * list type * pattern =>
    let (_, p) := x in pattern_variables p) x) =
  pattern_variables (pattern_rec x).
Proof.
  induction x as [|[[f τl] p] l IHl]; [reflexivity|].
  simpl. f_equal. exact IHl.
Qed.

Lemma pattern_variables_subst:
  forall p tae,
    pattern_variables p =
    pattern_variables (subst_pattern p tae).
Proof.
  induction p as [x ty|ty|pl IH|c ta p IH| |] using pattern_rect.
  * intro tae. simpl. reflexivity.
  * intro tae. simpl. reflexivity.
  * intro tae.
    induction pl as [|p pl IHpl]; [reflexivity|].
    rewrite pattern_variables_pattern_tuple.
    simpl subst_pattern.
    rewrite pattern_variables_pattern_tuple.
    inversion IH; subst. f_equal; [firstorder|].
    apply IHpl. assumption.
  * intro tae. simpl. firstorder.
  * intros.
    induction x; [reflexivity|].
    inverse x0. specialize (IHx X0). clear X0.
    destruct a as [[? ?] z].
    specialize(X tae).
    simpl. f_equal; assumption.
  * intro tae. simpl. firstorder.
Qed.

Lemma wf_subst_pattern:
  forall p tau tae,
    well_formed_pattern s p tau ->
    well_formed_pattern s (subst_pattern p tae) (subst tau tae).
Proof.
  induction p as [x ty| ty | pl IH | c ta p IH| |p1 p2 IH1 IH2] using pattern_rect.
  - intros tau tae H.
    inversion H; subst.
    econstructor.
  - intros tau tae H.
    inversion H; subst.
    econstructor.
  - intros tau tae H.
    revert pl tau H IH. induction pl as [|p pl IHpl].
    + intros tau H IH. inversion H; subst. simpl. constructor.
    + intros tau H IH. inversion H; subst. simpl. constructor.
      inversion IH; subst. now apply H4.
      enough(
        well_formed_pattern s (subst_pattern (pattern_tuple pl) tae)
        (subst (Prod tyl) tae)) by assumption.
      apply IHpl. assumption. inversion IH; subst. assumption.
    clear - H5. intro x; specialize (H5 x). contradict H5.
    destruct H5 as [A B].
    rewrite (eq_refl:
      pattern_tuple (map _ pl) =
      subst_pattern (pattern_tuple _) _) in B.
      rewrite <- pattern_variables_subst in A.
      rewrite <- pattern_variables_subst in B.
      split;assumption.
  - intros tau tae H.
    inversion H; subst.
    simpl. constructor 5 with (c_ta:=c_ta) (c_in:=c_in);
      [|assumption|now rewrite length_map].
    enough(well_formed_pattern s (subst_pattern p tae)
      (subst' c_in c_ta (subst_list_type ta tae))) by assumption.
    rewrite subst_comm.
    2: now symmetry.
    2:{ destruct wf_s as [[_ [wfc _]] _ _].
      specialize(wfc c); rewrite H5 in wfc. assumption. }
    now apply IH.
  - induction x; [inversion 1|]; intros.
    inverse x0. specialize (IHx X0). clear X0.
    destruct a as [[f ?] z].
    inversion H; subst.
    + simpl. clear IHx.
      apply pattern_rec_one_wf with (f_ta:=f_ta) (f_in:=f_in); [assumption|..].
      rewrite length_map; assumption.
      rewrite(eq_refl:map (fun ty => subst ty tae) l = subst_list_type l tae).
      rewrite subst_comm.
      now apply X. symmetry. assumption.
      destruct wf_s as [_ [_ [wff _]] _ _].
      specialize(wff f); rewrite H5 in wff. assumption.
    + simpl. apply pattern_rec_next_wf.
      inverse H5; [|inverse H9].
      apply pattern_rec_one_wf with (f_ta :=f_ta) (f_in:=f_in); [assumption|..].
      rewrite length_map; assumption.
      rewrite(eq_refl:map _ l = subst_list_type l _).
      rewrite subst_comm.
      now apply X. symmetry. assumption.
      destruct wf_s as [_ [_ [wff _]] _ _].
      specialize(wff f); rewrite H3 in wff. assumption.
      now apply IHx.
      rewrite <- pattern_variables_subst.
      assert(G:=@eq_refl _ (subst_pattern (pattern_rec x) tae)).
      simpl subst_pattern in G at 1; rewrite G; clear G.
      rewrite <- pattern_variables_subst. assumption.
  - intros τ tae H. inversion_clear H.
    apply IH1 with (tae:=tae) in H0. apply IH2 with (tae:=tae) in H1.
    simpl subst_pattern.
    enough(H:forall p tae,
      map (fun (z:string * type) => let (x, y) := z in (x, subst y tae))
        (pattern_variables_with_type p) =
      pattern_variables_with_type (subst_pattern p tae)).
      constructor; [eassumption..|]. rewrite <- 2 H.
      apply sameList_map with (1:=H2).
    clear; intros p tae.
    induction p as [x ty|ty|pl IH|c ta p IH| |] using pattern_rect; auto.
    + induction pl; [auto|]; simpl.
      rewrite map_app; f_equal.
      * inversion_clear IH; assumption.
      * inversion_clear IH. 
        exact(IHpl (ltac:(assumption))).
    + induction x as [|[[f τl] p] pl IHpl]; [auto|]; simpl.
      rewrite map_app; f_equal.
      * inversion_clear x0; assumption.
      * inversion_clear x0. 
        exact(IHpl (ltac:(assumption))).
Qed.

Lemma extEnv_subst_env:
  forall p e tae,
    extEnv (subst_pattern p tae) (subst_env e tae) =
    subst_env (extEnv p e) tae.
Proof.
  induction p as [x ty|ty| |p pl IHp IHpl|c ta p IH| |p1 p2 IH1 _] using pattern_ind;
      try reflexivity.
  - intros. simpl subst_pattern.
    assert(H:
      forall p pl e,
        extEnv (pattern_tuple (p :: pl)) e =
        extEnv (pattern_tuple pl) (extEnv p e)) by trivial.
    rewrite 2 H.
    rewrite IHp.
    rewrite (eq_refl:
      pattern_tuple (map (fun y => subst_pattern y tae) pl) =
      subst_pattern (pattern_tuple pl) tae).
    rewrite IHpl.
    reflexivity.
  - intros. simpl. apply IH.
  - induction x; [reflexivity|].
    apply Forall_cons_iff in x0.
    destruct x0 as [Ha Hx].
    specialize(IHx Hx). clear Hx.
    destruct a as [[? ?] z]. intros.
    simpl subst_pattern.
    assert(A:
    extEnv (pattern_rec ((s0,subst_list_type l tae,subst_pattern z tae) :: (map (fun y => let '(f,ta,p):=y in (f, subst_list_type ta tae,
      subst_pattern p tae)) x))) (subst_env e tae) =
      extEnv (subst_pattern (pattern_rec x) tae)
      (extEnv (subst_pattern z tae) (subst_env e tae))) by reflexivity.
    rewrite A. clear A. rewrite Ha. apply IHx.
  - intros e tae. simpl. apply IH1.
Qed.

Lemma wf_subst:
  forall t e tau tae,
    well_formed_term s e t tau ->
    well_formed_term s (subst_env e tae) (subst_term t tae) (subst tau tae).
Proof.
  set (Ps:=fun sk => forall e tau tae, well_formed_skel s e sk tau ->
  well_formed_skel s (subst_env e tae) (subst_skel sk tae) (subst tau tae)).
  induction t using term_rect with (Ps:=Ps).
  - intros.
    inversion H; subst.
    + eapply term_var_val_wf. simpl.
      now apply findEnv_subst_env_some.
    + eapply term_var_unspec_wf; [eassumption|..].
      rewrite <- subst_list_type_length. assumption.
      apply subst_comm. assumption.
      destruct wf_s as [_ _ [_ wft] _].
      now apply (wft x ta ty).
    + simpl.
      eapply term_var_spec_wf.
      eassumption.
      rewrite <- subst_list_type_length. assumption.
      rewrite <- subst_comm.
      reflexivity.
      assumption.
      destruct wf_s as [_ _ _ [_ wft]].
      eapply wft. eassumption.
  - intros.
    inversion H; subst. simpl subst_term.
    assert (G:subst (Base c_out ta) tae = Base c_out
    (subst_list_type ta tae)) by reflexivity.
    rewrite G.
    eapply term_constructor_wf.
    rewrite <- subst_list_type_length. exact H4.
    rewrite subst_comm.
    eapply IHt.
    exact H6.
    exact H4.
    destruct wf_s as [[_ [cf _]] _].
    specialize(cf c).
    now rewrite H7 in cf.
    exact H7.
  - intros.
    inversion H0; subst.
    simpl.
    constructor.
    clear - H H3. revert tyl H3.
    induction lt as [|t tl]; destruct tyl as [|ty tyl];
        [constructor|inversion 1..|].
    inversion 1; subst.
    inversion H; subst; clear H.
    specialize(IHtl H5 tyl H6).
    simpl. constructor; [|assumption].
    now apply H2.
  - intros.
    inversion H; subst. simpl.
    econstructor. rewrite extEnv_subst_env.
    eapply IHt. eassumption.
    now apply wf_subst_pattern.
  - intros. inverse H. simpl.
    rewrite <- subst_comm.
    + apply term_field_wf with (f_out:=f_out).
      * unfold subst_list_type. rewrite length_map. assumption.
      * specialize (IHt e (Base f_out ta) tae). now apply IHt.
      * assumption.
    + assumption.
    + destruct wf_s as [_ [_ [ff _]] _].
      specialize(ff f).
      now rewrite H7 in ff.
  - intros. inverse H. simpl.
    apply term_nth_wf.
    * specialize (IHt e (Prod ta) tae). now apply IHt.
    * apply map_nth_error with (f:=fun y => subst y tae) in H6.
      assumption.
  - intros. inverse H. simpl. constructor.
    + intros. rewrite map_map.
      specialize(H1 x H f_ta f_in H0).
      apply in_map_iff in H1. destruct H1 as [x0 Hx0].
      apply in_map_iff. exists x0.
      destruct x0 as [[f ?] ?]. exact Hx0.
    + destruct tl; [destruct H2; reflexivity|discriminate].
    + clear H1.
      revert e tae f_out tyl H4 X. induction tl.
      * intros. simpl. constructor.
      * intros.
        inverse H4.
        inverse X.
        simpl in X0.
        simpl. apply record_cons_wf with (f_ta:=f_ta) (f_in:=f_in).
        -- assumption.
        -- rewrite length_map. assumption.
        -- simpl. fold (subst_list_type tyl tae).
           simpl.
           rewrite -> subst_comm.
           ++ now apply X0.
           ++ assumption.
           ++ destruct wf_s as [_ [_ [ff _]] _].
              specialize(ff f).
              now rewrite H6 in ff.
        -- destruct tl; [constructor|].
        exact(IHtl (ltac:(discriminate)) e tae f_out tyl H9 X1).
  - intros.
    inverse H.
    simpl. constructor. 
    + rewrite (eq_refl:Base f_out (map (fun ty => subst ty tae) tyl) = subst (Base f_out tyl) tae).
      now apply IHt.
    + clear H3 IHt t.
      revert e tae f_out tyl H5 X. induction tl.
      * intros. simpl. constructor.
      * intros.
        inverse H5.
        inverse X.
        specialize(IHtl e tae f_out tyl H8 X1). clear X1.
        simpl in X0.
        simpl. apply record_cons_wf with (f_ta:=f_ta) (f_in:=f_in).
        -- assumption.
        -- rewrite length_map. assumption.
        -- simpl. fold (subst_list_type tyl tae).
           simpl.
           rewrite -> subst_comm.
           ++ now apply X0.
           ++ assumption.
           ++ destruct wf_s as [_ [_ [ff _]] _].
              specialize(ff f).
              now rewrite H4 in ff.
        -- assumption.
  - intros f tau tae H'.
    inversion H'; subst.
    simpl. constructor.
    clear - H4 H. induction l; [constructor|].
    inversion H; inversion H4; subst.
    constructor; firstorder.
  - intros f tau tae H.
    inversion H; subst.
    simpl; econstructor 3 with (tyi:=subst tyi tae).
    + rewrite(eq_refl:
        Arrow (subst tyi tae) (subst tau tae) =
        subst (Arrow tyi tau) tae).
      now apply IHt1.
    + now apply IHt2.
  - intros e tau tae H. simpl.
    econstructor. apply IHt. inversion_clear H. assumption.
  - intros e tau tae H. simpl. inversion_clear H.
    econstructor.
    + apply IHt. eassumption.
    + apply Forall_map. apply Forall_impl with (2:=H1).
      clear. intros [p sk]. simpl. apply wf_subst_pattern.
    + revert Ps X H2. induction psl; intros.
      * constructor.
      * inverse H2. inverse H1. inverse X.
        unfold P''s in X0. simpl in X0.
        simpl. constructor.
        -- destruct a as [p s]. simpl.
          unfold Ps in X0. rewrite extEnv_subst_env.
          now apply X0.
        -- apply IHpsl; assumption.
  - intros e tau tae H. simpl. inversion_clear H.
    econstructor.
    + apply IHt. eassumption.
    + apply wf_subst_pattern. assumption.
    + rewrite extEnv_subst_env. apply IHt0. assumption.
  - intros e tau tae H. simpl. inversion_clear H.
    econstructor.
Qed.

Lemma findEnv_In:
  forall {A} l s (x:A),
  findEnv s l = Some x ->
  In (s, x) l.
Proof.
  induction l.
  - inversion 1.
  - intros. simpl in H.
    destruct (fst a =? s0) eqn:B.
    + destruct a as [s y]. simpl in *.
      injection H as ->.
      apply eqb_eq in B. subst.
      now left.
    + right. firstorder.
Qed.

Lemma pattern_variables_with_type_first:
  forall p,
  map fst (pattern_variables_with_type p) =
  (pattern_variables p).
Proof.
  induction p as [x ty|ty| |p pl IHp IHpl|c ta p IH | pl IHpl|p1 p2 IH1 IH2] using pattern_ind.
  (* pattern_var *)
  - reflexivity.
  (* pattern_wildcard  *)
  - reflexivity.
  (* pattern_tuple [] *)
  - reflexivity.
  (* pattern_tuple (_ :: _) *)
  - rewrite pattern_variables_pattern_tuple.
    rewrite pattern_variables_with_type_pattern_tuple.
    rewrite map_app. f_equal; assumption.
  (* pattern_constr *)
  - assumption.
  (* pattern_rec *)
  - induction pl as [|[[f τl] p] pq IHpq]; [reflexivity|].
    apply Forall_cons_iff in IHpl.
    destruct IHpl as [Hp Hpq].
    specialize(IHpq Hpq); clear Hpq.
    rewrite pattern_variables_with_type_pattern_rec.
    rewrite pattern_variables_pattern_rec.
    rewrite map_app. f_equal; assumption.
  (* pattern_or *)
  - simpl; assumption.
Qed.

Lemma extEnv_not_pattern_variables:
  forall x e p,
  ~In x (pattern_variables p) ->
  findEnv x (extEnv p e) = findEnv x e.
Proof.
  intros n e p H; revert H e.
  induction p as [x ty|ty| |p pl IHp IHpl|c ta p IH | pl IHpl|p1 p2 IH1 IH2] using pattern_ind; try auto; intros H e.
  - simpl in H.
    assert(ne:x <> n) by (contradict H; auto); clear H.
    simpl. rewrite <- eqb_neq in ne; rewrite ne; clear ne.
    reflexivity.
  - rewrite extEnv_pattern_tuple.
    rewrite pattern_variables_pattern_tuple in H.
    rewrite in_app_iff in H.
    apply Decidable.not_or in H.
    destruct H as [Hp Hpl].
    rewrite IHpl; [rewrite IHp|apply Hpl]; [reflexivity|apply Hp].
  - revert e; induction pl as [|[[f τl] p] pq IHpq]; [reflexivity|]; intro e.
    rewrite extEnv_pattern_rec.
    rewrite pattern_variables_pattern_rec in H.
    rewrite in_app_iff in H.
    apply Decidable.not_or in H.
    destruct H as [not_in_p not_in_pq].
    apply Forall_cons_iff in IHpl.
    destruct IHpl as [Hp Hpq].
    specialize(IHpq Hpq not_in_pq); clear Hpq not_in_pq.
    specialize(Hp not_in_p); clear not_in_p.
    rewrite IHpq, Hp. reflexivity.
Qed.

Lemma extEnv_pattern_variables:
  forall p x τ τ' e,
  well_formed_pattern s p τ' ->
  In (x, τ) (pattern_variables_with_type p) ->
  findEnv x (extEnv p e) = Some τ.
Proof.
  induction p as [x ty|ty| |p pl IHp IHpl|c ta p IH | pl IHpl|p1 p2 IH1 IH2] using pattern_ind; try auto; intros n τ τ' e wf Hn; try (destruct Hn; fail).
  - destruct Hn as [Hn|[]]. injection Hn as -> ->. simpl.
    rewrite eqb_refl; reflexivity.
  - rewrite extEnv_pattern_tuple.
    rewrite pattern_variables_with_type_pattern_tuple in Hn.
    rewrite in_app_iff in Hn.
    destruct Hn as [Hnp|Hnpl]; [clear IHpl|clear IHp].
    + inversion wf; subst; clear wf.
      rewrite extEnv_not_pattern_variables; [eapply IHp;eassumption|].
      specialize(H4 n). contradict H4.
      split; [|assumption].
      rewrite <- pattern_variables_with_type_first.
      rewrite(eq_refl:n = fst (n, τ)).
      now apply in_map.
    + inversion wf; subst; clear wf.
      eapply IHpl; eassumption.
  - inversion wf; subst; clear wf.
    simpl. eapply IH; [eassumption|].
    simpl in Hn. assumption.
  - revert e; induction pl as [|[[f τl] p] pq IHpq]; intro e; [inversion wf|].
    apply Forall_cons_iff in IHpl.
    destruct IHpl as [IHp Hpq].
    specialize(IHpq Hpq); clear Hpq.
    inversion wf; subst; clear wf.
    + simpl in *. clear IHpq. rewrite app_nil_r in Hn.
      specialize(IHp _ _ _ e H6 Hn); assumption.
    + rewrite extEnv_pattern_rec.
      rewrite pattern_variables_with_type_pattern_rec in Hn.
      apply in_app_iff in Hn.
      destruct Hn as [Hnp|Hnpq]; [clear IHpq|clear IHp].
      * inversion H4; subst; clear H4.
        -- rewrite extEnv_not_pattern_variables; [eapply IHp; eassumption|].
           specialize(H6 n); contradict H6.
           split; [|assumption].
           rewrite <- pattern_variables_with_type_first.
           rewrite(eq_refl:n = fst (n, τ)).
           now apply in_map.
        -- inversion H8.
      * apply IHpq; assumption.
  - simpl in Hn. simpl. inversion wf; subst; clear wf. firstorder.
Qed.

Lemma extEnv_pattern_or:
  forall e p1 p2 τ,
  well_formed_pattern s (pattern_or p1 p2) τ ->
  forall x, findEnv x (extEnv p1 e) = findEnv x (extEnv p2 e).
Proof.
  intros e p1 p2 τ wf n.
  assert(sLτ:
    sameList (pattern_variables_with_type p1) (pattern_variables_with_type p2)
  ) by now inversion_clear wf.
  assert(wf1:well_formed_pattern s p1 τ) by now inversion_clear wf.
  assert(wf2:well_formed_pattern s p2 τ) by now inversion_clear wf.
  clear wf.
  assert(sL:sameList (pattern_variables p1) (pattern_variables p2)).
  { apply sameList_map with (f:=fst) in sLτ.
    rewrite 2 pattern_variables_with_type_first in sLτ.
    assumption. }
  destruct (in_dec string_dec n (pattern_variables p1)) as [i|ni1].
  - rewrite <- pattern_variables_with_type_first in i.
    apply in_map_iff in i.
    destruct i as [[n' τ'] [eq i]]. simpl in eq; subst n'.
    rewrite extEnv_pattern_variables with (1:=wf1) (2:=i).
    apply sLτ in i.
    rewrite extEnv_pattern_variables with (1:=wf2) (2:=i).
    reflexivity.
  - assert(ni2:~In n (pattern_variables p2)) by (contradict ni1; now apply sL).
    rewrite 2 extEnv_not_pattern_variables; [reflexivity|assumption..].
Qed.

Lemma type_cval_record_field:
  forall v vl f f_τl f_in f_out τl ,
    type_cvalue (cval_record vl) (Base f_out τl) ->
    findEnv f vl = Some v ->
    find f (s_ftype s) = Some (f_τl, f_out, f_in) ->
    type_cvalue v (subst' f_in f_τl τl).
Proof.
  intros.
  destruct H as [[|] H]; [destruct H|].
  inversion H; subst; clear H.
  apply Forall_forall with (x:=(f, v)) in H6.
  - rewrite H1 in H6.
    destruct H6 as [_ H6].
    exists n. assumption.
  - apply findEnv_In; assumption.
Qed.

Lemma extEnv_ok:
  forall p v ty e g e',
  env_ok type_cvalue g e ->
  well_formed_pattern s p ty ->
  add_asn e p v = Some e' ->
  type_cvalue v ty ->
  env_ok type_cvalue (extEnv p g) e'.
Proof.
  induction p as [x ty|ty| |p pl IHp IHpl|c ta p IH | pl IHpl|p1 p2 IH1 IH2]
      using pattern_ind; intros v τ e g e' ok wf add Hv n.
  - simpl in add. injection add as <-.
    inversion_clear wf. simpl.
    destruct (eqb x n); [assumption|]. apply ok.
  - inversion_clear wf. simpl in add; injection add as <-.
    simpl. apply ok.
  - inversion_clear wf.
    assert(H:e = e') by
      (destruct v; try destruct l; simpl in add; try congruence).
    subst e'. simpl. apply ok.
  - rewrite extEnv_pattern_tuple.
    inversion wf; subst; clear wf.
    apply add_asn_pattern_tuple in add.
    destruct add as [e'' [w [wl [-> [He'' Hvlwl]]]]].
    apply type_cval_tuple_forall in Hv.
    apply Forall2_cons_iff in Hv.
    destruct Hv as [Hw Hwl].
    apply type_cval_tuple_forall in Hwl.
    eapply IHpl; [|eassumption..].
    eapply IHp; [|eassumption..].
    assumption.
  - simpl in add.
    destruct v; try congruence.
    destruct (c=?s0) eqn:H; try congruence.
    apply eqb_eq in H; subst s0.
    simpl.
    inversion wf; subst; clear wf.
    apply type_cval_constr_destr in Hv.
    destruct Hv as [? [? [? [Hfsig Hv]]]].
    rewrite H4 in Hfsig; injection Hfsig as <- <- <-.
    eapply IH; [|eassumption..].
    assumption.
  - revert n.
    enough(A:env_ok type_cvalue (extEnv (pattern_rec pl) g) e') by assumption.
    revert IHpl e g e' ok wf add. induction pl as [|[[f ta] p] pq IHpq];
      intros IHpl e g e' ok wf add; [inversion wf|].
    apply Forall_cons_iff in IHpl.
    destruct IHpl as [IHp Hpq].
    specialize(IHpq Hpq); clear Hpq.
    assert(H:pq = [] \/ well_formed_pattern s (pattern_rec pq) τ).
    { destruct pq; [now left|right; now inversion wf]. }
    destruct H as [->|wfpq].
    + intro n.
      inversion wf; subst. 2:{ inversion H5. }
      destruct v; simpl in add; try congruence.
      destruct (findEnv f l) eqn:Hf; [|congruence].
      destruct (add_asn e p c) eqn:He'; [|congruence].
      injection add as ->.
      simpl. eapply IHp; try eassumption.
      simpl in Hv.
      eapply type_cval_record_field; eassumption.
    + intro n.
      rewrite extEnv_pattern_rec.
      apply add_asn_pattern_rec in add.
      destruct add as [e'' [w [vl [-> [Hf [He'' He']]]]]].
      eapply IHpq; [|eassumption..].
      inversion wf; subst. inversion wfpq.
      inversion H4; subst. 2:{ inversion H8. }
      eapply IHp; try eassumption.
      eapply type_cval_record_field; eassumption.
  - inversion wf; subst.
    simpl in add.
    destruct (add_asn e p1 v) eqn:Hp1v.
    + injection add as <-. simpl.
      eapply IH1; eassumption.
    + simpl. rewrite extEnv_pattern_or with (1:=wf).
      eapply IH2; eassumption.
Qed.

Lemma wf_apply_last:
 forall l i a o,
 well_formed_apply i (app l [a]) o <->
 well_formed_apply i l (Arrow a o).
Proof.
  induction l; intros; split.
  - inversion 1; subst.
    inversion H4; subst.
    constructor.
  - inversion 1; subst.
    constructor. constructor.
  - inversion 1; subst.
    apply IHl in H4.
    constructor. assumption.
  - inversion 1; subst.
    apply IHl in H4.
    simpl. constructor. assumption.
Qed.

Lemma wf_apply_inj:
 forall l1 l2 i o1 o2,
 length l1 = length l2 ->
 well_formed_apply i l1 o1 ->
 well_formed_apply i l2 o2 ->
 l1 = l2 /\ o1 = o2.
Proof.
  induction l1 as [|a1 l1]; destruct l2 as [|a2 l2]; [|inversion 1..|].
  - split; [reflexivity|].
    inversion H0; inversion H1; subst. reflexivity.
  - intros i o1 o2 Hlen wf1 wf2.
    injection Hlen as Hlen.
    inversion wf1; inversion wf2; subst.
    injection H5 as -> ->.
    destruct(IHl1 l2 ν o1 o2); [assumption..|].
    subst; split; reflexivity.
Qed.

(*
Lemma type_rec_cons_l_inv:
  forall vl f t tl f_out tyl,
  type_record ((f, t) :: tl) vl (Base f_out tyl) ->
  exists f_ta f_in,
    find f (s_ftype s) = Some (f_ta, f_out, f_in) /\
    length f_ta = length tyl /\
    type_ext_term t (subst' f_in f_ta tyl) /\
    type_record tl vl (Base f_out tyl).
Proof.
  induction vl.
  - intros. inverse H. repeat eexists; eassumption.
  - intros. inverse H.
    + repeat eexists; eassumption.
    + specialize (IHvl f t tl f_out tyl H8).
      destruct IHvl as (f_ta'&f_in'&A&B&C&D).
      exists f_ta', f_in'.
      repeat split; [assumption..|].
      apply type_rec_cons_r with (f_ta:=f_ta) (f_in:=f_in); assumption.
Qed.

Lemma type_rec_cons_r_inv:
  forall tl vl f v f_out tyl,
  type_record tl ((f, v) :: vl) (Base f_out tyl) ->
  exists f_ta f_in,
    find f (s_ftype s) = Some (f_ta, f_out, f_in) /\
    length f_ta = length tyl /\
    type_cvalue v (subst' f_in f_ta tyl) /\
    type_record tl vl (Base f_out tyl).
Proof.
  induction tl.
  - intros. inverse H. do 2 eexists; repeat split; eassumption.
  - intros. inverse H.
    + specialize (IHtl vl f v f_out tyl H8).
      destruct IHtl as (f_ta'&f_in'&A&B&C&D).
      exists f_ta', f_in'.
      repeat split; [assumption..|].
      apply type_rec_cons_l with (f_ta:=f_ta) (f_in:=f_in); assumption.
    + do 2 eexists; repeat split; eassumption.
Qed.

Lemma type_rec_app_end_r:
  forall vl v tyl f f_ta f_out f_in tl,
    find f (s_ftype s) = Some (f_ta, f_out, f_in) ->
    length f_ta = length tyl ->
    type_cvalue v (subst' f_in f_ta tyl) ->
    type_record tl vl (Base f_out tyl) ->
    type_record tl (vl ++ [(f, v)]) (Base f_out tyl).
Proof.
  induction vl.
  - intros. simpl.
    apply type_rec_cons_r with (f_ta:=f_ta) (f_in:=f_in); assumption.
  - intros. specialize (IHvl v tyl f f_ta f_out f_in tl H H0 H1).
    destruct a as [f' v']. simpl.
    apply type_rec_cons_r_inv in H2. destruct H2 as (f_ta'&f_in'&A&B&C&D).
    apply type_rec_cons_r with (f_ta:=f_ta') (f_in:=f_in'); [assumption..|].
    firstorder.
Qed.
 *)

Lemma type_cvalue_rec:
  forall tl f_out tyl,
  (forall x, In x (s_field s) ->
    forall f_ta f_in,
    find x (s_ftype s) = Some (f_ta, f_out, f_in) ->
    In x (map (fun y => match y with (f, _) => f end) tl)) ->
  Forall (fun '(f, v) =>
    match find f (s_ftype s) with
    | None => False
    | Some (f_ta, f'_out, f_in) =>
        f_out = f'_out /\ type_cvalue v (subst' f_in f_ta tyl)
    end) tl ->
  type_cvalue (cval_record tl) (Base f_out tyl).
Proof.
  intros.
  enough(A:exists n, Forall (fun '(f, v) =>
    match find f (s_ftype s) with
    | Some (f_ta, f'_out, f_in) =>
            f_out = f'_out /\ type_cvalue_n n v (subst' f_in f_ta tyl)
    | None => False
    end) tl).
  - destruct A as [n A].
    exists (S n). constructor; [assumption|].
    clear - A. revert tl A.
    induction tl; [constructor|].
    intro A. inverse A.
    constructor; [|firstorder].
    clear H2 IHtl. destruct a as [f v].
    destruct (find f (s_ftype s)); [|assumption].
    destruct p as [[? ?] ?].
    destruct H1. split; assumption.
  - clear H. revert tl H0; induction tl.
    + exists 0. constructor.
    + intro A. inverse A.
      specialize (IHtl H2).
      destruct IHtl as [n IHtl].
      destruct a as [f v].
      destruct (find f (s_ftype s)) eqn:A; [|inverse H1].
      destruct p as [[f'_ta f'_out] f'_in].
      destruct H1 as [<- [m B]].
      exists (max m n). constructor.
      -- rewrite A. split; [reflexivity|].
         apply type_cvalue_inc' with (m:=m); [|assumption].
         apply PeanoNat.Nat.le_max_l.
      -- apply Forall_forall.
         intros.
         apply Forall_forall with (1:=IHtl) in H.
         destruct x as [f' v'].
         destruct (find f' (s_ftype s)) eqn:A'; [|inverse H].
         destruct p as [[f''_ta f''_out] f''_in].
         destruct H as [C D].
         split; [assumption|].
         apply type_cvalue_inc' with (m:=n); [|assumption].
         apply PeanoNat.Nat.le_max_r.
Qed.

Lemma Forall2_rev:
  forall A B (P:A -> B -> Prop) l1 l2,
  Forall2 P l1 l2 -> Forall2 P (rev l1) (rev l2).
Proof.
  induction l1; destruct l2; [constructor|inversion 1..|].
  intro H; inverse H.
  simpl. apply Forall2_app; [firstorder|].
  constructor; [assumption|constructor].
Qed.

Lemma map_fst_combine:
  forall A B (x:list A) (y:list B),
    length x = length y ->
    map fst (combine x y) = x.
Proof.
  induction x; destruct y; [reflexivity|inversion 1..|].
  intros H; inverse H. simpl.
  f_equal; firstorder.
Qed.

Lemma ext_term_tuple_ss_length:
  forall tl tl',
  interp_term_ss s u (ext_term_tuple tl) (ext_term_tuple tl') ->
  length tl = length tl'.
Proof.
  induction tl; [inversion 1|].
  destruct tl'; [inversion 1|].
  intros A; inverse A; [reflexivity|].
  simpl. f_equal. firstorder.
Qed.

Lemma type_rec_imp_type_tuple:
  forall l ty,
    type_record l ty ->
    exists tyl,
      type_ext_term (ext_term_tuple (map snd l)) (Prod tyl).
Proof.
  induction l.
  - intros. exists []. repeat constructor.
  - intros ty A.
    specialize (IHl ty).
    inverse A.
    specialize (IHl H5).
    destruct IHl as [tyl' IHl].
    exists ((subst' f_in f_ta tyl) :: tyl').
    simpl. constructor.
    constructor; [assumption|].
    now inverse IHl.
Qed.

Lemma env_ok_forall':
  forall P e g,
  env_ok P g e <->
  Forall (fun '(x, _) =>
    match findEnv x g, findEnv x e with
    | None, None => True
    | Some τ, Some v => P v τ
    | _, _ => False
    end) e /\
  Forall (fun '(x, _) =>
    match findEnv x g, findEnv x e with
    | None, None => True
    | Some τ, Some v => P v τ
    | _, _ => False
    end) g.
Proof.
  intros P e g; split.
  - intros ok; split.
    + apply Forall_forall.
      intros [x _] _.
      apply ok.
    + apply Forall_forall.
      intros [x _] _.
      apply ok.
  - intros [He Hg].
    intro x.
    destruct (findEnv x g) eqn:Hxg.
    + clear He. assert(Hin:=Hxg).
      apply findEnv_In in Hin.
      assert(A:=proj1 (Forall_forall _ _) Hg (x, t) Hin).
      simpl in A. rewrite Hxg in A. exact A.
    + destruct (findEnv x e) eqn:Hxe.
      * clear Hg.
        assert(A:=proj1 (Forall_forall _ _) He (x, c)).
        simpl in A. rewrite Hxe, Hxg in A.
        apply findEnv_In in Hxe. now apply A.
      * trivial.
Qed.

Lemma env_ok_forall:
  forall P e g,
  env_ok P g e <->
  Forall (fun x =>
    match findEnv x g, findEnv x e with
    | None, None => True
    | Some τ, Some v => P v τ
    | _, _ => False
    end) (map fst e ++ map fst g)%list.
Proof.
  intros.
  rewrite env_ok_forall'.
  rewrite Forall_app. rewrite 2 Forall_map.
  split; intros [A B]; (split; [clear B|clear A]).
  apply Forall_impl with (2:=A). intros [x v]; simpl. auto.
  apply Forall_impl with (2:=B). intros [x v]; simpl. auto.
  apply Forall_impl with (2:=A). intros [x v]; simpl. auto.
  apply Forall_impl with (2:=B). intros [x v]; simpl. auto.
Qed.

Lemma env_ok_n:
  forall e g,
  env_ok type_cvalue g e <->
  exists n : nat, env_ok (type_cvalue_n n) g e.
Proof.
  intros.
  split.
  - intro H.
    apply env_ok_forall in H.
    enough(A:exists n,
        Forall (fun x : string =>
         match findEnv x g with
         | Some τ =>
             match findEnv x e with
             | Some v => type_cvalue_n n v τ
             | None => False
             end
         | None => match findEnv x e with
                   | Some _ => False
                   | None => True
                   end
         end) (map fst e ++ map fst g)).
    { destruct A as [n A]; exists n; now apply env_ok_forall. }
    revert H. generalize (map fst e ++ map fst g)%list.
    induction l as [|x l IHl]; [exists 0; constructor|].
    intros H. apply Forall_cons_iff in H.
    destruct H as [H Hl].
    specialize(IHl Hl). clear Hl.
    destruct IHl as [n IHl].
    destruct (findEnv x g) as [τ|] eqn:Hxg;
        destruct (findEnv x e) as [v|] eqn:Hxe; try (destruct H; fail).
    + destruct H as [m H].
      apply type_cvalue_inc' with (n:=(max m n)) in H;
                [|apply PeanoNat.Nat.le_max_l].
      exists (max m n).
      constructor; [rewrite Hxg, Hxe; assumption|].
      apply Forall_impl with (2:=IHl). clear.
      intros.
      destruct (findEnv a g);
      destruct (findEnv a e);
      [|destruct H..|trivial].
      apply type_cvalue_inc' with (m:=n); [apply PeanoNat.Nat.le_max_r|].
      assumption.
    + exists n.
      constructor; [|assumption].
      rewrite Hxg, Hxe; trivial.
  - intros.
    destruct H as [n H].
    apply env_ok_forall in H. apply env_ok_forall.
    apply Forall_impl with (2:=H).
    clear H.
    intros a H.
    destruct (findEnv a g) as [τ|] eqn:Hag;
    destruct (findEnv a e) as [v|] eqn:Hae;
    [|destruct H..|trivial].
    now exists n.
  Qed.







Scheme ivss_ind := Induction for interp_term_ss Sort Prop.
Scheme isss_ind := Induction for interp_skel_ss Sort Prop.






(* Now we prove the subject reduction:
   if a term has type t, then its reduction also has type t *)

Theorem subject_reduction_term:
  forall v v' ty,
    type_ext_term v ty ->
    interp_term_ss s u v v' ->
    type_ext_term v' ty.
Proof.
  intros v v' ty Htype Hinterp.
  revert Hinterp ty Htype.
  (* We prove the property for every possible reduction step *)
  induction 1 using ivss_ind; subst; intros ?ty Htype.
  (* itss_var_val *)
  - inversion Htype; subst.
    inversion H2; subst.
    destruct H as [envok H].
    inversion H; subst.
    apply env_ok_destr with (x:=x) in envok.
    destruct envok as [_ envok].
    constructor. firstorder.
  (* itss_var_spec *)
  - inversion Htype; subst.
    inversion H2; subst.
    destruct H as [envok H].
    inversion H; subst.
    rewrite e0 in H5.
    injection H5 as <- <- <-.
    constructor.
    exists []. split; [constructor|].
    rewrite (eq_refl:[] = subst_env' [] ta tyl) at 1.
    apply wf_subst.
    clear - e0.
    destruct wf_s as [_ _ _ [_ wfv]].
    now apply (wfv x ta τ t).
  (* itss_var_unspec_0 *)
  - inversion Htype; subst.
    destruct H2 as [g [envok H]].
    inversion H; subst.
    constructor.
    assert(uok:=u_ok x).
    rewrite H4 in uok.
    rewrite e0 in uok.
    destruct uok as [_ [_ uok]].
    specialize(uok ta [] w v0).
    destruct uok as [ty'l [ty'o [take [_ G]]]].
    inversion take; subst. assumption.
  (* itss_var_unspec_S *)
  - inversion Htype; subst.
    destruct H2 as [g [envok H]].
    inversion H; subst.
    constructor.
    assert(uok:=u_ok x).
    exists 1.
    econstructor.
    + econstructor; [eassumption..|reflexivity].
    + constructor.
    + constructor.
  (* itss_constr_embed *)
  - inversion Htype; subst.
    inversion H2; subst.
    destruct H as [envok H].
    inversion H; subst.
    eapply type_ext_term_constr; [eassumption|].
    econstructor. econstructor.
    split; eassumption.
  (* itss_constr *)
  - inversion Htype; subst. econstructor; [eassumption|].
    apply IHHinterp. assumption.
  (* itss_constr_extract *)
  - inversion Htype; subst.
    inversion H4; subst.
    constructor.
    destruct H0 as [n H0]. destruct n as [|n]; [inversion H0|].
    exists (S n).
    econstructor; [eassumption|].
    assumption.
  (* itss_tuple_embed *)
  - inversion Htype; subst.
    inversion H2; subst.
    destruct H as [envok H].
    inversion H; subst.
    constructor.
    clear Htype H2 H. revert tyl H3.
    induction tl; intros tyl H; destruct tyl; [constructor|inversion H..|].
    inversion H; subst. simpl map.
    constructor; [|firstorder].
    constructor. econstructor.
    split; eassumption.
  (* itss_tuple_l *)
  - inversion Htype; subst.
    destruct tyl as [|ty tyl]; inversion H0; subst.
    constructor. constructor; [firstorder|assumption].
  (* itss_tuple_r *)
  - inversion Htype; subst.
    destruct tyl as [|ty tyl]; inversion H0; subst.
    assert(A:type_ext_term (ext_term_tuple tl) (Prod tyl)) by now constructor.
    specialize (IHHinterp (Prod tyl) A).
    inversion IHHinterp; subst.
    constructor. constructor; assumption.
  (* itss_tuple_extract *)
  - inversion Htype; subst. clear Htype.
    constructor.
    apply type_cval_tuple_forall.
    revert tyl H0.
    induction vl as [|v vl];
        destruct tyl as [|ty tyl]; [constructor|inversion 1..|].
    inversion 1; subst.
    constructor; [|firstorder].
    now inversion H3.
  (* itss_func *)
  - inversion Htype; subst.
    inversion H2.
    destruct H as [envok H].
    enough(envok':exists n, env_ok (type_cvalue_n n) x e).
    + destruct envok' as [n envok'].
    inversion H; subst.
    constructor.
    exists (S n). constructor 4 with (g:=x); assumption.
    + apply env_ok_n; assumption.
  (* itss_field_embed *)
  - inverse Htype.
    destruct H2 as [x [env_ok wft]].
    inverse wft.
    apply type_ext_term_field with (f_out:=f_out); [assumption|].
    constructor. exists x. split; assumption.
  (* itss_field *)
  - inverse Htype.
    apply type_ext_term_field with (f_out:=f_out); [assumption|].
    firstorder.
  (* itss_field_extract *) 
  - inverse Htype. inverse H3. destruct H0 as [[|n] H0]; inverse H0.
    assert(A:In (f,w) r) by now apply findEnv_In.
    assert(B:=proj1 (Forall_forall _ r) H5 _ A). simpl in B.
    rewrite H1 in B. destruct B as [_ B].
    constructor. now exists n.
  (* itss_nth_embed *)
  - inverse Htype.
    destruct H2 as [x [env_ok wft]].
    inverse wft.
    apply type_ext_term_nth with (tyl:=ta); [assumption|].
    constructor. exists x. split; assumption.
  (* itss_nth *)
  - inverse Htype.
    apply type_ext_term_nth with (tyl:=tyl); [assumption|].
    firstorder.
  (* itss_nth_extract *) 
  - inverse Htype. inverse H3.
    destruct H0 as [[|m] H0]; inverse H0.
    constructor. exists m.
    revert n tl tyl w ty e H1 H3. clear.
    induction n.
    + intros tl tyl w ty A B all.
      destruct tl; inverse A.
      destruct tyl; inverse B.
      now inverse all.
    + intros tl tyl w ty A B all.
      destruct tl; inverse A.
      destruct tyl; inverse B.
      inverse all. clear H4.
      specialize(IHn tl tyl w ty H0 H1 H6). exact IHn.
  (* itss_rec_embed *)
  - inverse Htype. destruct H2 as [x [eok A]].
    inverse A.
    constructor.
    + intros.
      specialize(H0 x0 H f_ta f_in H2).
      apply in_map_iff in H0. destruct H0 as [[[f ?] t] [<- Hin]].
      apply in_map_iff. exists (f, cont_term e t).
      split; [reflexivity|]. apply in_map_iff.
      exists (f, l, t); now split.
    + clear H0 H1.
      revert ftl x e eok H3. induction ftl.
      * intros. constructor.
      * intros. inverse H3.
        specialize(IHftl x e eok H8).
        apply type_rec_cons with (f_ta:=f_ta) (f_in:=f_in); try assumption.
        constructor. exists x. split; assumption.
  (* itss_set_embed *)
  - inverse Htype. destruct H2 as [x [eok A]]. inverse A.
    econstructor. constructor. exists x. split; assumption.
    clear H2. revert ftl x e eok H4. induction ftl.
    + intros. constructor.
    + intros. inverse H4.
      specialize(IHftl x e eok H8).
      apply type_rec_cons with (f_ta:=f_ta) (f_in:=f_in); try assumption.
      constructor. exists x. split; assumption.
  (* itss_set_l *)
  - inverse Htype. constructor; [|assumption]. firstorder.
  (* itss_rec *)
  - inverse Htype.
    apply type_ext_term_record.
    rewrite map_fst_combine; [assumption|].
    apply ext_term_tuple_ss_length in Hinterp.
    rewrite length_map. rewrite length_map in Hinterp. assumption.
    revert IHHinterp H1. clear. revert ftl tl'.
    induction ftl; [constructor|]. destruct tl'; [constructor|].
    intros A H.
    specialize(IHftl tl').
    match type of IHftl with | ?H -> _ => assert(C:H) end.
    + intros.
      destruct ty; try (inverse Htype; fail).
      inverse H.
      specialize(A (Prod (subst' f_in f_ta tyl::l))).
      match type of A with | ?H -> _ => assert(C:H) end.
      * inverse Htype.
        constructor. simpl. constructor; assumption.
      * specialize(A C). clear - A. inverse A. constructor.
        inverse H1. assumption.
    + specialize(IHftl C). clear C.
      simpl. inverse H.
      apply type_rec_cons with (f_ta:=f_ta) (f_in:=f_in); try assumption.
      * assert(B:=type_rec_imp_type_tuple _ _ H7).
        destruct B as [tyl' B].
        specialize(A (Prod ((subst' f_in f_ta tyl) :: tyl'))).
        match type of A with | ?H -> _ => assert(C:H) end.
        -- constructor. inverse B. simpl. constructor; assumption.
        -- specialize(A C); clear C. inverse A. inverse H1. assumption.
      * firstorder.
  (* itss_set_r *)
  - inverse Htype.
    apply type_ext_term_rec_set; [assumption|].
    clear v H1.
    revert IHHinterp H3. clear. revert ftl tl'.
    induction ftl; [constructor|]. destruct tl'; [constructor|].
    intros A H.
    specialize(IHftl tl').
    match type of IHftl with | ?H -> _ => assert(C:H) end.
    + destruct ty as [|f_out tyl | |]; try (inverse H; fail).
      intros.
      destruct ty; try (inverse Htype; fail).
      inverse H.
      specialize(A (Prod (subst' f_in f_ta tyl::l))).
      match type of A with | ?H -> _ => assert(C:H) end.
      * inverse Htype.
        constructor. simpl. constructor; assumption.
      * specialize(A C). clear - A. inverse A. constructor.
        inverse H1. assumption.
    + specialize(IHftl C). clear C.
      simpl. inverse H.
      apply type_rec_cons with (f_ta:=f_ta) (f_in:=f_in); try assumption.
      * assert(B:=type_rec_imp_type_tuple _ _ H6).
        destruct B as [tyl' B].
        specialize(A (Prod ((subst' f_in f_ta tyl) :: tyl'))).
        match type of A with | ?H -> _ => assert(C:H) end.
        -- constructor. inverse B. simpl. constructor; assumption.
        -- specialize(A C); clear C. inverse A. inverse H1. assumption.
      * firstorder.
  (* itss_rec_unfold *)
  - inverse Htype. constructor. apply type_cvalue_rec.
    + intros. specialize(H0 x H f_ta f_in H2).
      rewrite map_map in H0.
      simpl in H0. clear - H0.
      erewrite map_ext_Forall; [eassumption|].
      apply Forall_forall. intros [? ?] _; reflexivity.
    + clear H0. revert ftl H1. induction ftl; [constructor|].
      intro A. inverse A. specialize(IHftl H6).
      clear H6. constructor; [|assumption].
      destruct a; injection H as ? ?; subst.
      rewrite H3. split; [reflexivity|].
      now inverse H5.
  (* itss_set_unfold *)
  - inverse Htype.
    constructor.
    destruct ty as [|f_out tyl| |];
        try (inverse H1; destruct H0 as [[|] A]; inverse A; fail).
    apply type_cvalue_rec.
    + inverse H1.
      destruct H0 as [[|n] A]; inverse A.
      intros. specialize(H2 x H f_ta f_in H0).
      rewrite map_app. apply in_or_app. right.
      assumption.
    + inverse H1.
      destruct H0 as [[|n] A]; inverse A. clear H2.
      apply Forall_app. split.
      * clear - H3. revert ftl2 H3. induction ftl2 as [|[]].
        -- constructor.
        -- intro A; inverse A. constructor; [|firstorder].
           rewrite H4. inverse H6. split; [reflexivity|assumption].
      * clear - H4. revert ftl1 H4. induction ftl1 as [|[]].
        -- constructor.
        -- intro A; inverse A. constructor; [|firstorder].
           destruct (find s0 (s_ftype s)); [|assumption].
           destruct p as [[? ?] f'_out].
           destruct H1. split; [assumption|].
           exists n; assumption.
Qed.

Theorem subject_reduction_skel:
  forall sk sk' ty,
    type_ext_skel sk ty ->
    interp_skel_ss s u gen sk sk' ->
    type_ext_skel sk' ty.
Proof.
  intros v v' ty Htype Hinterp.
  revert Hinterp ty Htype.
  (* We prove the property for every possible reduction step *)
  induction 1 using isss_ind; subst; intros ?ty Htype.
  (* i_branch_ss *)
  - inversion Htype; subst.
    destruct H2 as [g [envok H2]].
    inversion H2; subst.
    apply Forall_forall with (x:= ski) in H4; [|assumption].
    constructor. exists g; split; assumption.
  (* i_match_embed *)
  - inversion Htype; subst.
    destruct H2 as [g [envok H2]].
    inversion H2; subst.
    apply type_ext_skel_match with (ty:=tym) (g:=g).
    + assumption.
    + constructor. exists g. split; assumption.
    + apply forall_imp2 with (1:=H6) (2:=H7). clear.
      intros [p sk] A B. split; assumption.
  (* i_match_l *)
  - inversion Htype; subst.
    apply type_ext_skel_match with (ty:=ty0) (g:=g); try assumption.
    apply subject_reduction_term with (v:=a); assumption.
  (* i_match_extract *)
  - simpl.
    assert (A:exists p, In (p,sk) psl /\ add_asn e p v = Some e').
    + induction psl as [|[p1 sk1] psl IH]; [inverse e0|].
      simpl get_match in e0.
      destruct add_asn eqn:B in e0.
      * injection e0 as -> ->. exists p1. split; [left; reflexivity|assumption].
      * destruct (IH e0) as [p [? ?]]; [|exists p; split; [right|]; assumption].
        clear - Htype. inverse Htype.
        apply type_ext_skel_match with (ty:=ty0) (g:=g); [assumption..|].
        inverse H5; assumption.
    + destruct A as [p [A A']].
      inverse Htype. rewrite Forall_forall in H5.
      specialize (H5 _ A). simpl in H5. destruct H5 as [wfp wfs].
      constructor. exists (extEnv p g). split; [|assumption].
      inverse H4.
      apply extEnv_ok with (v:=v) (ty:=ty0) (e:=e); assumption.
  (* i_return_embed *)
  - inversion Htype; subst.
    destruct H2 as [g [envok H2]].
    inversion H2; subst.
    constructor. constructor.
    exists g; split; assumption.
  (* i_return_next *)
  - inversion_clear Htype.
    constructor.
    apply subject_reduction_term with (v:=t); assumption.
  (* i_return_extract *)
  - inversion_clear Htype.
    inversion_clear H.
    constructor.
    assumption.
  (* i_apply_embed *)
  - inversion_clear Htype.
    destruct H as [g [envok H]].
    inversion_clear H.
    constructor 5 with (tyi:=tyi) (tyo:=ty);
    constructor; exists g; split; assumption.
  (* isss_apply_l *)
  - inversion_clear Htype.
    constructor 5 with (tyi:=tyi) (tyo:=ty); [firstorder|assumption..].
    now apply subject_reduction_term with (v:=f).
  (* isss_apply_r *)
  - inversion_clear Htype.
    constructor 5 with (tyi:=tyi) (tyo:=ty); [assumption|].
    now apply subject_reduction_term with (v:=a).
  (* isss_apply_clos *)
  - inversion_clear Htype; subst.
    inversion_clear H.
    inversion_clear H0.
    destruct H1 as [[|n] H1]; inversion H1; subst.
    constructor.
    exists (extEnv p g).
    split; [|assumption].
    assert(A:=proj2 (env_ok_n e g) (ex_intro _ n H5)).
    apply extEnv_ok with (v:=a) (ty:=tyi) (e:=e); assumption.
  (* isss_apply_unspec_done *)
  - inversion_clear Htype.
    inverse H.
    destruct H2 as [[|n] H2]; inverse H2.
    inverse H6.
    constructor.
    assert(uok:=u_ok x).
    rewrite e in uok. rewrite H4 in uok.
    destruct uok as [_ [Hlen uok]].
    specialize(uok ta (app vl [a]) res w0).
    specialize(Hlen ta (app vl [a]) res w0).
    destruct uok as [ty'l [ty'o [takeargs [all2 t]]]].
    apply wf_apply_last in H8.
    apply take_args_wf_apply in takeargs.
    assert(Hlen': length (tyl ++[tyi]) = length ty'l).
    { rewrite length_app. simpl.
      apply forall2_length in all2.
      rewrite <- all2, length_app. simpl.
      f_equal.
      apply forall2_length in H7.
      rewrite H7; reflexivity.
    }
    assert(A:=wf_apply_inj _ _ _ _ _ Hlen' H8 takeargs).
    destruct A as [_ <-]; assumption.
  (* isss_apply_unspec_cont *)
  - inversion_clear Htype.
    inverse H; inverse H0.
    constructor.
    destruct H2 as [[|n2] H2]; [destruct H2|].
    destruct H1 as [[|n1] H1]; [destruct H1|].
    inverse H2.
    exists (S(max (S n1) n2)).
    apply type_cvalue_inc' with (n:=max (S n1) n2) in H1;
          [|apply PeanoNat.Nat.le_max_l].
    apply wf_apply_last in H8.
    econstructor; try eassumption.
    apply Forall2_app; [|constructor; [assumption|constructor]].
    eapply forall2_imp with (1:=H7).
    clear; intros x y. apply type_cvalue_inc'.
    apply PeanoNat.Nat.le_max_r.
  (* i_letin_embed *)
  - inversion_clear Htype.
    destruct H as [g [envok H]].
    inversion_clear H.
    constructor 4 with (g:=g) (ty':=ty1); try assumption.
    constructor. exists g. split; assumption.
  (* i_letin_next_l *)
  - inversion_clear Htype.
    constructor 4 with (g:=g) (ty':=ty'); try assumption.
    now apply IHHinterp.
  (* i_letin_extract *)
  - inversion_clear Htype.
    inversion_clear H2.
    constructor.
    exists (extEnv p g).
    split; [|assumption].
    apply extEnv_ok with (v:=v) (ty:=ty') (e:=e); assumption.
  (* i_exists_ss *)
  - 
    inversion_clear Htype.
    destruct H as [gamma [envok H]].
    inversion H; subst.
    constructor.
    now apply gen_ok.
Qed.


