Require Import String List.
Require Import Coq.micromega.Lia.

From Necro Require Import Dict Skeleton Concrete Concrete_rec Induction.

Import ListNotations DictNotations.


Lemma forall2_imp:
  forall A B al bl (P Q: A -> B -> Prop),
    (forall x y, P x y -> Q x y) ->
    Forall2 P al bl ->
    Forall2 Q al bl.
Proof.
  intros A B al bl P Q Himp.
  revert al; induction bl; destruct al; [econstructor|inversion 1..].
  subst. constructor; firstorder.
Qed.

Section Equivalence.
  Variable s: skeletal_semantics.
  Variable u: dict (nat * unspec_function).
  Variable g: type -> cvalue -> Prop.
  Notation "'interp_term_rec'" := (interp_term_rec s u g).
  Notation "'interp_skel_rec'" := (interp_skel_rec s u g).
  Notation "'interp_term_n'" := (interp_term_n s u g).
  Notation "'interp_skel_n'" := (interp_skel_n s u g).
  Notation "'interp_term'" := (interp_term s u).
  Notation "'interp_skel'" := (interp_skel s u g).
  Notation "'iter_n'" := (iter_n s u g).

  (***************************************************************************)
  (*                          First part: rec -> BS                          *)
  (***************************************************************************)

  Lemma rec_to_bs:
    forall n,
      (forall e t v,
      interp_term_n n e t v ->
      interp_term e t v) /\
      (forall e sk v,
      interp_skel_n n e sk v ->
      interp_skel e sk v).
  Proof.
    induction n; [split; destruct 1|].
    destruct IHn as [IHv IHs].
    repeat split.
    - intros e t v Hiv.
      unfold "interp_term_n" in Hiv.
      simpl in Hiv.
      destruct (iter_n n) as [Hv Hs] eqn:Hiter in Hiv.
      inversion Hiv;
        econstructor; try eassumption; apply IHv;
        unfold "interp_term_n"; rewrite Hiter; assumption.
    - intros e sk v His.
      unfold "interp_skel_n" in His.
      simpl in His.
      destruct (iter_n n) as [Hv Hs] eqn:Hiter in His.
      inversion His;
        try (econstructor; subst; try eassumption;
          try (apply IHv; unfold "interp_term_n"; rewrite Hiter; eassumption);
          try (apply IHs; unfold "interp_skel_n"; rewrite Hiter; eassumption);
          fail).
  Qed.

  Theorem rec_to_bs_term:
    forall e t v,
    interp_term_rec e t v ->
    interp_term e t v.
  Proof.
    intros.
    destruct H as [n H].
    now apply rec_to_bs with (n:=n).
  Qed.

  Theorem rec_to_bs_skel:
    forall e sk v,
    interp_skel_rec e sk v ->
    interp_skel e sk v.
  Proof.
    intros.
    destruct H as [n H].
    now apply rec_to_bs with (n:=n).
  Qed.



  (***************************************************************************)
  (*                          Second part: BS → Rec                          *)
  (***************************************************************************)

  Lemma iter_n_inc:
    forall n,
      ( forall e t v,
        interp_term_n n e t v ->
        interp_term_n (S n) e t v) /\
      ( forall e sk v,
        interp_skel_n n e sk v ->
        interp_skel_n (S n) e sk v).
  Proof.
    induction n; [repeat split; inversion 1|].
    destruct (iter_n n) as [Hiv His] eqn:Hiter.
    destruct IHn as [IHnv IHns].
    unfold "interp_term_n" in IHnv.
    simpl in IHnv. rewrite Hiter in IHnv.
    unfold "interp_skel_n" in IHns.
    simpl in IHns. rewrite Hiter in IHns.
    repeat split.
    - intros e t v H.
      unfold "interp_term_n" in H.
      simpl in H. rewrite Hiter in H.
      inversion H; subst.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        now econstructor 1.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        econstructor 2; eassumption.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        econstructor 3; eassumption.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        econstructor 4; [eassumption..|].
        apply IHnv. assumption.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        econstructor 5.
        apply IHnv. assumption.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        econstructor 6.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        econstructor 7.
        apply IHnv. assumption.
        apply IHnv. assumption.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        econstructor 8.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        econstructor 9.
        apply IHnv. eassumption.
        assumption.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        econstructor 10.
        apply IHnv. eassumption. eassumption.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        econstructor 11.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        econstructor 12.
        apply IHnv. eassumption.
        apply IHnv. eassumption.
      + unfold "interp_term_n".
        simpl. rewrite Hiter.
        econstructor 13.
        apply IHnv. eassumption.
        apply IHnv. eassumption.
    - intros e sk v H.
      unfold "interp_skel_n" in H.
      simpl in H. rewrite Hiter in H.
      inversion H; subst.
      + unfold "interp_skel_n".
        simpl. rewrite Hiter.
        econstructor 1; [eassumption|].
        now apply IHns.
      + unfold "interp_skel_n".
        simpl. rewrite Hiter.
        econstructor 2.
        * apply IHnv. eassumption.
        * eassumption.
        * apply IHns. assumption.
      + unfold "interp_skel_n".
        simpl. rewrite Hiter.
        econstructor 3.
        now apply IHnv.
      + unfold "interp_skel_n".
        simpl. rewrite Hiter.
        econstructor 4.
        * apply IHnv; eassumption.
        * eapply IHnv; eassumption.
        * eassumption.
        * eapply IHns; eassumption.
      + unfold "interp_skel_n".
        simpl. rewrite Hiter.
        econstructor 5.
        * apply IHnv; eassumption.
        * eapply IHnv; eassumption.
        * eassumption.
        * eassumption.
      + unfold "interp_skel_n".
        simpl. rewrite Hiter.
        econstructor 6.
        * apply IHnv; eassumption.
        * eapply IHnv; eassumption.
      + unfold "interp_skel_n".
        simpl. rewrite Hiter.
        econstructor 7; [|eassumption|]; now apply IHns.
      + unfold "interp_skel_n".
        simpl. rewrite Hiter.
        econstructor 8; [eassumption..|].
        assumption.
  Qed.

  Lemma iter_n_strong_inc:
    forall n m, m >= n ->
      ( forall e t v,
        interp_term_n n e t v ->
        interp_term_n m e t v) /\
      ( forall e sk v,
        interp_skel_n n e sk v ->
        interp_skel_n m e sk v).
  Proof.
    induction 1; [tauto|].
    split; intros; apply iter_n_inc; apply IHle; assumption.
  Qed.

  Theorem bs_to_rec_term:
    forall e t v,
    interp_term e t v ->
    interp_term_rec e t v.
  Proof.
    apply it_ind; intros.
    - exists 1. now constructor 1.
    - exists 1. econstructor 2; eassumption.
    - exists 1. econstructor 3; eassumption.
    - destruct H2 as [n H2].
      exists (S n).
      unfold "interp_term_n" in *.
      destruct (iter_n n) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 4; eassumption.
    - destruct H0 as [n H0].
      exists (S n).
      unfold "interp_term_n" in *.
      destruct (iter_n n) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 5; eassumption.
    - exists 1. econstructor 6.
    - destruct H0 as [n1 H0].
      destruct H2 as [n2 H2].
      apply iter_n_strong_inc with (m:=max n1 n2) in H0; [|lia].
      apply iter_n_strong_inc with (m:=max n1 n2) in H2; [|lia].
      exists (S (max n1 n2)).
      unfold "interp_term_n" in *.
      destruct (iter_n (max n1 n2)) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 7; eassumption.
    - exists 1. econstructor 8; eassumption.
    - destruct H0 as [n0 H0]. exists (S n0).
      unfold "interp_term_n" in *.
      destruct (iter_n n0) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 9; eassumption.
    - destruct H0 as [n0 H0]. exists (S n0).
      unfold "interp_term_n" in *.
      destruct (iter_n n0) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 10; eassumption.
    - exists 1. constructor 11.
    - destruct H0 as [n0 H0]. destruct H2 as [n2 H2].
      apply iter_n_strong_inc with (m:=max n0 n2) in H0; [|lia].
      apply iter_n_strong_inc with (m:=max n0 n2) in H2; [|lia].
      exists (S (max n0 n2)).
      unfold "interp_term_n" in *.
      destruct (iter_n (max n0 n2)) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 12; eassumption.
    - destruct H0 as [n0 H0]. destruct H2 as [n2 H2].
      apply iter_n_strong_inc with (m:=max n0 n2) in H0; [|lia].
      apply iter_n_strong_inc with (m:=max n0 n2) in H2; [|lia].
      exists (S (max n0 n2)).
      unfold "interp_term_n" in *.
      destruct (iter_n (max n0 n2)) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 13; eassumption.
  Qed.

  Lemma all_iv_max:
    forall e tl vl,
    Forall2 (interp_term_rec e) tl vl ->
    exists n,
      Forall2 (interp_term_n n e) tl vl.
  Proof.
    intro e; induction tl; destruct vl; [exists 0; constructor|inversion 1..|].
    inversion_clear 1.
    destruct H0 as [n1 A].
    apply IHtl in H1.
    destruct H1 as [n2 B].
    exists (max n1 n2).
    constructor.
    apply iter_n_strong_inc with (n:=n1); [lia|assumption].
    clear - B.
    eapply forall2_imp ;[apply iter_n_strong_inc with (n:=n2); lia|assumption].
  Qed.


  Theorem bs_to_rec_skel:
      forall e sk v,
      interp_skel e sk v ->
      interp_skel_rec e sk v.
  Proof.
    refine (
      let HHHiv := (fun e v c => interp_term_rec e v c) in
    (fun H1 H2 H3 H4 H5 H6 H7 H8 =>
      (is_ind s u g HHHiv _ bs_to_rec_term H1 H2 H3 H4 H5 H6 H7 H8))
      _ _ _ _ _ _ _ _); intros.
    - destruct H1 as [n H1].
      exists (S n).
      unfold "interp_skel_n" in *.
      destruct (iter_n n) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 1; eassumption.
    - destruct H0 as [n1 H0].
      destruct H3 as [n2 H3].
      apply iter_n_strong_inc with (m:=max n1 n2) in H0; [|lia].
      apply iter_n_strong_inc with (m:=max n1 n2) in H3; [|lia].
      exists (S (max n1 n2)).
      unfold "interp_skel_n" in *.
      unfold "interp_term_n" in *.
      destruct (iter_n _) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 2; eassumption.
    - destruct H0 as [n H0].
      exists (S n).
      unfold "interp_term_n" in H0.
      unfold "interp_skel_n".
      destruct (iter_n n) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 3; eassumption.
    - destruct H0 as [n0 H0].
      destruct H2 as [n2 H2].
      destruct H5 as [n5 H5].
      apply iter_n_strong_inc with (m:=max n0 (max n2 n5)) in H0; [|lia].
      apply iter_n_strong_inc with (m:=max n0 (max n2 n5)) in H2; [|lia].
      apply iter_n_strong_inc with (m:=max n0 (max n2 n5)) in H5; [|lia].
      exists (S (max n0 (max n2 n5))).
      unfold "interp_term_n" in *.
      unfold "interp_skel_n" in *.
      destruct (iter_n (max n0 (max n2 n5))) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 4; eassumption.
    - destruct H0 as [n0 H0].
      destruct H2 as [n2 H2].
      apply iter_n_strong_inc with (m:=max n0 n2) in H0; [|lia].
      apply iter_n_strong_inc with (m:=max n0 n2) in H2; [|lia].
      exists (S (max n0 n2)).
      unfold "interp_term_n" in *.
      unfold "interp_skel_n" in *.
      destruct (iter_n (max n0 n2)) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 5; eassumption.
    - destruct H0 as [n0 H0].
      destruct H2 as [n2 H2].
      apply iter_n_strong_inc with (m:=max n0 n2) in H0; [|lia].
      apply iter_n_strong_inc with (m:=max n0 n2) in H2; [|lia].
      exists (S (max n0 n2)).
      unfold "interp_term_n" in *.
      unfold "interp_skel_n" in *.
      destruct (iter_n (max n0 n2)) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 6; eassumption.
    - destruct H0 as [n1 H0].
      destruct H3 as [n2 H3].
      apply iter_n_strong_inc with (m:=max n1 n2) in H0; [|lia].
      apply iter_n_strong_inc with (m:=max n1 n2) in H3; [|lia].
      exists (S (max n1 n2)).
      unfold "interp_skel_n" in *.
      destruct (iter_n (max n1 n2)) as [Hiv His] eqn:Hiter.
      simpl. rewrite Hiter.
      econstructor 7; eassumption.
    - exists 1.
      unfold "interp_skel_n" in *.
      destruct (iter_n) as [Hiv His] eqn:Hiter.
      injection Hiter as _ <-. simpl.
      econstructor 8; eassumption.
  Qed.



  (***************************************************************************)
  (*                          Third part: BS ↔ Rec                           *)
  (***************************************************************************)

  Theorem bs_rec_eq_term:
    forall e t v,
    interp_term e t v <->
    interp_term_rec e t v.
  Proof.
    intros; split; [apply bs_to_rec_term| apply rec_to_bs_term].
  Qed.

  Theorem bs_rec_eq_skel:
    forall e sk v,
    interp_skel e sk v <->
    interp_skel_rec e sk v.
  Proof.
    intros; split; [apply bs_to_rec_skel| apply rec_to_bs_skel].
  Qed.

End Equivalence.
