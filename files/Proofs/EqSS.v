Require Import String List.
Require Import Coq.micromega.Lia.

From Necro Require Import Dict Skeleton.
From Necro Require Import Concrete Concrete_ss Induction.
Require Import Concrete_ebs.

Import ListNotations DictNotations.

Section Equivalence.
  Variable s: skeletal_semantics.
  Variable u: dict (nat * unspec_function).
  Variable g: type -> cvalue -> Prop.
  Notation "'interp_term_ss'" := (interp_term_ss s u).
  Notation "'interp_skel_ss'" := (interp_skel_ss s u g).
  Notation "'interp_term'" := (interp_term s u).
  Notation "'interp_skel'" := (interp_skel s u g).
  Notation "'interp_term_ebs'" := (interp_term_ebs s u).
  Notation "'interp_skel_ebs'" := (interp_skel_ebs s u g).

  (* We define the iteration of a relation *)
  Inductive red [A] (P: A -> A -> Prop): A -> A -> Prop :=
  | red_0: forall a, red P a a
  | red_next: forall a b c, P a b -> red P b c -> red P a c.

  Ltac inverse H := inversion H; subst; clear H.

  (***************************************************************************)
  (*                           First part: SS → BS                           *)
  (***************************************************************************)

  Lemma i_tuple:
    forall e tl vl,
    Forall2 (interp_term e) tl vl ->
    interp_term e (term_tuple tl) (cval_tuple vl).
  Proof.
    induction tl; intros [|v vl] H; [constructor|inversion H..|].
    inverse H.
    constructor; [assumption|].
    apply IHtl; assumption.
  Qed.

  Lemma combine_split:
    forall A B (l:list (A * B)),
    combine (map fst l) (map snd l) = l.
  Proof.
    induction l as [|[a b] l]; [reflexivity|].
    simpl. now f_equal.
  Qed.

  Lemma Forall2_app':
    forall {A B} (P:A->B->Prop) l1 l2 l,
    Forall2 P (l1 ++ l2) l ->
    exists l'1 l'2,
    l = (l'1 ++ l'2)%list /\
    Forall2 P l1 l'1 /\
    Forall2 P l2 l'2.
  Proof.
    induction l1.
    - simpl. intros. exists [], l.
      split; [reflexivity|].
      split; [constructor|assumption].
    - intros.
      inverse H.
      specialize(IHl1 l2 l' H4). clear H4.
      destruct IHl1 as (l'1&l'2&A1&A2&A3).
      exists (y::l'1), l'2.
      split; [simpl; f_equal; assumption|].
      split; [|assumption].
      constructor; assumption.
  Qed.

  Lemma Forall2_rev:
    forall A B (P:A -> B -> Prop) l1 l2,
    Forall2 P l1 l2 -> Forall2 P (rev l1) (rev l2).
  Proof.
    induction l1; destruct l2; [constructor|inversion 1..|].
    intro H; inverse H.
    simpl. apply Forall2_app; [firstorder|].
    constructor; [assumption|constructor].
  Qed.


  (* Concatenation lemmae *)

  Lemma concat_record:
    forall tl,
      Forall (fun '(_, t) => forall y z,
         interp_term_ss t y -> interp_term_ebs y z -> interp_term_ebs t z) tl ->
    forall y z,
      interp_term_ss (ext_term_record tl) y ->
      interp_term_ebs y z ->
      interp_term_ebs (ext_term_record tl) z.
  Proof.
    induction tl; intros.
    + inverse H0; [inverse H3|].
      destruct ftl as [|]; [clear H3|inversion H3].
      inversion H1; subst. constructor.
    + destruct a as [f a]. inverse H.
      specialize(IHtl H5). clear H5.
      inverse H0.
      * destruct tl' as [|b tl']; [inverse H2|].
        simpl in *.
        inverse H2.
        -- inverse H1. constructor; [firstorder|].
           now rewrite combine_split in H7.
        -- inverse H1.
           apply iebs_record_none_cons; [assumption|].
           eapply IHtl; [|eassumption].
           now constructor.
      * inverse H1. clear.
        induction ftl as [|[f v]]; repeat constructor; assumption.
  Qed.


  Lemma concat_term:
    forall x y z,
    interp_term_ss x y ->
    interp_term_ebs y z ->
    interp_term_ebs x z.
  Proof.
    induction x using ext_term_rec.
    (* ret_term *)
    - inversion 1.
    (* cont_term *)
    - inversion 1; inversion 1; subst.
      (* term_var TVLet *)
      + constructor. econstructor; eassumption.
      (* term_var TVUnspec (0) *)
      + constructor. econstructor; eassumption.
      (* term_var TVUnspec (S) *)
      + constructor. econstructor; eassumption.
      (* term_var TVUnspec *)
      + constructor. econstructor; eassumption.
      (* term_constructor *)
      + constructor. econstructor. now inversion H8.
      (* term_tuple *)
      + constructor. apply i_tuple. clear - H5.
        revert tl H5; induction vl.
        * intros [|]; [auto|inversion 1].
        * intros [|]; inversion 1; subst.
          constructor; [now inversion H2|].
          firstorder.
      (* term_func *)
      + constructor. constructor.
      (* term_field *)
      + inversion H6; subst. constructor. econstructor; eassumption.
      (* term_nth *)
      + inversion H6; subst. constructor. econstructor; eassumption.
      (* term_field *)
      + destruct ftl as [|]; [|discriminate H5]. constructor. constructor.
      (* term_record None *)
      + rewrite <- H4 in H3.
        constructor.
        destruct ftl as [|[[f' ?] t'] ftl]; [discriminate H4|injection H4].
        intros ? -> <-. constructor. now inversion H5.
        clear - H0 H6.
        revert tl ftl vl H0 H6.
        induction tl; destruct ftl as [|[[f ?] t] ftl]; destruct vl as [|v vl];
          [constructor|inversion 1; inversion 1..|].
          intros A B.
          simpl in A; injection A as -> ?.
          inversion B; subst.
          constructor; [now inversion H2|]. apply IHtl; [reflexivity|assumption].
      (* term_record (Some _) *)
      + constructor. clear H.
        inversion H3; subst. clear - H2 H4. inversion H2; subst.
        constructor; [assumption|]. clear - H4.
        revert ftl vl3 H4.
        induction ftl as [|[[f ?] t] ftl IHtl]; destruct vl3 as [|v vl];
          [constructor|inversion 1; inversion 1..|].
          inversion 1; subst. constructor; [now inversion H1|].
          apply IHtl; assumption.
    (* ext_term_constr *)
    - inversion 1.
      + inversion 1; subst. constructor. firstorder.
      + inversion 1; subst. constructor. constructor.
    (* ext_term_tuple *)
    - revert H; induction vl; intros.
      + inversion H0; subst.
        destruct vl as [|]; [clear H3|inversion H3].
        inversion H1; subst. constructor. constructor.
      + inversion H0; subst.
        * inversion H1; subst. constructor.
          destruct vl0 as [|c cl]; [inversion H3|].
          inversion H3; subst.
          constructor; [|assumption].
          inversion H; subst. firstorder.
        * inversion H1; subst. constructor.
          destruct vl0 as [|c cl]; [inversion H3|].
          inversion H3; subst.
          constructor; [assumption|].
          inversion H; subst.
          assert(A:interp_term_ebs (ext_term_tuple tl') (cval_tuple cl))
          by now constructor.
          specialize(IHvl H8 _ _ H5 A).
          now inversion IHvl.
        * inversion H1; subst. constructor. clear.
          induction vl0; intros; simpl; constructor.
          constructor. assumption.
  (* ext_term_field *)
  - inversion 1; subst.
    + inversion 1; subst.
      econstructor; [|eassumption].
      apply IHx with (y:=t'); assumption.
    + inversion 1; subst.
      econstructor; [|eassumption]. constructor.
  (* ext_term_nth *)
  - inversion 1; subst.
    + inversion 1; subst.
      econstructor; [|eassumption].
      apply IHx with (y:=t'); assumption.
    + inversion 1; subst.
      econstructor; [|eassumption]. constructor.
  (* ext_term_record None *)
  - apply concat_record. assumption.
  (* ext_term_record Some *)
  - inversion 1; subst.
    (* itss_set_l *)
    + inversion 1; subst.
      constructor; [firstorder|assumption].
    (* itss_set_r *)
    + inversion 1; subst.
      constructor; [assumption|].
      inverse H5.
      eapply concat_record; [eassumption|..|eassumption].
      constructor; eassumption.
    (* itss_set_embed *)
    + inversion 1; subst. constructor; [constructor|].
      clear. induction ftl2 as [|[f ?]]; repeat constructor; assumption.
  Qed.

  Lemma concat_skel:
    forall x y z,
    interp_skel_ss x y ->
    interp_skel_ebs y z ->
    interp_skel_ebs x z.
  Proof.
    induction x using ext_skel_rec;
    intros.
    - inversion H.
    - inverse H.
      + inverse H0. constructor. apply i_branch with (ski:=ski); assumption.
      + inverse H0. inverse H3. inverse H6.
        constructor. eapply i_match; eassumption.
      + inverse H0. inverse H1. constructor. apply i_return. assumption.
      + inverse H0; subst.
        * constructor. econstructor 4; [| |eassumption..].
          ** inversion H2; subst; eauto.
          ** inversion H3; subst; eauto.
        * constructor. econstructor 5; [| |eassumption..].
          ** inversion H2; subst; eauto.
          ** inversion H3; subst; eauto.
        * constructor. econstructor 6; [| |eassumption..].
          ** inversion H2; subst; eauto.
          ** inversion H4; subst; eauto.
      + inverse H0. inverse H5. constructor.
        apply i_letin with (e':=e') (v:=v1); assumption.
      + inverse H0. constructor.
        apply i_exists with (e:=e); assumption.
    - inverse H.
      + inverse H0. constructor. now apply concat_term with (y:=t').
      + inverse H0. repeat constructor.
    - inverse H.
      + inverse H0.
        econstructor; [|eassumption..].
        eapply concat_term; eassumption.
      + inverse H0. econstructor.
        * constructor.
        * eassumption.
        * constructor; assumption.
    - inverse H.
      + inverse H0.
        econstructor; [|eassumption..].
        apply IHx with (y:=s'); eassumption.
      + inverse H0. econstructor; [econstructor|eassumption..].
    - inverse H; inverse H0.
      + econstructor 6; try eassumption. now apply concat_term with (y:=f').
      + econstructor 7; try eassumption. now apply concat_term with (y:=f').
      + econstructor 8; try eassumption. now apply concat_term with (y:=f').
      + econstructor 6; try eassumption. now apply concat_term with (y:=a').
      + econstructor 7; try eassumption. now apply concat_term with (y:=a').
      + econstructor 8; try eassumption. now apply concat_term with (y:=a').
      + econstructor 6; try eassumption; constructor.
      + econstructor 7; try eassumption; constructor.
      + econstructor 8; try eassumption; constructor.
  Qed.



  (* Lemmae involving ebs *)

  Lemma ebs_to_bs_term:
    forall e t v,
    interp_term_ebs (cont_term e t) v ->
    interp_term e t v.
  Proof.
    now inversion 1.
  Qed.

  Lemma ss_to_ebs_term:
    forall x v,
    red interp_term_ss x (ret_term v) ->
    interp_term_ebs x v.
  Proof.
    intros. remember (ret_term v) as e eqn:He.
    induction H.
    - subst; constructor.
    - apply concat_term with (y:=b); firstorder.
  Qed.

  Lemma ebs_to_bs_skel:
    forall e t v,
    interp_skel_ebs (cont_skel e t) v ->
    interp_skel e t v.
  Proof.
    now inversion 1.
  Qed.

  Lemma ss_to_ebs_skel:
    forall x v,
    red interp_skel_ss x (ret_skel v) ->
    interp_skel_ebs x v.
  Proof.
    intros. remember (ret_skel v) as e eqn:He.
    induction H.
    - subst; constructor.
    - apply concat_skel with (y:=b); firstorder.
  Qed.



  (* First main two theorems: ss → bs *)

  Theorem ss_to_bs_term:
    forall e t v,
    red (interp_term_ss) (cont_term e t) (ret_term v) ->
    interp_term e t v.
  Proof.
    intros.
    apply ebs_to_bs_term.
    apply ss_to_ebs_term.
    assumption.
  Qed.

  Theorem ss_to_bs_skel:
    forall e sk v,
    red (interp_skel_ss) (cont_skel e sk) (ret_skel v) ->
    interp_skel e sk v.
  Proof.
    intros.
    apply ebs_to_bs_skel.
    apply ss_to_ebs_skel.
    assumption.
  Qed.











  (***************************************************************************)
  (*                          Second part: BS → SS                           *)
  (***************************************************************************)




  Lemma red_end:
    forall {A} (P: A -> A -> Prop) a b c, P b c -> red P a b -> red P a c.
  Proof.
    intros.
    induction H0.
    - eapply red_next; [eassumption|apply red_0].
    - eapply red_next; [eassumption|firstorder].
  Qed.

  Lemma red_slice:
    forall {A} (P: A -> A -> Prop) a b c, red P a b -> red P b c -> red P a c.
  Proof.
    intros.
    induction H0.
    - assumption.
    - apply IHred. eapply red_end; eassumption.
  Qed.

  Lemma red_destr_end:
    forall {A} (P: A -> A -> Prop) a c,
      red P a c -> a = c \/ (exists b, red P a b /\ P b c).
  Proof.
    intros. induction H; subst.
    - left; reflexivity.
    - right. destruct IHred.
      + exists a. subst. split; [apply red_0|assumption].
      + destruct H1 as [b0 [? ?]]. exists b0.
        split; [eapply red_next;eassumption|assumption].
  Qed.

  Ltac id := apply red_0.
  Ltac next := eapply red_next.
  Ltac last := eapply red_end.
  Ltac slice := eapply red_slice.



  (* Auxiliary embedding lemmas for big-step to small-step *)

  Lemma bs_to_ss_constr:
    forall c ta a b,
      red interp_term_ss a b ->
      red interp_term_ss
        (ext_term_constr c ta a)
        (ext_term_constr c ta b).
  Proof.
    induction 1.
    - id.
    - next. econstructor; eassumption. eassumption.
  Qed.

  Lemma bs_to_ss_tuple_l:
    forall a b l,
      red interp_term_ss a b ->
      red interp_term_ss (ext_term_tuple (a :: l)) (ext_term_tuple (b :: l)).
  Proof.
    intros.
    induction H.
    - id.
    - next. eapply itss_tuple_l; eassumption. eassumption.
  Qed.

  Lemma bs_to_ss_tuple_r:
    forall a l l',
      red interp_term_ss
        (ext_term_tuple l)
        (ext_term_tuple l') ->
      red interp_term_ss
        (ext_term_tuple (ret_term a :: l))
        (ext_term_tuple (ret_term a :: l')).
  Proof.
    intros.
    set (P:= fun l l' =>
      match l, l' with
      | ext_term_tuple l, ext_term_tuple l' =>
        red interp_term_ss
          (ext_term_tuple (ret_term a :: l))
          (ext_term_tuple (ret_term a :: l'))
      | _, _ => True
      end).
    enough (P (ext_term_tuple l) (ext_term_tuple l')) by assumption.
    induction H.
    - destruct a0; simpl; trivial; id.
    - destruct a0; simpl; trivial.
      destruct c; simpl; trivial.
      destruct b; try (inversion H; fail).
      + inversion H0. inversion H1.
      + simpl in IHred.
        next; [|eassumption]. constructor; assumption.
  Qed.

  Lemma bs_to_ss_set_l:
    forall a b l,
      red interp_term_ss a b ->
      red interp_term_ss
        (ext_term_rec_set a l)
        (ext_term_rec_set b l).
  Proof.
    intros.
    induction H.
    - id.
    - next. eapply itss_set_l; eassumption. eassumption.
  Qed.

  Lemma bs_to_ss_set_r:
    forall a l l',
      red interp_term_ss
        (ext_term_record l)
        (ext_term_record l') ->
      red interp_term_ss
        (ext_term_rec_set (ret_term a) l)
        (ext_term_rec_set (ret_term a) l').
  Proof.
    intros.
    set (P:= fun l l' =>
      match l, l' with
      | ext_term_record l, ext_term_record l' =>
        red interp_term_ss
          (ext_term_rec_set (ret_term a) l)
          (ext_term_rec_set (ret_term a) l')
      | _, _ => True
      end).
    enough (P (ext_term_record l) (ext_term_record l')) by assumption.
    induction H.
    - destruct a0; simpl; trivial. id.
    - destruct a0; simpl; trivial.
      destruct c; simpl; trivial.
      destruct b; try (inversion H; fail).
      + inversion H0. inversion H1.
      + simpl in IHred.
        next; [|eassumption].
        inverse H.
        now constructor.
  Qed.

  Lemma bs_to_ss_record_l:
    forall f a b l,
      red interp_term_ss a b ->
      red interp_term_ss
        (ext_term_record ((f, a) :: l))
        (ext_term_record ((f, b) :: l)).
  Proof.
    intros.
    induction H.
    - id.
    - next.
        eapply itss_rec. simpl. constructor. eassumption.
        simpl. rewrite combine_split. assumption.
  Qed.

  Lemma bs_to_ss_record_r:
    forall f a l l',
      red interp_term_ss
        (ext_term_record l)
        (ext_term_record l') ->
      red interp_term_ss
        (ext_term_record ((f, ret_term a) :: l))
        (ext_term_record ((f, ret_term a) :: l')).
  Proof.
    intros.
    set (P:= fun l l' =>
      match l, l' with
      | ext_term_record l, ext_term_record l' =>
        red interp_term_ss
          (ext_term_record ((f, ret_term a) :: l))
          (ext_term_record ((f, ret_term a) :: l'))
      | _, _ => True
      end).
    enough (P (ext_term_record l) (ext_term_record l')) by assumption.
    induction H.
    - destruct a0; simpl; trivial. id.
    - destruct a0; simpl; trivial.
      destruct c; simpl; trivial.
      destruct b; try (inversion H; fail).
      + inversion H0; inversion H1; destruct o; trivial.
      + unfold P in IHred. clear P.
        inversion H.
        next; [|eassumption]. inverse H.
        rewrite(eq_refl: (f,ret_term a) :: combine _ _ = combine (f::_)
        (ret_term a :: _)). constructor. simpl.
        constructor. assumption.
  Qed.

  Lemma bs_to_ss_field:
    forall a b f,
      red interp_term_ss a b ->
      red interp_term_ss (ext_term_field a f) (ext_term_field b f).
  Proof.
    intros.
    induction H.
    - id.
    - next. eapply itss_field; eassumption. eassumption.
  Qed.

  Lemma bs_to_ss_nth:
    forall a b f,
      red interp_term_ss a b ->
      red interp_term_ss (ext_term_nth a f) (ext_term_nth b f).
  Proof.
    intros.
    induction H.
    - id.
    - next. eapply itss_nth; eassumption. eassumption.
  Qed.

  Lemma bs_to_ss_return:
    forall a b,
      red interp_term_ss a b ->
      red interp_skel_ss
        (ext_skel_return a)
        (ext_skel_return b).
  Proof.
    intros a b.
    induction 1.
    - id.
    - next; [|eassumption]. econstructor; eassumption.
  Qed.

  Lemma bs_to_ss_match:
    forall e psl a b,
      red interp_term_ss a b ->
      red interp_skel_ss
        (ext_skel_match e a psl)
        (ext_skel_match e b psl).
  Proof.
    intros e psl a b.
    induction 1.
    - id.
    - next; [|eassumption]. econstructor; eassumption.
  Qed.

  Lemma bs_to_ss_letin:
    forall e p a b sk,
      red interp_skel_ss a b ->
      red interp_skel_ss
        (ext_skel_letin e p a sk)
        (ext_skel_letin e p b sk).
  Proof.
    intros a b.
    induction 1.
    - id.
    - next; [|eassumption]. econstructor; eassumption.
  Qed.

  Lemma ext_term_tuple_safe_cont:
    forall l e t,
    red interp_term_ss (ext_term_tuple l) (cont_term e t) -> False.
  Proof.
    intros l e t H.
    set (P := fun (y y0: ext_term) =>
      match y, y0 with
      | ext_term_tuple l, Concrete_ss.cont_term e t => False
      | _, _ => True
      end).
    enough(P (ext_term_tuple l) (cont_term e t)) by assumption.
    induction H.
    - destruct a; simpl; trivial.
    - destruct a; simpl; trivial. destruct c; simpl; trivial.
      inversion H; subst; simpl in IHred; try (destruct IHred; fail).
      inversion H0. inversion H1.
  Qed.

  Lemma ext_term_tuple_safe_field:
    forall l t f,
    red interp_term_ss (ext_term_tuple l) (ext_term_field t f) -> False.
  Proof.
    intros l t f H.
    set (P := fun (y y0: ext_term) =>
      match y, y0 with
      | ext_term_tuple l, ext_term_field _ _ => False
      | _, _ => True
      end).
    enough(P (ext_term_tuple l) (ext_term_field t f)) by assumption.
    induction H.
    - destruct a; simpl; trivial.
    - destruct a; simpl; trivial. destruct c; simpl; trivial.
      inversion H; subst; simpl in IHred; try (destruct IHred; fail).
      inversion H0. inversion H1.
  Qed.

  Lemma ext_term_tuple_safe_nth:
    forall l t f,
    red interp_term_ss (ext_term_tuple l) (ext_term_nth t f) -> False.
  Proof.
    intros l t f H.
    set (P := fun (y y0: ext_term) =>
      match y, y0 with
      | ext_term_tuple l, ext_term_nth _ _ => False
      | _, _ => True
      end).
    enough(P (ext_term_tuple l) (ext_term_nth t f)) by assumption.
    induction H.
    - destruct a; simpl; trivial.
    - destruct a; simpl; trivial. destruct c; simpl; trivial.
      inversion H; subst; simpl in IHred; try (destruct IHred; fail).
      inversion H0. inversion H1.
  Qed.

  Lemma ext_term_tuple_safe_rec:
    forall l fl,
    red interp_term_ss (ext_term_tuple l) (ext_term_record fl) -> False.
  Proof.
    intros l fl H.
    set (P := fun (y y0: ext_term) =>
      match y, y0 with
      | ext_term_tuple l, ext_term_record _ => False
      | _, _ => True
      end).
    enough(P (ext_term_tuple l) (ext_term_record fl)) by assumption.
    induction H.
    - destruct a; simpl; trivial.
    - destruct a; simpl; trivial. destruct c; simpl; trivial.
      inversion H; subst; simpl in IHred; try (destruct IHred; fail).
      inversion H0. inversion H1.
  Qed.

  Lemma ext_term_record_safe_cont:
    forall l e t,
    red interp_term_ss (ext_term_record l) (cont_term e t) -> False.
  Proof.
    intros l e t H.
    set (P := fun (y y0: ext_term) =>
      match y, y0 with
      | ext_term_record l, Concrete_ss.cont_term e t => False
      | _, _ => True
      end).
    enough(P (ext_term_record l) (cont_term e t)) by assumption.
    induction H.
    - destruct a; simpl; trivial.
    - destruct a; simpl; trivial.
      destruct c; simpl; trivial.
      inversion H; subst; simpl in IHred; try (destruct IHred; fail);
        inversion H0; inversion H1.
  Qed.

  Lemma ext_term_record_safe_field:
    forall l t f,
    red interp_term_ss (ext_term_record l) (ext_term_field t f) -> False.
  Proof.
    intros l t f H.
    set (P := fun (y y0: ext_term) =>
      match y, y0 with
      | ext_term_record l, Concrete_ss.ext_term_field t f => False
      | _, _ => True
      end).
    enough(P (ext_term_record l) (ext_term_field t f)) by assumption.
    induction H.
    - destruct a; simpl; trivial.
    - destruct a; simpl; trivial.
      destruct c; simpl; trivial.
      inversion H; subst; simpl in IHred; try (destruct IHred; fail);
        inversion H0; inversion H1.
  Qed.

  Lemma ext_term_record_safe_nth:
    forall l t f,
    red interp_term_ss (ext_term_record l) (ext_term_nth t f) -> False.
  Proof.
    intros l t f H.
    set (P := fun (y y0: ext_term) =>
      match y, y0 with
      | ext_term_record l, Concrete_ss.ext_term_nth t f => False
      | _, _ => True
      end).
    enough(P (ext_term_record l) (ext_term_nth t f)) by assumption.
    induction H.
    - destruct a; simpl; trivial.
    - destruct a; simpl; trivial.
      destruct c; simpl; trivial.
      inversion H; subst; simpl in IHred; try (destruct IHred; fail);
        inversion H0; inversion H1.
  Qed.

  Lemma ext_term_record_safe_set:
    forall l1 t l2,
    red interp_term_ss (ext_term_record l1) (ext_term_rec_set t l2) -> False.
  Proof.
    intros l1 t l2 H.
    set (P := fun (y y0: ext_term) =>
      match y, y0 with
      | ext_term_record l1, Concrete_ss.ext_term_rec_set t l2 => False
      | _, _ => True
      end).
    enough(P (ext_term_record l1) (ext_term_rec_set t l2)) by assumption.
    induction H.
    - destruct a; simpl; trivial.
    - destruct a; simpl; trivial.
      destruct c; simpl; trivial.
      inversion H; subst; simpl in IHred; try (destruct IHred; fail);
        inversion H0; inversion H1.
  Qed.

  Lemma bs_to_ss_skel_apply_l:
    forall a b l,
    red interp_term_ss a b ->
    red interp_skel_ss
      (ext_skel_apply a l)
      (ext_skel_apply b l).
  Proof.
    intros a b l. induction 1; [id|].
    next; [|eassumption]. now constructor.
  Qed.

  Lemma bs_to_ss_skel_apply_r:
    forall a b,
      red interp_term_ss a b ->
    forall vl,
    red interp_skel_ss
      (ext_skel_apply (ret_term vl) a)
      (ext_skel_apply (ret_term vl) b).
  Proof.
    intros.
    induction H; subst; [id|].
    next; [|eassumption].
    constructor; assumption.
  Qed.

  Lemma bs_to_ss_skel_apply:
    forall f vf a va,
      red interp_term_ss f (ret_term vf) ->
      red interp_term_ss a (ret_term va) ->
      red interp_skel_ss
        (ext_skel_apply f a) (ext_skel_apply (ret_term vf) (ret_term va)). 
  Proof.
    intros f vf a va Hf Ha.
    slice.
    + apply bs_to_ss_skel_apply_l; eassumption.
    + apply bs_to_ss_skel_apply_r; eassumption.
  Qed.





  (* And the first theorem : Big-step implies Small-step *)

  Theorem bs_to_ss_term:
    forall e t v,
    interp_term e t v ->
    red interp_term_ss (cont_term e t) (ret_term v).
  Proof.
    apply it_ind; intros.
    - next; [econstructor; eassumption|id].
    - next; [econstructor; eassumption|id].
    - next; [econstructor; eassumption|id].
    - next; [econstructor; eassumption|assumption].
    - next. eapply itss_constr_embed.
      last. eapply itss_constr_extract.
      eapply bs_to_ss_constr. eassumption.
    - next. econstructor. next. simpl.
      erewrite(eq_refl:nil = map _ nil). eapply itss_tuple_extract. id.
    - next. eapply itss_tuple_embed.
      last. eapply itss_tuple_extract.
      simpl. slice.
        apply bs_to_ss_tuple_l. eassumption.
      apply bs_to_ss_tuple_r. clear - H2.
      revert tl vl H2.
      induction tl; destruct vl; intros.
      + constructor.
      + apply ss_to_bs_term in H2. inversion H2.
      + apply ss_to_bs_term in H2. inversion H2.
      +
      inversion H2; subst.
      inversion H; subst. clear H.
      assert(H:=H0).
      apply red_destr_end in H.
      destruct H as [|[b [H H']]]; [inversion H|].
      inversion H'; subst.
      apply ext_term_tuple_safe_cont in H. destruct H.
      apply ext_term_tuple_safe_cont in H. destruct H.
      assumption.
      apply ext_term_tuple_safe_field in H. destruct H.
      apply ext_term_tuple_safe_nth in H. destruct H.
    - next. econstructor. id.
    - next. econstructor. slice. apply bs_to_ss_field. eassumption.
      next. apply itss_field_extract; eassumption. id.
    - next. econstructor. slice. apply bs_to_ss_nth. eassumption.
      next. apply itss_nth_extract; eassumption. id.
    - next. constructor. next. simpl.
      rewrite (eq_refl:[] = map (fun '(f, t) => (f, ret_term t)) []).
      apply itss_rec_unfold. id.
    - next. constructor. simpl. slice.
      eapply bs_to_ss_record_l; eassumption.
      last. apply itss_rec_unfold.
      simpl.
      apply bs_to_ss_record_r. clear - H2.
      inversion H2; subst; clear H2. inversion H; subst; clear H.
      apply red_destr_end in H0. destruct H0 as [A|[b [A B]]];
          [discriminate A|].
      inversion B; subst; clear B.
      + apply ext_term_record_safe_cont in A. destruct A.
      + apply ext_term_record_safe_cont in A. destruct A.
      + apply ext_term_record_safe_field in A. destruct A.
      + apply ext_term_record_safe_nth in A. destruct A.
      + assumption.
      + apply ext_term_record_safe_set in A. destruct A.
    - slice.
      next; [constructor|].
      apply bs_to_ss_set_l; eassumption.
      slice. eapply bs_to_ss_set_r.
      inversion H2; subst; clear H2.
      inversion H3; subst; clear H3.
      apply red_destr_end in H4.
      destruct H4 as [A|[b [A B]]]; [discriminate A|].
      inversion B; subst; clear B.
      + apply ext_term_record_safe_cont in A; destruct A.
      + apply ext_term_record_safe_cont in A; destruct A.
      + apply ext_term_record_safe_field in A; destruct A.
      + apply ext_term_record_safe_nth in A; destruct A.
      + exact A.
      + apply ext_term_record_safe_set in A; destruct A.
      + next. apply itss_set_unfold. id.
  Qed.


  Theorem bs_to_ss_skel:
    forall e sk v,
    interp_skel e sk v ->
    red interp_skel_ss (cont_skel e sk) (ret_skel v).
  Proof.
    apply is_ind with
      (Hit := (fun e v c => red interp_term_ss (cont_term e v) (ret_term c)))
      (1:=bs_to_ss_term); intros.
    (* H_branch *)
    - next. econstructor. eassumption. eassumption.
    (* H_match *)
    - next. constructor.
      slice.
      apply bs_to_ss_match. eassumption.
      next. apply i_match_extract. eassumption. eassumption.
    (* H_return *)
    - next. econstructor.
     last. econstructor.
     apply bs_to_ss_return. assumption.
    (* H_apply_clos *)
    - next; [constructor|].
      slice; [eapply bs_to_ss_skel_apply_l; eassumption|].
      slice; [eapply bs_to_ss_skel_apply_r; eassumption|].
      slice; [|].
      next; [econstructor; eassumption|id].
      assumption.
    (* H_apply_unspec_done *)
    - next; [constructor|].
      slice; [eapply bs_to_ss_skel_apply_l; eassumption|].
      slice; [eapply bs_to_ss_skel_apply_r; eassumption|].
      slice; [|].
      next; [econstructor; eassumption|id].
      id.
    (* H_apply_unspec_cont *)
    - next; [constructor|].
      slice; [eapply bs_to_ss_skel_apply_l; eassumption|].
      slice; [eapply bs_to_ss_skel_apply_r; eassumption|].
      slice; [|].
      next; [econstructor; eassumption|id].
      id.
    (* H_letin *)
    - next. econstructor.
     slice.
     apply bs_to_ss_letin. eassumption.
     next. eapply isss_letin_extract. eassumption. eassumption.
    (* H_exists *)
    - next. apply isss_exists with (v:=v). eassumption.
      apply red_0.
  Qed.





  (***************************************************************************)
  (*                           Third part: SS ↔ BS                           *)
  (***************************************************************************)

  Theorem ss_bs_eq_term:
    forall e t v,
    interp_term e t v <->
    red (interp_term_ss) (cont_term e t) (ret_term v).
  Proof.
    intros; split; [apply bs_to_ss_term| apply ss_to_bs_term].
  Qed.

  Theorem ss_bs_eq_skel:
    forall e sk v,
    interp_skel e sk v <->
    red (interp_skel_ss) (cont_skel e sk) (ret_skel v).
  Proof.
    intros; split; [apply bs_to_ss_skel| apply ss_to_bs_skel].
  Qed.


  End Equivalence.
