(** This file proves the equivalence between WellFormed.v and
    WellFormedComputable.v *)

Require Import String List.
Import ListNotations.

(** See https://gitlab.inria.fr/skeletons/necro-coq/-/tree/master/files *)
From Necro Require Import Dict Skeleton WellFormed.
From Necro Require Import WellFormedComputable.
Open Scope bool.

Ltac congruence' :=
  simpl in *; congruence.




(* Auxiliary lemmae *)

Lemma eq_type_ok:
  forall τ τ',
  eq_type τ τ' = true <-> τ = τ'.
Proof.
  (* First, prove a lemma that will be used twice *)
  assert(aux:
    let eq_type_list := (fix reccall τl1 τl2 :=
      match τl1, τl2 with
      | [], [] => true
      | τ1 :: τq1, τ2 :: τq2 => (eq_type τ1 τ2 && reccall τq1 τq2)%bool
      | _, _ => false
      end)
    in forall τl,
    forall (Hall:Forall_type (fun τ => forall τ', eq_type τ τ' = true <-> τ = τ') τl),
    forall τl', eq_type_list τl τl' = true <-> τl = τl').
  1:{ intro eq_type_list.
     induction τl as [|τ τq Hτq].
    + intros _ [|? ?]; [split; reflexivity|].
      split; simpl; congruence'.
    + intros Hall [|τ' τq']; [split; simpl; congruence'|].
      inversion Hall as [| A B Hττ' Hall' [E F]]; subst A B; clear Hall.
      specialize(Hτq Hall'); clear Hall'.
      simpl. rewrite Bool.andb_true_iff.
      split.
      * intros [Heqττ' Heqτqτq'].
        apply Hττ' in Heqττ'. apply Hτq in Heqτqτq'. subst. reflexivity.
      * injection 1 as -> ->. split; [now apply Hττ'|now apply Hτq].
  }
  induction τ using type_rect; destruct τ'; try (simpl; split; congruence').
  (* Var *)
  - simpl; rewrite eqb_eq.
    split; [intros ->|injection 1 as ->]; auto.
  (* Base *)
  - split.
    + simpl. intro H. apply andb_prop in H.
      destruct H as [Hxs Hl].
      apply eqb_eq in Hxs; subst.
      f_equal. apply aux; assumption.
    + injection 1 as -> ->. simpl. apply andb_true_intro.
      split; [apply eqb_refl|]. apply aux; [assumption|reflexivity].
  (* Prod *)
  - split.
    + simpl. intro H.
      f_equal. apply aux; assumption.
    + injection 1 as ->. simpl.
      apply aux; [assumption|reflexivity].
  (* Arrow *)
  - split.
    + simpl. intro H. apply andb_prop in H.
      destruct H as [Hτ1 Hτ2].
      apply IHτ1 in Hτ1. apply IHτ2 in Hτ2.
      subst; reflexivity.
    + injection 1 as <- <-. simpl.
      apply andb_true_intro.
      split; [apply IHτ1|apply IHτ2]; reflexivity.
Qed.

Lemma eq_type_refl:
  forall τ,
  eq_type τ τ = true.
Proof.
  intro τ. apply eq_type_ok. reflexivity.
Qed.

Lemma mem_In:
  forall {X} eq,
    (forall x y, eq x y = true <-> x = y) ->
  forall (x:X) l,
  mem eq x l = true <-> In x l.
Proof.
  intros X eq Heq; induction l.
  + split; [simpl; congruence'| intros []].
  + split.
    - simpl.
      intro A. apply Bool.orb_prop in A.
      destruct A as [A|A]; [left|right].
      * symmetry. now rewrite Heq in A.
      * firstorder.
    - intros [A|A].
      * rewrite A. simpl. destruct (Heq x x) as [_ H].
        rewrite (H eq_refl); reflexivity.
      * simpl. apply IHl in A. rewrite A. now rewrite Bool.orb_true_r.
Qed.

Lemma mem_In_str:
  forall x l,
  mem eqb x l = true <-> In x l.
Proof.
  apply mem_In. apply eqb_eq.
Qed.

Lemma overlap_In:
  forall a b,
    no_overlap eqb a b = true <-> forall x : string, ~ (In x a /\ In x b).
Proof.
  induction a as [|ah aq IHa].
  - intro b. split; [|reflexivity]. intros _ x. tauto.
  - intro b; split.
    + intro H. simpl in H. apply andb_prop in H.
      destruct H as [Hah Haq].
      intros x [[Fah|Faq] Fb].
      * subst. apply mem_In_str in Fb. rewrite Fb in Hah. discriminate Hah.
      * exact(proj1 (IHa b) Haq x (conj Faq Fb)).
    + intro H. simpl. apply andb_true_intro. split.
      * destruct (mem eqb ah b) eqn:ahb; [exfalso|reflexivity].
        apply mem_In_str in ahb.
        exact(H ah (conj (or_introl _ eq_refl) ahb)).
      * refine(proj2 (IHa b) _).
        intro x. specialize(H x). contradict H.
        destruct H as [Haq Hb].
        split; [right|]; assumption.
Qed.

Lemma no_overlap_app_r_r:
  forall {A} eq,
  (forall x y, eq x y = true <-> x = y) ->
  forall l (l1 l2:list A),
  no_overlap eq l (l1 ++ l2) = true ->
  no_overlap eq l l2 = true.
Proof.
  intros A eq Heq.
  induction l as [|a l IHl]; [reflexivity|].
  intros l1 l2 H.
  simpl in H; apply andb_prop in H; destruct H as [Ha Hl].
  simpl; apply andb_true_intro.
  split; [|firstorder].
  destruct (mem eq a (l1 ++ l2)) eqn:Ha'; [discriminate Ha|clear Ha].
  destruct(mem eq a l2) eqn:H; [|reflexivity].
  apply Bool.not_true_iff_false in Ha'.
  contradict Ha'.
  apply mem_In; apply mem_In in H; try exact Heq.
  apply in_app_iff; right.
  assumption.
Qed.

Lemma no_overlap_nil_r_true:
  forall {A} eq (l:list A),
  no_overlap eq l [] = true.
Proof.
  now induction l.
Qed.

Lemma pattern_variables_tuple:
  forall l,
  concat (map pattern_variables l) = pattern_variables (pattern_tuple l).
Proof.
  induction l as [|p l IHl]; [reflexivity|].
  simpl. f_equal. exact IHl.
Qed.

Lemma pattern_variables_record:
  forall x,
  concat (map (fun x : string * list type * pattern =>
    let (_, p) := x in pattern_variables p) x) =
  pattern_variables (pattern_rec x).
Proof.
  induction x as [|[[f τl] p] l IHl]; [reflexivity|].
  simpl. f_equal. exact IHl.
Qed.

Lemma wf_pattern_record_first {ss}:
  forall f τl p fpl τ,
  well_formed_pattern ss (pattern_rec ((f, τl, p) :: fpl)) τ ->
  well_formed_pattern ss (pattern_rec ((f, τl, p) :: [] )) τ.
Proof.
  intros f τl p fpl τ wf.
  inversion wf; subst; clear wf.
  - eapply pattern_rec_one_wf; eassumption.
  - assumption.
Qed.

Lemma type_pattern_record_first {ss}:
  forall f τl p fpl τ,
  type_pattern ss (pattern_rec ((f, τl, p) :: fpl)) = Some τ ->
  type_pattern ss (pattern_rec ((f, τl, p) :: [] )) = Some τ.
Proof.
  intros f τl p fpl τ wf.
  simpl in wf; simpl.
  destruct find as [[[f_ta f_out] f_in]|]; [|congruence].
  destruct type_pattern as [τ'|]; [|congruence']. simpl in *.
  destruct eq_type; [|congruence'].
  destruct Nat.eqb; [|congruence'].
  destruct (_ fpl) in wf; [|congruence'].
  destruct (no_overlap) in wf; [|congruence'].
  destruct (no_overlap_list) in wf; [|congruence'].
  rewrite no_overlap_nil_r_true. assumption.
Qed.

Lemma pattern_rec_one_destr {ss}:
  forall f τl p τ,
  well_formed_pattern ss (pattern_rec [(f, τl, p)]) τ <->
  exists f_ta f_out f_in,
    τ = Base f_out τl /\
    find f (s_ftype ss) = Some (f_ta, f_out, f_in) /\
    Datatypes.length τl = Datatypes.length f_ta /\
    well_formed_pattern ss p (subst' f_in f_ta τl).
Proof.
  intros f τl p τ; split.
  - inversion 1; subst.
    + do 3 eexists; split; [|split; [|split]]; [reflexivity|eassumption..].
    + inversion H6.
  - intros [f_ta [f_out [f_in [-> [fsig [len wfp]]]]]].
    apply pattern_rec_one_wf with f_ta f_in; assumption.
Qed.

Lemma overlap_record_ind:
  forall pl f τl p,
   no_overlap_list eqb (map (fun (x:string * list type * pattern) =>
   let (_, p):= x in pattern_variables p) pl) = true ->
  (forall x,
    ~ (In x (pattern_variables p) /\
       In x (pattern_variables (pattern_rec pl)))) ->
   no_overlap_list eqb (map (fun (x:string * list type * pattern) =>
   let (_, p):= x in pattern_variables p) ((f,τl,p) ::pl)) = true.
Proof.
  intros pl f τl p IH H.
  simpl. rewrite IH. apply overlap_In in H.
  rewrite pattern_variables_record. rewrite H. reflexivity.
Qed.

(*
Lemma type_pattern_overlap {ss}:
  forall fpl τ,
    type_pattern ss (pattern_rec fpl) = Some τ ->
    no_overlap_list eqb (map (fun (x:string * list type * pattern) =>
    let (_, p):= x in pattern_variables p) fpl) = true.
Proof.
  induction fpl as [|[[f τl] p] fpl IH]; [reflexivity|].
  intros τ H.
 *)

Lemma type_pattern_rec_cons {ss}:
  forall f τl p fpl τ,
  type_pattern ss (pattern_rec ((f, τl, p) ::  [])) = Some τ ->
  type_pattern ss (pattern_rec (              fpl)) = Some τ ->
  (forall x,
    ~ (In x (pattern_variables p) /\
       In x (pattern_variables (pattern_rec fpl)))) ->
  type_pattern ss (pattern_rec ((f, τl, p) :: fpl)) = Some τ.
Proof.
  intros f τl p fpl τ Hf Hfpl overlap.
  destruct fpl as [|[[f' τl'] p'] fpl]; [assumption|].
  simpl in Hf; simpl.
  destruct find as [[[f_ta f_out] f_in]|] eqn:fsig; [|congruence].
  destruct type_pattern as [τ'|] eqn:Hp; [|congruence']; simpl in Hf; simpl.
  destruct eq_type eqn:eq; [|congruence']; simpl in Hf; simpl.
  destruct Nat.eqb eqn:eqb; [|congruence']; simpl in Hf; simpl.
  rewrite no_overlap_nil_r_true in Hf. injection Hf as <-.
  inversion Hfpl; subst; clear Hfpl.
  destruct (find f') as [[[f'_ta f'_out] f'_in]|] eqn:f'sig; [|congruence].
  destruct type_pattern as [τ''|] eqn:Hp' in H0; [|congruence']; simpl in H0.
  destruct eq_type eqn:eq' in H0; [|congruence']; simpl in H0; simpl.
  destruct Nat.eqb eqn:eqb' in H0; [|congruence']; simpl in H0; simpl.
  destruct (_ fpl) eqn:tr in H0; [|congruence']; simpl in H0; simpl.
  destruct no_overlap eqn:ol in H0; [|congruence']; simpl in H0; simpl.
  destruct no_overlap_list eqn:ol2 in H0; [|congruence']; simpl in H0; simpl.
  injection H0 as -> ->.
  rewrite eqb_refl; simpl.
  assert(A:=eq_type_refl (Prod τl)); simpl in A; rewrite A; clear A.
  rewrite Hp'; simpl. rewrite eq'; simpl. rewrite eqb'; simpl.
  rewrite tr; simpl.
  apply overlap_In in overlap. rewrite pattern_variables_record.
  simpl in overlap. simpl. rewrite overlap. simpl.
  rewrite pattern_variables_record in ol; simpl in ol; rewrite ol.
  rewrite ol2. reflexivity.
Qed.

Lemma ok_fields_ok:
  forall fl {A B} ss f_out (tl: list (string * A * B)),
      (forall x, In x fl ->
        forall f_ta f_in,
        find x (s_ftype ss) = Some (f_ta, f_out, f_in) ->
        In x (map (fun y => match y with (f, _, _) => f end) tl))
      <->
      ok_fields ss f_out tl fl = true.
Proof.
  induction fl as [|f fl IH];
    [split; [reflexivity|intros _ _ []]|].
  intros A B ss f_out tl; split; intro H.
  - simpl. apply andb_true_intro.
    split; [|apply IH; firstorder].
    (* if f does not exist, it is easy *)
    destruct (find f (s_ftype ss)) as [[[? f_out'] ?]|] eqn:Hfsig; [|reflexivity].
    (* if f is not a field in this type, still easy *)
    destruct (eqb f_out' f_out) eqn:Hfout; [simpl|reflexivity].
    (* so f_out is f_out' *) apply eqb_eq in Hfout; subst f_out'.
    specialize(H f (ltac:(left; reflexivity)) _ _ Hfsig).
    apply mem_In_str in H. exact H.
  - (* first extract parts about f and fl in H *)
    simpl in H. apply andb_prop in H. destruct H as [Hf Hfl].
    intros x [->|Hx] f_ta f_in Hfsig.
    + (* focus on Hf *) clear Hfl IH.
      rewrite Hfsig, eqb_refl in Hf. simpl in Hf.
      apply mem_In_str. exact Hf.
    + (* focus on Hfl *) clear Hf.
      specialize(IH A B ss f_out tl).
      firstorder.
Qed.

Lemma ok_type_record_1 ss:
    let type_record := (fix rec e f_out τl tl {struct tl} :=
    match tl with
    | [] => true
    | (f, τl', t) :: tq =>
        match type_term ss e t with
        | None => false
        | Some τt =>
            match find f (s_ftype ss) with
            | None => false
            | Some (f_ta, f_out', f_in) =>
              Nat.eqb (length f_ta) (length τl) &&
              eq_type (Prod τl) (Prod τl') &&
              eqb f_out f_out' &&
              eq_type τt (subst' f_in f_ta τl)
            end
        end && rec e f_out τl tq
    end)
    in
    forall e f_out τl tl,
    Forall_type
      (P't
         (fun t : term =>
          forall (e : typing_env) (τ : type),
          type_term ss e t = Some τ -> well_formed_term ss e t τ)) tl ->
    type_record e f_out τl tl = true ->
    well_formed_record ss e tl (Base f_out τl).
Proof.
  intros type_record. intros e f_out τl tl; revert e f_out τl;
    induction tl as [|[[f τl'] t] tl IH].
  - intros; constructor.
  - intros e f_out τl X H.
      simpl in H.
      destruct (type_term ss e t) as [τt|] eqn:Hτt in H;
            simpl in H; [|congruence].
      destruct (find f (s_ftype ss)) as [[[f_ta f_out'] f_in]|] eqn:Hfsig in H;
            simpl in H; [|congruence].
      destruct (Nat.eqb _ _) eqn:Hlen in H;
            simpl in H; [|congruence].
            apply PeanoNat.Nat.eqb_eq in Hlen.
      destruct (eq_type (Prod τl) (Prod τl')) eqn:Heq in H.
        2:{ simpl in Heq; rewrite Heq in H. simpl in H; discriminate H. }
        assert(Heq' := Heq). simpl in Heq'. rewrite Heq' in H. clear Heq'.
        apply eq_type_ok in Heq; injection Heq as <-. simpl in H.
      apply andb_prop in H. destruct H as [H Htl].
      apply andb_prop in H. destruct H as [Heq Heqτt].
      apply eq_type_ok in Heqτt. subst τt.
      apply eqb_eq in Heq. subst f_out'.
      inversion_clear X.
      econstructor; eauto.
Qed.

Lemma ok_type_record_2 ss:
    let type_record := (fix rec e f_out τl tl {struct tl} :=
    match tl with
    | [] => true
    | (f, τl', t) :: tq =>
        match type_term ss e t with
        | None => false
        | Some τt =>
            match find f (s_ftype ss) with
            | None => false
            | Some (f_ta, f_out', f_in) =>
              Nat.eqb (length f_ta) (length τl) &&
              eq_type (Prod τl) (Prod τl') &&
              eqb f_out f_out' &&
              eq_type τt (subst' f_in f_ta τl)
            end
        end && rec e f_out τl tq
    end)
    in
    forall e f_out τl tl,
    Forall_type
      (P't
         (fun t : term =>
          forall (e : typing_env) (τ : type),
          well_formed_term ss e t τ -> type_term ss e t = Some τ )) tl ->
    well_formed_record ss e tl (Base f_out τl) ->
    type_record e f_out τl tl = true.
Proof.
  intros type_record. intros e f_out τl tl; revert e f_out τl;
    induction tl as [|[[f τl'] t] tl IH].
  - intros. reflexivity.
  - intros e f_out τl X H.
    inversion_clear X. simpl in X0.
    inversion_clear H.
    specialize(IH e f_out τl X1 H3). clear H3 X1.
    apply X0 in H2; clear X0.
    simpl. rewrite H2, H0, H1, PeanoNat.Nat.eqb_refl.
    simpl.
    assert(G:=eq_refl: Prod τl = Prod τl).
    rewrite <- eq_type_ok in G. simpl in G. rewrite G. clear G.
    simpl. rewrite eqb_refl, eq_type_refl, IH. reflexivity.
Qed.

Lemma incl_list_ok:
  forall {X} eq,
    (forall x y, eq x y = true <-> x = y) ->
    forall (l1 l2: list X),
      incl_list eq l1 l2 = true <-> incl l1 l2.
Proof.
  intros X eq Heq; induction l1.
  + intros. simpl. split; [|reflexivity].
    intros _ _ [].
  + intro l2; simpl; split.
    * intro H; apply andb_prop in H.
      destruct H as [Ha Hl1].
      intros x [->|H]; [|now apply IHl1].
      now apply mem_In in Ha.
    * intro H. apply andb_true_intro.
      apply incl_cons_inv in H.
      destruct H as [Ha Hl1].
      split; [|now apply IHl1].
      now apply mem_In with (1:=Heq) in Ha.
Qed.

Lemma incl_list_ok_str:
  forall l1 l2,
    incl_list eqb l1 l2 = true <-> incl l1 l2.
Proof incl_list_ok eqb eqb_eq.

Lemma same_list_ok:
  forall {X} eq,
    (forall x y, eq x y = true <-> x = y) ->
 forall (l1 l2:list X),
    same_list eq l1 l2 = true <-> sameList l1 l2.
Proof.
  intros X eq Heq l1 l2; split.
  - intro H; apply andb_prop in H; destruct H as [A B].
    apply incl_list_ok with (1:=Heq) in A, B. now split.
  - intros [A B].
    apply andb_true_intro.
    apply incl_list_ok with (1:=Heq) in A, B. now split.
Qed.

Lemma same_list_ok_str:
  forall l1 l2,
    same_list eqb l1 l2 = true <-> sameList l1 l2.
Proof same_list_ok eqb eqb_eq.

Lemma forallb_andb {A}:
  forall (l: list A) f g,
    forallb (fun x => f x && g x) l =
    forallb (fun x => f x) l && forallb (fun x => g x) l.
Proof.
  intros l f g.
  destruct(forallb (fun x => f x && g x) l) eqn:H; symmetry.
  - assert(G:=proj1 (forallb_forall _ _) H); clear H.
    apply andb_true_intro.
    split.
    all: apply forallb_forall.
    all: intros x H; specialize(G x H).
    all: simpl in G.
    all: apply andb_prop in G; tauto.
  - apply Bool.not_true_iff_false.
    apply Bool.not_true_iff_false in H.
    contradict H.
    apply andb_prop in H.
    destruct H as [H1 H2].
    apply forallb_forall.
    intros x H.
    apply forallb_forall with (x:=x) (2:=H) in H1.
    apply forallb_forall with (x:=x) (2:=H) in H2.
    apply andb_true_intro. now split.
Qed.

Lemma eq_str_type:
  forall x y : string * type,
  (fun '(x, τ) '(x', τ') => (x =? x')%string && eq_type τ τ') x y =
  true <-> x = y.
Proof.
    intros [x τ] [x' τ']; split.
    + intro H. apply andb_prop in H; destruct H as [A B].
      apply eqb_eq in A; apply eq_type_ok in B.
      now subst.
    + injection 1 as <- <-.
      rewrite eqb_refl, eq_type_refl.
      reflexivity.
Qed.


(* Patterns *)

Lemma type_pattern_imp_wf_pattern:
  forall ss p ty, type_pattern ss p = Some ty -> well_formed_pattern ss p ty.
Proof.
  induction p using pattern_rect.
  - inversion 1. subst. constructor.
  - inversion 1. subst. constructor.
  - revert x x0. induction x as [|p pl IHpl].
    + intros _ τ. inversion 1; subst. constructor.
    + inversion 1; subst. intros τ.
      simpl.
      destruct (type_pattern ss p) as [τ0|] eqn:Hτ0; [|simpl; congruence'].
      destruct (
          (fix list (pl0 : list pattern) : option (list type) :=
             match pl0 with
             | [] => Some []
             | p0 :: pq =>
                 τ1 ← type_pattern ss p0 ;
                 τq ← list pq ;
                 Some (τ1 :: τq)
             end) pl
      ) eqn:Hlist; [|simpl; congruence'].
      destruct no_overlap eqn:overlap; [|simpl; congruence'].
      simpl. destruct no_overlap_list eqn:overlaplist; [|simpl; congruence'].
      injection 1 as <-.
      constructor.
      * now apply H1.
      * apply IHpl.
        -- inversion x0. assumption.
        -- clear - Hlist overlaplist.
           simpl. rewrite Hlist. rewrite overlaplist. reflexivity.
      * 
        clear - overlap.
        rewrite (pattern_variables_tuple pl) in overlap.
        apply overlap_In. assumption.
  - inversion 1.
    destruct (find x (s_ctype ss)) as [[[c_ta c_in] c_out]|] eqn:Hc;
        [|simpl in *; congruence'].
    destruct (type_pattern ss p) eqn:Hp; [|simpl in *; congruence'].
    specialize (IHp t eq_refl). simpl in H1.
    destruct (eq_type t (subst' c_in c_ta x0)) eqn:Hb1;
        simpl in H1; [|congruence'].
    destruct (Nat.eqb) eqn:Hb2 in H1;
        simpl in H1; [|congruence'].
    injection H1 as <-.
    eapply pattern_constr_wf.
    + apply eq_type_ok in Hb1. subst. eassumption.
    + assumption.
    + apply PeanoNat.Nat.eqb_eq in Hb2. assumption.
  - destruct x as [|[[f τl] p] pl]; [simpl; congruence'|].
    (* destruct pl as [|[[f2 τl2] p2] pl].
    + inversion 1.
    destruct (find f (s_ftype ss)) as [[[f_ta f_in] f_out]|] eqn:Hf;
        [|congruence'].
    destruct (type_pattern ss p) eqn:Hp; [|congruence'].
    destruct (eq_type t (subst' f_in f_ta τl)) eqn:Hb1;
        simpl in H1; [|congruence'].
    destruct (Nat.eqb) eqn:Hb2 in H1;
        simpl in H1; [|congruence'].
    injection H1 as <-.
    eapply pattern_rec_one_wf; [eassumption|now apply EqNat.beq_nat_eq_stt|].
    inversion_clear x0. clear X0. apply eq_type_ok in Hb1. subst.
    now apply X. 
    + *)
    inversion 1.
    destruct (find f (s_ftype ss)) as [[[f_ta f_out] f_in]|] eqn:Hf;
        [|congruence'].
    destruct (type_pattern ss p) eqn:Hp; [|congruence'].
    simpl in H1.
    destruct (eq_type t (subst' f_in f_ta τl)) eqn:Hb1;
        simpl in H1; [|congruence'].
    destruct (Nat.eqb) eqn:Hb2 in H1;
        simpl in H1; [|congruence'].
    set (type_record f_out_fst τl_fst := (fix type_rec pl :=
      match pl with
      | [] => true
      | (f, τl, p) :: q =>
          match find f (s_ftype ss) with
          | Some (f_ta, f_out, f_in) =>
              eqb f_out f_out_fst &&
                  eq_type (Prod τl) (Prod τl_fst) &&
              match type_pattern ss p with
              | Some τ =>
                  eq_type τ (subst' f_in f_ta τl) &&
                  Nat.eqb (length τl) (length f_ta)
              | None => false
              end
          | None => false
          end && type_rec q
      end)%bool).
    destruct (type_record f_out τl pl) eqn:Hb3;
      assert(A:=Hb3); unfold type_record in A; simpl in A; rewrite A in H1; clear A;
      [|congruence'].
    destruct no_overlap eqn:Hb7 in H1;
      [|congruence'].
    destruct no_overlap_list eqn:Hb5 in H1;
      [|congruence'].
    injection H1 as <-.
    apply eq_type_ok in Hb1. subst.
    inversion_clear x0. clear H. revert pl X0 Hb3 Hb5 Hb7.
    induction pl as [|[[f' τl'] p'] pl IHpl]; intros.
    + (* case 0: use pattern_rec_one_wf *)
      apply pattern_rec_one_wf with (f_ta:=f_ta) (f_in:=f_in).
      * assumption.
      * now apply PeanoNat.Nat.eqb_eq.
      * apply X. assumption.
    + inversion_clear X0.
      apply andb_prop in Hb3.
      simpl in Hb5. apply andb_prop in Hb5.
      destruct Hb3 as [Hb3 Hb4].
      destruct Hb5 as [Hb5 Hb6].
      simpl in Hb7; assert(Hb8 := Hb7); apply no_overlap_app_r_r in Hb7.
      specialize(IHpl X2 Hb4 Hb6 Hb7); clear X2 Hb4 type_record.
      apply pattern_rec_next_wf.
      * 
        destruct pl; [exact IHpl|inversion IHpl; assumption].
      * enough(A:
        well_formed_pattern ss (pattern_rec [(f', τl', p')]) (Base f_out τl)).
        1:{ destruct pl; [exact A|].
           apply pattern_rec_next_wf; [exact A| |].
           inversion IHpl; assumption. apply overlap_In.
           rewrite <- pattern_variables_record. assumption.
         }
        destruct (find f' (s_ftype ss)) as [[[f'_ta f'_out] f'_in]|] eqn:Hf';
            [|congruence'].
        destruct (f'_out =? f_out)%string eqn:H; [|discriminate Hb3].
        apply eqb_eq in H; subst f'_out.
        destruct (eq_type (Prod τl') (Prod τl)) eqn:H; [|discriminate Hb3].
        apply eq_type_ok in H; injection H as ->. simpl in Hb3.
        destruct (type_pattern ss p') eqn:Hp'; [|congruence'].
        apply andb_prop in Hb3.
        destruct Hb3 as [eqtype eqlength].
        apply eq_type_ok in eqtype. subst t.
        apply pattern_rec_one_wf with (f_ta:=f'_ta) (f_in:=f'_in).
        ** assumption.
        ** now apply PeanoNat.Nat.eqb_eq.
        ** apply X1. reflexivity.
      * apply overlap_In.
        rewrite <- pattern_variables_record. assumption.
      * apply eqb_eq.
  - intros. cbn in H.
    destruct (type_pattern ss p1) as [τ1|] eqn:Hp1; [|congruence']; simpl in H.
    destruct (type_pattern ss p2) as [τ2|] eqn:Hp2; [|congruence']; simpl in H.
    destruct (eq_type τ1 τ2) eqn:eqtype; [|congruence']; simpl in H.
    destruct (same_list _ _) eqn:eqlist; [|congruence']; simpl in H.
    apply eq_type_ok in eqtype; subst τ2.
    injection H as ->.
    specialize(IHp1 _ eq_refl); specialize(IHp2 _ eq_refl).
    constructor; [assumption..|].
    apply same_list_ok with (2:=eqlist).
    apply eq_str_type.
Qed.

Lemma wf_pattern_imp_type_pattern:
  forall ss p ty, well_formed_pattern ss p ty -> type_pattern ss p = Some ty.
Proof.
  induction p using pattern_rect.
  - inversion 1; reflexivity.
  - inversion 1; reflexivity.
  - revert x x0. induction x as [|p pl IHpl].
    + intros _ τ. inversion 1; subst. reflexivity.
    + inversion_clear 1.
      specialize(IHpl H0). clear H0.
      intros τ Hwf.
      inversion_clear Hwf. simpl.
      apply H in H0. rewrite H0. clear H0.
      apply IHpl in H1. simpl in H1.
      destruct (
          (fix list (pl0 : list pattern) : option (list type) :=
             match pl0 with
             | [] => Some []
             | p0 :: pq =>
                 τ1 ← type_pattern ss p0 ;
                 τq ← list pq ;
                 Some (τ1 :: τq)
             end) pl
      ); [|congruence'].
      destruct (no_overlap_list eqb (map pattern_variables pl)) eqn:overlaplist;
        [|congruence']. injection H1 as ->.
        apply overlap_In in H2.
        rewrite pattern_variables_tuple. rewrite H2. reflexivity.
  - inversion_clear 1.
    simpl. apply IHp in H0. clear IHp.
    rewrite H1. clear H1.
    rewrite H0. clear H0. simpl.
    rewrite eq_type_refl. rewrite H2. rewrite PeanoNat.Nat.eqb_refl.
    simpl. reflexivity.
  - rename x into fpl, x0 into Hall; intros τ.
    induction fpl as [|[[f τl] p] fpl IHfpl]; [inversion 1|].
    inversion Hall; subst; clear Hall.
    specialize (IHfpl X0); clear X0.
    rename X into IHp.
    intros H.
    inversion H; subst.
    + apply IHp in H7; simpl.
      rewrite H5, H7; simpl.
      rewrite eq_type_refl, H6, PeanoNat.Nat.eqb_refl; simpl.
      rewrite no_overlap_nil_r_true.
      reflexivity.
    + apply type_pattern_rec_cons.
      * clear dependent fpl.
        apply pattern_rec_one_destr in H5.
        destruct H5 as [f_ta [f_out [f_in [-> [fsig [Hlen wfp]]]]]].
        apply IHp in wfp; simpl.
        rewrite fsig, wfp; simpl.
        rewrite eq_type_refl, Hlen, PeanoNat.Nat.eqb_refl.
        rewrite no_overlap_nil_r_true. reflexivity.
      * firstorder.
      * assumption.
  - inversion_clear 1. simpl.
    apply IHp1 in H0; rewrite H0.
    apply IHp2 in H1; rewrite H1.
    simpl. rewrite eq_type_refl.
    simpl.
    rewrite (proj2 (same_list_ok _ eq_str_type (pattern_variables_with_type p1) (pattern_variables_with_type p2))); [reflexivity|].
    assumption.
Qed.

Proposition wf_pattern_eq:
  forall ss p ty, type_pattern ss p = Some ty <-> well_formed_pattern ss p ty.
Proof.
  split; [apply type_pattern_imp_wf_pattern|apply wf_pattern_imp_type_pattern].
Qed.



(* Terms and skeletons *)

(* These two functions are glorified [eq_refl]s *)
Lemma unfold_type_term:
  forall ss (e:typing_env) (t:term),
    type_term ss e t =
  let type_record := (fix rec e f_out τl tl {struct tl} :=
  match tl with
  | [] => true
  | (f, τl', t) :: tq =>
      match type_term ss e t with
      | None => false
      | Some τt =>
          match find f (s_ftype ss) with
          | None => false
          | Some (f_ta, f_out', f_in) =>
            Nat.eqb (length f_ta) (length τl) &&
            eq_type (Prod τl) (Prod τl') &&
            eqb f_out f_out' &&
            eq_type τt (subst' f_in f_ta τl)
          end
      end && rec e f_out τl tq
  end)
  in
  match t with
  | term_var (TVLet x τ) =>
      τ' ← findEnv x e ;
      if eq_type τ τ' then Some τ' else None
  | term_var (TVUnspec x τ args) =>
      tsig ← find x (s_unspec_term_decl ss) ;
      let (ta, ty) := tsig in
      if Nat.eqb (length ta) (length args) &&
         eq_type τ (subst' ty ta args)
      then Some τ else None
  | term_var (TVSpec x τ args) =>
      tsig ← find x (s_spec_term_decl ss) ;
      let '(ta, ty, _) := tsig in
      if Nat.eqb (length ta) (length args) &&
         eq_type τ (subst' ty ta args)
      then Some τ else None
  | term_constructor c τl t =>
      csig ← find c (s_ctype ss) ;
      let '(c_ta, c_in, c_out) := csig in
      τt ← type_term ss e t ;
      if eq_type τt (subst' c_in c_ta τl) &&
         Nat.eqb (length c_ta) (length τl)
      then Some (Base c_out τl)
      else None
  | term_field t τl f =>
      fsig ← find f (s_ftype ss) ;
      let '(f_ta, f_out, f_in) := fsig in
      τt ← type_term ss e t ;
      if eq_type τt (Base f_out τl) &&
         Nat.eqb (length f_ta) (length τl)
      then Some (subst' f_in f_ta τl)
      else None
  | term_nth t τl n =>
      τt ← type_term ss e t ;
      τ_out ← nth_error τl n ;
      if eq_type τt (Prod τl)
      then Some τ_out
      else None
  | term_rec_make [] => None
  | term_rec_make (((f, τl, τ) :: _) as tl) =>
      fsig ← find f (s_ftype ss) ;
      let '(_, f_out, _) := fsig in
        if type_record e f_out τl tl &&
           ok_fields ss f_out tl (s_field ss)
        then Some (Base f_out τl)
        else None
  | term_rec_set t tl =>
      match type_term ss e t with
      | Some (Base f_out τl) =>
          if type_record e f_out τl tl
          then Some (Base f_out τl)
          else None
      | _ => None
      end
  | term_tuple tl =>
      τl ← (fix list tl :=
        match tl with
        | [] => Some []
        | th :: tq =>
            τ ← type_term ss e th ;
            τl ← list tq ;
            Some (τ :: τl)
        end) tl ;
      Some (Prod τl)
  | term_func p s =>
      τp ← type_pattern ss p ;
      τs ← type_skel ss (extEnv p e) s ;
      Some (Arrow τp τs)
  end.
Proof. intros ss e t. destruct t; reflexivity. Qed.

Lemma unfold_type_skel:
  forall ss (e:typing_env) (s:skeleton),
    type_skel ss e s =
    match s with
    | skel_branch τ sl =>
        if (fix branch_ok sl :=
          match sl with
          | [] => true
          | sh :: sq =>
              match type_skel ss e sh with
              | Some τ' => eq_type τ τ'
              | None => false
              end && branch_ok sq
          end) sl
        then Some τ else None
    | skel_return t =>
        type_term ss e t
    | skel_letin p s1 s2 =>
        τ1 ← type_skel ss e s1 ;
        τp ← type_pattern ss p ;
        τ2 ← type_skel ss (extEnv p e) s2 ;
        if eq_type τ1 τp
        then Some τ2
        else None
    | skel_exists τ => Some τ
    | skel_match t τs psl =>
        let check_match τ_term τ_match :=
          (fix rec psl :=
            match psl with
            | [] => true
            | (p, s) :: psq =>
                match type_pattern ss p, type_skel ss (extEnv p e) s with
                | Some τp, Some τs =>
                    eq_type τp τ_term && eq_type τs τ_match
                | _, _ => false
                end && rec psq
            end)
        in
        τt ← type_term ss e t ;
        if check_match τt τs psl then Some τs else None
    | skel_apply f a =>
        τf ← type_term ss e f ;
        τa ← type_term ss e a ;
        match τf with
        | Arrow τi τo => if eq_type τi τa then Some τo else None
        | _ => None
        end
    end.
Proof. intros ss e s. destruct s; reflexivity. Qed.

Ltac unfold_hyp H :=
  match type of H with
  | type_term _ _ _ = _ =>
      rewrite unfold_type_term in H; simpl in H
  | type_skel _ _ _ = _ =>
      rewrite unfold_type_skel in H; simpl in H
  | _ => fail
  end.
Ltac unfold_goal :=
  match goal with
  | [ |- type_term _ _ _ = _ ] =>
      rewrite unfold_type_term; simpl
  | [ |- type_skel _ _ _ = _ ] =>
      rewrite unfold_type_skel; simpl
  | _ => fail
  end.

Tactic Notation "unfold1" := unfold_goal.
Tactic Notation "unfold1" hyp(H) := unfold_hyp H.

Lemma type_term_imp_wf_term:
  forall ss t g ty,
    type_term ss g t = Some ty ->
    well_formed_term ss g t ty.
Proof.
  (* Since well-formedness is a property that is defined in a structurally
     decreasing way, we can prove this lemma only by an induction on [t] *)
  intro ss.
  refine(
  (fun (Pt:term -> Prop) (Ps:skeleton -> Prop) A B C D E F G H I J K L M N =>
    (term_rect Pt Ps A B C D E F G H I J K L M N))
  (fun t => forall e τ, type_term ss e t = Some τ -> well_formed_term ss e t τ) (* Pt *)
  (fun s => forall e τ, type_skel ss e s = Some τ -> well_formed_skel ss e s τ) (* Ps *)
    _ _ _ _ _ _ _ _ _ _ _ _ _ _
  ).
  (* The structure for all premisses is basically the same. We introduce the
     hypothesis [type_term ss e t = Some τ] or [type_skel ss e s = Some τ], we
     unfold the first level, since e have information on the head constructor of
     [t] (resp. [s]), then using the [destruct] tactic, we extract the
     information from H into manageable pieces, and then we reconstruct the
     [well_formed] goal. *)
  - (* var *)
    intros [x τ|x τ τl|x τ τl] e τ' H.
    + unfold1 H.
      destruct (findEnv x e) as [τ''|] eqn:Hxe; simpl in H; [|congruence].
      destruct (eq_type τ τ'') eqn:Hττ''; simpl in H; [|congruence].
      injection H as <-. apply eq_type_ok in Hττ''. subst τ''. now constructor.
    + unfold1 H.
      destruct (find x (s_unspec_term_decl ss)) as [[τl_t τ_t]|] eqn:Htsig;
          simpl in H; [|congruence].
      destruct (Nat.eqb (length τl_t) (length τl)) eqn:Hlength;
          simpl in H; [|congruence].
      destruct (eq_type τ (subst' τ_t τl_t τl)) eqn:Hτ; simpl in H; [|congruence].
      injection H as <-. apply PeanoNat.Nat.eqb_eq in Hlength. apply eq_type_ok in Hτ.
      econstructor; eauto.
    + unfold1 H.
      destruct (find x (s_spec_term_decl ss)) as [[[τl_t τ_t] t]|] eqn:Htsig;
          simpl in H; [|congruence].
      destruct (Nat.eqb (length τl_t) (length τl)) eqn:Hlength;
          simpl in H; [|congruence].
      destruct (eq_type τ (subst' τ_t τl_t τl)) eqn:Hτ; simpl in H; [|congruence].
      injection H as <-. apply PeanoNat.Nat.eqb_eq in Hlength. apply eq_type_ok in Hτ.
      econstructor; eauto.
  - (* cons *)
    intros c τl t IH e τ H.
    + unfold1 H.
    destruct (find c (s_ctype ss)) as [[[c_τl c_in] c_out]|] eqn:Hcsig;
          simpl in H; [|congruence].
    destruct (type_term ss e t) as [τt|] eqn:Ht in H;
          simpl in H; [|congruence].
    destruct (eq_type τt _) eqn:Hτt in H;
          simpl in H; [|congruence].
          apply eq_type_ok in Hτt; subst τt.
    destruct (Nat.eqb _ _) eqn:Hlen in H;
          simpl in H; [|congruence].
    apply PeanoNat.Nat.eqb_eq in Hlen.
    injection H as <-.
    econstructor; eauto.
  - (* tuple *)
    (* We perform an induction on tl since the lists are not managed in the same
       way in both versions *)
    intro tl; induction tl as [|t tl IHtl].
    + intros _ e τ H. inversion H. repeat constructor.
    + intros Hall. inversion_clear Hall. specialize (IHtl H0). clear H0. rename H into IH.
      intros e τ H.
      unfold1 H.
    destruct (type_term ss e t) as [τt|] eqn:Ht in H;
          simpl in H; [|congruence].
    destruct (_ tl) as [τl|] eqn:Hlist in H;
          simpl in H; [|congruence].
    injection H as <-.
    constructor. constructor; [firstorder|].
    enough(well_formed_term ss e (term_tuple tl) (Prod τl)) by now inversion_clear H.
    apply IHtl. clear IH IHtl Ht.
    unfold1.
    rewrite Hlist. reflexivity.
  - (* func *)
    intros p s IH e τ H.
    unfold1 H.
    destruct (type_pattern ss p) as [τp|] eqn:Hp in H;
          simpl in H; [|congruence].
    destruct (type_skel ss (extEnv p e) s) as [τs|] eqn:Hs in H;
          simpl in H; [|congruence].
    injection H as <-.
    econstructor; [firstorder|]. now apply wf_pattern_eq.
  - (* field *)
    intros t τl f IH e τ H.
    unfold1 H.
    destruct (find f (s_ftype ss)) as [[[f_τl f_out] f_in]|] eqn:Hfsig;
          simpl in H; [|congruence].
    destruct (type_term ss e t) as [τt|] eqn:Ht in H;
          simpl in H; [|congruence].
    destruct (eq_type τt _) eqn:Hτt in H;
          simpl in H; [|congruence].
          apply eq_type_ok in Hτt; subst τt.
    destruct (Nat.eqb _ _) eqn:Hlen in H;
          simpl in H; [|congruence].
          apply PeanoNat.Nat.eqb_eq in Hlen.
    injection H as <-.
    econstructor; eauto.
  - (* nth *)
    intros t τl f IH e τ H.
    unfold1 H.
    destruct (type_term ss e t) as [τt|] eqn:Ht in H;
          simpl in H; [|congruence].
    destruct (nth_error) as [τ_out|] eqn:Hτ_out in H;
          simpl in H; [|congruence].
    destruct (eq_type τt _) eqn:Hτt in H;
          simpl in H; [|congruence].
          rewrite eq_type_ok in Hτt; subst τt.
    injection H as <-.
    econstructor; eauto. 
  - (* rec_make *)
    intros tl Hall e τ H.
    unfold1 H.
    destruct tl as [|[[f τl] ?]] eqn:Htl; [congruence|].
    destruct (find f (s_ftype ss)) as [[[f_τl f_out] f_in]|] eqn:Hfsig in H;
        simpl in H; [|congruence].
    destruct (type_term ss e t) as [τt|] eqn:Hτt in H;
        simpl in H; [|congruence].
    destruct (Nat.eqb (length f_τl) (length τl)) eqn:Hlen in H;
        simpl in H; [|congruence].
    destruct (_ τl τl) in H; simpl in H; [|congruence].
    rewrite eqb_refl in H. simpl in H.
    destruct (eq_type _ _) eqn:Heqτt in H;
    simpl in H; [|congruence].
    apply eq_type_ok in Heqτt. subst τt.
    set(type_record := (fix rec e f_out τl tl {struct tl} :=
    match tl with
    | [] => true
    | (f, τl', t) :: tq =>
        match type_term ss e t with
        | None => false
        | Some τt =>
            match find f (s_ftype ss) with
            | None => false
            | Some (f_ta, f_out', f_in) =>
              Nat.eqb (length f_ta) (length τl) &&
              eq_type (Prod τl) (Prod τl') &&
              eqb f_out f_out' &&
              eq_type τt (subst' f_in f_ta τl)
            end
        end && rec e f_out τl tq
    end)).
    destruct (type_record e f_out τl l) eqn:Hrec.
    all:assert(Hrec':=Hrec); unfold type_record in Hrec'; simpl in Hrec';
      rewrite Hrec' in H; clear Hrec'.
    2:simpl in H; congruence.
    destruct ok_fields eqn:Hok in H;
        simpl in H; [|congruence].
    injection H as <-.
    constructor.
    + apply ok_fields_ok. assumption.
    + discriminate.
    + apply ok_type_record_1; [assumption|].
      apply andb_true_intro.
      split; [|exact Hrec]. clear Hrec type_record.
      rewrite Hτt, Hfsig, Hlen, eq_type_refl, eqb_refl, eq_type_refl.
      reflexivity.
  - (* rec_set *)
    intros t tl IH Hall e τ H.
    unfold1 H.
    destruct (type_term ss e t) as [[|f_out τl| |]|] eqn:Hτt in H;
        simpl in H; try congruence.
    set(type_record := (fix rec e f_out τl tl {struct tl} :=
    match tl with
    | [] => true
    | (f, τl', t) :: tq =>
        match type_term ss e t with
        | None => false
        | Some τt =>
            match find f (s_ftype ss) with
            | None => false
            | Some (f_ta, f_out', f_in) =>
              Nat.eqb (length f_ta) (length τl) &&
              eq_type (Prod τl) (Prod τl') &&
              eqb f_out f_out' &&
              eq_type τt (subst' f_in f_ta τl)
            end
        end && rec e f_out τl tq
    end)).
    destruct (type_record e f_out τl tl) eqn:Hrec.
    all:assert(Hrec':=Hrec); unfold type_record in Hrec'; simpl in Hrec';
      rewrite Hrec' in H; clear Hrec'.
    2:congruence.
    apply ok_type_record_1 with (1:=Hall) in Hrec. clear type_record.
    injection H as <-.
    constructor; firstorder.
  - (* branchings *)
    (* We perform an induction on sl since the lists are not managed in the same
       way in both versions *)
    intros τ sl; induction sl as [|s sl IHsl].
    + intros _ e τ' H. unfold1 H. injection H as <-. repeat constructor.
    + inversion_clear 1. specialize(IHsl H1). clear H1. rename H0 into IH.
      intros e τ' H.
      unfold1 H.
      destruct (type_skel ss e s) as [τs|] eqn:Hs in H;
            simpl in H; [|congruence].
      destruct (eq_type τ τs) eqn:Hτs in H;
            simpl in H; [|congruence].
            rewrite eq_type_ok in Hτs; subst τs.
      destruct (_ sl) eqn:Hbranchok in H;
            simpl in H; [|congruence].
      injection H as <-.
      constructor. constructor; [firstorder|]. clear Hs IH.
      enough(well_formed_skel ss e (skel_branch τ sl) τ) by now inversion_clear H.
      apply IHsl. clear IHsl. unfold1. now rewrite Hbranchok.
  - (* apply *)
    intros t tl IHt IHtl e τ H.
    unfold1 H.
    destruct (type_term ss e t) as [τt|] eqn:Hτt in H;
          simpl in H; [|congruence].
    destruct (type_term ss e tl) as [τtl|] eqn:Hτtl in H;
          simpl in H; [|congruence].
    destruct τt as [| | |τti τto]; try congruence.
    destruct (eq_type τti τtl) eqn:eqtype; [|congruence].
    injection H as ->. apply eq_type_ok in eqtype. subst.
    econstructor; [apply IHt|apply IHtl]; eassumption.
  - (* return *) intros t IH e τ H.
    unfold1 H. constructor. now apply IH.
  - (* match *)
    intros t τ_match psl IH Hall e τ H.
    unfold1 H.
    destruct (type_term ss e t) as [τt|] eqn:Hτt in H;
          simpl in H; [|congruence].
    destruct (_ psl) eqn:Hmatch in H;
          simpl in H; [|congruence].
    injection H as ->.
    enough(Hpsl:
      Forall (fun y => well_formed_pattern ss (fst y) τt) psl  /\
      Forall (fun y => well_formed_skel ss (extEnv (fst y) e) (snd y) τ) psl).
    + destruct Hpsl; econstructor; [|eassumption..].
      apply IH. eassumption.
    + clear - Hmatch Hall. revert Hall Hmatch.
      induction psl as [|[p s] psq IH]; [split; constructor|].
      intros IHIH H.
      inversion_clear IHIH.
      apply andb_prop in H.
      destruct H as [H H'].
      specialize(IH X0 H'). clear X0 H'.
      destruct IH as [A B].
        destruct (type_pattern ss p) as [τp|]eqn:C; [|simpl in H; congruence].
        destruct type_skel eqn:D in H; [|congruence].
        apply andb_prop in H; destruct H as [H J].
        apply eq_type_ok in H, J. subst.
      split; (constructor; [simpl|assumption]); clear A B.
      * apply wf_pattern_eq. assumption.
      * simpl in X. apply X. clear X. assumption.
  - (* letin *)
    intros p s1 s2 IHs1 IHs2 e τ H.
    unfold1 H.
    destruct (type_skel ss e s1) as [τ1|] eqn:Hτ1 in H;
          simpl in H; [|congruence].
    destruct (type_pattern ss p) as [τp|] eqn:Hτp in H;
          simpl in H; [|congruence].
    destruct (type_skel ss _ s2) as [τ2|] eqn:Hτ2 in H;
          simpl in H; [|congruence].
    destruct (eq_type τ1 τp) eqn:Heq in H;
          simpl in H; [|congruence].
          apply eq_type_ok in Heq; subst τp.
    injection H as ->.
    econstructor; eauto. now apply wf_pattern_eq.
  - (* exist *)
    intros τex e τ H. injection H as ->.
    constructor.
Qed.

Lemma wf_term_imp_type_term:
  forall ss t g ty,
    well_formed_term ss g t ty ->
    type_term ss g t = Some ty.
Proof.
  (* Since well-formedness is a property that is defined in a structurally
     decreasing way, we can prove this lemma only by an induction on [t] *)
  intro ss.
  refine(
  (fun (Pt:term -> Prop) (Ps:skeleton -> Prop) A B C D E F G H I J K L M N =>
    (term_rect Pt Ps A B C D E F G H I J K L M N))
  (fun t => forall e τ, well_formed_term ss e t τ -> type_term ss e t = Some τ) (* Pt *)
  (fun s => forall e τ, well_formed_skel ss e s τ -> type_skel ss e s = Some τ) (* Ps *)
    _ _ _ _ _ _ _ _ _ _ _ _ _ _
  ).
  (* The structure for all premisses is once again basically the same.
     We introduce the hypothesis [well_formed_term ss e t τ] or
     [well_formed_skel ss e s τ], we perform an inversion to extract the information,
     we unfold the first [type_term] or [type_skel] of the goal, since the head
     constructor is known, and then we use the hypothesis to get to [Some τ = Some τ]. *)
  - (* var *)
    intros [x τ|x τ τl|x τ τl] e τ' H.
    + inversion_clear H. unfold1.
      rewrite H0; simpl.
      rewrite eq_type_refl; simpl.
      reflexivity.
    + inversion_clear H. unfold1.
      rewrite H0; simpl.
      rewrite H1, PeanoNat.Nat.eqb_refl; simpl.
      rewrite H2, eq_type_refl; simpl.
      reflexivity.
    + inversion_clear H. unfold1.
      rewrite H0; simpl.
      rewrite H1, PeanoNat.Nat.eqb_refl; simpl.
      rewrite H2, eq_type_refl; simpl.
      reflexivity.
  - (* cons *)
    intros c τl t IH e τ H.
    inversion_clear H. unfold1.
    rewrite H2; simpl.
    apply IH in H1; rewrite H1; simpl.
    rewrite eq_type_refl; simpl.
    rewrite H0, PeanoNat.Nat.eqb_refl; simpl.
    reflexivity.
  - (* tuple *)
    induction lt as [|t tq IHtq].
    1:{ intros _ e τ H.
      inversion_clear H.
      inversion_clear H0.
      reflexivity.
    }
    intros Hall. inversion_clear Hall.
    specialize(IHtq H0); clear H0.
    rename H into IHt.
    intros e τ H.
    inversion_clear H.
    inversion_clear H0.
    specialize(IHt _ _ H); clear H.
    unfold1.
    rewrite IHt; simpl.
    assert(A:well_formed_term ss e (term_tuple tq) (Prod l')) by now constructor.
    specialize(IHtq _ _ A); clear A.
    unfold1 IHtq.
    destruct(_ tq) as [τl|] eqn:Hfix in IHtq; simpl in IHtq; [|congruence].
    injection IHtq as <-.
    rewrite Hfix; simpl.
    reflexivity.
  - (* func *)
    intros p sk IH e τ H.
    inversion_clear H. unfold1.
    apply wf_pattern_eq in H1.
    apply IH in H0; clear IH.
    rewrite H1; simpl.
    rewrite H0; simpl.
    reflexivity.
  - (* field *)
    intros t τl f IH e τ H.
    inversion_clear H. unfold1.
    apply IH in H1; clear IH.
    rewrite H2; simpl.
    rewrite H1; simpl.
    rewrite eqb_refl; simpl.
    assert(A:=eq_type_refl (Prod τl));
      simpl in A; rewrite A; clear A; simpl.
    rewrite H0, PeanoNat.Nat.eqb_refl; simpl.
    reflexivity.
  - (* nth *)
    intros t τl n IH e τ H.
    inversion_clear H. unfold1.
    apply IH in H0; clear IH.
    rewrite H0; simpl.
    rewrite H1; simpl.
    assert(A:=eq_type_refl (Prod τl));
      simpl in A; rewrite A; clear A; simpl.
    reflexivity.
  - (* rec_make *)
    intros tl Hall e τ H.
    inversion_clear H. unfold1.
    apply ok_fields_ok in H0.
    destruct tl as [|[[f τl] ?] tq]; [now destruct H1|]. clear H1.
    inversion Hall; subst. simpl in X.
    inversion H2; subst.
    rewrite H7; simpl.
    apply X in H10; clear X.
    rewrite H10; simpl.
    rewrite H9, PeanoNat.Nat.eqb_refl; simpl.
    assert(A:=eq_type_refl (Prod tyl));
      simpl in A; rewrite A; clear A; simpl.
    rewrite eqb_refl, eq_type_refl; simpl.
    rewrite ok_type_record_2; [|assumption..]; simpl.
    rewrite H0; simpl.
    reflexivity.
  - (* rec_set *)
    intros t tl IH Hall e τ H.
    inversion_clear H. unfold1.
    apply IH in H0; clear IH.
    rewrite H0; simpl.
    apply ok_type_record_2 in H1; simpl in H1; [now rewrite H1|assumption].
  - (* branchings *)
    intros τ sl; revert τ; induction sl as [|s sq IHsq].
    1:{ intros τ _ e τ' H.
      inversion_clear H.
      reflexivity.
    }
    intros τ Hall. inversion_clear Hall.
    specialize(IHsq τ H0); clear H0.
    rename H into IHs.
    intros e τ' H.
    inversion H; subst.
    inversion_clear H4.
    specialize(IHs _ _ H0); clear H0.
    unfold1.
    rewrite IHs; simpl.
    assert(A:well_formed_skel ss e (skel_branch τ' sq) τ') by now constructor.
    rewrite eq_type_refl.
    specialize(IHsq _ _ A); clear A.
    unfold1 IHsq.
    destruct(_ sq) eqn:Hfix in IHsq; simpl in IHsq; [|congruence].
    rewrite Hfix.
    reflexivity.
  - (* applys *)
    intros f a IHf IHa e τ H.
    inversion_clear H.
    apply IHf in H0. apply IHa in H1.
    unfold1.
    rewrite H0, H1; simpl.
    rewrite eq_type_refl; reflexivity.
  - (* return *)
    intros t IH e τ H. inversion_clear H.
    unfold1. now apply IH.
  - (* matches *)
    intros t τ_match psl IHt IHpsl e τ H.
    inversion_clear H.
    apply IHt in H0; clear IHt.
    unfold1.
    rewrite H0; simpl.
    destruct (_ psl) eqn:Hpsl; [reflexivity|].
    exfalso. revert Hpsl.
    apply Bool.eq_true_false_abs.
    revert psl IHpsl H1 H2. clear.
    induction psl as [|[p s] psq IHpsq]; [reflexivity|].
    intros X Y Z.
    inversion_clear X.
    inversion_clear Y.
    inversion_clear Z.
    apply andb_true_intro. split; [|firstorder].
    clear X1 H0 H2 IHpsq. apply X0 in H1; clear X0.
    apply wf_pattern_eq in H; simpl in *.
    rewrite H; simpl.
    rewrite H1; simpl.
    rewrite 2 eq_type_refl.
    reflexivity.
  - (* letin *)
    intros p s1 s2 IHs1 IHs2 e τ H.
    inversion_clear H.
    rename H0 into Hs1, H1 into Hp, H2 into Hs2.
    unfold1.
    apply IHs1 in Hs1; apply IHs2 in Hs2; apply wf_pattern_eq in Hp.
    rewrite Hs1, Hp, Hs2; simpl.
    rewrite eq_type_refl; simpl.
    reflexivity.
  - (* exist *)
    intros τ_ex e τ H.
    inversion_clear H.
    unfold1. reflexivity.
Qed.

Proposition wf_term_eq:
  forall ss e t ty, type_term ss e t = Some ty <-> well_formed_term ss e t ty.
Proof.
  split; [apply type_term_imp_wf_term|apply wf_term_imp_type_term].
Qed.



Proposition wf_ctype_eq:
  forall ss,
    is_well_formed_ctype ss = true <-> well_formed_ctype ss.
Proof.
  intro ss.
  split.
  - intro H. unfold is_well_formed_ctype in H.
    apply andb_prop in H. destruct H as [H ok_c_τl].
    apply andb_prop in H. destruct H as [samelist ok_c_in].
    split; [|split].
    + now apply same_list_ok_str.
    + clear ok_c_τl. intros c.
      destruct (find c (s_ctype ss)) as [[[c_ta c_in] c_out]|] eqn:Hcsig;
          [|trivial].
      assert(H:=proj1 (forallb_forall _ _) ok_c_in c).
      clear ok_c_in.
      assert(a1: In c (keys (s_ctype ss))).
      { apply keys_spec. exists (c_ta, c_in, c_out). assumption. }
      specialize(H a1); clear a1. simpl in H.
      rewrite Hcsig in H. apply incl_list_ok_str. assumption.
    + clear ok_c_in.
      apply same_list_ok_str in samelist.
      assert(H:=samelist).
      destruct H as [H _].
      intros τ c c' c_τl c_τ c'_τl c'_τ Hcsig Hc'sig.
      assert(Hc:In c (keys (s_ctype ss))) by (apply keys_spec; eexists; eauto).
      assert(Hc':In c' (keys (s_ctype ss))) by (apply keys_spec; eexists; eauto).
      revert τ c c' c_τl c_τ c'_τl c'_τ Hc Hc' Hcsig Hc'sig.
      revert H ok_c_τl.
      generalize (keys (s_ctype ss)).
      induction l; [simpl; destruct 3|].
      intros d_constr IH τ c c' c_τl c_τ c'_τl c'_τ Hc Hc' Hcsig Hc'sig.
      assert(A:incl l (s_constructor ss)).
      { clear - d_constr. intros ? ?; apply d_constr; right; auto. }
      specialize (IHl A); clear A.
      simpl in IH. apply andb_prop in IH. destruct IH as [IH1 IH2].
      rewrite forallb_andb in IH2.
      apply andb_prop in IH2. destruct IH2 as [IH2 IH3].
      specialize (IHl IH3); clear IH3.
      apply andb_prop in IH1. destruct IH1 as [IH1 IH3].
      destruct (find a (s_ctype ss)) as [[[a_τl a_in] a_out]|] eqn:Hasig;
          simpl in IH1; [|congruence].
      clear IH1.
      destruct Hc as [->|Hc]; destruct Hc' as [->|Hc'].
      * rewrite Hcsig in Hc'sig; injection Hc'sig as -> ->. reflexivity.
      * rewrite Hasig in Hcsig; injection Hcsig as -> -> ->.
        apply forallb_forall with (x:=c') in IH3; [|assumption].
        rewrite Hc'sig, eqb_refl in IH3.
        apply PeanoNat.Nat.eqb_eq in IH3.
        assumption.
      * rewrite Hasig in Hc'sig; injection Hc'sig as -> -> ->.
        apply forallb_forall with (x:=c) in IH2; [|assumption].
        rewrite Hcsig, eqb_refl in IH2.
        apply PeanoNat.Nat.eqb_eq in IH2.
        assumption.
      * specialize(IHl _ _ _ _ _ _ _ Hc Hc' Hcsig Hc'sig). exact IHl.
  - intros [A [B C]].
    unfold is_well_formed_ctype.
    apply same_list_ok_str in A.
    rewrite A. simpl.
    apply andb_true_intro; split.
    + apply forallb_forall. intros x H.
      apply keys_spec in H; destruct H as [[[c_ta c_in] c_out] H].
      rewrite H.
      apply incl_list_ok_str.
      specialize(B x). rewrite H in B. assumption.
    + apply forallb_forall. intros c Hc.
      apply forallb_forall. intros c' Hc'.
      apply keys_spec in Hc; destruct Hc as [[[c_ta c_in] c_out] Hcsig].
      apply keys_spec in Hc'; destruct Hc' as [[[c'_ta c'_in] c'_out] Hc'sig].
      rewrite Hcsig, Hc'sig.
      destruct(eqb c_out c'_out) eqn:e; [|reflexivity].
      apply eqb_eq in e; subst c'_out.
      specialize(C _ _ _ _ _ _ _ Hcsig Hc'sig).
      rewrite C; apply PeanoNat.Nat.eqb_refl.
Qed.

Proposition wf_ftype_eq:
  forall ss,
    is_well_formed_ftype ss = true <-> well_formed_ftype ss.
Proof.
  intro ss.
  split.
  - intro H. unfold is_well_formed_ftype in H.
    apply andb_prop in H. destruct H as [H ok_f_τl].
    apply andb_prop in H. destruct H as [samelist ok_f_in].
    split; [|split].
    + now apply same_list_ok_str.
    + clear ok_f_τl. intros f.
      destruct (find f (s_ftype ss)) as [[[f_ta f_out] f_in]|] eqn:Hfsig;
          [|trivial].
      assert(H:=proj1 (forallb_forall _ _) ok_f_in f).
      clear ok_f_in.
      assert(a1: In f (keys (s_ftype ss))).
      { apply keys_spec. exists (f_ta, f_out, f_in). assumption. }
      specialize(H a1); clear a1. simpl in H.
      rewrite Hfsig in H. apply incl_list_ok_str. assumption.
    + clear ok_f_in.
      apply same_list_ok_str in samelist.
      assert(H:=samelist).
      destruct H as [H _].
      intros τ f f' f_τl f_τ f'_τl f'_τ Hfsig Hf'sig.
      assert(Hf:In f (keys (s_ftype ss))) by (apply keys_spec; eexists; eauto).
      assert(Hf':In f' (keys (s_ftype ss))) by (apply keys_spec; eexists; eauto).
      revert τ f f' f_τl f_τ f'_τl f'_τ Hf Hf' Hfsig Hf'sig.
      revert H ok_f_τl.
      generalize (keys (s_ftype ss)).
      induction l; [simpl; destruct 3|].
      intros d_field IH τ f f' f_τl f_τ f'_τl f'_τ Hf Hf' Hfsig Hf'sig.
      assert(A:incl l (s_field ss)).
      { clear - d_field. intros ? ?; apply d_field; right; auto. }
      specialize (IHl A); clear A.
      simpl in IH. apply andb_prop in IH. destruct IH as [IH1 IH2].
      rewrite forallb_andb in IH2.
      apply andb_prop in IH2. destruct IH2 as [IH2 IH3].
      specialize (IHl IH3); clear IH3.
      apply andb_prop in IH1. destruct IH1 as [IH1 IH3].
      destruct (find a (s_ftype ss)) as [[[a_τl a_in] a_out]|] eqn:Hasig;
          simpl in IH1; [|congruence].
      clear IH1.
      destruct Hf as [->|Hf]; destruct Hf' as [->|Hf'].
      * rewrite Hfsig in Hf'sig; injection Hf'sig as -> ->. reflexivity.
      * rewrite Hasig in Hfsig; injection Hfsig as -> -> ->.
        apply forallb_forall with (x:=f') in IH3; [|assumption].
        rewrite Hf'sig, eqb_refl in IH3.
        apply PeanoNat.Nat.eqb_eq in IH3.
        assumption.
      * rewrite Hasig in Hf'sig; injection Hf'sig as -> -> ->.
        apply forallb_forall with (x:=f) in IH2; [|assumption].
        rewrite Hfsig, eqb_refl in IH2.
        apply PeanoNat.Nat.eqb_eq in IH2.
        assumption.
      * specialize(IHl _ _ _ _ _ _ _ Hf Hf' Hfsig Hf'sig). exact IHl.
  - intros [A [B C]].
    unfold is_well_formed_ftype.
    apply same_list_ok_str in A.
    rewrite A. simpl.
    apply andb_true_intro; split.
    + apply forallb_forall. intros x H.
      apply keys_spec in H; destruct H as [[[f_ta f_out] f_in] H].
      rewrite H.
      apply incl_list_ok_str.
      specialize(B x). rewrite H in B. assumption.
    + apply forallb_forall. intros f Hf.
      apply forallb_forall. intros f' Hf'.
      apply keys_spec in Hf; destruct Hf as [[[f_ta f_out] f_in] Hfsig].
      apply keys_spec in Hf'; destruct Hf' as [[[f'_ta f'_out] f'_in] Hf'sig].
      rewrite Hfsig, Hf'sig.
      destruct(eqb f_out f'_out) eqn:e; [|reflexivity].
      apply eqb_eq in e; subst f'_out.
      specialize(C _ _ _ _ _ _ _ Hfsig Hf'sig).
      rewrite C; apply PeanoNat.Nat.eqb_refl.
Qed.


Lemma wf_unspec_terms_eq:
  forall ss,
    is_well_formed_unspec_terms ss = true <-> well_formed_unspec_terms ss.
Proof.
  intro ss; split; intro H.
  - unfold is_well_formed_unspec_terms in H.
    apply andb_prop in H.
    destruct H as [A B].
    apply same_list_ok_str in A.
    constructor; [assumption|clear A].
    intros x τl τ Htsig.
    apply forallb_forall with (x:=x) in B.
    + rewrite Htsig in B. apply incl_list_ok_str in B. exact B.
    + apply keys_spec. eexists. eassumption.
  - unfold is_well_formed_unspec_terms.
    apply andb_true_intro.
    destruct H as [A B].
    apply same_list_ok_str in A.
    split; [assumption|clear A].
    apply forallb_forall.
    intros x H.
    apply keys_spec in H.
    destruct H as [[τl τ] H].
    rewrite H.
    apply incl_list_ok_str. eapply B; eassumption.
Qed.

Lemma wf_spec_terms_eq:
  forall ss,
    is_well_formed_spec_terms ss = true <-> well_formed_spec_terms ss.
Proof.
  intro ss; split; intro H.
  - unfold is_well_formed_spec_terms in H.
    apply andb_prop in H.
    destruct H as [A B].
    apply same_list_ok_str in A.
    constructor; [assumption|clear A].
    intros x τl τ t Htdef.
    apply forallb_forall with (x:=x) in B.
    + rewrite Htdef in B.
      apply andb_prop in B.
      destruct B as [B C].
      apply incl_list_ok_str in B.
      split; [exact B|].
      apply wf_term_eq.
      destruct (type_term ss [] t); [|discriminate C].
      apply eq_type_ok in C. now subst.
    + apply keys_spec. eexists. eassumption.
  - unfold is_well_formed_unspec_terms.
    apply andb_true_intro.
    destruct H as [A B].
    apply same_list_ok_str in A.
    split; [assumption|clear A].
    apply forallb_forall.
    intros x H.
    apply keys_spec in H.
    destruct H as [[[τl τ] t] H].
    rewrite H.
    apply andb_true_intro.
    specialize (B _ _ _ _ H).
    destruct B as [B C].
    split; [now apply incl_list_ok_str|].
    apply wf_term_eq in C. rewrite C.
    apply eq_type_refl.
Qed.

Lemma wf_ss_eq:
  forall ss,
    is_well_formed_semantics ss = true <-> well_formed_semantics ss.
Proof.
  intro ss. split.
  - intro H.
    unfold is_well_formed_semantics in H.
    apply andb_prop in H; destruct H as [H D].
    apply andb_prop in H; destruct H as [H C].
    apply andb_prop in H; destruct H as [A B].
    apply wf_ctype_eq in A.
    apply wf_ftype_eq in B.
    apply wf_unspec_terms_eq in C.
    apply wf_spec_terms_eq in D.
    now constructor.
  - intro H. destruct H as [A B C D].
    apply wf_ctype_eq in A.
    apply wf_ftype_eq in B.
    apply wf_unspec_terms_eq in C.
    apply wf_spec_terms_eq in D.
    unfold is_well_formed_semantics.
    rewrite A,B,C,D.
    reflexivity.
Qed.


