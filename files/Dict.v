Require Import List String.


(* TODO: use a better instantiation *)

Inductive dict (A:Type) :=
| empty
| add: string -> A -> dict A -> dict A.

Fixpoint find {A} x (d:dict A): option A :=
  match d with
  | empty _ => None
  | add _ y v d =>
      if eqb x y then Some v else find x d
  end.

Fixpoint keys {A} (d: dict A): list string :=
  match d with
  | empty _ => nil
  | add _ x _ d => x :: keys d
  end.


(* General lemmae with implementation dependent proofs *)

(* Lemma dict_ind:
  forall {A} (P: dict A -> Prop),
  (P (empty A)) ->
  (forall x v d, P d -> P (add A x v d)) ->
  forall d, P d.
 Proof dict_ind. *)

Lemma find_empty:
  forall A x, find x (empty A) = None.
Proof.
  reflexivity.
Qed.

Lemma find_success:
  forall A k v d, find k (add A k v d) = Some v.
Proof.
  intros; simpl. rewrite eqb_refl; reflexivity.
Qed.

Lemma find_add:
  forall A k k' v d, k <> k' -> find k (add A k' v d) = find k d.
Proof.
  intros A k k' v d neq; simpl.
  apply eqb_neq in neq; rewrite neq.
  reflexivity.
Qed.

Lemma keys_empty:
  forall A, keys (empty A) = nil.
Proof.
  reflexivity.
Qed.

Lemma keys_add:
  forall A x y v d,
    In y (keys (add A x v d))
    <-> y = x \/ In y (keys d).
Proof.
  intros A x y v d; simpl.
  split; (intros [H|H]; [left|right]); auto.
Qed.





(* General lemmae that are implied by the preceeding lemmae *)

Lemma keys_spec:
  forall A (d:dict A),
    forall x, (exists v, find x d = Some v) <-> In x (keys d).
Proof.
  intros A d; induction d as [|x v dq] using dict_ind.
  - intro x; split; intro H.
    + destruct H as [? H]. rewrite find_empty in H. discriminate H.
    + rewrite keys_empty in H. destruct H.
  - intro y; split; intro H.
    + destruct H as [w H].
      destruct (string_dec y x) as [e|n].
      * subst. rewrite find_success in H. injection H as <-.
        apply keys_add. now left.
      * apply keys_add. right.
        rewrite find_add with (1:=n) in H.
        apply IHdq. now exists w.
    + apply keys_add in H.
      destruct (string_dec y x) as [->|n].
      * exists v; apply find_success.
      * destruct H as [|H]; [congruence|].
        apply IHdq in H. destruct H as [w H].
        exists w. now rewrite find_add.
Qed.




Module DictNotations.
  Notation "∅" := (empty _).
  Notation "a ← { x → y }" := (add _ x y a) (left associativity, at level 20).
End DictNotations.

Arguments empty {_}.
Arguments add [_] _ _ _.
