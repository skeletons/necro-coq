Require Import String List.
From Necro Require Import Dict.
Import ListNotations DictNotations.

(** See https://gitlab.inria.fr/skeletons/necro-coq/-/tree/master/files *)
From Necro Require Import Skeleton WellFormed Concrete.

Section InductionInterpValue.
  Variable s : skeletal_semantics.
  Variable u : dict (nat * unspec_function).

  Variable Hit : env -> term -> cvalue -> Prop.

  Variable H_var_val :
    forall e x ty v,
        findEnv x e = Some v ->
        Hit e (term_var (TVLet x ty)) v.

  Variable H_var_unspec_O :
    forall e x ty ta v w,
        find x u = Some (0, v) ->
        v ta [] w ->
        Hit e (term_var (TVUnspec x ty ta)) w.

  Variable H_var_unspec_S :
    forall e x ty ta v n,
        find x u = Some (S n, v) ->
        Hit e (term_var (TVUnspec x ty ta)) (cval_unspec n x ta []).

  Variable H_var_spec :
    forall e t x ty ta tyl ty' v,
        find x (s_spec_term_decl s) = Some (ta, ty, t) ->
        length ta = length tyl ->
        interp_term s u [] (subst_term' t ta tyl) v ->
        Hit [] (subst_term' t ta tyl) v ->
        Hit e (term_var (TVSpec x ty' tyl)) v.

  Variable H_constr :
    forall e arg c ta v,
        interp_term s u e arg v ->
        Hit e arg v ->
        Hit e (term_constructor c ta arg) (cval_constructor c v).

  Variable H_tuple_nil :
    forall e, Hit e (term_tuple []) (cval_tuple []).

  Variable H_tuple_cons :
    forall e t tl v vl,
        interp_term s u e t v ->
        Hit e t v ->
        interp_term s u e (term_tuple tl) (cval_tuple vl) ->
        Hit e (term_tuple tl) (cval_tuple vl)  ->
        Hit e (term_tuple (t :: tl)) (cval_tuple (v::vl)).

  Variable H_func :
    forall p sk e,
    Hit e (term_func p sk) (cval_closure p sk e).

  Variable H_field: forall e t ta f cl v,
      interp_term s u e t (cval_record cl) ->
      Hit e t (cval_record cl) ->
      findEnv f cl = Some v ->
      Hit e (term_field t ta f) v.

  Variable H_nth: forall e t ta n cl v,
      interp_term s u e t (cval_tuple cl) ->
      Hit e t (cval_tuple cl) ->
      nth_error cl n = Some v ->
      Hit e (term_nth t ta n) v.

  Variable H_rec_make_nil: forall e,
      Hit e (term_rec_make []) (cval_record []).

  Variable H_rec_make_cons: forall e f ta t tl v vl,
      interp_term s u e t v ->
      Hit e t v ->
      interp_term s u e (term_rec_make tl) (cval_record vl) ->
      Hit e (term_rec_make tl) (cval_record vl) ->
      Hit e (term_rec_make ((f,ta,t)::tl)) (cval_record ((f,v)::vl)).

  Variable H_rec_set: forall e t tl vl1 vl2,
      interp_term s u e t (cval_record vl1) ->
      Hit e t (cval_record vl1) ->
      interp_term s u e (term_rec_make tl) (cval_record vl2) ->
      Hit e (term_rec_make tl) (cval_record vl2) ->
      Hit e (term_rec_set t tl) (cval_record (vl2++vl1)).


Fixpoint it_ind e v c i {struct i} : Hit e v c :=
  match i as i0 in (interp_term _ _ e v c) return (Hit e v c) with
  | i_var_let _ _ e x ty v e1 => H_var_val e x ty v e1
  | i_var_unspec_O _ _ e x ty ta v w e1 e2 =>
      H_var_unspec_O e x ty ta v w e1 e2
  | i_var_unspec_S _ _ e x ty ta v n e1 =>
      H_var_unspec_S e x ty ta v n e1
  | i_var_spec _ _ e t x ty ta tyl ty' v e1 e2 e3 =>
      H_var_spec e t x ty ta tyl ty' v e1 e2 e3
        (it_ind [] (subst_term' t ta tyl) v e3)
  | i_constr _ _ e arg c ta v i0 =>
      H_constr e arg c ta v i0 (it_ind e arg v i0)
  | i_tuple_nil _ _ e => H_tuple_nil e
  | i_tuple_cons _ _ e t tl v vl i0 i1 =>
      H_tuple_cons e t tl v vl i0 (it_ind e t v i0) i1
        (it_ind e (term_tuple tl) (cval_tuple vl) i1)
  | i_func _ _ p sk e => H_func p sk e
  | i_field _ _ e t ta f cl v i eq => H_field e t ta f cl v i 
      (it_ind e t (cval_record cl) i) eq
  | i_nth _ _ e t ta n cl v i eq => H_nth e t ta n cl v i 
      (it_ind e t (cval_tuple cl) i) eq
  | i_rec_make_nil _ _ e => H_rec_make_nil e
  | i_rec_make_cons _ _ e f ta t tl v vl i1 i2 => H_rec_make_cons e f ta t
      tl v vl i1 (it_ind e _ _ i1) i2 (it_ind e _ _ i2)
  | i_rec_set _ _ e t tl vl1 vl2 i1 i2 => H_rec_set e t tl vl1 vl2
      i1 (it_ind e _ _ i1) i2 (it_ind e _ _ i2)
  end.
End InductionInterpValue.


Section InductionInterpSkel.
  Variable s : skeletal_semantics.
  Variable u : dict (nat * unspec_function).
  Variable g : type -> cvalue -> Prop.
  Variable Hit : env -> term -> cvalue -> Prop.
  Variable His : env -> skeleton -> cvalue -> Prop.

  Variable it_ind: forall e v c,
    interp_term s u e v c ->
    Hit e v c.

  Variable H_branch :
    forall ty e v sks ski,
        In ski sks ->
        interp_skel s u g e ski v ->
        His e ski v ->
        His e (skel_branch ty sks) v.

  Variable H_match:
    forall tm ty sk psl e e' v w,
      interp_term s u e tm v ->
      Hit e tm v ->
      get_match e psl v = Some (sk, e') ->
      interp_skel s u g e' sk w ->
      His e' sk w ->
      His e (skel_match tm ty psl) w.

  Variable H_return :
    forall e t v,
        interp_term s u e t v ->
        Hit e t v ->
        His e (skel_return t) v.

  Variable H_apply_clos :
    forall e f p sk e' e'' a v res,
      interp_term s u e f (cval_closure p sk e') ->
      Hit e f (cval_closure p sk e') ->
      interp_term s u e a v ->
      Hit e a v ->
      add_asn e' p v = Some e'' ->
      interp_skel s u g e'' sk res ->
      His e'' sk res ->
      His e (skel_apply f a) res.

  Variable H_apply_unspec_done:
    forall e f a v w res m x ta vl,
      interp_term s u e f (cval_unspec 0 x ta vl) ->
      Hit e f (cval_unspec 0 x ta vl) ->
      interp_term s u e a v ->
      Hit e a v ->
      find x u = Some (m, w) ->
      w ta (app vl [v]) res ->
      His e (skel_apply f a) res.

  Variable H_apply_unspec_cont:
    forall e f a n x ta vl v,
      interp_term s u e f (cval_unspec (S n) x ta vl) ->
      Hit e f (cval_unspec (S n) x ta vl) ->
      interp_term s u e a v ->
      Hit e a v ->
      His e (skel_apply f a) (cval_unspec n x ta (vl ++ [v])).

  Variable H_letin :
    forall e e' p s1 s2 v w,
        interp_skel s u g e s1 v ->
        His e s1 v ->
        add_asn e p v = Some e' ->
        interp_skel s u g e' s2 w ->
        His e' s2 w ->
        His e (skel_letin p s1 s2) w.

  Variable H_exists :
    forall e ty v,
        g ty v ->
        His e (skel_exists ty) v.

Fixpoint is_ind e s0 c i {struct i} : His e s0 c :=
  match i as i0 in (interp_skel _ _ _ e0 s1 c0) return (His e0 s1 c0) with
  | i_branch _ _ _ ty e0 v sks ski i0 i1 =>
      H_branch ty e0 v sks ski i0 i1 (is_ind e0 ski v i1)
  | i_match _ _ _ tm ty s psl e e' v w it gm is =>
      H_match tm ty s psl e e' v w it (it_ind e tm v it) gm is (is_ind e' s w is)
  | i_return _ _ _ e0 t v i0 => H_return e0 t v i0 (it_ind e0 t v i0)
  | i_letin _ _ _ e0 e' p s1 s2 v w i0 a i1 =>
      H_letin e0 e' p s1 s2 v w i0 (is_ind e0 s1 v i0) a i1 (is_ind e' s2 w i1)
  | i_exists _ _ _ e ty v H =>
      H_exists e ty v H
  | i_apply_clos _ _ _ e0 f p sk e' e'' arg v res itf itv a is =>
      H_apply_clos e0 f p sk e' e'' arg v res itf (it_ind _ _ _ itf) itv (it_ind _ _ _ itv) a is (is_ind _ _ _ is)
  | i_apply_unspec_done _ _ _ e0 f arg v w res m x ta vl itf ita Hx Hw =>
  H_apply_unspec_done e0 f arg v w res m x ta vl itf (it_ind _ _ _ itf) ita (it_ind _ _ _ ita) Hx Hw
  | i_apply_unspec_cont _ _ _ e0 f arg n x ta vl v itf ita =>
      H_apply_unspec_cont e0 f arg n x ta vl v itf (it_ind _ _ _ itf) ita (it_ind _ _ _ ita)
  end.

End InductionInterpSkel.


Section InductionCterm.

  Inductive Forall_list {A} (P:A->Type) : list A -> Type :=
  | Forall_list_nil: Forall_list P []
  | Forall_list_cons x l: P x -> Forall_list P l -> Forall_list P (x :: l).
  Arguments Forall_list_nil {_} {_}.
  Arguments Forall_list_cons {_} {_} _ _ _ _.

  Variable s : skeletal_semantics.

  (* In case we need an induction on cvalue *)
  Fixpoint cvalue_rect (P: cvalue -> Type)
           base constr tuple closure unspec rec v :=
  let Plist := (fix Plist l :=
      match l return Forall_list P l with
      | []     => Forall_list_nil
      | h :: q => Forall_list_cons h q
        (cvalue_rect P base constr tuple closure unspec rec h) (Plist q)
      end) in
  let Plist2 := (fix Plist2 l :=
      match l return Forall_list (fun '(s,h) => P h) l with
      | []     => Forall_list_nil
      | (s,h) :: q => Forall_list_cons (s,h) q
        (cvalue_rect P base constr tuple closure unspec rec h) (Plist2 q)
      end) in
  match v with
  | cval_base A a => base A a
  | cval_constructor c v =>
    constr c v (cvalue_rect P base constr tuple closure unspec rec v)
  | cval_tuple lv => tuple lv (Plist lv)
  | cval_closure str sk lsv => closure str sk lsv (Plist2 lsv)
  | cval_record pl => rec pl (Plist2 pl)
  | cval_unspec n name lt lv => unspec n name lt lv (Plist lv)
  end.
End InductionCterm.
