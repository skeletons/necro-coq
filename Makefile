SHELL=bash
red=`tput setaf 1``tput setab 7`
reset=`tput sgr0`
COQ_TEST_DIRS = $(shell ls -d test/*/)
COQ_PROOFS_DIRS = $(addprefix proofs/,$(shell ls proofs))

NAME := necro-coq
# WARNING : GIT TAG is prefixed with "v"
VERSION := 0.17.1

TGZ := $(NAME)-v$(VERSION).tar.gz
MD5TXT := $(TGZ).md5sum.txt
OPAM_NAME := necrocoq

# GIT VARIABLES
GITLAB := gitlab.inria.fr
GITPROJECT := skeletons
GIT_REPO := git@$(GITLAB):$(GITPROJECT)/$(NAME).git
GIT_RELEASE := https://$(GITLAB)/$(GITPROJECT)/$(NAME)/-/archive/v$(VERSION)/$(TGZ)

# OPAM VARIABLES
OPAM_REPO_GIT := ../opam-repository
OPAM_FILE := packages/$(OPAM_NAME)/$(OPAM_NAME).$(VERSION)/opam

##########################################################################################
#                       TARGET                                                           #
##########################################################################################

nothing:

# Change version number everywhere where it's specified, and update Makefile
new_version:
	@rm -f $(TGZ) $(MD5TXT)
	@read -p "Please enter new version (current is $(VERSION)): " g;\
	sed -i.bak -E "s#archive/v$(VERSION)#archive/v$$g#" ./necrocoq.opam.template;\
	sed -i.bak -E "s#v$(VERSION).tar.gz#v$$g.tar.gz#" ./necrocoq.opam.template;\
	sed -i.bak -E "s/^VERSION := $(VERSION)/VERSION := $$g/" ./Makefile;\
	sed -i.bak -E "s/^\(version $(VERSION)\)$$/\(version $$g\)/" ./dune-project;\
	git commit ./dune-project ./Makefile ./necrocoq.opam.template -m "Release version $$g";\
	git tag -a v$$g -fm 'OPAM necrocoq package for deployment'
	git push --follow-tags

opam_update :
	@rm -f $(TGZ) $(MD5TXT)
	@wget -q $(GIT_RELEASE)
	@md5sum $(TGZ) > $(MD5TXT)
	@sed -i.bak -E 's|^([[:space:]]checksum: "md5[[:space:]]*=[[:space:]]*).*"|\1'$$(cat $(MD5TXT) | cut -d" " -f 1)'"|' necrocoq.opam.template
	@dune build
	@cd $(OPAM_REPO_GIT); git checkout necro; git pull --rebase
	@mkdir -p $(OPAM_REPO_GIT)/packages/$(OPAM_NAME)/$(OPAM_NAME).$(VERSION)/
	@cp necrocoq.opam $(OPAM_REPO_GIT)/$(OPAM_FILE)

release:
	make new_version
	make opam_update
	@cd $(OPAM_REPO_GIT); git checkout necro; \
		git add packages/necrocoq && git commit -am "Push new Necro Coq version" && git push

.PHONY: opam_update new_version release nothing
