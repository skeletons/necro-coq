## Version 0.18
- Update to Necro Lib 0.16

### Version 0.17.1
- supports recent Coq version (8.14 is minimum required)
- `UserType` are printed as `Base` to be compatible with provided Coq files

## Version 0.17
- adding or-patterns
- Switching to application to only one term

### Version 0.16.7
- Add WellFormedComputable, which provides computable wellformedness checkers for terms, skeletons, skeletal semantics.
- Add EqWF: a proof of consistency between WellFormed and WellFormedComputable
*note:* Now you don't need to trust that the `well_formed` tactic will work, and
you can even know if a semantics is *not* well formed

### Version 0.16.6.2
- Fix on WellFormed

### Version 0.16.6.1
- Forgot to update Subject Reduction

### Version 0.16.6
- Add specification for Dict.keys
- Small fix on WellFormed (more restrictive now)

### Version 0.16.5.2
- Fix WellFormed.v (a record with no field is not well-formed)

### Version 0.16.5.1
- Fix

### Version 0.16.5
- Kind of a downgrade, Necro Coq doesn't handle monads anymore, use necrotrans beforehand
- Fix deprecation warnings

### Version 0.16.4
- Update to Necro Lib 0.11
- Add `--version` option

### Version 0.16.3
- Use dune-sites to put and find template

### Version 0.16.2
- Switch to dune

### Version 0.16.1.1
- Fix makefile so that opam works

### Version 0.16.1
- Updating to Necro Lib 0.10 (without switching to dune)

### Version 0.16.0.2
- Fix dependency

### Version 0.16.0.1
- Fix Makefile

## Version 0.16
- Did some software engineering, some cleaning up and reordering of files
- Files are supposedly available with opam installation
  (with `From Necro Require Import …`)

## Version 0.15
- Update to Necro Lib v0.9

### Version 0.14.1
- Update necro-tests with matches
- Fix pattern-matching support
- Update tests and proofs

## Version 0.14
- Add support for match

## Version 0.13
- cutting `term_record` into two constructors `term_rec_make` and
  `term_rec_set`, like in Necro Lib (v0.7)

## Version 0.12
- Handling records now

## Version 0.11
- Use string for constructors, term names, and base types

## Version 0.10
- renamed `value` to `term` in Skel's AST

## Version 0.9
- updated to necro-lib v1.5: `value_func` takes a pattern as input instead of a
  variable
