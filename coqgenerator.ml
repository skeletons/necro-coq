open Necro.TypedAST
open Coqprinting
open Necro.Util

let spec =
	SMap.filter (fun _ (_, (_, _, vo)) -> vo <> None)
let unspec =
	SMap.filter (fun _ (_, (_, _, vo)) -> vo = None)

(* replaces a line of the type C[{# name #}] where C is a context with C
 * applied to the term of name in the map *)
let search_and_replace map line =
	Str.global_substitute
		(Str.regexp "{# \\([a-zA-Z0-9_]*\\) #}")
		(fun s -> match SMap.find_opt (Str.matched_group 1 s) map with Some s -> s |
		None -> failwith (Str.matched_group 1 s))
		line

(** [getlines ic] takes all the lines available in the input channel and puts
    them in a string. **)
let getlines ic =
	let rec f acc =
		try f ((input_line ic) :: acc) with End_of_file -> String.concat "\n" (List.rev acc)
	in f []

let gen sem =
	let constructors = Necro.Skeleton.get_all_constructors sem in
	let fields = Necro.Skeleton.get_all_fields sem in
	let smap = SMap.empty in
	let smap = SMap.add "base_type" (def_base_type sem.ss_types) smap in
	let smap = SMap.add "types" (def_types sem.ss_types) smap in
	let smap = SMap.add "constructor" (def_constructor constructors) smap in
	let smap = SMap.add "field" (def_field fields) smap in
	let smap = SMap.add "ctype" (def_ctype sem constructors) smap in
	let smap = SMap.add "ftype" (def_ftype sem fields) smap in
	let smap = SMap.add "unspec_term" (unspec_term (unspec sem.ss_terms)) smap in
	let smap = SMap.add "spec_term" (spec_term (spec sem.ss_terms)) smap in
	let smap = SMap.add "unspec_term_decl"
		(unspec_term_decl sem (unspec sem.ss_terms)) smap in
	let smap = SMap.add "defs"
		(spec_term_def sem (spec sem.ss_terms)) smap in
	let smap = SMap.add "sigs"
		(spec_term_sig sem sem.ss_terms) smap in
	let smap = SMap.add "spec_term_decl"
		(spec_term_decl sem (spec sem.ss_terms)) smap in

	(* open template file and return specialized result *)
	let template_path =
		begin match Mysites.Sites.template with
		| [ path ] -> path
		| _ -> assert false
		end
	in
	let template = template_path ^ "/NecroCoqTemplate.v" in
	let ic = open_in template in
	let contents = getlines ic in
	search_and_replace smap contents

