Require Import String List Dict.
Require Import ZArith.
Import ListNotations DictNotations.

(** See https://gitlab.inria.fr/skeletons/necro-coq/-/tree/master/files *)
Require Import Skeleton WellFormed Concrete Concrete_ndam.

(* This is the deterministic version with backtracking
 * - unspec terms return a list of all possible results (no infinity)
 * - we always take the first branch, but backtrack if we are stuck
 * - we also backtrack if we see the "exists" constructor for skeletons
 * - 'step' function is computable
 * - we can get 'stuck' in a looping branch and never reach a result
 *   even if a correct result exists on another branch
 *)

Section ConcreteAM.

  Variable s: skeletal_semantics.

  (* an unspec function returns a list of all possible outcomes *)
  Definition unspec_function_am: Type :=
    list type -> list cvalue -> list cvalue.

  Variable u_am:
    dict (nat * unspec_function_am).

  Notation "'ldtt'" := (list (string * list type * term)).
  Notation "'ldpt'" := (list (string * list type * pattern)).
  Notation "'ldrt'" := (list (string * cvalue)).

  (* Failure Continuations *)
  Inductive fkt : Type :=
  | f_empty: fkt
  | f_sk: skeleton -> env -> krt -> fkt -> fkt
  | f_list: (list cvalue) -> krt -> fkt -> fkt.

  (* States of the Abstract Machine *)
  Inductive amstate : Type :=
  | KR: krt -> cvalue -> fkt -> amstate
  | KD: kdt -> ldrt -> fkt -> amstate
  | KL: klt -> (list cvalue) -> fkt -> amstate
  | KE: ket -> env -> fkt -> amstate
  | B:  fkt -> amstate
  | T:  term -> env -> krt -> fkt -> amstate
  | UR: cvalue -> ldtt -> env -> krt -> fkt -> amstate
  | GF: cvalue -> string -> krt -> fkt -> amstate
  | GN: cvalue -> nat -> krt -> fkt -> amstate
  | GS: option cvalue -> krt -> fkt -> amstate
  | D:  ldtt -> env -> kdt -> fkt -> amstate
  | LK: env -> string -> krt -> fkt -> amstate
  | TL: (list term) -> env -> klt -> fkt -> amstate
  | SK: skeleton -> env -> krt -> fkt -> amstate
  | A:  (list cvalue) -> cvalue -> krt -> fkt -> amstate
  | L:  (list cvalue) -> krt -> fkt -> amstate
  | P:  pattern -> cvalue -> env -> ket -> fkt -> amstate
  | PD: ldpt -> ldrt -> env -> ket -> fkt -> amstate
  | PL: (list pattern) -> (list cvalue) -> env -> ket -> fkt -> amstate.
  (** The states of the deterministic abstract machine are the same as the
      non-deterministic's. There is only one more state, [B fk] which pops a
      failure continuation, in order to backtrack. *)

  Definition step (ams : amstate) : option amstate :=
    match ams with
    (* mode KR *)
    | KR (kr_id)                r _  => None (* end of computation *)
    | KR (kr_constr c k)        r fk => Some (KR k (cval_constructor c r) fk)
    | KR (kr_list l e k)        r fk => Some (TL l e (kl_list k r) fk)
    | KR (kr_let p sk e k)      r fk => Some (P  p r e (ke_let sk k) fk)
    | KR (kr_app1 al e k)       r fk => Some (TL al e (kl_app2 r k) fk)
    | KR (kr_app3 rl k)         r fk => Some (A  rl r k fk)
    | KR (kr_field d k)         r fk => Some (GF r d k fk)
    | KR (kr_nth n k)           r fk => Some (GN r n k fk)
    | KR (kr_up ldt e k)        r fk => Some (UR r ldt e k fk)
    | KR (kr_ldt ldt e d k)     r fk => Some (D  ldt e (kd_cons d r k) fk)
    | KR (kr_ldp p e ldp ldr k) r fk => Some (P  p r e (ke_ldp ldp ldr k) fk)
    (* mode KD *)
    | KD (kd_id)         _   _  => None
    | KD (kd_rec k)      ldr fk => Some (KR k (cval_record ldr) fk)
    | KD (kd_up ldr1 k)  ldr fk => Some (KR k (cval_record (ldr++ldr1)) fk)
    | KD (kd_cons d r k) ldr fk => Some (KD k ((d,r)::ldr) fk)
    (* mode KL *)
    | KL (kl_id)       _  _  => None
    | KL (kl_tuple k)  rl fk => Some (KR k (cval_tuple rl) fk)
    | KL (kl_list k r) rl fk => Some (KL k (r::rl) fk)
    | KL (kl_app2 r k) rl fk => Some (A  rl r k fk)
    (* mode KE *)
    | KE (ke_id)            _  _ => None
    | KE (ke_let sk k)      e fk => Some (SK sk e k fk)
    | KE (ke_pat l rl k)    e fk => Some (PL l rl e k fk)
    | KE (ke_app sk al k)   e fk => Some (SK sk e (kr_app3 al k) fk)
    | KE (ke_ldp ldp ldr k) e fk => Some (PD ldp ldr e k fk)
    (* mode B *)
    | B  (f_empty)        => None (* fail *)
    | B  (f_sk sk e k fk) => Some (SK sk e k fk)
    | B  (f_list rl k fk) => Some (L  rl k fk)
    (* mode T *)
    | T  (term_constructor c _ v)      e k fk => Some (T  v e (kr_constr c k) fk)
    | T  (term_tuple vl)               e k fk => Some (TL vl e (kl_tuple k) fk)
    | T  (term_func p sk)              e k fk => Some (KR k (cval_closure p sk e) fk)
    | T  (term_var (TVLet x _))        e k fk => Some (LK e x k fk)
    | T  (term_var (TVUnspec x _ tyl)) e k fk =>
        match find x u_am with
        | None => Some (B  fk)
        | Some (0, itp) => Some (L  (itp tyl []) k fk)
        | Some (S n, _) => Some (KR k (cval_unspec n x tyl []) fk)
        end
    | T  (term_var (TVSpec x _ tyl))   e k fk =>
        match find x (s_spec_term_decl s) with
        | None => Some (B  fk)
        | Some (ta, _, t) =>
            if (beq_nat (length ta) (length tyl)) then
              Some (T  (subst_term' t ta tyl) [] k fk)
            else Some (B  fk)
        end
    | T  (term_field v _ d)            e k fk => Some (T  v e (kr_field d k) fk)
    | T  (term_nth v _ n)              e k fk => Some (T  v e (kr_nth n k) fk)
    | T  (term_rec_make ldt)        e k fk => Some (D  ldt e (kd_rec k) fk)
    | T  (term_rec_set v ldt)    e k fk => Some (T  v e (kr_up ldt e k) fk)
    (* mode UR *)
    | UR (cval_record ldr) ldt e k fk => Some (D  ldt e (kd_up ldr k) fk)
    | UR _                 _   _ _ fk => Some (B  fk)
    (* mode GF *)
    | GF (cval_record ldr) d k fk => Some (LK ldr d k fk)
    | GF _                 _ _ fk => Some (B  fk)
    (* mode GN *)
    | GN (cval_tuple rl) n k fk => Some (GS (nth_error rl n) k fk)
    | GN _               _ _ fk => Some (B  fk)
    (* mode GS *)
    | GS (Some r) k fk => Some (KR k r fk)
    | GS (None)   _ fk => Some (B  fk)
    (* mode D *)
    | D  []             e k fk => Some (KD k [] fk)
    | D  ((d,_,v)::ldt) e k fk => Some (T  v e (kr_ldt ldt e d k) fk)
    (* mode LK *)
    | LK []         _ _ fk => Some (B  fk)
    | LK ((y,r)::e) x k fk =>
               if x =? y then Some (KR k r fk)
                         else Some (LK e x k fk)
    (* mode TL *)
    | TL []     _ k fk => Some (KL k [] fk)
    | TL (v::l) e k fk => Some (T  v e (kr_list l e k) fk)
    (* mode SK *)
    | SK (skel_branch _ [])       _ _ fk => Some (B  fk)
    | SK (skel_branch nt (sk::l)) e k fk => Some (SK sk e k (f_sk (skel_branch nt l) e k fk))
    | SK (skel_letin p sk1 sk2)   e k fk => Some (SK sk1 e (kr_let p sk2 e k) fk)
    | SK (skel_exists _ _ _)      _ _ fk => Some (B  fk)
    | SK (skel_return v)          e k fk => Some (T  v e k fk)
    | SK (skel_apply v vl)        e k fk => Some (T  v e (kr_app1 vl e k) fk)
    (* mode A *)
    | A  []      r                             k fk => Some (KR k r fk)
    | A  (r::rl) (cval_closure p sk e)         k fk => Some (P p r e (ke_app sk rl k) fk)
    | A  (r::rl) (cval_unspec 0 f ta parg)     k fk =>
        match find f u_am with
        | None => Some (B  fk)
        | Some (_, itp) => Some (L  (itp ta (parg++[r])%list) (kr_app3 rl k) fk)
        end
    | A  (r::rl) (cval_unspec (S n) f ta parg) k fk =>
        Some (A  rl (cval_unspec n f ta (parg++[r])%list) k fk)
    | A  (r::rl) _                             _ fk => Some (B  fk)
    (* mode L *)
    | L  []     _ fk => Some (B  fk)
    | L  (r::l) k fk => Some (KR k r (f_list l k fk))
    (* mode P *)
    | P  (pattern_wildcard _)    _                       e k fk => Some (KE k e fk)
    | P  (pattern_var x _)       r                       e k fk => Some (KE k ((x,r)::e) fk)
    | P  (pattern_constr c1 _ p) (cval_constructor c2 r) e k fk =>
        if c1 =? c2 then Some (P  p r e k fk)
                    else Some (B  fk)
    | P  (pattern_tuple pl)      (cval_tuple rl)         e k fk => Some (PL pl rl e k fk)
    | P  (pattern_rec ldp)       (cval_record ldr)       e k fk => Some (PD ldp ldr e k fk)
    | P  _                       _                       e k fk => Some (B  fk)
    (* mode PD *)
    | PD []             _   e k fk => Some (KE k e fk)
    | PD ((d,_,p)::ldp) ldr e k fk => Some (LK ldr d (kr_ldp p e ldp ldr k) fk)
    (* mode PL *)
    | PL []      []      e k fk => Some (KE k e fk)
    | PL (p::pl) (r::rl) e k fk => Some (P  p r e (ke_pat pl rl k) fk)
    | PL _       _       _ _ fk => Some (B  fk)
    end.

  Inductive stepprop : amstate -> amstate -> Prop :=
  | stepp: forall a b, step(a) = Some(b) -> stepprop a b.

  Definition inject_term (t : term) (e : env) : amstate := T t e kr_id f_empty.
  Definition inject_skel (sk : skeleton) (e : env) : amstate := SK sk e kr_id f_empty.

  Fixpoint evalfuel (n : nat) (a : amstate) : option cvalue :=
    match a with
    | KR kr_id r _ => Some r
    | _ => match n with
           | 0 => None
           | S m => match step a with Some b => evalfuel m b | _ => None end
           end
    end.

End ConcreteAM.
