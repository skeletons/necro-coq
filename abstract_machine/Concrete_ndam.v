Require Import String List Dict.
Require Import ZArith.
Import ListNotations DictNotations.

(** See https://gitlab.inria.fr/skeletons/necro-coq/-/tree/master/files *)
Require Import Skeleton WellFormed Concrete.

(* This is the non-deterministic version
 * - unspec terms return a single result non-deterministically
 * - we take a branch non-deterministically
 * - no backtracking (we're stuck if wrong choice anywhere)
 *)

Section ConcreteNDAM.

  Variable s: skeletal_semantics.
  Variable u: dict (nat * unspec_function).
  Variable g: type -> cvalue -> Prop.

  (* record terms *)
  Notation "'ldtt'" := (list (string * list type * term)).
  (* record pattern *)
  Notation "'ldpt'" := (list (string * list type * pattern)).
  (* record values *)
  Notation "'ldrt'" := (list (string * cvalue)).

  (* Success Continuations *)

  (* krt is the type of continuations whose hole stands for a value *)
  Inductive krt : Type :=
  | kr_id: krt
  | kr_constr: string -> krt -> krt
  | kr_list: (list term) -> env -> klt -> krt
  | kr_let: pattern -> skeleton -> env -> krt -> krt
  | kr_app1: (list term) -> env -> krt -> krt
  | kr_app3: (list cvalue) -> krt -> krt
  | kr_field: string -> krt -> krt
  | kr_nth: nat -> krt -> krt
  | kr_up: ldtt -> env -> krt -> krt
  | kr_ldt: ldtt -> env -> string -> kdt -> krt
  | kr_ldp: pattern -> env -> ldpt -> ldrt -> ket -> krt
  with kdt : Type :=
  (* kdt is the type of continuations whose hole stands for a record *)
  | kd_id: kdt
  | kd_rec: krt -> kdt
  | kd_up: ldrt -> krt -> kdt
  | kd_cons: string -> cvalue -> kdt -> kdt
  with klt : Type :=
  (* klt is the type of continuations whose hole stands for a list of values *)
  | kl_id: klt
  | kl_tuple: krt -> klt
  | kl_list: klt -> cvalue -> klt
  | kl_app2: cvalue -> krt -> klt
  with ket : Type :=
  (* ket is the type of continuations whose hole stands for an environment *)
  | ke_id: ket
  | ke_let: skeleton -> krt -> ket
  | ke_pat: (list pattern) -> (list cvalue) -> ket -> ket
  | ke_app: skeleton -> (list cvalue) -> krt -> ket
  | ke_ldp: ldpt -> ldrt -> ket -> ket.


  (* States of the Abstract Machine *)
  Inductive amstate : Type :=
  | KR: krt -> cvalue -> amstate
  | KD: kdt -> ldrt -> amstate
  | KL: klt -> (list cvalue) -> amstate
  | KE: ket -> env -> amstate
  | T:  term -> env -> krt -> amstate
  | UR: cvalue -> ldtt -> env -> krt -> amstate
  | GF: cvalue -> string -> krt -> amstate
  | GN: cvalue -> nat -> krt -> amstate
  | GS: option cvalue -> krt -> amstate
  | D:  ldtt -> env -> kdt -> amstate
  | LK: env -> string -> krt -> amstate
  | TL: (list term) -> env -> klt -> amstate
  | SK: skeleton -> env -> krt -> amstate
  | A:  (list cvalue) -> cvalue -> krt -> amstate
  | P:  pattern -> cvalue -> env -> ket -> amstate
  | PD: ldpt -> ldrt -> env -> ket -> amstate
  | PL: (list pattern) -> (list cvalue) -> env -> ket -> amstate.

  (** Here are the different states and what they stand for:
     - The states beginning with K are continuation states. They are used when
     the current computation is fully evaluated, and we need to pop the
     continuation list. More precisely:
       + [KR]: we computed a value, now pop a krt continuation
       + [KD]: we computed a record, now pop a kdt continuation
       + [KL]: we computed a list of values, now pop a klt continuation
       + [KE]: we computed an environment, now pop a ket continuation
     - The following states aim at producing a value, their last argument is the
     krt continuation stating what to do when the value is evaluated
       + [T t e]: evaluate term t in environment e
       + [UR v ldt e]: update record v with fields ldt, evaluated in environment e
       + [GF v f]: get field f from record value v
       + [GN v n]: get n-th component from tuple value v
       + [GS vo]: extract the content of an option value
       + [LK e x]: look for variable x in the environment e
       + [SK sk e k]: evaluate skeleton sk in environment e
       + [A args f]: apply f to args
     - The following state aims at producing a record, its last argument is
     the kdt continuation stating what to do when the record is evaluated
       + [D ldt e]: evaluate record term ldt in environment e
     - The following state aims at producing a list of values, its last argument
     is the klt continuation stating what to do when the list of values
     is evaluated
       + [TL tl e] evaluate list of terms tl in environment e
     - The following states aim at producing an environment, their last argument
     is the klt continuation stating what to do when the environment is
     evaluated
       + [P p v e]: extend environment e by matching pattern p with value v
       + [PD ldp ldr e]: extend environment e by matching record pattern ldp
         with record value ldr
       + [PL pl vl e]: extend environment e by matching pattern list pl with
       value vl
   *)


  Inductive amstep : amstate -> amstate -> Prop :=
  (* mode KR *)
  | am_kr_constr: forall c k r,
      amstep (KR (kr_constr c k) r)
             (KR k (cval_constructor c r))
  | am_kr_list: forall l e k r,
      amstep (KR (kr_list l e k) r)
             (TL l e (kl_list k r))
  | am_kr_let: forall p sk e k r,
      amstep (KR (kr_let p sk e k) r)
             (P  p r e (ke_let sk k))
  | am_kr_app1: forall arglist e k r,
      amstep (KR (kr_app1 arglist e k) r)
             (TL arglist e (kl_app2 r k))
  | am_kr_app3: forall rl k r,
      amstep (KR (kr_app3 rl k) r)
             (A  rl r k)
  | am_kr_field: forall d k r,
      amstep (KR (kr_field d k) r)
             (GF r d k)
  | am_kr_nth: forall n k r,
      amstep (KR (kr_nth n k) r)
             (GN r n k)
  | am_kr_up: forall ldt e k r,
      amstep (KR (kr_up ldt e k) r)
             (UR r ldt e k)
  | am_kr_ldt: forall ldt e d k r,
      amstep (KR (kr_ldt ldt e d k) r)
             (D  ldt e (kd_cons d r k))
  | am_kr_ldp: forall p e ldp ldr k r,
      amstep (KR (kr_ldp p e ldp ldr k) r)
             (P  p r e (ke_ldp ldp ldr k))
  (* mode KD *)
  | am_kd_rec: forall k ldr,
      amstep (KD (kd_rec k) ldr)
             (KR k (cval_record ldr))
  | am_kd_up: forall ldr1 k ldr,
      amstep (KD (kd_up ldr1 k) ldr)
             (KR k (cval_record (ldr++ldr1)))
  | am_kd_cons: forall d r k ldr,
      amstep (KD (kd_cons d r k) ldr)
             (KD k ((d,r)::ldr))
  (* mode KL *)
  | am_kl_tuple: forall k rl,
      amstep (KL (kl_tuple k) rl)
             (KR k (cval_tuple rl))
  | am_kl_list: forall k r rl,
      amstep (KL (kl_list k r) rl)
             (KL k (r::rl))
  | am_kl_app2: forall r k rl,
      amstep (KL (kl_app2 r k) rl)
             (A  rl r k)
  (* mode KE *)
  | am_ke_let: forall sk k e,
      amstep (KE (ke_let sk k) e)
             (SK sk e k)
  | am_ke_pat: forall l rl k e,
      amstep (KE (ke_pat l rl k) e)
             (PL l rl e k)
  | am_ke_app: forall sk al k e,
      amstep (KE (ke_app sk al k) e)
             (SK sk e (kr_app3 al k))
  | am_ke_ldp: forall ldp ldr k e,
      amstep (KE (ke_ldp ldp ldr k) e)
             (PD ldp ldr e k)
  (* mode T *)
  | am_v_tvlet: forall x _tp e k,
      amstep (T  (term_var (TVLet x _tp)) e k)
             (LK e x k)
  | am_v_spec: forall x ta _ty _ty' tyl t e k,
      find x (s_spec_term_decl s) = Some (ta, _ty, t) ->
      length ta = length tyl ->
      amstep (T  (term_var (TVSpec x _ty' tyl)) e k)
             (T  (subst_term' t ta tyl) [] k)
  | am_v_unspec_0: forall x _ty tyl itp r e k,
      find x u = Some (0, itp) ->
      itp tyl [] r ->
      amstep (T  (term_var (TVUnspec x _ty tyl)) e k)
             (KR k r)
  | am_v_unspec_more: forall n x _ty tyl _itp e k,
      find x u = Some (S n, _itp) ->
      amstep (T  (term_var (TVUnspec x _ty tyl)) e k)
             (KR k (cval_unspec n x tyl []))
  | am_v_constr: forall _nt c v e k,
      amstep (T  (term_constructor c _nt v) e k)
             (T  v e (kr_constr c k))
  | am_v_tuple: forall vl e k,
      amstep (T  (term_tuple vl) e k)
             (TL vl e (kl_tuple k))
  | am_v_func: forall p sk e k,
      amstep (T  (term_func p sk) e k)
             (KR k (cval_closure p sk e))
  | am_v_field: forall v _tyl d e k,
      amstep (T  (term_field v _tyl d) e k)
             (T  v e (kr_field d k))
  | am_v_nth: forall v _tyl n e k,
      amstep (T  (term_nth v _tyl n) e k)
             (T  v e (kr_nth n k))
  | am_v_rec_some: forall v ldt e k,
      amstep (T  (term_rec_set v ldt) e k)
             (T  v e (kr_up ldt e k))
  | am_v_rec_none: forall ldt e k,
      amstep (T  (term_rec_make ldt) e k)
             (D  ldt e (kd_rec k))
  (* mode UR *)
  | am_ur_rec: forall ldr1 ldt e k,
      amstep (UR (cval_record ldr1) ldt e k)
             (D  ldt e (kd_up ldr1 k))
  (* mode GF *)
  | am_gf_rec: forall ldr d k,
      amstep (GF (cval_record ldr) d k)
             (LK ldr d k)
  (* mode GN *)
  | am_gn_tuple: forall rl n k,
      amstep (GN (cval_tuple rl) n k)
             (GS (nth_error rl n) k)
  (* mode GS *)
  | am_gs_some: forall r k,
      amstep (GS (Some r) k)
             (KR k r)
  (* mode D *)
  | am_d_nil: forall e k,
      amstep (D  [] e k)
             (KD k [])
  | am_d_some: forall d _tyl v ldt e k,
      amstep (D  ((d,_tyl,v)::ldt) e k)
             (T  v e (kr_ldt ldt e d k))
  (* mode LK *)
  | am_lk_same: forall x r _e k,
      amstep (LK ((x,r)::_e) x k)
             (KR k r)
  | am_lk_diff: forall x y _r e k,
      x<>y ->
      amstep (LK ((y,_r)::e) x k)
             (LK e x k)
  (* mode TL *)
  | am_vl_nil: forall _e k,
      amstep (TL [] _e k)
             (KL k [])
  | am_vl_cons: forall v l e k,
      amstep (TL (v::l) e k)
             (T  v e (kr_list l e k))
  (* mode SK *)
  | am_sk_branch: forall nt sk skl e k,
      In sk skl ->
      amstep (SK (skel_branch nt (skl)) e k)
             (SK sk e k)
  | am_sk_letin: forall p sk1 sk2 e k,
      amstep (SK (skel_letin p sk1 sk2) e k)
             (SK sk1 e (kr_let p sk2 e k))
  | am_sk_exists: forall p ty sk r e k,
      g ty r ->
      amstep (SK (skel_exists p ty sk) e k)
             (P  p r e (ke_let sk k))
  | am_sk_return: forall v e k,
      amstep (SK (skel_return v) e k)
             (T  v e k)
  | am_sk_apply: forall v arglist e k,
      amstep (SK (skel_apply v arglist) e k)
             (T  v e (kr_app1 arglist e k))
  (* mode A *)
  | am_r_nil: forall r k,
      amstep (A  [] r k)
             (KR k r)
  | am_r_unspec_1: forall _n x itp ta parg arg al k r,
      find x u = Some (_n, itp) ->
      itp ta (parg++[arg])%list r ->
      amstep (A  (arg::al) (cval_unspec 0 x ta parg) k)
             (A  al r k)
  | am_r_unspec_more: forall x n ta parg arg al k,
      amstep (A  (arg::al) (cval_unspec (S n) x ta parg) k)
             (A  al (cval_unspec n x ta (parg++[arg])%list) k)
  | am_r_clos: forall e p sk arg al k,
      amstep (A  (arg::al) (cval_closure p sk e) k)
             (P  p arg e (ke_app sk al k))
  (* mode P *)
  | am_p_wild: forall _nt _r e k,
      amstep (P  (pattern_wildcard _nt) _r e k)
             (KE k e)
  | am_p_var: forall _nt x r e k,
      amstep (P  (pattern_var x _nt) r e k)
             (KE k ((x,r)::e))
  | am_p_constr: forall _nt c p r e k,
      amstep (P  (pattern_constr c _nt p) (cval_constructor c r) e k)
             (P  p r e k)
  | am_p_tuple: forall pl rl e k,
      amstep (P  (pattern_tuple pl) (cval_tuple rl) e k)
             (PL pl rl e k)
  | am_p_rec: forall ldp ldr e k,
      amstep (P  (pattern_rec ldp) (cval_record ldr) e k)
             (PD ldp ldr e k)
  (* mode PD *)
  | am_pd_nil: forall ldr e k,
      amstep (PD [] ldr e k)
             (KE k e)
  | am_pd_some: forall d _tyl p ldp ldr e k,
      amstep (PD ((d,_tyl,p)::ldp) ldr e k)
             (LK ldr d (kr_ldp p e ldp ldr k))
  (* mode PL *)
  | am_pl_nil_nil: forall e k,
      amstep (PL [] [] e k)
             (KE k e)
  | am_pl_cons_cons: forall p pl r rl e k,
      amstep (PL (p::pl) (r::rl) e k)
             (P  p r e (ke_pat pl rl k)).
End ConcreteNDAM.
