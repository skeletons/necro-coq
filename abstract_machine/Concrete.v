Require Import String List Dict.
Import ListNotations DictNotations.
Open Scope string_scope.

(** See https://gitlab.inria.fr/skeletons/necro-coq/-/tree/master/files *)
Require Import Skeleton WellFormed.

Section Concrete.

  Variable s: skeletal_semantics.

  Inductive cvalue : Type :=
  (* for unspecified terms, use any term of any type *)
  | cval_base : forall A, A -> cvalue
  | cval_constructor : string -> cvalue -> cvalue
  | cval_tuple: list cvalue -> cvalue
  (* [cval_closure p sk e] is the closure for [λ p → sk] in the environment [e] *)
  | cval_closure: pattern -> skeleton -> list (string * cvalue) -> cvalue
  | cval_record: list (string * cvalue) -> cvalue
  (* cval_unspec n x ta vl means it is the unspecified term x with type
     arguments ta, applied to arguments vl, and there are still (S n) arguments
     missing. *)
  | cval_unspec: nat -> string -> list type -> list cvalue -> cvalue.

  (* Environment is a list of associations. As there is no way to remove a term
     from an environment, it is handled like a stack *)
  Definition env := list (string * cvalue).

  (* [f ta vl w] means that [f] applied to type arguments [ta] and to terms
     [vl] may yield the result [w]. *)
  Definition unspec_function: Type :=
    list type -> list cvalue -> cvalue -> Prop.

  (* unspec_term_interp x = (n, f) means that [x] expects [n] arguments and is
     interpreted as [f] (see unspec_function). *)
  Variable unspec_term_interp:
    dict (nat * unspec_function).

  (* a generator for existentials. [gen_exists ty v] means that [v] is a valid
     term of type [ty]. *)
  Variable gen_exists:
    type -> cvalue -> Prop.

  Inductive add_asn: env -> pattern -> cvalue -> env -> Prop :=
  | add_asn_wildcard: forall e ty v,
      add_asn e (pattern_wildcard ty) v e
  | add_asn_var: forall e x v ty,
      add_asn e (pattern_var x ty) v ((x, v) :: e)
  | add_asn_constr: forall e c p v f ta,
      add_asn e p v f ->
      add_asn e (pattern_constr c ta p) (cval_constructor c v) f
  | add_asn_tuple_nil e:
      add_asn e (pattern_tuple []) (cval_tuple []) e
  | add_asn_tuple_cons e:
      forall p pl (t: cvalue) (tl: list cvalue) f g,
      add_asn e p t f ->
      add_asn f (pattern_tuple pl) (cval_tuple tl) g ->
      add_asn e (pattern_tuple (p::pl)) (cval_tuple (t::tl)) g
  | add_asn_rec_nil e cl:
      add_asn e (pattern_rec []) (cval_record cl) e
  | add_asn_rec_cons e e' e'' f c cl ta p pl:
      findEnv f cl = Some c ->
      add_asn e p c e' ->
      add_asn e' (pattern_rec pl) (cval_record cl) e'' ->
      add_asn e (pattern_rec ((f,ta,p) :: pl)) (cval_record cl) e''.



  Inductive interp_term: env -> term -> cvalue -> Prop :=
  | i_var_val: forall e x ty v,
      findEnv x e = Some v ->
      interp_term e (term_var (TVLet x ty)) v
  | i_var_unspec_O: forall e x ty ta v w,
      find x unspec_term_interp = Some (0, v) ->
      v ta [] w ->
      interp_term e (term_var (TVUnspec x ty ta)) w
  | i_var_unspec_S: forall e x ty ta v n,
      find x unspec_term_interp = Some (S n, v) ->
      interp_term e (term_var (TVUnspec x ty ta)) (cval_unspec n x ta [])
  | i_var_spec: forall e t x ty ta tyl ty' v,
      find x (s_spec_term_decl s) = Some (ta, ty, t) ->
      length ta = length tyl ->
      interp_term [] (subst_term' t ta tyl) v ->
      interp_term e (term_var (TVSpec x ty' tyl)) v
  | i_constr: forall e arg c ta v,
      interp_term e arg v ->
      interp_term e (term_constructor c ta arg) (cval_constructor c v)
  | i_tuple_nil: forall e,
      interp_term e (term_tuple []) (cval_tuple [])
  | i_tuple_cons: forall e t tl v vl,
      interp_term e t v ->
      interp_term e (term_tuple tl) (cval_tuple vl) ->
      interp_term e (term_tuple (t::tl)) (cval_tuple (v::vl))
  | i_func: forall p sk e,
      interp_term e (term_func p sk) (cval_closure p sk e)
  | i_field: forall e t ta f cl v,
      interp_term e t (cval_record cl) ->
      findEnv f cl = Some v ->
      interp_term e (term_field t ta f) v
  | i_nth: forall e t ta n cl v,
      interp_term e t (cval_tuple cl) ->
      nth_error cl n = Some v ->
      interp_term e (term_nth t ta n) v
  | i_rec_make_nil: forall e,
      interp_term e (term_rec_make []) (cval_record [])
  | i_rec_make_cons: forall e f ta t tl v vl,
      interp_term e t v ->
      interp_term e (term_rec_make tl) (cval_record vl) ->
      interp_term e (term_rec_make ((f,ta,t)::tl)) (cval_record ((f,v)::vl))
  | i_rec_set: forall e t tl vl1 vl2,
      interp_term e t (cval_record vl1) ->
      interp_term e (term_rec_make tl) (cval_record vl2) ->
      interp_term e (term_rec_set t tl) (cval_record (vl2++vl1)).

  Inductive interp_skel: env -> skeleton -> cvalue -> Prop :=
  | i_branch: forall ty e v sks ski,
      In ski sks ->
      interp_skel e ski v ->
      interp_skel e (skel_branch ty sks) v
  | i_return: forall e t v,
      interp_term e t v ->
      interp_skel e (skel_return t) v
  | i_apply : forall e t tl vl v res,
      interp_term e t v ->
      List.Forall2 (interp_term e) tl vl ->
      apply v vl res ->
      interp_skel e (skel_apply t tl) res
  | i_letin: forall e e' p s1 s2 v w,
      interp_skel e s1 v ->
      add_asn e p v e' ->
      interp_skel e' s2 w ->
      interp_skel e (skel_letin p s1 s2) w
  | i_exists: forall e e' p ty sk v w,
      gen_exists ty v ->
      add_asn e p v e' ->
      interp_skel e' sk w ->
      interp_skel e (skel_exists p ty sk) w

  with apply: cvalue -> list cvalue -> cvalue -> Prop :=
  | i_apply_0: forall v,
      apply v [] v
  | i_apply_clos: forall v w args res e e' p sk,
    add_asn e p v e' ->
    interp_skel e' sk w ->
    apply w args res ->
    apply (cval_closure p sk e) (v :: args) res
  | i_apply_unspec_done: forall v args res res_part m n x ta vl,
    find x unspec_term_interp = Some (m, v) ->
    List.length args >= S n ->
    v ta (app vl (firstn (S n) args)) res_part ->
    apply res_part (skipn (S n) args) res ->
    apply (cval_unspec n x ta vl) args res
  | i_apply_unspec_cont: forall args n x ta vl,
    List.length args < S n ->
    apply (cval_unspec n x ta vl) args
        (cval_unspec (n - List.length args) x ta (vl ++ args)).



  Inductive blocks_term: env -> term -> Prop :=
  | b_var_unspec_0: forall e x ty ta v,
      find x unspec_term_interp = Some (0, v) ->
      (forall w, ~ v ta [] w) ->
      blocks_term e (term_var (TVUnspec x ty ta))
  | b_var_spec: forall e t x ty ta tyl ty',
      find x (s_spec_term_decl s) = Some (ta, ty, t) ->
      length ta = length tyl ->
      blocks_term [] (subst_term' t ta tyl) ->
      blocks_term e (term_var (TVSpec x ty' tyl))
  | b_constr: forall e arg c ta,
      blocks_term e arg ->
      blocks_term e (term_constructor c ta arg)
  | b_tuple: forall e vl,
      Exists (blocks_term e) vl ->
      blocks_term e (term_tuple vl).

  Inductive blocks_skel: env -> skeleton -> Prop :=
  | b_branch: forall ty e sks,
      Forall (blocks_skel e) sks ->
      blocks_skel e (skel_branch ty sks)
  | b_return: forall e t,
      blocks_term e t ->
      blocks_skel e (skel_return t)
  | b_apply : forall e t tl,
      (forall v vl,
        interp_term e t v ->
        List.Forall2 (interp_term e) tl vl ->
        blocks_apply v vl) ->
      blocks_skel e (skel_apply t tl)
  | b_letin: forall e p s1 s2,
      (forall v e',
        interp_skel e s1 v ->
        add_asn e p v e' ->
        blocks_skel e' s2) ->
      blocks_skel e (skel_letin p s1 s2)
  | b_exists: forall e e' p ty sk,
      (forall v, gen_exists ty v ->
        add_asn e p v e' ->
        blocks_skel e' sk) ->
      blocks_skel e (skel_exists p ty sk)

  with blocks_apply: cvalue -> list cvalue -> Prop :=
  | b_apply_clos: forall p v args e sk,
      (forall w e',
        add_asn e p v e' ->
        interp_skel e' sk w ->
        blocks_apply w args) ->
      blocks_apply (cval_closure p sk e) (v :: args)
  | b_apply_unspec_done: forall v args m n x ta vl,
    find x unspec_term_interp = Some (m, v) ->
    List.length args >= S n ->
    (forall res_part,
      v ta (app vl (firstn (S n) args)) res_part ->
      blocks_apply res_part (skipn (S n) args)) ->
    blocks_apply (cval_unspec n x ta vl) args.
End Concrete.
