(** This file provides the definition of a well-formed semantics. It also
    defines a tactic well_formed that may be used to automatically derive the
    proof of well-formedness of any given semantics. *)

Require Import String List.
Import ListNotations.

(** See https://gitlab.inria.fr/skeletons/necro-coq/-/tree/master/files *)
Require Import Dict Skeleton.


Section WellFormed.

  (** We suppose a skeletal semantics given *)
  Variable ss: skeletal_semantics.

  (** List manipulation *)
  Definition sameList {A} (l1 l2:list A) : Prop :=
    incl l1 l2 /\ incl l2 l1.

  (** Environment manipulation *)
  Definition typing_env := list (string * type).

  Fixpoint findEnv {A} (s:string) (e:list (string * A)) :=
    match e with
    | [] => None
    | x :: q =>
        if eqb (fst x) s then Some (snd x)
        else findEnv s q
    end.

  Definition extEnvVar (x:string * type) (e:typing_env) :=
    x :: e.

  Fixpoint extEnv (p:pattern) (e:typing_env) :=
    let list :=
      (fix rec pl e :=
      match pl with
      | [] => e
      | p :: pl => rec pl (extEnv p e)
      end)
    in
    match p with
    | pattern_wildcard _ => e
    | pattern_var x ty => extEnvVar (x, ty) e
    | pattern_constr c _ p => extEnv p e
    | pattern_tuple pl => list pl e
    | pattern_rec pl =>
      (fix rec pl e :=
      match pl with
      | [] => e
      | (_, _, p) :: pl => rec pl (extEnv p e)
      end) pl e
    end.

  (** Extract list of variables *)
  Fixpoint pattern_variables (p:pattern): list string :=
    let list :=
      (fix rec pl :=
        match pl with
        | [] => []
        | p :: pl => app (pattern_variables p) (rec pl)
        end)
    in
    match p with
    | pattern_var tv _ => [tv]
    | pattern_tuple pl => list pl
    | pattern_constr c ty p => pattern_variables p
    | pattern_wildcard _ => []
    | pattern_rec pl =>
      (fix rec pl :=
        match pl with
        | [] => []
        | (_, _, p) :: pl =>
            app (pattern_variables p) (rec pl)
        end) pl
    end.

  Fixpoint type_variables (ty:type): list string :=
    let type_variables_list := fix rec tyl :=
      match tyl with
      | [] => []
      | a :: q => type_variables a ++ rec q
      end in
    match ty with
    | Var a => [a]
    | Base _ ta => type_variables_list ta
    | Prod tyl => type_variables_list tyl
    | Arrow t1 t2 => type_variables t1 ++ type_variables t2
    end.


  (** Type variable substitutions *)
  Fixpoint subst (ty:type) (tae: typing_env): type :=
    let subst_list tyl :=
      map (fun ty => subst ty tae) tyl
    in
    match ty with
    | Var x =>
      match findEnv x tae with
      | Some ty => ty
      | None => ty
      end
    | Prod xl => Prod (subst_list xl)
    | Base s tl => Base s (subst_list tl)
    | Arrow s1 s2 => Arrow (subst s1 tae) (subst s2 tae)
    end.

  Definition subst' (ty:type) ta tyl :=
    let tae := combine ta tyl
    in subst ty tae.

  Definition subst_list_type (tl: list type) tae :=
    map (fun ty => subst ty tae) tl.

  Fixpoint subst_pattern (p:pattern) tae :=
    match p with
    | pattern_var x y =>
        pattern_var x (subst y tae)
    | pattern_wildcard y =>
        pattern_wildcard (subst y tae)
    | pattern_tuple pl =>
        pattern_tuple (map (fun y => subst_pattern y tae) pl)
    | pattern_constr c tyl p =>
        pattern_constr c (subst_list_type tyl tae) (subst_pattern p tae)
    | pattern_rec pl =>
        pattern_rec (map (fun y =>
        match y with
        | (f, ta, p) => (f, ta, subst_pattern p tae)
        end) pl)
    end.

  Definition subst_typed_var (tv:typed_var) tae :=
    match tv with
    | TVLet x y => TVLet x (subst y tae)
    | TVSpec x y ta => TVSpec x (subst y tae) (subst_list_type ta tae)
    | TVUnspec x y ta => TVUnspec x (subst y tae) (subst_list_type ta tae)
    end.

  Fixpoint subst_term (t:term) tae :=
    match t with
    | term_var tv =>
        term_var (subst_typed_var tv tae)
    | term_tuple tl =>
        term_tuple (map (fun t => subst_term t tae) tl)
    | term_constructor c tyl t =>
        term_constructor c (subst_list_type tyl tae) (subst_term t tae)
    | term_func p sk =>
        term_func (subst_pattern p tae) (subst_skel sk tae)
    | term_field t tyl f =>
        term_field (subst_term t tae) (subst_list_type tyl tae) f
    | term_nth t tyl f =>
        term_nth (subst_term t tae) (subst_list_type tyl tae) f
    | term_rec_make tl =>
        term_rec_make  (map (fun y =>
          match y with
          | (f, ta, t) => (f, subst_list_type ta tae, subst_term t tae)
          end) tl)
    | term_rec_set t tl =>
        term_rec_set (subst_term t tae) (map (fun y =>
          match y with
          | (f, ta, t) => (f, subst_list_type ta tae, subst_term t tae)
          end) tl)
    end

  with subst_skel (sk:skeleton) tae :=
    match sk with
    | skel_branch tau skl =>
        skel_branch (subst tau tae) (map (fun sk => subst_skel sk tae) skl)
    | skel_return t =>
        skel_return (subst_term t tae)
    | skel_apply t tl =>
      skel_apply
        (subst_term t tae)
        (map (fun t => subst_term t tae) tl)
    | skel_letin p sk1 sk2 =>
      skel_letin
        (subst_pattern p tae)
        (subst_skel sk1 tae)
        (subst_skel sk2 tae)
    | skel_exists p ty sk =>
      skel_exists
        (subst_pattern p tae)
        (subst ty tae)
        (subst_skel sk tae)
    end.

  Definition subst_term' t ta tyl :=
    let tae := combine ta tyl in
    subst_term t tae.


  (** Well-formedness predicates *)

  Inductive well_formed_pattern : pattern -> type -> Prop :=
  | pattern_var_wf tv ty :
    well_formed_pattern (pattern_var tv ty) ty
  | pattern_wildcard_wf ty:
    well_formed_pattern (pattern_wildcard ty) ty
  | pattern_tuple_nil_wf:
    well_formed_pattern (pattern_tuple []) (Prod [])
  | pattern_tuple_cons_wf p pl ty tyl:
    well_formed_pattern p ty ->
    well_formed_pattern (pattern_tuple pl) (Prod tyl) ->
    (forall x,
      ~ (In x (pattern_variables p) /\
         In x (pattern_variables (pattern_tuple pl)))) ->
    well_formed_pattern (pattern_tuple (p::pl)) (Prod (ty::tyl))
  | pattern_constr_wf c tyl p c_ta c_in c_out:
      well_formed_pattern p (subst' c_in c_ta tyl) ->
      find c (s_ctype ss) = Some (c_ta, c_in, c_out) ->
      length tyl = length c_ta ->
      well_formed_pattern (pattern_constr c tyl p) (Base c_out tyl)
  | pattern_rec_one_wf f f_ta f_out f_in ta p tyl:
      find f (s_ftype ss) = Some (f_ta, f_out, f_in) ->
      length tyl = length f_ta ->
      well_formed_pattern p (subst' f_in f_ta tyl) ->
      well_formed_pattern (pattern_rec [(f, ta, p)]) (Base f_out tyl)
  | pattern_rec_next_wf p pl ty:
      well_formed_pattern (pattern_rec [p]) ty ->
      well_formed_pattern (pattern_rec pl) ty ->
      well_formed_pattern (pattern_rec (p :: pl)) ty.

  Inductive well_formed_term: typing_env -> term -> type -> Prop :=
  | term_var_val_wf x e ty:
    findEnv x e = Some ty ->
    well_formed_term e (term_var (TVLet x ty)) ty
  | term_var_unspec_wf x e ta ty tyl ty_out:
    find x ss.(s_unspec_term_decl) = Some (ta, ty) ->
    length ta = length tyl ->
    subst' ty ta tyl = ty_out ->
    well_formed_term e (term_var (TVUnspec x ty_out tyl)) ty_out
  | term_var_spec_wf x e ta ty tyl t ty_out:
    find x ss.(s_spec_term_decl) = Some (ta, ty, t) ->
    length ta = length tyl ->
    subst' ty ta tyl = ty_out ->
    well_formed_term e (term_var (TVSpec x ty_out tyl)) ty_out
  | term_constructor_wf e c tyl t c_ta c_in c_out:
    length c_ta = length tyl ->
    well_formed_term e t (subst' c_in c_ta tyl) ->
    find c (s_ctype ss) = Some (c_ta, c_in, c_out) ->
    well_formed_term e (term_constructor c tyl t) (Base c_out tyl)
  | term_field_wf e f tyl t f_ta f_in f_out:
    length f_ta = length tyl ->
    well_formed_term e t (Base f_out tyl) ->
    find f (s_ftype ss) = Some (f_ta, f_out, f_in) ->
    well_formed_term e (term_field t tyl f) (subst' f_in f_ta tyl)
  | term_nth_wf e n tyl t ty:
    well_formed_term e t (Prod tyl) ->
    nth_error tyl n = Some ty ->
    well_formed_term e (term_nth t tyl n) ty
  | term_rec_make_wf e tl f_out tyl:
      (forall x, In x (s_field ss) ->
        forall f_ta f_in,
        find x (s_ftype ss) = Some (f_ta, f_out, f_in) ->
        In x (map (fun y => match y with (f, _, _) => f end) tl)) ->
      well_formed_record e tl (Base f_out tyl) ->
      well_formed_term e (term_rec_make tl) (Base f_out tyl)
  | term_rec_set_wf e t tl ty:
      well_formed_term e t ty ->
      well_formed_record e tl ty ->
      well_formed_term e (term_rec_set t tl) ty
  | term_tuple_wf e tl tyl:
    Forall2 (well_formed_term e) tl tyl ->
    well_formed_term e (term_tuple tl) (Prod tyl)
  | term_func_wf e p typ s ty:
    well_formed_skel (extEnv p e) s ty ->
    well_formed_pattern p typ ->
    well_formed_term e (term_func p s) (Arrow typ ty)

  with well_formed_record: typing_env -> list (string * list type * term) -> type -> Prop :=
  | record_nil_wf e ty:
      well_formed_record e [] ty
  | record_cons_wf e t tyl f f_ta f_out f_in tl:
      find f (s_ftype ss) = Some (f_ta, f_out, f_in) ->
      length f_ta = length tyl ->
      well_formed_term e t (subst' f_in f_ta tyl) ->
      well_formed_record e tl (Base f_out tyl) ->
      well_formed_record e ((f, tyl, t) :: tl) (Base f_out tyl)

  with well_formed_skel: typing_env -> skeleton -> type -> Prop :=
  | skel_branch_wf e ty sl:
    Forall (fun s => well_formed_skel e s ty) sl ->
    well_formed_skel e (skel_branch ty sl) ty
  | skel_return_wf e ty t:
    well_formed_term e t ty ->
    well_formed_skel e (skel_return t) ty
  | skel_apply_wf e t f_ty tyl tl ty:
    well_formed_term e t f_ty ->
    List.Forall2 (well_formed_term e) tl tyl ->
    well_formed_apply f_ty tyl ty ->
    well_formed_skel e (skel_apply t tl) ty
  | skel_letin_wf e p s1 s2 ty1 ty2:
    well_formed_skel e s1 ty1 ->
    well_formed_pattern p ty1 ->
    well_formed_skel (extEnv p e) s2 ty2 ->
    well_formed_skel e (skel_letin p s1 s2) ty2
  | skel_exists_wf e p ty sk ty2:
    well_formed_pattern p ty ->
    well_formed_skel (extEnv p e) sk ty2 ->
    well_formed_skel e (skel_exists p ty sk) ty2

  with well_formed_apply: type -> list type -> type -> Prop :=
  | apply_0_wf ty:
    well_formed_apply ty [] ty
  | apply_S_wf i o ty tyl :
    well_formed_apply o tyl ty ->
    well_formed_apply (Arrow i o) (i::tyl) ty.

  Definition well_formed_ctype: Prop :=
    sameList (keys (s_ctype ss)) (s_constructor ss) /\
    (forall c,
      match find c (s_ctype ss) with
      | Some (c_ta, c_in, c_out) => incl (type_variables c_in) c_ta
      | None => True
      end) /\
    (forall ty c c', forall c_ta c_ty c'_ta c'_ty,
      Dict.find c (Skeleton.s_ctype ss) = Some (c_ta, c_ty, ty) ->
      Dict.find c' (Skeleton.s_ctype ss) = Some (c'_ta, c'_ty, ty) ->
      length c_ta = length c'_ta).

  Definition well_formed_ftype: Prop :=
    sameList (keys (s_ftype ss)) (s_field ss) /\
    (forall f,
      match find f (s_ftype ss) with
      | Some (f_ta, f_out, f_in) => incl (type_variables f_in) f_ta
      | None => True
      end) /\
    (forall ty f f', forall f_ta f_ty f'_ta f'_ty,
      Dict.find f (Skeleton.s_ftype ss) = Some (f_ta, f_ty, ty) ->
      Dict.find f' (Skeleton.s_ftype ss) = Some (f'_ta, f'_ty, ty) ->
      length f_ta = length f'_ta).

  Definition well_formed_unspec_terms: Prop :=
    sameList (keys (s_unspec_term_decl ss)) (s_unspec_term ss) /\
    (forall x ta ty,
      find x (s_unspec_term_decl ss) = Some (ta, ty) ->
      incl (type_variables ty) ta).

  Definition well_formed_spec_terms: Prop :=
    sameList (keys (s_spec_term_decl ss)) (s_spec_term ss) /\
    (forall x ta ty t,
      find x (s_spec_term_decl ss) = Some (ta, ty, t) ->
      incl (type_variables ty) ta /\
      well_formed_term [] t ty).


  Inductive well_formed_semantics: Prop :=
  | sk_sem_wf:
    well_formed_ctype ->
    well_formed_ftype ->
    well_formed_unspec_terms ->
    well_formed_spec_terms ->
    well_formed_semantics.
End WellFormed.


(* A tactic to prove well-formedness of a semantics *)
Ltac well_formed:=
  match goal with
  | [ |- well_formed_semantics _ ] =>
    econstructor; well_formed
  | [ |- well_formed_ctype _ ] =>
      repeat split;
        [ well_formed
        | well_formed
        | intro; simpl; repeat destruct (_ =? _)%string; well_formed
        | simpl; intros; repeat destruct (_ =? _)%string; congruence
        ]
  | [ |- well_formed_ftype _ ] =>
      repeat split;
        [ well_formed
        | well_formed
        | intro; simpl; repeat destruct (_ =? _)%string; well_formed
        | simpl; intros; repeat destruct (_ =? _)%string; congruence
        ]
  | [ |- well_formed_unspec_terms _ ] =>
      split; [split|]; [well_formed..|];
      intros ? ? ?; simpl; let A := fresh in intro A;
      first [ discriminate A |
      repeat destruct (_ =? _)%string; [injection A;clear A..|discriminate A];
      intros; subst; well_formed ]
  | [ |- well_formed_spec_terms _ ] =>
      split; [split; well_formed|];
      intros ? ? ? ?;
      simpl; repeat destruct (_ =? _)%string; [..|discriminate 1];
      injection 1 as ? ? ?; intros; subst;
      split; well_formed
    | [ |- well_formed_term _ _ ?t ?ty ] =>
        match eval compute in t with
        | term_var (TVLet _ _ ) =>
            eapply term_var_val_wf; reflexivity
        | term_var (TVUnspec _ _ _) => econstructor;
            reflexivity
        | term_var (TVSpec _ _ _) => econstructor ;
            reflexivity
        | term_func _ _ => econstructor;
            well_formed
        | term_constructor _ _ _ => econstructor;
            [..|reflexivity]; [reflexivity|well_formed]
        | term_field _ _ _ => econstructor;
            [..|reflexivity]; [reflexivity|well_formed]
        | term_nth _ _ _ => econstructor; [|reflexivity]; well_formed
        | term_tuple _ => constructor;
            repeat eapply Forall2_cons; [well_formed..|eapply Forall2_nil]
        | term_rec_make _ => econstructor;
            [|well_formed];
            let A := fresh "A" in
            intros ? A;
            repeat destruct A as [<-|A]; [..|destruct A];
            intros ? ?;
            intros A; injection A; clear A; intros; subst;
            simpl; tauto
        | term_rec_set _ _ => econstructor; well_formed
        end
    | [ |- well_formed_record _ _ [] _ ] => econstructor; well_formed
    | [ |- well_formed_record _ _ (_::_) _ ] => econstructor;
        [reflexivity|reflexivity|]; well_formed
    | [ |- well_formed_skel _ _ ?sk ?ty ] =>
        match eval compute in sk with
        | skel_branch _ _ =>
            constructor;
            repeat eapply Forall_cons; [well_formed..|eapply Forall_nil]
        | skel_letin _ _ _ =>
            econstructor; well_formed
        | skel_return _ =>
            econstructor; well_formed
        | skel_apply _ _ => econstructor;
            [ well_formed
            | repeat eapply Forall2_cons;
                [well_formed..|eapply Forall2_nil]
            | well_formed
            ]
        | _ => idtac sk
        end
    | [ |- well_formed_pattern _ ?p _ ] =>
        match eval compute in p with
        | pattern_constr _ _ _ =>
            econstructor; [|reflexivity..]; well_formed
        | pattern_tuple [] => econstructor
        | pattern_rec (_ :: []) => eapply pattern_rec_one_wf;
            [reflexivity|]; well_formed
        | pattern_rec (_ :: _ :: _) => eapply pattern_rec_next_wf;
            well_formed
        | pattern_tuple _ => econstructor ;
            [well_formed..
            | let H1 := fresh in let H2 := fresh in intros ? [H1 H2];
              repeat destruct H1 as [H1|H1]; repeat destruct H2 as [H2|H2];
              subst; simpl in *; congruence
            ]
        | pattern_var _ _ => econstructor
        | pattern_wildcard _ => econstructor
        | _ => idtac p
        end
    | [ |- well_formed_apply _ _ _ _ ] =>
        repeat econstructor
  | [ |- incl _ _ ] => intro; well_formed
  | [ |- In _ _ -> _ ] =>
      let In := fresh in intro In;
      repeat destruct In as [<-|In]; [..|destruct In]; well_formed
  | [ |- In _ _ ] => simpl; tauto
  | [ |- True ] => exact I
  end.
