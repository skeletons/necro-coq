Require Import String List Dict.
Require Import Skeleton WellFormed Induction Concrete Concrete_ndam RTC.
Require Import ZArith.
Import ListNotations DictNotations.

(*============================================================================*)
(*========================= Parameters and Notations =========================*)
(*============================================================================*)

Parameter s: skeletal_semantics.

Parameter u: dict (nat * unspec_function).

Notation "'interp_term'" := (interp_term s u).

Parameter g: type -> cvalue -> Prop.

Notation "'amstep'" := (amstep s u g).

Notation "'interp_skel'" := (interp_skel s u g).
Notation "'apply'" := (apply s u g).

Notation "'steps'" := (rtc amstep).
Notation "'nsteps'" := (rtcn amstep).

(*============================================================================*)
(*======================== Substituing Continuations =========================*)
(*============================================================================*)

Fixpoint subst_kr (kr : krt) (kd : kdt) (kl : klt) (ke : ket) (x : krt) : krt :=
  match x with
  | kr_id => kr
  | kr_constr c y => kr_constr c (subst_kr kr kd kl ke y)
  | kr_list vl e y => kr_list vl e (subst_kl kr kd kl ke y)
  | kr_let p sk e y => kr_let p sk e (subst_kr kr kd kl ke y)
  | kr_app1 vl e y => kr_app1 vl e (subst_kr kr kd kl ke y)
  | kr_app3 rl y => kr_app3 rl (subst_kr kr kd kl ke y)
  | kr_field d y => kr_field d (subst_kr kr kd kl ke y)
  | kr_nth n y => kr_nth n (subst_kr kr kd kl ke y)
  | kr_up ldt e y => kr_up ldt e (subst_kr kr kd kl ke y)
  | kr_ldt ldt e d y => kr_ldt ldt e d (subst_kd kr kd kl ke y)
  | kr_ldp p e ldp ldr y => kr_ldp p e ldp ldr (subst_ke kr kd kl ke y)
  end
with subst_kd (kr : krt) (kd : kdt) (kl : klt) (ke : ket) (x : kdt) : kdt :=
  match x with
  | kd_id => kd
  | kd_rec y => kd_rec (subst_kr kr kd kl ke y)
  | kd_up ldr y => kd_up ldr (subst_kr kr kd kl ke y)
  | kd_cons d r y => kd_cons d r (subst_kd kr kd kl ke y)
  end
with subst_kl (kr : krt) (kd : kdt) (kl : klt) (ke : ket) (x : klt) : klt :=
  match x with
  | kl_id => kl
  | kl_tuple y => kl_tuple (subst_kr kr kd kl ke y)
  | kl_list y r => kl_list (subst_kl kr kd kl ke y) r
  | kl_app2 r y => kl_app2 r (subst_kr kr kd kl ke y)
  end
with subst_ke (kr : krt) (kd : kdt) (kl : klt) (ke : ket) (x : ket) : ket :=
  match x with
  | ke_id => ke
  | ke_let sk y => ke_let sk (subst_kr kr kd kl ke y)
  | ke_pat pl rl y => ke_pat pl rl (subst_ke kr kd kl ke y)
  | ke_app sk al y => ke_app sk al (subst_kr kr kd kl ke y)
  | ke_ldp ldp ldr y => ke_ldp ldp ldr (subst_ke kr kd kl ke y)
  end.

Definition subst_ams (kr : krt) (kd : kdt) (kl : klt) (ke : ket) (ams : amstate) : amstate :=
  match ams with
  | KR x r => KR (subst_kr kr kd kl ke x) r
  | KD x ldr => KD (subst_kd kr kd kl ke x) ldr
  | KL x rl => KL (subst_kl kr kd kl ke x) rl
  | KE x e => KE (subst_ke kr kd kl ke x) e
  | T v e x => T v e (subst_kr kr kd kl ke x)
  | UR r ldt e x => UR r ldt e (subst_kr kr kd kl ke x)
  | GF r d x => GF r d (subst_kr kr kd kl ke x)
  | GN r n x => GN r n (subst_kr kr kd kl ke x)
  | GS ro x => GS ro (subst_kr kr kd kl ke x)
  | D ldt e x => D ldt e (subst_kd kr kd kl ke x)
  | LK e st x => LK e st (subst_kr kr kd kl ke x)
  | TL vl e x => TL vl e (subst_kl kr kd kl ke x)
  | SK sk e x => SK sk e (subst_kr kr kd kl ke x)
  | A rl r x => A rl r (subst_kr kr kd kl ke x)
  | P p r e x => P p r e (subst_ke kr kd kl ke x)
  | PD ldp ldr e x => PD ldp ldr e (subst_ke kr kd kl ke x)
  | PL pl rl e x => PL pl rl e (subst_ke kr kd kl ke x)
  end.

Lemma step_subst : forall a b kr kd kl ke,
    amstep a b -> amstep (subst_ams kr kd kl ke a) (subst_ams kr kd kl ke b).
Proof. intros. inversion H; subst; clear H; solve [econstructor; eauto]. Qed.

Lemma steps_subst : forall kr kd kl ke a b a' b',
    steps a b ->
    a' = (subst_ams kr kd kl ke a) ->
    b' = (subst_ams kr kd kl ke b) ->
    steps a' b'.
Proof.
  intros. subst. induction H; [apply rtc_ret|].
  eapply rtc_step; eauto. apply step_subst; auto.
Qed.

Lemma nsteps_subst : forall kr kd kl ke n a b a' b',
    nsteps n a b ->
    a' = (subst_ams kr kd kl ke a) ->
    b' = (subst_ams kr kd kl ke b) ->
    nsteps n a' b'.
Proof.
  intros. subst. induction H; [apply rtcn_ret|].
  eapply rtcn_step; eauto. apply step_subst; auto.
Qed.

Definition final (b : amstate) : Prop :=
  (exists r, b = (KR kr_id r)) \/
  (exists ldr, b = (KD kd_id ldr)) \/
  (exists rl, b = (KL kl_id rl)) \/
  (exists e, b = (KE ke_id e)).

(*============================================================================*)
(*================================= TACTICS ==================================*)
(*============================================================================*)

Ltac head t := match t with
  | ?t' _ => head t'
  | _ => t
  end.
Ltac first_arg t :=
  match t with
  | ?a0 ?a1 _ => first_arg (a0 a1)
  | ?f ?a1 => a1
  | _ => fail
  end.
(* Ltac second_arg t := *)
(*   match t with *)
(*   | ?a0 ?a1 ?a2 _ => second_arg (a0 a1 a2) *)
(*   | ?f ?a1 ?a2 => a2 *)
(*   | _ => fail *)
(*   end. *)
Ltac head_const t :=
  let t' := head t in is_constructor t'.
Ltac fahc t :=
  let t' := first_arg t in head_const t'.
Ltac is_id t :=
  lazymatch t with
  | kr_id => idtac
  | kd_id => idtac
  | kl_id => idtac
  | ke_id => idtac
  | _ => fail
  end.
Ltac is_noid t := tryif (is_id t) then fail else idtac.
Ltac fahc_noid t :=
  let t' := first_arg t in
  let t'' := head t' in
  is_noid t''; is_constructor t''.
Ltac fahc_id t :=
  let t' := first_arg t in is_id t'.
Ltac same_but_last t1 t2 :=
  match t1 with
  | ?t _ => match t2 with
            | t _ => idtac
            | _ => fail
            end
  | _ => fail
  end.
Ltac last_arg t :=
  match t with
  | _ ?t' => t'
  | _ => fail
  end.
  
Tactic Notation "invert" constr(H) :=
  inversion H; subst; clear H.
Tactic Notation "invert" constr(H) "as" simple_intropattern(I) :=
  inversion H as I; subst; clear H.

Tactic Notation "inverts" constr(H) :=
  invert H; simpl in *.
Tactic Notation "inverts" constr(H) "as" simple_intropattern(I) :=
  invert H as I; simpl in *.

Ltac myset x expr := let H := fresh "H" in
                     set (x := expr);
                     assert (H : x = expr) by reflexivity; clearbody x.

Ltac ret := first [apply rtc_ret | apply rtcn_ret].
Ltac step := first [eapply rtc_step | eapply rtcn_step].
Ltac onestep := step; [ |ret].
Ltac trans := first [eapply rtc_trans | eapply rtcn_trans2].
Ltac extrans := first [eapply rtc_extrans | eapply rtcn_extrans].
Ltac exstep := extrans; [onestep |].
Ltac exqtrans := eapply rtc_exqtrans.
Ltac exqstep := exqtrans; [onestep |].
Ltac exqret := eapply rtc_exqret.

Lemma rtcn_excut {A} a b m n0 (c c' : A -> amstate) d:
  nsteps n0 a b ->
  (exists (x:A) (n1 n2:nat), nsteps n1 b (c x) /\
                             nsteps n2 (c' x) d /\ n1+n2=m) ->
  (exists (x:A) (n1 n2:nat), nsteps n1 a (c x) /\
                             nsteps n2 (c' x) d /\ n1+n2=n0+m).
Proof.
  intros H1 [x [n1 [n2 [Hbc [Hcd Hnm]]]]].
  exists x, (n0+n1), n2. repeat split; auto.
  trans; eauto. lia.
Qed.

Ltac nexcuttrans := eapply rtcn_excut.
Ltac nexcut :=
  try match goal with
      | |- context[_ = S ?m] => replace (S m) with (1 + m) by auto
      end;
  nexcuttrans; [onestep ; solve [eauto | econstructor; eauto] |].

Ltac destrhyps := repeat match goal with
  | [ H : True |- _ ] => clear H
  | [ H : False |- _ ] => exfalso; auto
  | [ H : _ \/ _ |- _ ] => destruct H; subst; simpl in *
  | [ H : _ /\ _ |- _ ] => destruct H; subst; simpl in *
  | [ H : exists _, _ |- _ ] => destruct H; subst; simpl in *
  | [ H : ?a = ?b |- _ ] => head_const a; head_const b; invert H
  | [ H : ?x <> ?x |- _] => solve [destruct H; reflexivity]
  | _ => simpl in *
  end.

Ltac specrefl H a b :=
    let ta := head a in let tb := head b in
    is_constructor ta;
    lazymatch ta with
    |tb => first [ specialize (H _ _ _ _ eq_refl eq_refl)
                 | specialize (H _ _ _ _ eq_refl)
                 | specialize (H _ _ _ eq_refl eq_refl)
                 | specialize (H _ _ _ eq_refl)
                 | specialize (H _ _ eq_refl eq_refl)
                 | specialize (H _ _ eq_refl)]
    | _ => clear H end.


Ltac smarthyps := match goal with
  | [ H : amstep ?a ?b |- _] => fahc a; invert H
  | [ H : steps ?a ?b, HF : final ?b |- _] => fahc_noid a;
      let HH := fresh "H" in
      invert H; [solve [destruct HF as [[? HH]|[[? HH]|[? HH]]]; invert HH] | ]
  | [ H : steps ?a ?b |- _] => fahc_noid a; fahc_id b; invert H
  | [ H : steps ?a ?b |- _] => fahc_id a; invert H
  | [ H : nsteps (S ?n) ?a ?b |- _] => fahc a; invert H
  | [ H : nsteps ?n ?a ?b |- _] => fahc_noid a;
      destruct n as [|n]; invert H
  | [ H : final ?b |- _ ] => head_const b; unfold final in H
  | [ H : nsteps ?n ?a ?b |- _] => fahc_id a;
      let HH := fresh "H" in
      invert H as [|? ? ? ? HH]; [|invert HH]
  | [ IH : forall _, _<_ -> forall _ _, _ -> (_ /\ _),
      H1 : nsteps ?n1 ?a1 _ |- _ ] =>
      let kid := last_arg a1 in is_id kid;
      destruct (IH n1 ltac:(lia) _ _ H1); clear H1; destrhyps
  | [ H : forall _ _, ?a = (?b _ _) -> _ |- _] => specrefl H a b
  | [ H : forall _ _ _, ?a = (?b _ _ _) -> _ |- _] => specrefl H a b
  | [ H : forall _ _ _ _, ?a = (?b _ _ _ _) -> _ |- _] => specrefl H a b
  | _ => idtac
  end.

Ltac substk k H :=
  first [ eapply (steps_subst k kd_id kl_id ke_id _ _ _ _ H)
        | eapply (steps_subst kr_id k kl_id ke_id _ _ _ _ H)
        | eapply (steps_subst kr_id kd_id k ke_id _ _ _ _ H)
        | eapply (steps_subst kr_id kd_id kl_id k _ _ _ _ H)
        | eapply (nsteps_subst k kd_id kl_id ke_id _ _ _ _ _ H)
        | eapply (nsteps_subst kr_id k kl_id ke_id _ _ _ _ _ H)
        | eapply (nsteps_subst kr_id kd_id k ke_id _ _ _ _ _ H)
        | eapply (nsteps_subst kr_id kd_id kl_id k _ _ _ _ _ H)]; eauto;simpl.

Ltac smartgoal := match goal with
  | [ |- amstep ?a ?b] => fahc a; solve [econstructor; eauto]
  | [ |- steps ?a ?b] => fahc_noid a; try solve [ret]; step
  | [ |- steps ?a ?b] => fahc_id a; ret
  | [ H : steps ?a _ |- steps ?a _] => trans; [exact H|]
  | [ H : steps ?a1 _ |- steps ?a2 _] => same_but_last a1 a2;
      let kid := last_arg a1 in is_id kid;
      let k := last_arg a2 in
      trans; [substk k H|]
  | [ |- nsteps (S ?n) ?a ?b] => step
  | [ |- nsteps 0 ?a ?b] => ret
  | [ H : nsteps ?n ?a _ |- nsteps ?m ?a _] => trans; [exact H| |]; eauto
  | [ |- exists _, steps ?a _ /\ _] => fahc_noid a; exqstep
  | [ |- exists _, steps ?a _ /\ _] => fahc_id a; exqret
  | [ H : steps ?a _ |- exists _, steps ?a _ /\ _] => exqtrans; [exact H|]
  | [ H : steps ?a1 _ |- exists _, steps ?a2 _ /\ _] => same_but_last a1 a2;
      let kid := last_arg a1 in is_id kid;
      let k := last_arg a2 in
      exqtrans; [substk k H|]
  | [ |- exists _ n _, (nsteps n ?a _ /\ _)] => fahc_noid a; nexcut
  | [ |- exists _ n _, (nsteps n ?a _ /\ _)] => fahc_id a;
      let r := last_arg a in eexists r, 0, _; split; eauto
  | [ H : nsteps ?n1 ?a1 _ |- exists _ n2 _, (nsteps n2 ?a2 _ /\ _)] =>
        same_but_last a1 a2;
        let kid := last_arg a1 in is_id kid;
        let k := last_arg a2 in
        nexcuttrans; [substk k H|]
  | [ IH : forall _, _<_ -> _ , H1 : nsteps ?n1 ?a1 _
      |- exists _ n2 _, (nsteps n2 ?a2 _ /\ _)] =>
        same_but_last a1 a2;
        let knoid := last_arg a1 in is_noid knoid;
        destruct (IH n1 ltac:(lia) _ _ H1 ltac:(auto));
        clear H1; destrhyps
  | _ => auto
  end.

Ltac smartauto := repeat (simpl in *; destrhyps; repeat smarthyps; repeat smartgoal).

(*============================================================================*)
(*================================== LEMMAS ==================================*)
(*============================================================================*)

Definition cut_first_nat_prop (n : nat) (a b : amstate) : Prop :=
  final b ->
  ((forall v e kr, a = (T v e kr) -> exists r' n1 n2,
     (nsteps n1 (T v e kr_id) (KR kr_id r')) /\
     (nsteps n2 (KR kr r') b) /\ (n1+n2=n)) /\
   (forall r ldt e kr, a = (UR r ldt e kr) -> exists r' n1 n2,
     (nsteps n1 (UR r ldt e kr_id) (KR kr_id r')) /\
     (nsteps n2 (KR kr r') b) /\ (n1+n2=n)) /\
   (forall r d kr, a = (GF r d kr) -> exists r' n1 n2,
     (nsteps n1 (GF r d kr_id) (KR kr_id r')) /\
     (nsteps n2 (KR kr r') b) /\ (n1+n2=n)) /\
   (forall r m kr, a = (GN r m kr) -> exists r' n1 n2,
     (nsteps n1 (GN r m kr_id) (KR kr_id r')) /\
     (nsteps n2 (KR kr r') b) /\ (n1+n2=n)) /\
   (forall ro kr, a = (GS ro kr) -> exists r' n1 n2,
     (nsteps n1 (GS ro kr_id) (KR kr_id r')) /\
     (nsteps n2 (KR kr r') b) /\ (n1+n2=n)) /\
   (forall ldt e kd, a = (D ldt e kd) -> exists ldr' n1 n2,
     (nsteps n1 (D ldt e kd_id) (KD kd_id ldr')) /\
     (nsteps n2 (KD kd ldr') b) /\ (n1+n2=n)) /\
   (forall e x kr, a = (LK e x kr) -> exists r' n1 n2,
     (nsteps n1 (LK e x kr_id) (KR kr_id r')) /\
     (nsteps n2 (KR kr r') b) /\ (n1+n2=n)) /\
   (forall vl e kl, a = (TL vl e kl) -> exists rl' n1 n2,
     (nsteps n1 (TL vl e kl_id) (KL kl_id rl')) /\
     (nsteps n2 (KL kl rl') b) /\ (n1+n2=n)) /\
   (forall sk e kr, a = (SK sk e kr) -> exists r' n1 n2,
     (nsteps n1 (SK sk e kr_id) (KR kr_id r')) /\
     (nsteps n2 (KR kr r') b) /\ (n1+n2=n)) /\
   (forall rl r kr, a = (A rl r kr) -> exists r' n1 n2,
     (nsteps n1 (A rl r kr_id) (KR kr_id r')) /\
     (nsteps n2 (KR kr r') b) /\ (n1+n2=n)) /\
   (forall p r e ke, a = (P p r e ke) -> exists e' n1 n2,
     (nsteps n1 (P p r e ke_id) (KE ke_id e')) /\
     (nsteps n2 (KE ke e') b) /\ (n1+n2=n)) /\
   (forall ldp ldr e ke, a = (PD ldp ldr e ke) -> exists e' n1 n2,
     (nsteps n1 (PD ldp ldr e ke_id) (KE ke_id e')) /\
     (nsteps n2 (KE ke e') b) /\ (n1+n2=n)) /\
   (forall pl rl e ke, a = (PL pl rl e ke) -> exists e' n1 n2,
     (nsteps n1 (PL pl rl e ke_id) (KE ke_id e')) /\
     (nsteps n2 (KE ke e') b) /\ (n1+n2=n))).

Lemma cut_first_nat : forall m a b, nsteps m a b -> cut_first_nat_prop m a b.
Proof.
  induction m using (well_founded_induction lt_wf). intros.
  repeat split; intro xx; intros; subst; destruct xx; try solve[smartauto].
Qed.

Definition cut_first_prop (a b : amstate) : Prop :=
  final b ->
  ((forall v e kr, a = (T v e kr) -> exists r',
     (steps (T v e kr_id) (KR kr_id r')) /\
     (steps (KR kr r') b)) /\
   (forall r ldt e kr, a = (UR r ldt e kr) -> exists r',
     (steps (UR r ldt e kr_id) (KR kr_id r')) /\
     (steps (KR kr r') b)) /\
   (forall r d kr, a = (GF r d kr) -> exists r',
     (steps (GF r d kr_id) (KR kr_id r')) /\
     (steps (KR kr r') b)) /\
   (forall r m kr, a = (GN r m kr) -> exists r',
     (steps (GN r m kr_id) (KR kr_id r')) /\
     (steps (KR kr r') b)) /\
   (forall ro kr, a = (GS ro kr) -> exists r',
     (steps (GS ro kr_id) (KR kr_id r')) /\
     (steps (KR kr r') b)) /\
   (forall ldt e kd, a = (D ldt e kd) -> exists ldr',
     (steps (D ldt e kd_id) (KD kd_id ldr')) /\
     (steps (KD kd ldr') b)) /\
   (forall e x kr, a = (LK e x kr) -> exists r',
     (steps (LK e x kr_id) (KR kr_id r')) /\
     (steps (KR kr r') b)) /\
   (forall vl e kl, a = (TL vl e kl) -> exists rl',
     (steps (TL vl e kl_id) (KL kl_id rl')) /\
     (steps (KL kl rl') b)) /\
   (forall sk e kr, a = (SK sk e kr) -> exists r',
     (steps (SK sk e kr_id) (KR kr_id r')) /\
     (steps (KR kr r') b)) /\
   (forall rl r kr, a = (A rl r kr) -> exists r',
     (steps (A rl r kr_id) (KR kr_id r')) /\
     (steps (KR kr r') b)) /\
   (forall p r e ke, a = (P p r e ke) -> exists e',
     (steps (P p r e ke_id) (KE ke_id e')) /\
     (steps (KE ke e') b)) /\
   (forall ldp ldr e ke, a = (PD ldp ldr e ke) -> exists e',
     (steps (PD ldp ldr e ke_id) (KE ke_id e')) /\
     (steps (KE ke e') b)) /\
   (forall pl rl e ke, a = (PL pl rl e ke) -> exists e',
     (steps (PL pl rl e ke_id) (KE ke_id e')) /\
     (steps (KE ke e') b))).

Lemma cut_first : forall a b, steps a b -> cut_first_prop a b.
Proof.
  intros. destruct (rtc_rtcn _ H) as [n Hn]; clear H.
  intro HF. specialize (cut_first_nat _ _ _ Hn HF) as ?; clear Hn HF.
  repeat split; intros; smartauto;
    eexists; split; apply rtcn_rtc; eexists; eauto.
Qed.

Lemma findenv_lk : forall x e v,
  findEnv x e = Some v <-> steps (LK e x kr_id) (KR kr_id v).
Proof.
  induction e; intros; simpl in *; destrhyps.
  - split; intro; smartauto.
  - destruct a; simpl in *.
    destruct (string_dec s0 x); subst.
    { rewrite eqb_refl. split; intros; smartauto. }
    rewrite <- eqb_neq in n. rewrite n. split; intros.
    + rewrite IHe in H. econstructor; eauto. constructor.
      apply eqb_neq. rewrite eqb_sym. auto.
    + rewrite IHe. smartauto. rewrite eqb_neq in n. destrhyps.
Qed.

Lemma addasn_p : forall p v e e',
  add_asn e p v e' <-> steps (P p v e ke_id) (KE ke_id e').
Proof.
  split.
  - intro. induction H; smartauto.
    rewrite findenv_lk in H. smartauto.
  - revert p v e e'.
    remember (fun p => (forall (v : cvalue) (e e' : env),
    steps (P p v e ke_id) (KE ke_id e') -> add_asn e p v e')) as Q.
    enough (forall p, Q p) by (intro; subst; auto).
    apply pattern_rect; subst Q; intros; simpl; smartauto;
      try solve [econstructor; eauto].
    + revert e e' rl x0 H1. induction x; intros; smartauto.
      { econstructor. }
      invert x0.
      specialize (cut_first _ _ H0 ltac:(unfold final; eauto)) as HH.
      smartauto. econstructor; eauto.
    + revert e e' ldr x0 H1. induction x; intros; smartauto.
      { econstructor. }
      invert x0.
      specialize (cut_first _ _ H0 ltac:(unfold final; eauto)) as HH.
      smartauto.
      specialize (cut_first _ _ H3 ltac:(unfold final; eauto)) as HH.
      smartauto.
      econstructor; eauto.
      apply findenv_lk; eauto.
Qed.

(*============================================================================*)
(*================================= THEOREMS =================================*)
(*============================================================================*)

(*================================= BS=>NDAM =================================*)

Theorem bs_to_ndam_term : forall e v r,
  interp_term e v r -> steps (T v e kr_id) (KR kr_id r).
Proof.
  intros e v r H.
  induction H using it_ind; smartauto.
  - rewrite findenv_lk in H. smartauto.
  - specialize (cut_first _ _ H2 ltac:(unfold final; eauto)) as HH.
    smartauto.
  - rewrite findenv_lk in H0. smartauto.
  - rewrite H0. smartauto.
  - specialize (cut_first _ _ H2 ltac:(unfold final; eauto)) as HH.
    smartauto.
  - specialize (cut_first _ _ H2 ltac:(unfold final; eauto)) as HH.
    smartauto.
Qed.

Lemma bs_to_ndam_term_list : forall e vl rl,
  Forall2 (interp_term e) vl rl ->
  steps (TL vl e kl_id) (KL kl_id rl).
Proof.
  intros e vl. induction vl; intros; invert H; smartauto.
  specialize (bs_to_ndam_term _ _ _ H2) as ?. smartauto.
  specialize (IHvl l' H4). smartauto.
Qed.

Theorem bs_to_ndam_skel : forall e sk r,
  interp_skel e sk r -> steps (SK sk e kr_id) (KR kr_id r).
Proof.
  intros e sk r H.
  pose (Hit (e : env) (v : term) (r : cvalue) := True).
  pose (Hap (r0 : cvalue) (rl : list cvalue) (r : cvalue) :=
       steps (A rl r0 kr_id) (KR kr_id r)).
  induction H using is_ind with (Hit := Hit) (Hap := Hap);
    unfold Hit, Hap in *; clear Hit Hap; smartauto.
  - apply bs_to_ndam_term; eauto.
  - specialize (bs_to_ndam_term _ _ _ H) as ?.
    specialize (bs_to_ndam_term_list _ _ _ H0) as ?. smartauto.
  - rewrite addasn_p in H0. smartauto.
  - rewrite addasn_p in H0. smartauto.
  - rewrite addasn_p in H. smartauto.
  - revert dependent args. revert vl.
    induction n; intros; (destruct args; [invert H0|]); smartauto.
    apply IHn; eauto.
    lia. rewrite <- app_assoc in *. auto.
  - revert args H vl. induction n; intros;
      (destruct args; [smartauto; rewrite app_nil_r; smartauto|]).
    + invert H. invert H1.
    + smartauto.
      replace (c::args) with ([c] ++ args)%list by auto.
      rewrite app_assoc.
      apply IHn. lia.
Qed.

(*================================= NDAM=>BS =================================*)

Ltac cut_nat H :=
  specialize (cut_first_nat _ _ _ H ltac:(unfold final; eauto));
  intro; clear H; smartauto.

Ltac slia := solve [subst; simpl in *; lia].

Theorem ndam_to_bs : forall a b, steps a b ->
  ((fun a b =>
    (forall e v r, (a = T v e kr_id) -> (b = KR kr_id r) ->
                   interp_term e v r) /\
    (forall r0 ldt e r, (a = UR r0 ldt e kr_id) -> (b = KR kr_id r) ->
                        exists ldr1 ldr2, r0 = cval_record ldr1 /\
                          interp_term e (term_rec_make ldt) (cval_record ldr2) /\
                          r = cval_record (ldr2++ldr1)) /\
    (forall r0 d r, (a = GF r0 d kr_id) -> (b = KR kr_id r) ->
                    exists ldr, r0 = cval_record ldr /\ findEnv d ldr = Some r) /\
    (forall r0 n r, (a = GN r0 n kr_id) -> (b = KR kr_id r) ->
                    exists rl, r0 = cval_tuple rl /\ nth_error rl n = Some r) /\
    (forall ro r, (a = GS ro kr_id) -> (b = KR kr_id r) ->
                  ro = Some r) /\
    (forall ldt e ldr, (a = D ldt e kd_id) -> (b = KD kd_id ldr) ->
                       interp_term e (term_rec_make ldt) (cval_record ldr)) /\
    (forall e x r, (a = LK e x kr_id) -> (b = KR kr_id r) ->
                   findEnv x e = Some r) /\
    (forall e vl rl, (a = TL vl e kl_id) -> (b = KL kl_id rl) ->
                     Forall2 (interp_term e) vl rl) /\
    (forall e sk r, (a = SK sk e kr_id) -> (b = KR kr_id r) ->
                    interp_skel e sk r) /\
    (forall rl r0 r, (a = A rl r0 kr_id) -> (b = KR kr_id r) ->
                     apply r0 rl r) /\
    (forall p v e e', (a = P p v e ke_id) -> (b = KE ke_id e') ->
                      (add_asn e p v e')) /\
    (forall ldp ldr e e', (a = PD ldp ldr e ke_id) -> (b = KE ke_id e') ->
                          (add_asn e (pattern_rec ldp) (cval_record ldr) e')) /\
    (forall pl vl e e', (a = PL pl vl e ke_id) -> (b = KE ke_id e') ->
                        (add_asn e (pattern_tuple pl) (cval_tuple vl) e')))
     a b).
Proof.
  intros a b Hab. eapply rtc_strong_induction.
  2:exact Hab. clear a b Hab.
  intros. repeat split; intros; subst.  
  - (* T *) destruct v; smartauto; try cut_nat H3; try solve [econstructor ; eauto].
    revert x H7. induction l; intros x HF; invert HF; econstructor; eauto.
  - (* UR *) destruct r0; smartauto. cut_nat H3. eauto.
  - (* GF *) destruct r0; smartauto. eauto.
  - (* GN *) destruct r0; smartauto. eauto.
  - (* GS *) destruct ro; smartauto.
  - (* D  *) revert e ldr H0. induction ldt; intros e ldr HD.
    + cut_nat HD. econstructor.
    + cut_nat HD. cut_nat H4. cut_nat H4. econstructor; eauto.
  - (* LK *) apply findenv_lk. apply rtcn_rtc. eauto.
  - (* TL *) destruct vl; smartauto. cut_nat H3. cut_nat H4.
  - (* SK *) destruct sk; smartauto; try cut_nat H3;
               try cut_nat H4; try solve [econstructor; eauto].
  - (* A *) destruct rl. { smartauto; econstructor; eauto. }
    destruct r0; smartauto.
    + cut_nat H3. cut_nat H4. econstructor; eauto.
    + econstructor; eauto. slia.
    + clear H m. invert H9.
      * myset args [c].
        replace (n0) with ((S n0) - List.length args) at 2 by slia.
        eapply i_apply_unspec_cont. slia.
      * econstructor; eauto. slia.
        rewrite <- app_assoc in H7. auto.
      * rewrite <- app_assoc. simpl.
        myset args (c::rl).
        replace (n0 - Datatypes.length rl) with
            ((S n0) - Datatypes.length args) by slia.
        apply i_apply_unspec_cont. slia.
  - (* P  *) apply addasn_p. apply rtcn_rtc. eauto.
  - (* PD *) apply addasn_p. apply rtcn_rtc. exists (S m). smartauto.
  - (* PL *) apply addasn_p. apply rtcn_rtc. exists (S m). smartauto.
Qed.
