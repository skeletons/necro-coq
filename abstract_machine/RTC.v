Require Import ZArith Init.Wf.
Require Export Psatz. (* for lia tactic *)

(* file about the reflexive transitive closure of a relation *)
(* there is a version with number of steps *)
(* there is a strong induction principle *)

Section RTC.

  Context {A: Type}.
  Variable R: A -> A -> Prop.

  (*==========================================================================*)
  (*============================== Definitions ===============================*)
  (*==========================================================================*)

  Inductive rtc: A -> A -> Prop :=
  | rtc_ret: forall a, rtc a a
  | rtc_step: forall a b c, R a b -> rtc b c -> rtc a c.

  Inductive rtcn: nat -> A -> A -> Prop :=
  | rtcn_ret: forall a, rtcn 0 a a
  | rtcn_step: forall n a b c, R a b -> rtcn n b c -> rtcn (S n) a c.

  (*==========================================================================*)
  (*================================ RTCN-RTC ================================*)
  (*==========================================================================*)

  Lemma rtc_rtcn {a b} :
    rtc a b -> exists n, rtcn n a b.
  Proof.
    induction 1.
    - exists 0. constructor.
    - destruct IHrtc as [n ?]. exists (S n). econstructor; eauto.
  Qed.

  Lemma rtcn_rtc {a b}:
    (exists n, rtcn n a b) -> rtc a b.
  Proof.
    intros [n H]. revert a b H.
    induction n; intros a b H; inversion H; econstructor; eauto.
  Qed.

  (*==========================================================================*)
  (*=============================== RTCN PROPS ===============================*)
  (*==========================================================================*)

  Lemma rtcn_trans a b c n1 n2:
    rtcn n1 a b -> rtcn n2 b c -> rtcn (n1+n2) a c.
  Proof. induction 1; intros; auto. econstructor; eauto. Qed.

  Lemma rtcn_trans2 a b c n1 n2 n3:
    rtcn n1 a b -> rtcn n2 b c -> n1+n2=n3 -> rtcn n3 a c.
  Proof.
    intro H. revert n3.
    induction H; intros; simpl in *; subst; auto.
    econstructor; eauto.
  Qed.

  Lemma rtcn_extrans a b n1 n2:
    rtcn n1 a b -> (exists c, rtcn n2 b c) -> (exists c, rtcn (n1+n2) a c).
  Proof.
    intro H. induction H; intros; auto.
    destruct (IHrtcn H1) as [c0 H2].
    exists c0.
    eapply rtcn_step; eauto.
  Qed.

  Lemma rtcn_last n a c:
    rtcn (S n) a c -> (exists b, rtcn n a b /\ R b c).
  Proof.
    set (m := S n); assert (H : m = S n) by reflexivity; clearbody m.
    revert a c n H. induction m.
    - intros. inversion H.
    - intros a c n Hmn Hac.
      inversion Hmn; subst; clear Hmn.
      inversion Hac; subst; clear Hac.
      destruct n.
      + inversion H1; subst; clear H1.
        exists a; split; eauto; econstructor.
      + specialize (IHm b c _ ltac:(eauto) H1). clear H1.
        destruct IHm as [b0 [Hb1 Hb2]].
        exists b0; split; auto.
        econstructor; eauto.
  Qed.

  (*==========================================================================*)
  (*=============================== RTC PROPS ================================*)
  (*==========================================================================*)

  Lemma rtc_trans a b c:
    rtc a b -> rtc b c -> rtc a c.
  Proof.
    intro H. revert c. induction H; intros; auto.
    eapply rtc_step; eauto.
  Qed.

  Lemma rtc_extrans a b:
    rtc a b -> (exists c, rtc b c) -> (exists c, rtc a c).
  Proof.
    intro H. induction H; intros; auto.
    destruct (IHrtc H1) as [c0 H2].
    exists c0.
    eapply rtc_step; eauto.
  Qed.

  Lemma rtc_exqtrans {B} a b (c : B -> A) (Q : B -> Prop):
    rtc a b ->
    (exists x, rtc b (c x) /\ Q x) ->
    (exists x, rtc a (c x) /\ Q x).
  Proof.
    intros H1 [x [H2 H3]].
    eexists; split; eauto. eapply rtc_trans; eauto.
  Qed.

  Lemma rtc_exqret {B} y (c : B -> A) (Q : B -> Prop):
    Q y ->
    (exists x, rtc (c y) (c x) /\ Q x).
  Proof.
    intros HQ. eexists; split; eauto. constructor.
  Qed.

  Lemma rtc_last a c:
    rtc a c -> (a=c) \/ (exists b, rtc a b /\ R b c).
  Proof.
    intros. destruct (rtc_rtcn H) as [[|n] Hac].
    - left. inversion Hac. auto.
    - right. destruct (rtcn_last _ _ _ Hac) as [b [H1 H2]].
      exists b; split; auto.
      apply rtcn_rtc; eauto.
  Qed.

  (*==========================================================================*)
  (*=========================== STRONG INDUCTION =============================*)
  (*==========================================================================*)

  Theorem rtcn_strong_induction : forall (P : A -> A -> Prop),
      (forall m, (forall n, n<m -> forall a b, rtcn n a b -> P a b) ->
                 (forall a b, rtcn m a b -> P a b))->
      (forall m a b, rtcn m a b -> P a b).
  Proof.
    intros P HH m.
    induction m using (well_founded_induction lt_wf).
    eauto.
  Qed.

  Theorem rtc_strong_induction : forall (P : A -> A -> Prop),
      (forall m, (forall n, n<m -> forall a b, rtcn n a b -> P a b) ->
                 (forall a b, rtcn m a b -> P a b))->
      (forall a b, rtc a b -> P a b).
  Proof.
    intros. destruct (rtc_rtcn H0) as [m Hm]; clear H0.
    eapply rtcn_strong_induction; eauto.
  Qed.

End RTC.
