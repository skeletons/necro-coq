Require Import String List Dict.
Require Import Skeleton WellFormed Induction Concrete Concrete_ndam Concrete_am RTC.
Require Import ZArith.
Import ListNotations DictNotations.

(*============================================================================*)
(*========================= Parameters and Notations =========================*)
(*============================================================================*)

Parameter s: skeletal_semantics.

Notation "'amstate'" := Concrete_am.amstate.
Notation "'ndamstate'" := Concrete_ndam.amstate.

Parameter u_am: dict (nat * unspec_function_am).

Definition ufam_to_uf (uam : unspec_function_am) : unspec_function :=
  fun tyl rl r => In r (uam tyl rl).

Definition u : (dict (nat * unspec_function)) :=
  (fix aux dico :=
     match dico with
     | empty => empty
     | add x (n,uam) dico2 => add x (n,ufam_to_uf uam) (aux dico2)
     end) u_am.

Lemma find_u_am : forall x n itp,
    find x u_am = Some (n, itp) -> find x u = Some (n, (ufam_to_uf itp)).
Proof.
  unfold u. induction u_am; intros; simpl in *. { inversion H. }
  intros. destruct a. simpl in *.
  destruct (x =? s0); simpl in *.
  - inversion H; subst. auto.
  - apply IHd. auto.
Qed.

Parameter g: type -> cvalue -> Prop.

Notation "'stepp'" := (stepprop s u_am).
Notation "'steps'" := (rtc stepp).
Notation "'nsteps'" := (rtcn stepp).

Notation "'ndstep'" := (Concrete_ndam.amstep s u g).
Notation "'ndsteps'" := (rtc ndstep).
Notation "'ndnsteps'" := (rtcn ndstep).

Notation "'NKR'" := Concrete_ndam.KR.
Notation "'NKD'" := Concrete_ndam.KD.
Notation "'NKL'" := Concrete_ndam.KL.
Notation "'NKE'" := Concrete_ndam.KE.
Notation "'NT'"  := Concrete_ndam.T.
Notation "'NUR'" := Concrete_ndam.UR.
Notation "'NGF'" := Concrete_ndam.GF.
Notation "'NGN'" := Concrete_ndam.GN.
Notation "'NGS'" := Concrete_ndam.GS.
Notation "'ND'"  := Concrete_ndam.D.
Notation "'NLK'" := Concrete_ndam.LK.
Notation "'NTL'" := Concrete_ndam.TL.
Notation "'NSK'" := Concrete_ndam.SK.
Notation "'NA'"  := Concrete_ndam.A.
Notation "'NP'"  := Concrete_ndam.P.
Notation "'NPD'" := Concrete_ndam.PD.
Notation "'NPL'" := Concrete_ndam.PL.

(*============================================================================*)
(*============================= TACTICS Part 1 ===============================*)
(*============================================================================*)

Tactic Notation "invert" constr(H) :=
  inversion H; subst; clear H.
Tactic Notation "invert" constr(H) "as" simple_intropattern(I) :=
  inversion H as I; subst; clear H.

Ltac head t := match t with
  | ?t' _ => head t'
  | _ => t
  end.
Ltac first_arg t :=
  match t with
  | ?a0 ?a1 _ => first_arg (a0 a1)
  | ?f ?a1 => a1
  | _ => fail
  end.
(* Ltac second_arg t := *)
(*   match t with *)
(*   | ?a0 ?a1 ?a2 _ => second_arg (a0 a1 a2) *)
(*   | ?f ?a1 ?a2 => a2 *)
(*   | _ => fail *)
(*   end. *)
Ltac head_const t :=
  let t' := head t in is_constructor t'.
Ltac fahc t :=
  let t' := first_arg t in head_const t'.
Ltac is_id t :=
  lazymatch t with
  | kr_id => idtac
  | kd_id => idtac
  | kl_id => idtac
  | ke_id => idtac
  | f_empty => idtac
  | _ => fail
  end.
Ltac is_noid t := tryif (is_id t) then fail else idtac.
Ltac fahc_noid t :=
  let t' := first_arg t in
  let t'' := head t' in
  is_noid t''; is_constructor t''.
Ltac fahc_id t :=
  let t' := first_arg t in is_id t'.
Ltac same_but_last t1 t2 :=
  match t1 with
  | ?t _ => match t2 with
            | t _ => idtac
            | _ => fail
            end
  | _ => fail
  end.
Ltac last_arg t :=
  match t with
  | _ ?t' => t'
  | _ => fail
  end.
Ltac is_bmode t :=
  let t' := head t in
  lazymatch t' with
  | B  => idtac
  | _ => fail
  end.
Ltac is_nobmode t := tryif (is_bmode t) then fail else idtac.

Ltac simplrewr :=
  repeat (simpl;
          match goal with
          | [ H : ?x = ?b |- context[?x] ] => head_const b; rewrite H
          | _ => auto
          end).

Ltac destrhyps := repeat match goal with
  | [ H : True |- _ ] => clear H
  | [ H : False |- _ ] => exfalso; auto
  | [ H : _ \/ _ |- _ ] => destruct H; subst; simpl in *
  | [ H : _ /\ _ |- _ ] => destruct H; subst; simpl in *
  | [ H : exists _, _ |- _ ] => destruct H; subst; simpl in *
  | [ H : ?a = ?b |- _ ] => head_const a; head_const b; invert H
  | [ H : match ?x with _ => _ end = ?b |- _ ] =>
      head_const b; destruct x eqn:?
  | _ => simpl in *
  end.

(*============================================================================*)
(*======================== Substituing Continuations =========================*)
(*============================================================================*)

Fixpoint substfk_fk (fk : fkt) (x : fkt) : fkt :=
  match x with
  | f_empty => fk
  | f_sk sk e k y => f_sk sk e k (substfk_fk fk y)
  | f_list rl k y => f_list rl k (substfk_fk fk y)
  end.
Definition substfk_ams (fk : fkt) (ams : amstate) : amstate := match ams with
  | KR k r y => KR k r (substfk_fk fk y)
  | KD k ldr y => KD k ldr (substfk_fk fk y)
  | KL k rl y => KL k rl (substfk_fk fk y)
  | KE k e y => KE k e (substfk_fk fk y)
  | B y => B (substfk_fk fk y)
  | T v e k y => T v e k (substfk_fk fk y)
  | UR r ldt e k y => UR r ldt e k (substfk_fk fk y)
  | GF r d k y => GF r d k (substfk_fk fk y)
  | GN r n k y => GN r n k (substfk_fk fk y)
  | GS ro k y => GS ro k (substfk_fk fk y)
  | D ldt e k y => D ldt e k (substfk_fk fk y)
  | LK e st k y => LK e st k (substfk_fk fk y)
  | TL vl e k y => TL vl e k (substfk_fk fk y)
  | SK sk e k y => SK sk e k (substfk_fk fk y)
  | A rl r k y => A rl r k (substfk_fk fk y)
  | L rl k y => L rl k (substfk_fk fk y)
  | P p r e k y => P p r e k (substfk_fk fk y)
  | PD ldp ldr e k y => PD ldp ldr e k (substfk_fk fk y)
  | PL pl rl e k y => PL pl rl e k (substfk_fk fk y)
  end.

Lemma stepp_substfk : forall a b fk,
    stepp a b -> stepp (substfk_ams fk a) (substfk_ams fk b).
Proof.
  intros. inversion H; subst; clear H.
  destruct a; simpl in H0; destrhyps; constructor; simplrewr.
Qed.

Lemma steps_substfk : forall fk a b a' b',
    steps a b ->
    a' = (substfk_ams fk a) ->
    b' = (substfk_ams fk b) ->
    steps a' b'.
Proof.
  intros. subst. induction H; [apply rtc_ret|].
  eapply rtc_step; eauto. apply stepp_substfk; auto.
Qed.

Lemma nsteps_substfk : forall n fk a b a' b',
    nsteps n a b ->
    a' = (substfk_ams fk a) ->
    b' = (substfk_ams fk b) ->
    nsteps n a' b'.
Proof.
  intros. subst. induction H; [apply rtcn_ret|].
  eapply rtcn_step; eauto. apply stepp_substfk; auto.
Qed.

Lemma substfk_empty : forall f, substfk_fk f_empty f = f.
Proof. induction f; simpl; try rewrite IHf; auto. Qed.

Lemma substfk_ams_empty : forall a, substfk_ams f_empty a = a.
Proof. destruct a; simpl; rewrite substfk_empty; auto. Qed.

Lemma substfk_assoc : forall f1 f2 f,
  substfk_fk f1 (substfk_fk f2 f) =
  substfk_fk (substfk_fk f1 f2) f.
Proof. induction f; simpl; try rewrite IHf; auto. Qed.

Lemma substfk_ams_assoc : forall f1 f2 a,
  substfk_ams f1 (substfk_ams f2 a) =
  substfk_ams (substfk_fk f1 f2) a.
Proof. destruct a; simpl; rewrite substfk_assoc; auto. Qed.

(*============================================================================*)
(*============================= TACTICS Part 2 ===============================*)
(*============================================================================*)

Ltac ret := first [apply rtc_ret | apply rtcn_ret].
Ltac step := first [eapply rtc_step | eapply rtcn_step].
Ltac onestep := step; [ |ret].
Ltac trans := first [eapply rtc_trans | eapply rtcn_trans2].

Lemma rtcn_excut a b c m n0 fk:
  nsteps n0 a b ->
  (exists (n1 : nat) (c' : amstate), nsteps n1 b c' /\
    ((n1 = m /\ c = substfk_ams fk c') \/
     (exists n2 : nat, nsteps n2 (B fk) c /\ c' = B f_empty /\ n1+n2 = m))) ->
  (exists (n1 : nat) (c' : amstate), nsteps n1 a c' /\
    ((n1 = n0+m /\ c = substfk_ams fk c') \/
     (exists n2 : nat, nsteps n2 (B fk) c /\ c' = B f_empty /\ n1+n2 = n0+m))).
Proof.
  intros H1 [n1 [c' [Hbc' [[] | [n2 [? []]]]]]]; subst.
  - exists (n0+m), c'. split; auto. trans; eauto.
  - exists (n0+n1), (B f_empty). split; [trans; eauto|].
    right. eexists; repeat split; eauto. lia.
Qed.

Ltac nexcuttrans :=
  repeat lazymatch goal with
         | |- context[_ = S ?m] => replace (S m) with (1 + m) by auto
         | |- context[_ = ?m1 + ?m2] => idtac
         | |- context[_ = ?m] => tryif (head_const m) then fail else
             (replace m with (m + 0) by auto)
      end;
    eapply rtcn_excut.
Ltac nexcut := nexcuttrans;
  [onestep; solve [eauto | econstructor; simplrewr; eauto] |].

Ltac specrefl H a b :=
    let ta := head a in let tb := head b in
    is_constructor ta;
    lazymatch ta with
    |tb => first [ specialize (H _ _ _ _ eq_refl)
                 | specialize (H _ _ _ eq_refl)
                 | specialize (H _ _ eq_refl)]
    | _ => clear H end.

Ltac smarthyps := match goal with
  | [ H : context[substfk_ams f_empty ?a] |- _ ] =>
    rewrite substfk_ams_empty in H
  | [ H : context[substfk_ams ?f1 (substfk_ams ?f2 ?a)] |- _ ] =>
    rewrite substfk_ams_assoc in H
  | [ IH : forall _, _<_ -> _ , H1 : nsteps ?n ?a1 ?a2 |- _] =>
        is_nobmode a1; is_nobmode a2;
        let fempty := last_arg a1 in is_id fempty;
        let HH := fresh "H" in
        pose (IH n ltac:(lia) _ _ H1) as HH; clearbody HH;
        clear H1; destrhyps
  | [ H : stepp ?a ?b |- _] => fahc a; invert H
  | [ H : nsteps ?n ?a ?b |- _] => fahc a;
      destruct n as [|n]; invert H
  | [ H1 : ?a = ?b ?d, H2 : forall c, ?a = ?b c -> _ |- _] =>
      specialize (H2 d H1)
  | [ H : forall _, ?a = (?b _ _) -> _ |- _] => specrefl H a b
  | [ H : forall _ _, ?a = (?b _ _ _) -> _ |- _] => specrefl H a b
  | [ H : forall _ _ _, ?a = (?b _ _ _ _) -> _ |- _] => specrefl H a b
  | [ H : forall _ _ _ _, ?a = (?b _ _ _ _ _) -> _ |- _] => specrefl H a b
  | _ => idtac
  end.

Ltac substfk k H :=
  first [ eapply (steps_substfk k _ _ _ _ H)
        | eapply (nsteps_substfk _ k _ _ _ _ H)]; eauto;simpl.

Ltac smartgoal := match goal with
  | [ |- ndstep ?a ?b] => fahc a; solve [econstructor; eauto]
  | [ |- ndsteps ?a ?a] => solve [ret]
  | [ |- ndsteps ?a ?b] => fahc_noid a; try solve [ret];
                           step; [solve [econstructor; eauto]|]
  | [ |- context[substfk_ams f_empty ?a]] =>
    rewrite substfk_ams_empty
  | [ |- context[substfk_ams ?f1 (substfk_ams ?f2 ?a)]] =>
    rewrite substfk_ams_assoc
  | [H : (?n1 =? ?n2)%nat = true |- ?n1 = ?n2] => exact (beq_nat_true n1 n2 H)
  | [ H : ?x = ?b |- context[?x] ] => head_const b; rewrite H
  | [ |- nsteps 0 ?a ?b] => ret
  | [ H : nsteps ?n1 ?a1 _ |- exists n2 _, (nsteps n2 ?a2 _ /\ _)] =>
        same_but_last a1 a2;
        let kid := last_arg a1 in is_id kid;
        let k := last_arg a2 in
        nexcuttrans; [substfk k H|]; clear H
  | [ |- exists n2 _, (nsteps n2 ?a2 _ /\ ((n2 = 0 /\ _) \/ _))] =>
        exists 0; exists a2;
        split; [|left; split; [reflexivity|]]
  | [ |- exists n _, (nsteps n ?a _ /\ _)] => fahc_noid a; nexcut
  | [ IH : forall _, _<_ -> _ , H1 : nsteps ?n ?a1 _
      |- exists n1 _, (nsteps n1 ?a2 _ /\ _)] =>
        same_but_last a1 a2; is_nobmode a1;
        let knoid := last_arg a1 in is_noid knoid;
        edestruct (IH n ltac:(lia) _ _ H1);
        try solve [reflexivity];
        clear H1; destrhyps
  | [ H : nsteps ?n ?a ?b |- exists n' c', nsteps n' ?a c' /\ _ ] =>
    exists n, b; split; [exact H|]
  | [ |- exists n c, nsteps n (B f_empty) c /\ _ ] =>
    exists 0, (B f_empty); split; [ret | right; eauto]
  | _ => auto
  end.

Ltac smartauto := repeat (simpl in *; destrhyps; repeat smartgoal; repeat smarthyps).
(* Ltac smartstep := simpl in *; destrhyps; smarthyps; smartgoal. *)

(*============================================================================*)
(*================================== LEMMAS ==================================*)
(*============================================================================*)

Definition getfk (a : amstate) : fkt :=
  match a with
  | KR _ _     fk => fk
  | KD _ _     fk => fk
  | KL _ _     fk => fk
  | KE _ _     fk => fk
  | B          fk => fk
  | T  _ _ _   fk => fk
  | UR _ _ _ _ fk => fk
  | GF _ _ _   fk => fk
  | GN _ _ _   fk => fk
  | GS _ _     fk => fk
  | D  _ _ _   fk => fk
  | LK _ _ _   fk => fk
  | TL _ _ _   fk => fk
  | SK _ _ _   fk => fk
  | A  _ _ _   fk => fk
  | L  _ _     fk => fk
  | P  _ _ _ _ fk => fk
  | PD _ _ _ _ fk => fk
  | PL _ _ _ _ fk => fk
  end.

Definition removefk (a : amstate) : amstate :=
  match a with
  | KR k r fk => KR k r f_empty
  | KD k ldr fk => KD k ldr f_empty
  | KL k rl fk => KL k rl f_empty
  | KE k e fk => KE k e f_empty
  | B fk => B f_empty
  | T v e k fk => T v e k f_empty
  | UR r ldt e k fk => UR r ldt e k f_empty
  | GF r d k fk => GF r d k f_empty
  | GN r n k fk => GN r n k f_empty
  | GS ro k fk => GS ro k f_empty
  | D ldt e k fk => D ldt e k f_empty
  | LK e st k fk => LK e st k f_empty
  | TL vl e k fk => TL vl e k f_empty
  | SK sk e k fk => SK sk e k f_empty
  | A rl r k fk => A rl r k f_empty
  | L rl k fk => L rl k f_empty
  | P p r e k fk => P p r e k f_empty
  | PD ldp ldr e k fk => PD ldp ldr e k f_empty
  | PL pl rl e k fk => PL pl rl e k f_empty
  end.

Definition cut_fk_nat_prop (n : nat) (a b : amstate) : Prop :=
  let fk := getfk a in
  exists n' b', nsteps n' (removefk a) b' /\
  ((n' = n /\ b = substfk_ams fk b') \/
   (exists n'', nsteps n'' (B fk) b /\ b' = (B f_empty) /\ n'+n''=n)).

Lemma cut_fk_nat : forall m a b, nsteps m a b -> cut_fk_nat_prop m a b.
Proof.
  induction m using (well_founded_induction lt_wf). intros.
  unfold cut_fk_nat_prop.
  destruct a as [[]|[]|[]|[]| |[]|[]|[]|[]|[]|[]|[]|[]|[]|[]|[]|[]|[]|[]]; smartauto.
Qed.

(*============================================================================*)
(*================================= THEOREMS =================================*)
(*============================================================================*)

Definition finalcoerc (a : amstate) : option ndamstate :=
  match a with
  | KR kr_id r   fk => Some (NKR kr_id r)
  | KD kd_id ldr fk => Some (NKD kd_id ldr)
  | KL kl_id rl  fk => Some (NKL kl_id rl)
  | KE ke_id e   fk => Some (NKE ke_id e)
  | _ => None
  end.

Theorem am_to_ndam : forall a b, steps a b ->
  ((fun a b => forall b', finalcoerc b = Some b' ->
    (forall k r0,      (a = KR k r0 f_empty)      -> ndsteps (NKR k r0) b') /\
    (forall k ldr,     (a = KD k ldr f_empty)     -> ndsteps (NKD k ldr) b') /\
    (forall k rl0,     (a = KL k rl0 f_empty)     -> ndsteps (NKL k rl0) b') /\
    (forall k e0,      (a = KE k e0 f_empty)      -> ndsteps (NKE k e0) b') /\
    (forall e v k,     (a = T v e k f_empty)      -> ndsteps (NT v e k) b') /\
    (forall r ldt e k, (a = UR r ldt e k f_empty) -> ndsteps (NUR r ldt e k) b') /\
    (forall r d k,     (a = GF r d k f_empty)     -> ndsteps (NGF r d k) b') /\
    (forall r n k,     (a = GN r n k f_empty)     -> ndsteps (NGN r n k) b') /\
    (forall ro k,      (a = GS ro k f_empty)      -> ndsteps (NGS ro k) b') /\
    (forall ldt e k,   (a = D ldt e k f_empty)    -> ndsteps (ND ldt e k) b') /\
    (forall e st k,    (a = LK e st k f_empty)    -> ndsteps (NLK e st k) b') /\
    (forall e vl k,    (a = TL vl e k f_empty)    -> ndsteps (NTL vl e k) b') /\
    (forall e s k,     (a = SK s e k f_empty)     -> ndsteps (NSK s e k) b') /\
    (forall rl r0 k,   (a = A rl r0 k f_empty)    -> ndsteps (NA rl r0 k) b') /\
    (forall rl k,      (a = L rl k f_empty) ->
                       (exists r0, In r0 rl /\ ndsteps (NKR k r0) b')) /\
    (forall p v e k,   (a = P p v e k f_empty)    -> ndsteps (NP p v e k) b') /\
    (forall lp lr e k, (a = PD lp lr e k f_empty) -> ndsteps (NPD lp lr e k) b') /\
    (forall pl vl e k, (a = PL pl vl e k f_empty) -> ndsteps (NPL pl vl e k) b')
   ) a b).
Proof.
  intros a b Hab. eapply rtc_strong_induction.
  2:exact Hab. clear a b Hab.
  intros. repeat split; intros; subst.
  - (* KR *) destruct k; smartauto.
  - (* KD *) destruct k; smartauto.
  - (* KL *) destruct k; smartauto.
  - (* KE *) destruct k; smartauto.
  - (* T  *) destruct v; smartauto.
    + pose (find_u_am _ _ _ Heqo) as HF; clearbody HF. smartauto.
    + pose (find_u_am _ _ _ Heqo) as HF; clearbody HF. smartauto.
    + pose (beq_nat_true _ _ Heqb1). smartauto.
  - (* UR *) destruct r; smartauto.
  - (* GF *) destruct r; smartauto.
  - (* GN *) destruct r; smartauto.
  - (* GS *) destruct ro; smartauto.
  - (* D  *) destruct ldt; smartauto.
  - (* LK *) destruct e; smartauto.
    + rewrite eqb_eq in Heqb1. subst. smartauto.
    + rewrite eqb_neq in Heqb1. smartauto.
  - (* TL *) destruct vl; smartauto.
  - (* SK *) destruct s0; smartauto.
    destruct (cut_fk_nat _ _ _ H4). smartauto.
    + step. { constructor. left. eauto. }
      edestruct H2; destruct x0; smartauto.
    + clear - H1 H15. invert H15; [destruct b; smartauto |]. invert H.
      step; eauto. econstructor. right. auto.
  - (* R  *) destruct rl; smartauto.
    invert H2. { destruct b; smartauto. } invert H3.
    step; [|eassumption].
    pose (find_u_am _ _ _ Heqo) as HF; clearbody HF.
    econstructor; eauto.
  - (* L  *) destruct rl; smartauto.
    destruct (cut_fk_nat _ _ _ H4). smartauto.
    + exists c. split; eauto.
      edestruct H2; destruct x0; smartauto.
    + eexists; eauto.
  - (* P  *) destruct p; smartauto.
    rewrite eqb_eq in Heqb1. subst. smartauto.
  - (* PD *) destruct lp; smartauto.
  - (* PL *) destruct pl; smartauto.
Qed.
