open Am

let base_type =
  "clos" :: ("env" :: ("ident" :: ("lterm" :: [])))

let tau_clos =
  Base ("clos", [])

let tau_env =
  Base ("env", [])

let tau_ident =
  Base ("ident", [])

let tau_lterm =
  Base ("lterm", [])

let constructor =
  "App" :: ("Lam" :: ("Var" :: ("Clos" :: [])))

let field =
  []

let unspec_term =
  "extEnv" :: ("getEnv" :: [])

let spec_term =
  "eval" :: []

let unspec_term_decl =
  Add ("getEnv", ([], (Arrow ((Prod
    (tau_ident :: (tau_env :: []))), tau_clos))), (Add
    ("extEnv", ([], (Arrow ((Prod
    (tau_env :: (tau_ident :: (tau_clos :: [])))),
    tau_env))), Empty)))

let sigma_eval =
  TVSpec ("eval", (Arrow (tau_env, (Arrow (tau_lterm,
    tau_clos)))), [])

let sigma_extEnv =
  TVUnspec ("extEnv", (Arrow ((Prod
    (tau_env :: (tau_ident :: (tau_clos :: [])))),
    tau_env)), [])

let sigma_getEnv =
  TVUnspec ("getEnv", (Arrow ((Prod
    (tau_ident :: (tau_env :: []))), tau_clos)), [])

let delta_eval =
  Term_func ((Pattern_var ("s", tau_env)), (Skel_return (Term_func
    ((Pattern_var ("l", tau_lterm)), (Skel_branch (tau_clos,
    ((Skel_letin ((Pattern_constr ("Lam", [], (Pattern_tuple ((Pattern_var
    ("x", tau_ident)) :: ((Pattern_var ("t",
    tau_lterm)) :: []))))), (Skel_return (Term_var (TVLet ("l",
    tau_lterm)))), (Skel_return (Term_constructor ("Clos", [],
    (Term_tuple ((Term_var (TVLet ("x", tau_ident))) :: ((Term_var
    (TVLet ("t", tau_lterm))) :: ((Term_var (TVLet ("s",
    tau_env))) :: []))))))))) :: ((Skel_letin ((Pattern_constr ("Var",
    [], (Pattern_var ("x", tau_ident)))), (Skel_return (Term_var (TVLet
    ("l", tau_lterm)))), (Skel_apply ((Term_var sigma_getEnv),
    ((Term_tuple ((Term_var (TVLet ("x", tau_ident))) :: ((Term_var
    (TVLet ("s", tau_env))) :: []))) :: []))))) :: ((Skel_letin
    ((Pattern_constr ("App", [], (Pattern_tuple ((Pattern_var ("t1",
    tau_lterm)) :: ((Pattern_var ("t2", tau_lterm)) :: []))))),
    (Skel_return (Term_var (TVLet ("l", tau_lterm)))), (Skel_letin
    ((Pattern_constr ("Clos", [], (Pattern_tuple ((Pattern_var ("x",
    tau_ident)) :: ((Pattern_var ("t", tau_lterm)) :: ((Pattern_var
    ("s'", tau_env)) :: [])))))), (Skel_apply ((Term_var sigma_eval),
    ((Term_var (TVLet ("s", tau_env))) :: ((Term_var (TVLet ("t1",
    tau_lterm))) :: [])))), (Skel_letin ((Pattern_var ("w",
    tau_clos)), (Skel_apply ((Term_var sigma_eval), ((Term_var (TVLet
    ("s", tau_env))) :: ((Term_var (TVLet ("t2",
    tau_lterm))) :: [])))), (Skel_letin ((Pattern_var ("s''",
    tau_env)), (Skel_apply ((Term_var sigma_extEnv), ((Term_tuple
    ((Term_var (TVLet ("s'", tau_env))) :: ((Term_var (TVLet ("x",
    tau_ident))) :: ((Term_var (TVLet ("w",
    tau_clos))) :: [])))) :: []))), (Skel_apply ((Term_var
    sigma_eval), ((Term_var (TVLet ("s''", tau_env))) :: ((Term_var
    (TVLet ("t", tau_lterm))) :: [])))))))))))) :: [])))))))))

let spec_term_decl =
  Add ("eval", (([], (Arrow (tau_env, (Arrow (tau_lterm,
    tau_clos))))), delta_eval), Empty)

let ctype =
  Add ("Clos", (([], (Prod
    (tau_ident :: (tau_lterm :: (tau_env :: []))))), "clos"),
    (Add ("Var", (([], tau_ident), "lterm"), (Add ("Lam", (([], (Prod
    (tau_ident :: (tau_lterm :: [])))), "lterm"), (Add ("App",
    (([], (Prod (tau_lterm :: (tau_lterm :: [])))), "lterm"),
    Empty)))))))

let ftype =
  Empty

let lambdaSkelSem =
  { s_base_type = base_type; s_constructor = constructor; s_field = field;
    s_unspec_term = unspec_term; s_spec_term = spec_term; s_ctype = ctype;
    s_ftype = ftype; s_unspec_term_decl = unspec_term_decl;
    s_spec_term_decl = spec_term_decl }
