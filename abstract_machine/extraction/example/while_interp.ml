open Am
open While_skelsem

(* I can't force the signature or I forget constructors... *)
(* Keeping WhileSkelSemImplem transparent *)
module WhileSkelSemImplem (* : SkelSemImplemSig *) = struct
  let ss = whileSkelSem

  type vvalue =
    | Valueint of int
    | Valuebool of bool
  type basevalue =
    | Vint of int
    | Vbool of bool
    | Vvalue of vvalue
    | Vstate of (string * vvalue) list
    | Vlit of int
    | Vident of string
    | Vunit
  type cvalue =
    | Cval_base of basevalue
    | Cval_constructor of string * cvalue
    | Cval_tuple of cvalue list
    | Cval_closure of pattern * skeleton * (string * cvalue) list
    | Cval_record of (string * cvalue) list
    | Cval_unspec of int * string * necrotype list * cvalue list

  type unspec_function_am = necrotype list -> cvalue list -> cvalue list

  let function_add = function
    | a::b::[] -> begin
        match a, b with
        | Cval_base (Vint va), Cval_base (Vint vb) -> [Cval_base (Vvalue (Valueint (va+vb)))]
        | _,_ -> [] end
    | _ -> []
  
  let function_eq = function
    | a::b::[] -> begin
        match a, b with
        | Cval_base (Vint va), Cval_base (Vint vb) -> [Cval_base (Vvalue (Valuebool (va=vb)))]
        | _,_ -> [] end
    | _ -> []
  
  let function_isint = function
    | a::[] -> begin
        match a with
        | Cval_base (Vvalue (Valueint i)) -> [Cval_base (Vint i)]
        | _ -> [] end
    | _ -> []
  
  let function_isbool = function
    | a::[] -> begin
        match a with
        | Cval_base (Vvalue (Valuebool b)) -> [Cval_base (Vbool b)]
        | _ -> [] end
    | _ -> []
  
  let function_istrue = function
    | a::[] -> begin
        match a with
        | Cval_base (Vbool b) -> if b then [Cval_base Vunit] else []
        | _ -> [] end
    | _ -> []
  
  let function_isfalse = function
    | a::[] -> begin
        match a with
        | Cval_base (Vbool b) -> if b then [] else [Cval_base Vunit]
        | _ -> [] end
    | _ -> []
  
  let function_littoval = function
    | a::[] -> begin
        match a with
        | Cval_base (Vlit i) -> [Cval_base (Vvalue (Valueint i))]
        | _ -> [] end
    | _ -> []
  
  let function_neg = function
    | a::[] -> begin
        match a with
        | Cval_base (Vbool b) -> [Cval_base (Vvalue (Valuebool (not b)))]
        | _ -> [] end
    | _ -> []
  
  let function_read = function
    | a::b::[] -> begin
        match a,b with
        | Cval_base (Vident x), Cval_base (Vstate st) -> begin
            match List.assoc_opt x st with
            | Some v -> [Cval_base (Vvalue v)]
            | None -> [] end
        | _ -> [] end
    | _ -> []
  
  let function_write = function
    | a::b::c::[] -> begin
        match a,b,c with
        | Cval_base (Vident x), Cval_base (Vstate st), Cval_base (Vvalue v) ->
            [Cval_base (Vstate ((x,v)::st))]
        | _ -> [] end
    | _ -> []

  let rec dict_of_list (lst : (string * 'a) list) : 'a dict = match lst with
    | [] -> Empty
    | (s, x) :: lst2 -> Add (s, x, (dict_of_list lst2))
  
  let u_am : (int * unspec_function_am) dict =
    dict_of_list (
      ("add",      (2, fun _ -> function_add))::
      ("eq",       (2, fun _ -> function_eq))::
      ("isInt",    (1, fun _ -> function_isint))::
      ("isBool",   (1, fun _ -> function_isbool))::
      ("isFalse",  (1, fun _ -> function_isfalse))::
      ("isTrue",   (1, fun _ -> function_istrue))::
      ("litToVal", (1, fun _ -> function_littoval))::
      ("neg",      (1, fun _ -> function_neg))::
      ("read",     (2, fun _ -> function_read))::
      ("write",    (3, fun _ -> function_write))::
      [])

end

(*============================================================================*)
(*============================================================================*)
(*============================================================================*)
(*============================================================================*)

module WhileAM = AM(WhileSkelSemImplem)
open WhileAM

let xconst (l:int) =
  Cval_constructor ("Const", Cval_base (Vlit l))
let xvar (x:string) =
  Cval_constructor ("Var", Cval_base (Vident x))
let xplus e1 e2 =
  Cval_constructor ("Plus", Cval_tuple [e1; e2])
let xequal e1 e2 =
  Cval_constructor ("Equal", Cval_tuple [e1; e2])
let xnot e =
  Cval_constructor ("Not", e)

let xskip =
  Cval_constructor ("Skip", Cval_tuple [])
let xassign (x:string) e =
  Cval_constructor ("Assign", Cval_tuple [Cval_base (Vident x); e])
let xseq t1 t2 =
  Cval_constructor ("Seq", Cval_tuple [t1; t2])
let xif e t1 t2 =
  Cval_constructor ("If", Cval_tuple [e; t1; t2])
let xwhile e t =
  Cval_constructor ("While", Cval_tuple [e; t])

let emptyenv =
  Cval_base (Vstate [])

(*============================================================================*)
(*============================================================================*)

let rec printreswv (bv : basevalue) = match bv with
  | Vstate st -> Printf.printf "getting a state\n";
      printreswv (Vvalue (List.assoc "res" st))
  | Vvalue (Valueint i) -> Printf.printf "getting: %d\n" i
  | Vvalue (Valuebool b) -> Printf.printf "getting: %B\n" b
  | _ -> Printf.printf "getting some other basevalue\n"
let rec printres ropt = match ropt with
  | Some (Cval_base r) -> printreswv r
  | Some _ -> Printf.printf "getting result but not a base value!\n"
  | None -> Printf.printf "no result! might need to increase fuel!\n"

let _ =
  let val_eval_stmt : term = Term_var (sigma_eval_stmt) in
  let multstmt =
    xseq (xassign "res" (xconst 0))
   (xseq (xassign "a" (xconst 10))
   (xseq (xassign "b" (xconst 7))
         (xwhile (xnot (xequal (xvar ("a")) (xconst 0)))
            (xseq (xassign "a" (xplus (xvar "a") (xconst (-1))))
                  (xassign "res" (xplus (xvar "res") (xvar "b")))
   )))) in

  let vars : term = Term_var (TVLet ("s", tau_state)) in
  let vart : term = Term_var (TVLet ("t", tau_stmt)) in
  let sk = Skel_apply (val_eval_stmt, [vars ; vart]) in
  let amstate = inject_skel sk  [("s", emptyenv); ("t", multstmt)] in

  let ropt = evalfuel 13834 amstate in
  printres ropt
