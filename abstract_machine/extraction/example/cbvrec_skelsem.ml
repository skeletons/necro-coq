open Am

let base_type =
  "ident" :: ("lambdaterm" :: ("othervalue" :: ("state" :: ("value" :: []))))

let tau_ident =
  Base ("ident", [])

let tau_lambdaterm =
  Base ("lambdaterm", [])

let tau_othervalue =
  Base ("othervalue", [])

let tau_state =
  Base ("state", [])

let tau_value =
  Base ("value", [])

let constructor =
  "Vclos" :: ("Vfalse" :: ("Vfix" :: ("Vother" :: ("Vpair" :: ("Vtrue" :: ("Andl" :: ("App" :: ("Equals" :: ("Fix" :: ("For" :: ("If" :: ("Lam" :: ("Left" :: ("Let" :: ("Not" :: ("Pair" :: ("Plus" :: ("Right" :: ("Val" :: ("Var" :: ("While" :: [])))))))))))))))))))))

let field =
  []

let unspec_term =
  "add" :: ("eq" :: ("leq" :: ("read" :: ("succ" :: ("write" :: [])))))

let spec_term =
  "eval" :: ("neg" :: [])

let unspec_term_decl =
  Add ("write", ([], (Arrow (tau_ident, (Arrow (tau_state, (Arrow
    (tau_value, tau_state))))))), (Add ("succ", ([], (Arrow
    (tau_value, tau_value))), (Add ("read", ([], (Arrow
    (tau_ident, (Arrow (tau_state, tau_value))))), (Add
    ("leq", ([], (Arrow (tau_value, (Arrow (tau_value,
    tau_value))))), (Add ("eq", ([], (Arrow (tau_value, (Arrow
    (tau_value, tau_value))))), (Add ("add", ([], (Arrow
    (tau_value, (Arrow (tau_value, tau_value))))),
    Empty)))))))))))

let sigma_add =
  TVUnspec ("add", (Arrow (tau_value, (Arrow (tau_value,
    tau_value)))), [])

let sigma_eq =
  TVUnspec ("eq", (Arrow (tau_value, (Arrow (tau_value,
    tau_value)))), [])

let sigma_eval =
  TVSpec ("eval", (Arrow (tau_state, (Arrow (tau_lambdaterm,
    tau_value)))), [])

let sigma_leq =
  TVUnspec ("leq", (Arrow (tau_value, (Arrow (tau_value,
    tau_value)))), [])

let sigma_neg =
  TVSpec ("neg", (Arrow (tau_value, tau_value)), [])

let sigma_read =
  TVUnspec ("read", (Arrow (tau_ident, (Arrow (tau_state,
    tau_value)))), [])

let sigma_succ =
  TVUnspec ("succ", (Arrow (tau_value, tau_value)), [])

let sigma_write =
  TVUnspec ("write", (Arrow (tau_ident, (Arrow (tau_state, (Arrow
    (tau_value, tau_state)))))), [])

let delta_eval =
  Term_func ((Pattern_var ("s", tau_state)), (Skel_return (Term_func
    ((Pattern_var ("t", tau_lambdaterm)), (Skel_branch (tau_value,
    ((Skel_letin ((Pattern_constr ("Val", [], (Pattern_var ("v",
    tau_value)))), (Skel_return (Term_var (TVLet ("t",
    tau_lambdaterm)))), (Skel_return (Term_var (TVLet ("v",
    tau_value)))))) :: ((Skel_letin ((Pattern_constr ("Var", [],
    (Pattern_var ("x", tau_ident)))), (Skel_return (Term_var (TVLet
    ("t", tau_lambdaterm)))), (Skel_apply ((Term_var sigma_read),
    ((Term_var (TVLet ("x", tau_ident))) :: ((Term_var (TVLet ("s",
    tau_state))) :: [])))))) :: ((Skel_letin ((Pattern_constr ("Plus",
    [], (Pattern_tuple ((Pattern_var ("t1",
    tau_lambdaterm)) :: ((Pattern_var ("t2",
    tau_lambdaterm)) :: []))))), (Skel_return (Term_var (TVLet ("t",
    tau_lambdaterm)))), (Skel_letin ((Pattern_var ("v1",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("t1",
    tau_lambdaterm))) :: [])))), (Skel_letin ((Pattern_var ("v2",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("t2",
    tau_lambdaterm))) :: [])))), (Skel_apply ((Term_var sigma_add),
    ((Term_var (TVLet ("v1", tau_value))) :: ((Term_var (TVLet ("v2",
    tau_value))) :: [])))))))))) :: ((Skel_letin ((Pattern_constr
    ("Equals", [], (Pattern_tuple ((Pattern_var ("t1",
    tau_lambdaterm)) :: ((Pattern_var ("t2",
    tau_lambdaterm)) :: []))))), (Skel_return (Term_var (TVLet ("t",
    tau_lambdaterm)))), (Skel_letin ((Pattern_var ("v1",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("t1",
    tau_lambdaterm))) :: [])))), (Skel_letin ((Pattern_var ("v2",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("t2",
    tau_lambdaterm))) :: [])))), (Skel_apply ((Term_var sigma_eq),
    ((Term_var (TVLet ("v1", tau_value))) :: ((Term_var (TVLet ("v2",
    tau_value))) :: [])))))))))) :: ((Skel_letin ((Pattern_constr
    ("Andl", [], (Pattern_tuple ((Pattern_var ("t1",
    tau_lambdaterm)) :: ((Pattern_var ("t2",
    tau_lambdaterm)) :: []))))), (Skel_return (Term_var (TVLet ("t",
    tau_lambdaterm)))), (Skel_letin ((Pattern_var ("v",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("t1",
    tau_lambdaterm))) :: [])))), (Skel_branch (tau_value,
    ((Skel_letin ((Pattern_constr ("Vtrue", [], (Pattern_tuple []))),
    (Skel_return (Term_var (TVLet ("v", tau_value)))), (Skel_apply
    ((Term_var sigma_eval), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("t2",
    tau_lambdaterm))) :: [])))))) :: ((Skel_letin ((Pattern_constr
    ("Vfalse", [], (Pattern_tuple []))), (Skel_return (Term_var (TVLet ("v",
    tau_value)))), (Skel_return (Term_constructor ("Vfalse", [],
    (Term_tuple [])))))) :: [])))))))) :: ((Skel_letin ((Pattern_constr
    ("Not", [], (Pattern_var ("t'", tau_lambdaterm)))), (Skel_return
    (Term_var (TVLet ("t", tau_lambdaterm)))), (Skel_letin ((Pattern_var
    ("v", tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("t'",
    tau_lambdaterm))) :: [])))), (Skel_apply ((Term_var sigma_neg),
    ((Term_var (TVLet ("v", tau_value))) :: []))))))) :: ((Skel_letin
    ((Pattern_constr ("Lam", [], (Pattern_tuple ((Pattern_var ("x",
    tau_ident)) :: ((Pattern_var ("t'",
    tau_lambdaterm)) :: []))))), (Skel_return (Term_var (TVLet ("t",
    tau_lambdaterm)))), (Skel_return (Term_constructor ("Vclos", [],
    (Term_tuple ((Term_var (TVLet ("x", tau_ident))) :: ((Term_var
    (TVLet ("t'", tau_lambdaterm))) :: ((Term_var (TVLet ("s",
    tau_state))) :: []))))))))) :: ((Skel_letin ((Pattern_constr ("Fix",
    [], (Pattern_var ("t'", tau_lambdaterm)))), (Skel_return (Term_var
    (TVLet ("t", tau_lambdaterm)))), (Skel_return (Term_constructor
    ("Vfix", [], (Term_var (TVLet ("t'",
    tau_lambdaterm)))))))) :: ((Skel_letin ((Pattern_constr ("App", [],
    (Pattern_tuple ((Pattern_var ("t1",
    tau_lambdaterm)) :: ((Pattern_var ("t2",
    tau_lambdaterm)) :: []))))), (Skel_return (Term_var (TVLet ("t",
    tau_lambdaterm)))), (Skel_letin ((Pattern_var ("v",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("t1",
    tau_lambdaterm))) :: [])))), (Skel_branch (tau_value,
    ((Skel_letin ((Pattern_constr ("Vfix", [], (Pattern_var ("f",
    tau_lambdaterm)))), (Skel_return (Term_var (TVLet ("v",
    tau_value)))), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_constructor ("App", [],
    (Term_tuple ((Term_constructor ("App", [], (Term_tuple ((Term_var (TVLet
    ("f", tau_lambdaterm))) :: ((Term_constructor ("Fix", [], (Term_var
    (TVLet ("f", tau_lambdaterm))))) :: []))))) :: ((Term_var (TVLet
    ("t2", tau_lambdaterm))) :: []))))) :: [])))))) :: ((Skel_letin
    ((Pattern_constr ("Vclos", [], (Pattern_tuple ((Pattern_var ("x",
    tau_ident)) :: ((Pattern_var ("t'",
    tau_lambdaterm)) :: ((Pattern_var ("s'",
    tau_state)) :: [])))))), (Skel_return (Term_var (TVLet ("v",
    tau_value)))), (Skel_letin ((Pattern_var ("v2", tau_value)),
    (Skel_apply ((Term_var sigma_eval), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("t2",
    tau_lambdaterm))) :: [])))), (Skel_letin ((Pattern_var ("s''",
    tau_state)), (Skel_apply ((Term_var sigma_write), ((Term_var
    (TVLet ("x", tau_ident))) :: ((Term_var (TVLet ("s'",
    tau_state))) :: ((Term_var (TVLet ("v2",
    tau_value))) :: []))))), (Skel_apply ((Term_var sigma_eval),
    ((Term_var (TVLet ("s''", tau_state))) :: ((Term_var (TVLet ("t'",
    tau_lambdaterm))) :: [])))))))))) :: [])))))))) :: ((Skel_letin
    ((Pattern_constr ("While", [], (Pattern_tuple ((Pattern_var ("tp",
    tau_lambdaterm)) :: ((Pattern_var ("tf",
    tau_lambdaterm)) :: ((Pattern_var ("tx",
    tau_lambdaterm)) :: [])))))), (Skel_return (Term_var (TVLet ("t",
    tau_lambdaterm)))), (Skel_letin ((Pattern_var ("v",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_constructor ("App", [],
    (Term_tuple ((Term_var (TVLet ("tp", tau_lambdaterm))) :: ((Term_var
    (TVLet ("tx", tau_lambdaterm))) :: []))))) :: [])))), (Skel_branch
    (tau_value, ((Skel_letin ((Pattern_constr ("Vtrue", [],
    (Pattern_tuple []))), (Skel_return (Term_var (TVLet ("v",
    tau_value)))), (Skel_letin ((Pattern_var ("vr", tau_value)),
    (Skel_apply ((Term_var sigma_eval), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_constructor ("App", [], (Term_tuple
    ((Term_var (TVLet ("tf", tau_lambdaterm))) :: ((Term_var (TVLet
    ("tx", tau_lambdaterm))) :: []))))) :: [])))), (Skel_apply
    ((Term_var sigma_eval), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_constructor ("While", [], (Term_tuple
    ((Term_var (TVLet ("tp", tau_lambdaterm))) :: ((Term_var (TVLet
    ("tf", tau_lambdaterm))) :: ((Term_constructor ("Val", [], (Term_var
    (TVLet ("vr",
    tau_value))))) :: [])))))) :: [])))))))) :: ((Skel_letin
    ((Pattern_constr ("Vfalse", [], (Pattern_tuple []))), (Skel_return
    (Term_var (TVLet ("v", tau_value)))), (Skel_apply ((Term_var
    sigma_eval), ((Term_var (TVLet ("s", tau_state))) :: ((Term_var
    (TVLet ("tx",
    tau_lambdaterm))) :: [])))))) :: [])))))))) :: ((Skel_letin
    ((Pattern_constr ("For", [], (Pattern_tuple ((Pattern_var ("x",
    tau_ident)) :: ((Pattern_var ("low",
    tau_lambdaterm)) :: ((Pattern_var ("high",
    tau_lambdaterm)) :: ((Pattern_var ("tf",
    tau_lambdaterm)) :: ((Pattern_var ("tin",
    tau_lambdaterm)) :: [])))))))), (Skel_return (Term_var (TVLet ("t",
    tau_lambdaterm)))), (Skel_letin ((Pattern_var ("vlow",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("low",
    tau_lambdaterm))) :: [])))), (Skel_letin ((Pattern_var ("vhigh",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("high",
    tau_lambdaterm))) :: [])))), (Skel_letin ((Pattern_var ("comp",
    tau_value)), (Skel_apply ((Term_var sigma_leq), ((Term_var (TVLet
    ("vlow", tau_value))) :: ((Term_var (TVLet ("vhigh",
    tau_value))) :: [])))), (Skel_branch (tau_value, ((Skel_letin
    ((Pattern_constr ("Vtrue", [], (Pattern_tuple []))), (Skel_return
    (Term_var (TVLet ("comp", tau_value)))), (Skel_letin ((Pattern_var
    ("s'", tau_state)), (Skel_apply ((Term_var sigma_write),
    ((Term_var (TVLet ("x", tau_ident))) :: ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("vlow",
    tau_value))) :: []))))), (Skel_letin ((Pattern_var ("vin",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s'", tau_state))) :: ((Term_constructor ("App", [],
    (Term_tuple ((Term_var (TVLet ("tf", tau_lambdaterm))) :: ((Term_var
    (TVLet ("tin", tau_lambdaterm))) :: []))))) :: [])))), (Skel_letin
    ((Pattern_var ("vsucc", tau_value)), (Skel_apply ((Term_var
    sigma_succ), ((Term_var (TVLet ("vlow", tau_value))) :: []))),
    (Skel_apply ((Term_var sigma_eval), ((Term_var (TVLet ("s'",
    tau_state))) :: ((Term_constructor ("For", [], (Term_tuple
    ((Term_var (TVLet ("x", tau_ident))) :: ((Term_constructor ("Val",
    [], (Term_var (TVLet ("vsucc", tau_value))))) :: ((Term_constructor
    ("Val", [], (Term_var (TVLet ("vhigh", tau_value))))) :: ((Term_var
    (TVLet ("tf", tau_lambdaterm))) :: ((Term_constructor ("Val", [],
    (Term_var (TVLet ("vin",
    tau_value))))) :: [])))))))) :: [])))))))))))) :: ((Skel_letin
    ((Pattern_constr ("Vtrue", [], (Pattern_tuple []))), (Skel_return
    (Term_var (TVLet ("comp", tau_value)))), (Skel_apply ((Term_var
    sigma_eval), ((Term_var (TVLet ("s", tau_state))) :: ((Term_var
    (TVLet ("tin",
    tau_lambdaterm))) :: [])))))) :: [])))))))))))) :: ((Skel_letin
    ((Pattern_constr ("Let", [], (Pattern_tuple ((Pattern_var ("x",
    tau_ident)) :: ((Pattern_var ("t1",
    tau_lambdaterm)) :: ((Pattern_var ("t2",
    tau_lambdaterm)) :: [])))))), (Skel_return (Term_var (TVLet ("t",
    tau_lambdaterm)))), (Skel_letin ((Pattern_var ("v1",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("t1",
    tau_lambdaterm))) :: [])))), (Skel_letin ((Pattern_var ("s'",
    tau_state)), (Skel_apply ((Term_var sigma_write), ((Term_var
    (TVLet ("x", tau_ident))) :: ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("v1",
    tau_value))) :: []))))), (Skel_apply ((Term_var sigma_eval),
    ((Term_var (TVLet ("s'", tau_state))) :: ((Term_var (TVLet ("t2",
    tau_lambdaterm))) :: [])))))))))) :: ((Skel_letin ((Pattern_constr
    ("If", [], (Pattern_tuple ((Pattern_var ("t1",
    tau_lambdaterm)) :: ((Pattern_var ("t2",
    tau_lambdaterm)) :: ((Pattern_var ("t3",
    tau_lambdaterm)) :: [])))))), (Skel_return (Term_var (TVLet ("t",
    tau_lambdaterm)))), (Skel_letin ((Pattern_var ("v",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("t1",
    tau_lambdaterm))) :: [])))), (Skel_branch (tau_value,
    ((Skel_letin ((Pattern_constr ("Vtrue", [], (Pattern_tuple []))),
    (Skel_return (Term_var (TVLet ("v", tau_value)))), (Skel_apply
    ((Term_var sigma_eval), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("t2",
    tau_lambdaterm))) :: [])))))) :: ((Skel_letin ((Pattern_constr
    ("Vfalse", [], (Pattern_tuple []))), (Skel_return (Term_var (TVLet ("v",
    tau_value)))), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("t3",
    tau_lambdaterm))) :: [])))))) :: [])))))))) :: ((Skel_letin
    ((Pattern_constr ("Pair", [], (Pattern_tuple ((Pattern_var ("t1",
    tau_lambdaterm)) :: ((Pattern_var ("t2",
    tau_lambdaterm)) :: []))))), (Skel_return (Term_var (TVLet ("t",
    tau_lambdaterm)))), (Skel_letin ((Pattern_var ("v1",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("t1",
    tau_lambdaterm))) :: [])))), (Skel_letin ((Pattern_var ("v2",
    tau_value)), (Skel_apply ((Term_var sigma_eval), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("t2",
    tau_lambdaterm))) :: [])))), (Skel_return (Term_constructor
    ("Vpair", [], (Term_tuple ((Term_var (TVLet ("v1",
    tau_value))) :: ((Term_var (TVLet ("v2",
    tau_value))) :: [])))))))))))) :: ((Skel_letin ((Pattern_constr
    ("Right", [], (Pattern_var ("t'", tau_lambdaterm)))), (Skel_return
    (Term_var (TVLet ("t", tau_lambdaterm)))), (Skel_letin
    ((Pattern_constr ("Vpair", [], (Pattern_tuple ((Pattern_wildcard
    tau_value) :: ((Pattern_var ("v", tau_value)) :: []))))),
    (Skel_apply ((Term_var sigma_eval), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("t'",
    tau_lambdaterm))) :: [])))), (Skel_return (Term_var (TVLet ("v",
    tau_value)))))))) :: ((Skel_letin ((Pattern_constr ("Left", [],
    (Pattern_var ("t'", tau_lambdaterm)))), (Skel_return (Term_var
    (TVLet ("t", tau_lambdaterm)))), (Skel_letin ((Pattern_constr
    ("Vpair", [], (Pattern_tuple ((Pattern_var ("v",
    tau_value)) :: ((Pattern_wildcard tau_value) :: []))))),
    (Skel_apply ((Term_var sigma_eval), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("t'",
    tau_lambdaterm))) :: [])))), (Skel_return (Term_var (TVLet ("v",
    tau_value)))))))) :: []))))))))))))))))))))))

let delta_neg =
  Term_func ((Pattern_var ("v", tau_value)), (Skel_branch
    (tau_value, ((Skel_letin ((Pattern_constr ("Vtrue", [],
    (Pattern_tuple []))), (Skel_return (Term_var (TVLet ("v",
    tau_value)))), (Skel_return (Term_constructor ("Vfalse", [],
    (Term_tuple [])))))) :: ((Skel_letin ((Pattern_constr ("Vfalse", [],
    (Pattern_tuple []))), (Skel_return (Term_var (TVLet ("v",
    tau_value)))), (Skel_return (Term_constructor ("Vtrue", [],
    (Term_tuple [])))))) :: [])))))

let spec_term_decl =
  Add ("neg", (([], (Arrow (tau_value, tau_value))), delta_neg),
    (Add ("eval", (([], (Arrow (tau_state, (Arrow (tau_lambdaterm,
    tau_value))))), delta_eval), Empty)))

let ctype =
  Add ("While", (([], (Prod
    (tau_lambdaterm :: (tau_lambdaterm :: (tau_lambdaterm :: []))))),
    "lambdaterm"), (Add ("Var", (([], tau_ident), "lambdaterm"), (Add
    ("Val", (([], tau_value), "lambdaterm"), (Add ("Right", (([],
    tau_lambdaterm), "lambdaterm"), (Add ("Plus", (([], (Prod
    (tau_lambdaterm :: (tau_lambdaterm :: [])))), "lambdaterm"),
    (Add ("Pair", (([], (Prod
    (tau_lambdaterm :: (tau_lambdaterm :: [])))), "lambdaterm"),
    (Add ("Not", (([], tau_lambdaterm), "lambdaterm"), (Add ("Let",
    (([], (Prod
    (tau_ident :: (tau_lambdaterm :: (tau_lambdaterm :: []))))),
    "lambdaterm"), (Add ("Left", (([], tau_lambdaterm), "lambdaterm"),
    (Add ("Lam", (([], (Prod
    (tau_ident :: (tau_lambdaterm :: [])))), "lambdaterm"), (Add
    ("If", (([], (Prod
    (tau_lambdaterm :: (tau_lambdaterm :: (tau_lambdaterm :: []))))),
    "lambdaterm"), (Add ("For", (([], (Prod
    (tau_ident :: (tau_lambdaterm :: (tau_lambdaterm :: (tau_lambdaterm :: (tau_lambdaterm :: []))))))),
    "lambdaterm"), (Add ("Fix", (([], tau_lambdaterm), "lambdaterm"),
    (Add ("Equals", (([], (Prod
    (tau_lambdaterm :: (tau_lambdaterm :: [])))), "lambdaterm"),
    (Add ("App", (([], (Prod
    (tau_lambdaterm :: (tau_lambdaterm :: [])))), "lambdaterm"),
    (Add ("Andl", (([], (Prod
    (tau_lambdaterm :: (tau_lambdaterm :: [])))), "lambdaterm"),
    (Add ("Vtrue", (([], (Prod [])), "value"), (Add ("Vpair", (([], (Prod
    (tau_value :: (tau_value :: [])))), "value"), (Add ("Vother",
    (([], tau_othervalue), "value"), (Add ("Vfix", (([],
    tau_lambdaterm), "value"), (Add ("Vfalse", (([], (Prod [])),
    "value"), (Add ("Vclos", (([], (Prod
    (tau_ident :: (tau_lambdaterm :: (tau_state :: []))))),
    "value"), Empty)))))))))))))))))))))))))))))))))))))))))))

let ftype =
  Empty

let cbvrecSkelSem =
  { s_base_type = base_type; s_constructor = constructor; s_field = field;
    s_unspec_term = unspec_term; s_spec_term = spec_term; s_ctype = ctype;
    s_ftype = ftype; s_unspec_term_decl = unspec_term_decl;
    s_spec_term_decl = spec_term_decl }
