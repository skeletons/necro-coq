open Am

let base_type =
  "expr" :: ("ident" :: ("lit" :: ("state" :: ("stmt" :: ("value" :: ("vbool" :: ("vint" :: [])))))))

let tau_expr =
  Base ("expr", [])

let tau_ident =
  Base ("ident", [])

let tau_lit =
  Base ("lit", [])

let tau_state =
  Base ("state", [])

let tau_stmt =
  Base ("stmt", [])

let tau_value =
  Base ("value", [])

let tau_vbool =
  Base ("vbool", [])

let tau_vint =
  Base ("vint", [])

let constructor =
  "Assign" :: ("If" :: ("Seq" :: ("Skip" :: ("While" :: ("Const" :: ("Equal" :: ("Not" :: ("Plus" :: ("Var" :: [])))))))))

let field =
  []

let unspec_term =
  "add" :: ("eq" :: ("isBool" :: ("isFalse" :: ("isInt" :: ("isTrue" :: ("litToVal" :: ("neg" :: ("read" :: ("write" :: [])))))))))

let spec_term =
  "eval_expr" :: ("eval_stmt" :: [])

let unspec_term_decl =
  Add ("write", ([], (Arrow (tau_ident, (Arrow (tau_state, (Arrow
    (tau_value, tau_state))))))), (Add ("read", ([], (Arrow
    (tau_ident, (Arrow (tau_state, tau_value))))), (Add
    ("neg", ([], (Arrow (tau_vbool, tau_value))), (Add ("litToVal",
    ([], (Arrow (tau_lit, tau_value))), (Add ("isTrue", ([], (Arrow
    (tau_vbool, (Prod [])))), (Add ("isInt", ([], (Arrow
    (tau_value, tau_vint))), (Add ("isFalse", ([], (Arrow
    (tau_vbool, (Prod [])))), (Add ("isBool", ([], (Arrow
    (tau_value, tau_vbool))), (Add ("eq", ([], (Arrow
    (tau_vint, (Arrow (tau_vint, tau_value))))), (Add ("add",
    ([], (Arrow (tau_vint, (Arrow (tau_vint, tau_value))))),
    Empty)))))))))))))))))))

let sigma_add =
  TVUnspec ("add", (Arrow (tau_vint, (Arrow (tau_vint,
    tau_value)))), [])

let sigma_eq =
  TVUnspec ("eq", (Arrow (tau_vint, (Arrow (tau_vint,
    tau_value)))), [])

let sigma_eval_expr =
  TVSpec ("eval_expr", (Arrow (tau_state, (Arrow (tau_expr,
    tau_value)))), [])

let sigma_eval_stmt =
  TVSpec ("eval_stmt", (Arrow (tau_state, (Arrow (tau_stmt,
    tau_state)))), [])

let sigma_isBool =
  TVUnspec ("isBool", (Arrow (tau_value, tau_vbool)), [])

let sigma_isFalse =
  TVUnspec ("isFalse", (Arrow (tau_vbool, (Prod []))), [])

let sigma_isInt =
  TVUnspec ("isInt", (Arrow (tau_value, tau_vint)), [])

let sigma_isTrue =
  TVUnspec ("isTrue", (Arrow (tau_vbool, (Prod []))), [])

let sigma_litToVal =
  TVUnspec ("litToVal", (Arrow (tau_lit, tau_value)), [])

let sigma_neg =
  TVUnspec ("neg", (Arrow (tau_vbool, tau_value)), [])

let sigma_read =
  TVUnspec ("read", (Arrow (tau_ident, (Arrow (tau_state,
    tau_value)))), [])

let sigma_write =
  TVUnspec ("write", (Arrow (tau_ident, (Arrow (tau_state, (Arrow
    (tau_value, tau_state)))))), [])

let delta_eval_expr =
  Term_func ((Pattern_var ("s", tau_state)), (Skel_return (Term_func
    ((Pattern_var ("e", tau_expr)), (Skel_branch (tau_value,
    ((Skel_letin ((Pattern_constr ("Const", [], (Pattern_var ("i",
    tau_lit)))), (Skel_return (Term_var (TVLet ("e", tau_expr)))),
    (Skel_apply ((Term_var sigma_litToVal), ((Term_var (TVLet ("i",
    tau_lit))) :: []))))) :: ((Skel_letin ((Pattern_constr ("Var", [],
    (Pattern_var ("x", tau_ident)))), (Skel_return (Term_var (TVLet
    ("e", tau_expr)))), (Skel_apply ((Term_var sigma_read),
    ((Term_var (TVLet ("x", tau_ident))) :: ((Term_var (TVLet ("s",
    tau_state))) :: [])))))) :: ((Skel_letin ((Pattern_constr ("Plus",
    [], (Pattern_tuple ((Pattern_var ("t1", tau_expr)) :: ((Pattern_var
    ("t2", tau_expr)) :: []))))), (Skel_return (Term_var (TVLet ("e",
    tau_expr)))), (Skel_letin ((Pattern_var ("f1", tau_value)),
    (Skel_apply ((Term_var sigma_eval_expr), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("t1", tau_expr))) :: [])))),
    (Skel_letin ((Pattern_var ("i1", tau_vint)), (Skel_apply ((Term_var
    sigma_isInt), ((Term_var (TVLet ("f1", tau_value))) :: []))),
    (Skel_letin ((Pattern_var ("f2", tau_value)), (Skel_apply ((Term_var
    sigma_eval_expr), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("t2", tau_expr))) :: [])))),
    (Skel_letin ((Pattern_var ("i2", tau_vint)), (Skel_apply ((Term_var
    sigma_isInt), ((Term_var (TVLet ("f2", tau_value))) :: []))),
    (Skel_apply ((Term_var sigma_add), ((Term_var (TVLet ("i1",
    tau_vint))) :: ((Term_var (TVLet ("i2",
    tau_vint))) :: [])))))))))))))) :: ((Skel_letin ((Pattern_constr
    ("Equal", [], (Pattern_tuple ((Pattern_var ("t1",
    tau_expr)) :: ((Pattern_var ("t2", tau_expr)) :: []))))),
    (Skel_return (Term_var (TVLet ("e", tau_expr)))), (Skel_letin
    ((Pattern_var ("f1", tau_value)), (Skel_apply ((Term_var
    sigma_eval_expr), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("t1", tau_expr))) :: [])))),
    (Skel_letin ((Pattern_var ("i1", tau_vint)), (Skel_apply ((Term_var
    sigma_isInt), ((Term_var (TVLet ("f1", tau_value))) :: []))),
    (Skel_letin ((Pattern_var ("f2", tau_value)), (Skel_apply ((Term_var
    sigma_eval_expr), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("t2", tau_expr))) :: [])))),
    (Skel_letin ((Pattern_var ("i2", tau_vint)), (Skel_apply ((Term_var
    sigma_isInt), ((Term_var (TVLet ("f2", tau_value))) :: []))),
    (Skel_apply ((Term_var sigma_eq), ((Term_var (TVLet ("i1",
    tau_vint))) :: ((Term_var (TVLet ("i2",
    tau_vint))) :: [])))))))))))))) :: ((Skel_letin ((Pattern_constr
    ("Not", [], (Pattern_var ("e", tau_expr)))), (Skel_return (Term_var
    (TVLet ("e", tau_expr)))), (Skel_letin ((Pattern_var ("f1",
    tau_value)), (Skel_apply ((Term_var sigma_eval_expr), ((Term_var
    (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("e",
    tau_expr))) :: [])))), (Skel_letin ((Pattern_var ("b",
    tau_vbool)), (Skel_apply ((Term_var sigma_isBool), ((Term_var
    (TVLet ("f1", tau_value))) :: []))), (Skel_apply ((Term_var
    sigma_neg), ((Term_var (TVLet ("b",
    tau_vbool))) :: []))))))))) :: [])))))))))))

let delta_eval_stmt =
  Term_func ((Pattern_var ("s", tau_state)), (Skel_return (Term_func
    ((Pattern_var ("t", tau_stmt)), (Skel_branch (tau_state,
    ((Skel_letin ((Pattern_constr ("Skip", [], (Pattern_tuple []))),
    (Skel_return (Term_var (TVLet ("t", tau_stmt)))), (Skel_return
    (Term_var (TVLet ("s", tau_state)))))) :: ((Skel_letin
    ((Pattern_constr ("Assign", [], (Pattern_tuple ((Pattern_var ("x",
    tau_ident)) :: ((Pattern_var ("e", tau_expr)) :: []))))),
    (Skel_return (Term_var (TVLet ("t", tau_stmt)))), (Skel_letin
    ((Pattern_var ("v", tau_value)), (Skel_apply ((Term_var
    sigma_eval_expr), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("e", tau_expr))) :: [])))),
    (Skel_apply ((Term_var sigma_write), ((Term_var (TVLet ("x",
    tau_ident))) :: ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("v",
    tau_value))) :: []))))))))) :: ((Skel_letin ((Pattern_constr ("Seq",
    [], (Pattern_tuple ((Pattern_var ("t1", tau_stmt)) :: ((Pattern_var
    ("t2", tau_stmt)) :: []))))), (Skel_return (Term_var (TVLet ("t",
    tau_stmt)))), (Skel_letin ((Pattern_var ("s'", tau_state)),
    (Skel_apply ((Term_var sigma_eval_stmt), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("t1", tau_stmt))) :: [])))),
    (Skel_apply ((Term_var sigma_eval_stmt), ((Term_var (TVLet ("s'",
    tau_state))) :: ((Term_var (TVLet ("t2",
    tau_stmt))) :: [])))))))) :: ((Skel_letin ((Pattern_constr ("If",
    [], (Pattern_tuple ((Pattern_var ("cond",
    tau_expr)) :: ((Pattern_var ("true",
    tau_stmt)) :: ((Pattern_var ("false", tau_stmt)) :: [])))))),
    (Skel_return (Term_var (TVLet ("t", tau_stmt)))), (Skel_letin
    ((Pattern_var ("f1", tau_value)), (Skel_apply ((Term_var
    sigma_eval_expr), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("cond",
    tau_expr))) :: [])))), (Skel_letin ((Pattern_var ("b",
    tau_vbool)), (Skel_apply ((Term_var sigma_isBool), ((Term_var
    (TVLet ("f1", tau_value))) :: []))), (Skel_branch (tau_state,
    ((Skel_letin ((Pattern_wildcard (Prod [])), (Skel_apply ((Term_var
    sigma_isTrue), ((Term_var (TVLet ("b", tau_vbool))) :: []))),
    (Skel_apply ((Term_var sigma_eval_stmt), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("true",
    tau_stmt))) :: [])))))) :: ((Skel_letin ((Pattern_wildcard (Prod
    [])), (Skel_apply ((Term_var sigma_isFalse), ((Term_var (TVLet ("b",
    tau_vbool))) :: []))), (Skel_apply ((Term_var sigma_eval_stmt),
    ((Term_var (TVLet ("s", tau_state))) :: ((Term_var (TVLet ("false",
    tau_stmt))) :: [])))))) :: [])))))))))) :: ((Skel_letin
    ((Pattern_constr ("While", [], (Pattern_tuple ((Pattern_var ("cond",
    tau_expr)) :: ((Pattern_var ("t'", tau_stmt)) :: []))))),
    (Skel_return (Term_var (TVLet ("t", tau_stmt)))), (Skel_letin
    ((Pattern_var ("f1", tau_value)), (Skel_apply ((Term_var
    sigma_eval_expr), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("cond",
    tau_expr))) :: [])))), (Skel_letin ((Pattern_var ("b",
    tau_vbool)), (Skel_apply ((Term_var sigma_isBool), ((Term_var
    (TVLet ("f1", tau_value))) :: []))), (Skel_branch (tau_state,
    ((Skel_letin ((Pattern_wildcard (Prod [])), (Skel_apply ((Term_var
    sigma_isTrue), ((Term_var (TVLet ("b", tau_vbool))) :: []))),
    (Skel_letin ((Pattern_var ("s'", tau_state)), (Skel_apply ((Term_var
    sigma_eval_stmt), ((Term_var (TVLet ("s",
    tau_state))) :: ((Term_var (TVLet ("t'", tau_stmt))) :: [])))),
    (Skel_apply ((Term_var sigma_eval_stmt), ((Term_var (TVLet ("s'",
    tau_state))) :: ((Term_var (TVLet ("t",
    tau_stmt))) :: [])))))))) :: ((Skel_letin ((Pattern_wildcard (Prod
    [])), (Skel_apply ((Term_var sigma_isFalse), ((Term_var (TVLet ("b",
    tau_vbool))) :: []))), (Skel_return (Term_var (TVLet ("s",
    tau_state)))))) :: [])))))))))) :: [])))))))))))

let spec_term_decl =
  Add ("eval_stmt", (([], (Arrow (tau_state, (Arrow (tau_stmt,
    tau_state))))), delta_eval_stmt), (Add ("eval_expr", (([], (Arrow
    (tau_state, (Arrow (tau_expr, tau_value))))),
    delta_eval_expr), Empty)))

let ctype =
  Add ("Var", (([], tau_ident), "expr"), (Add ("Plus", (([], (Prod
    (tau_expr :: (tau_expr :: [])))), "expr"), (Add ("Not", (([],
    tau_expr), "expr"), (Add ("Equal", (([], (Prod
    (tau_expr :: (tau_expr :: [])))), "expr"), (Add ("Const", (([],
    tau_lit), "expr"), (Add ("While", (([], (Prod
    (tau_expr :: (tau_stmt :: [])))), "stmt"), (Add ("Skip", (([],
    (Prod [])), "stmt"), (Add ("Seq", (([], (Prod
    (tau_stmt :: (tau_stmt :: [])))), "stmt"), (Add ("If", (([],
    (Prod (tau_expr :: (tau_stmt :: (tau_stmt :: []))))),
    "stmt"), (Add ("Assign", (([], (Prod
    (tau_ident :: (tau_expr :: [])))), "stmt"),
    Empty)))))))))))))))))))

let ftype =
  Empty

let whileSkelSem =
  { s_base_type = base_type; s_constructor = constructor; s_field = field;
    s_unspec_term = unspec_term; s_spec_term = spec_term; s_ctype = ctype;
    s_ftype = ftype; s_unspec_term_decl = unspec_term_decl;
    s_spec_term_decl = spec_term_decl }
