open Am
open Lambda_skelsem

module LambdaSkelSemImplem (* : SkelSemImplemSig *) = struct
  let ss = lambdaSkelSem

  type basevalue =
    | Vident of string
    | Venv of (string * cvalue) list
  and cvalue =
    | Cval_base of basevalue
    | Cval_constructor of string * cvalue
    | Cval_tuple of cvalue list
    | Cval_closure of pattern * skeleton * (string * cvalue) list
    | Cval_record of (string * cvalue) list
    | Cval_unspec of int * string * necrotype list * cvalue list

  type unspec_function_am = necrotype list -> cvalue list -> cvalue list

  let function_getenv = function
    | a::[] -> begin
        match a with
        | Cval_tuple [Cval_base (Vident x); Cval_base (Venv st)] -> begin
            match List.assoc_opt x st with
            | Some v -> [v]
            | None -> [] end
        | _ -> [] end
    | _ -> []
  
  let function_extenv = function
    | a::[] -> begin
        match a with
        | Cval_tuple [Cval_base (Venv st); Cval_base (Vident x); v] ->
            [Cval_base (Venv ((x,v)::st))]
        | _ -> [] end
    | _ -> []
 
  let rec dict_of_list (lst : (string * 'a) list) : 'a dict = match lst with
    | [] -> Empty
    | (s, x) :: lst2 -> Add (s, x, (dict_of_list lst2))
  
  let u_am : (int * unspec_function_am) dict =
    dict_of_list (
      ("getEnv", (1, fun _ -> function_getenv))::
      ("extEnv", (1, fun _ -> function_extenv))::
      [])

end

(*============================================================================*)
(*============================================================================*)
(*============================================================================*)
(*============================================================================*)

module LambdaAM = AM(LambdaSkelSemImplem)
open LambdaAM

let xlam x t = Cval_constructor ("Lam", Cval_tuple [Cval_base (Vident x); t])
let xapp t1 t2 = Cval_constructor ("App", Cval_tuple [t1; t2])
let xvar x = Cval_constructor ("Var", Cval_base (Vident x))

let emptyenv = Cval_base (Venv [])

(*============================================================================*)
(*============================================================================*)

let rec lterm_to_string l = match l with
  | Cval_constructor ("Lam", Cval_tuple [Cval_base (Vident x); t]) ->
      Printf.sprintf "λ%s.%s" x (lterm_to_string t)
  | Cval_constructor ("App", Cval_tuple [t1; t2]) ->
      Printf.sprintf "(%s) (%s)" (lterm_to_string t1) (lterm_to_string t2)
  | Cval_constructor ("Var", Cval_base (Vident x)) -> x
  | _ -> failwith "shouldn't happen..."

let printrescval (c : cvalue) = match c with
  | Cval_constructor ("Clos", Cval_tuple [Cval_base (Vident x); l; e]) ->
      Printf.printf "getting closure λ%s.%s in some environment\n" x (lterm_to_string l)
  | _ -> Printf.printf "getting result but not a closure!\n"

let printres ropt = match ropt with
  | Some c -> printrescval c
  | None -> Printf.printf "no result! might need to increase fuel!\n"

let _ =
  let val_eval : term = Term_var (sigma_eval) in
  (* (λx. x x) (λy. y) *)
  let lambdaterm = xapp (xlam "x" (xapp (xvar "x") (xvar "x")))
                        (xlam "y" (xvar "y")) in
  let vars : term = Term_var (TVLet ("s", tau_env)) in
  let vart : term = Term_var (TVLet ("t", tau_lterm)) in
  let sk = Skel_apply (val_eval, [vars ; vart]) in
  let amstate = inject_skel sk  [("s", emptyenv); ("t", lambdaterm)] in

  let ropt = evalfuel 614 amstate in
  printres ropt
