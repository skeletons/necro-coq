open Am
open Cbvrec_skelsem

module CbvrecSkelSemImplem (* : SkelSemImplemSig *) = struct
  let ss = cbvrecSkelSem

  type basevalue =
    | Vint of int
    | Vstate of (string * cvalue) list
    | Vident of string
  and cvalue =
    | Cval_base of basevalue
    | Cval_constructor of string * cvalue
    | Cval_tuple of cvalue list
    | Cval_closure of pattern * skeleton * (string * cvalue) list
    | Cval_record of (string * cvalue) list
    | Cval_unspec of int * string * necrotype list * cvalue list

  type unspec_function_am = necrotype list -> cvalue list -> cvalue list

  let function_add = function
    | a::b::[] -> begin
        match a, b with
        | Cval_constructor ("Vother", Cval_base (Vint va)),
          Cval_constructor ("Vother", Cval_base (Vint vb)) ->
            [Cval_constructor ("Vother", Cval_base (Vint (va+vb)))]
        | _,_ -> [] end
    | _ -> []
  
  let function_eq = function
    | a::b::[] -> begin
        match a, b with
        | Cval_constructor ("Vother", Cval_base (Vint va)),
          Cval_constructor ("Vother", Cval_base (Vint vb)) ->
            if va=vb then [Cval_constructor ("Vtrue",  Cval_tuple [])]
                     else [Cval_constructor ("Vfalse", Cval_tuple [])]
        | _,_ -> [] end
    | _ -> []
  
  let function_read = function
    | a::b::[] -> begin
        match a,b with
        | Cval_base (Vident x), Cval_base (Vstate st) -> begin
            match List.assoc_opt x st with
            | Some v -> [v]
            | None -> [] end
        | _ -> [] end
    | _ -> []
  
  let function_write = function
    | a::b::c::[] -> begin
        match a,b,c with
        | Cval_base (Vident x), Cval_base (Vstate st), v ->
            [Cval_base (Vstate ((x,v)::st))]
        | _ -> [] end
    | _ -> []
  
  let function_leq = function
    | a::b::[] -> begin
        match a, b with
        | Cval_constructor ("Vother", Cval_base (Vint va)),
          Cval_constructor ("Vother", Cval_base (Vint vb)) ->
            if va<=vb then [Cval_constructor ("Vtrue",  Cval_tuple [])]
                      else [Cval_constructor ("Vfalse", Cval_tuple [])]
        | _,_ -> [] end
    | _ -> []
  
  let function_succ = function
    | a::[] -> begin
        match a with
        | Cval_constructor ("Vother", Cval_base (Vint va)) ->
            [Cval_constructor ("Vother", Cval_base (Vint (va+1)))]
        | _ -> [] end
    | _ -> []
  
  let rec dict_of_list (lst : (string * 'a) list) : 'a dict = match lst with
    | [] -> Empty
    | (s, x) :: lst2 -> Add (s, x, (dict_of_list lst2))
  
  let u_am : (int * unspec_function_am) dict =
    dict_of_list (
      ("add",   (2, fun _ -> function_add))::
      ("eq",    (2, fun _ -> function_eq))::
      ("read",  (2, fun _ -> function_read))::
      ("write", (3, fun _ -> function_write))::
      ("leq",   (2, fun _ -> function_leq))::
      ("succ",  (1, fun _ -> function_succ))::
      [])

end

(*============================================================================*)
(*============================================================================*)
(*============================================================================*)
(*============================================================================*)

module CbvrecAM = AM(CbvrecSkelSemImplem)
open CbvrecAM

let mkcv (c : string) (l : cvalue list) : cvalue =
  match l with
  | a::[] -> Cval_constructor (c, a)
  | _ -> Cval_constructor (c, Cval_tuple l)

let xvpair v1 v2 = mkcv "Vpair" [v1; v2]
let xvtrue = mkcv "Vtrue" []
let xvfalse = mkcv "Vfalse" []
let xvclos x t v = mkcv "Vclos" [Cval_base (Vident x); t; v]
let xvfix t = mkcv "Vfix" [t]
let xvint i = mkcv "Vother" [Cval_base (Vint i)]

let xval v = mkcv "Val" [v]
let xint i = xval (xvint i)
let xvar x = mkcv "Var" [Cval_base (Vident x)]
let xplus t1 t2 = mkcv "Plus" [t1; t2]
let xequals t1 t2 = mkcv "Equals" [t1; t2]
let xandl t1 t2 = mkcv "Andl" [t1; t2]
let xnot t = mkcv "Not" [t]
let xlam x t = mkcv "Lam" [Cval_base (Vident x); t]
let xfix t = mkcv "Fix" [t]
let xapp t1 t2 = mkcv "App" [t1; t2]
let xwhile t1 t2 t3 = mkcv "While" [t1; t2; t3]
let xfor x t1 t2 t3 t4 = mkcv "For" [Cval_base (Vident x); t1; t2; t3; t4]
let xlet x t1 t2 = mkcv "Let" [Cval_base (Vident x); t1; t2]
let xif t1 t2 t3 = mkcv "If" [t1; t2; t3]
let xpair t1 t2 = mkcv "Pair" [t1; t2]
let xright t = mkcv "Right" [t]
let xleft t = mkcv "Left" [t]

let emptyenv = Cval_base (Vstate [])

(*============================================================================*)
(*============================================================================*)

let rec printresbase (bv : basevalue) = match bv with
  | Vstate st -> Printf.printf "getting a state\n";
      printrescval (List.assoc "res" st)
  | Vint i -> Printf.printf "getting int: %d\n" i
  | Vident x -> Printf.printf "getting ident: %s\n" x
and printrescval (c : cvalue) = match c with
  | Cval_constructor ("Vother", Cval_base r) -> printresbase r
  | _ -> Printf.printf "getting result but not a base value!\n"
let printres ropt = match ropt with
  | Some c -> printrescval c
  | None -> Printf.printf "no result! might need to increase fuel!\n"

let _ =
  let val_eval : term = Term_var (sigma_eval) in
  let pred = xlam "n" (xplus (xvar "n") (xint (-1))) in
  let ifzero = xlam "n" (xlam "ft" (xlam "ff" (
               xif (xequals (xvar "n") (xint 0))
                   (xvar "ft") (xvar "ff")))) in
  (* be careful to give closures to ifzero, or both branches will execute *)
  let appifzero n ft ff x = xapp (xapp (xapp (xapp ifzero n) ft) ff) x in
  let triangleaux = xlam "f" (xlam "n"(
                 appifzero (xvar "n")
                   (xlam "_" (xint 0))
                   (xlam "_" (xplus (xvar "n") (xapp (xvar "f") (xapp pred (xvar "n")))))
                   (xint 42))) in
  let triangle = xfix triangleaux in

  let fiftyfive = (xapp triangle (xint 10)) in
  let vars : term = Term_var (TVLet ("s", tau_state)) in
  let vart : term = Term_var (TVLet ("t", tau_lambdaterm)) in
  let sk = Skel_apply (val_eval, [vars ; vart]) in
  let amstate = inject_skel sk  [("s", emptyenv); ("t", fiftyfive)] in

  let ropt = evalfuel 38918 amstate in
  printres ropt
