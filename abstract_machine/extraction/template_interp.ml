open Am
(*** replace with your skeletal semantics file name ***)
open Lang_skelsem

(* uncommenting the signature would make the module opaque, don't! *)
module LangSkelSemImplem (* : SkelSemImplemSig *) = struct

  (*** replace with your skeletal semantics name ***)
  let ss = langSkelSem
  
  (*** define your own possible basevalues ***)
  type basevalue =
    | Vint of int
    | Vbool of bool
    | ...

  type cvalue =
    | Cval_base of basevalue
    | Cval_constructor of string * cvalue
    | Cval_tuple of cvalue list
    | Cval_closure of pattern * skeleton * (string * cvalue) list
    | Cval_record of (string * cvalue) list
    | Cval_unspec of int * string * necrotype list * cvalue list

  type unspec_function_am = necrotype list -> cvalue list -> cvalue list

  (*** example of specification for unspecified terms ***)

  let function_add = function
    | a::b::[] -> begin
        match a, b with
        | Cval_base (Vint va), Cval_base (Vint vb) -> [Cval_base (Vint (va+vb))]
        | _,_ -> [] end
    | _ -> []
  
  let function_neg = function
    | a::[] -> begin
        match a with
        | Cval_base (Vbool b) -> [Cval_base (Vbool (not b))]
        | _ -> [] end
    | _ -> []

  let rec dict_of_list (lst : (string * 'a) list) : 'a dict = match lst with
    | [] -> Empty
    | (s, x) :: lst2 -> Add (s, x, (dict_of_list lst2))
  
  (*** give your specification here ***)
  let u_am : (int * unspec_function_am) dict =
    dict_of_list (
      ("add", (2, fun _ -> function_add))::
      ("neg", (1, fun _ -> function_neg))::
      (..., ...)::
      [])

end

(*============================================================================*)
(*============================================================================*)

module LangAM = AM(LangSkelSemImplem)
open LangAM
(*** now the abstract machine is instantiated and in scope ***)

(*** creating a few aliases might be handy for readability ***)
let xint (i:int) : cvalue =
  Cval_base (Vint i)
let xplus (e1:cvalue) (e2:cvalue) : cvalue =
  Cval_constructor ("Plus", Cval_tuple [e1; e2])
let xskip : cvalue =
  Cval_constructor ("Skip", Cval_tuple [])

(*** to use the AM, you need to create starting cvalues/environment/term/skeleton and AM state ***)
(*** more examples are available in folder "example" ***)

let _ =
  let evalfun : term = Term_var (sigma_eval) in
  let vart : term = Term_var (TVLet ("t", tau_expr)) in
  let sk : skeleton = Skel_apply (evalfun, [vart]) in
  let expr : cvalue = xplus (xint 3) (xint 4) in
  let env : env = [("t", expr)] in
  let amstate : amstate = inject_skel sk env in
  let fuel : int = 10000 in
  let ropt : cvalue option = evalfuel fuel amstate in
  match ropt with
  | Some (Cval_base (Vint i)) -> Printf.printf "output: %d\n" i
  | Some _ -> Printf.printf "output is not an int"
  | None -> Printf.printf "no output! maybe not enough fuel?"
