#!/bin/bash

# if script called with wrong file extensions
usage () {
  echo "Usage: \"$0 file.sk [out.ml]\""
  echo "By default, outputs to \"file_skelsem.ml\""
  exit 1
}

# directory of the script
dir=$(dirname "$0")

# necrocoq
necoq=$(command -v necrocoq)
if [[ -z $necoq ]]; then necoq="$dir""/../necrocoq"; fi

# if necrocoq doesn't exist
if [ ! -f "$necoq" ]; then
  echo "Can't find necrocoq!"
  exit 1
fi
# if coq doesn't exist
if [[ -z $(command -v coqc) ]]; then
  echo "Can't find coqc!"
  exit 1
fi

# if script is moved or necessary files deleted
if [ ! -f "$dir""/Skeleton.v" ] || [ ! -f "$dir""/Dict.v" ] || [ ! -f "$dir""/Extract.v" ]; then
  echo "This script requires auxiliary files \"Dict.v\", \"Skeleton.v\", and \"Extract.v\""
  exit 1
fi

# input file name
file=$1
# input should have extension ".sk"
if [[ "${file: -3}" != ".sk" ]]; then usage; fi
# input should exist
if [ ! -f "$file" ]; then
  echo "Input file \"$file\" does not exist..."
  exit 1
fi

# output file name
if [[ "$2" != "" ]]; then
  outfile="$2"
  if [[ "${outfile: -3}" != ".ml" ]]; then usage; fi
else
  outfile="${file:0: -3}""_skelsem.ml"
fi
# output should not exist, or we ask confirmation
if [ -f "$outfile" ]; then
  read -r -s -n 1 -p "Output file \"$outfile\" exists. Overwrite? [Y/n]" response
  echo
  if [[ $response =~ ^[yY]$ ]] || [[ -z $response ]]; then
    rm -f "$outfile"
  else
    echo "Canceling"
    exit 0
  fi
fi

# if input file is all letter, assume it's language name
language=$(basename -s ".sk" "$file")
language="${language,,}" # lowercase
if [[ ! $language =~ ^[a-zA-Z]*$ ]]; then language="language"; fi
module="$language""SkelSem"

# Coq extraction
coqc -R "$dir" "" "$dir""/Dict.v"
coqc -R "$dir" "" "$dir""/Skeleton.v"
"$necoq" "$file" -o "$dir""/Temp.v"
coqc -R "$dir" "" "$dir""/Temp.v"
coqc -R "$dir" "" "$dir""/Extract.v"

# delete comments
sed -i '/^[(][*][*].*[*][*][)]$/ {N;d}' temp.ml # one-line comments
sed -i '/^[(][*][*]/,/[*][*][)]/ {N;d}' temp.ml # multi-lines comments

# remove types defined in am.ml (pattern/term/skeletal_semantics/...)
sed -i '1,49d' temp.ml

# unicode stuff
sed -i -e "s/_UU03c4_/tau/g" temp.ml
sed -i -e "s/_UU03c3_/sigma/g" temp.ml
sed -i -e "s/_UU03b4_/delta/g" temp.ml

# change name of skeletal semantics
sed -i "s/^let sem =/let $module =/" temp.ml

# create outfile
echo "open Am" >> "$outfile"
echo "" >> "$outfile"
cat temp.ml >> "$outfile"

# clean up
rm -f temp.*
rm -f "$dir"/Extract.{vo*,glob}
rm -f "$dir"/.Extract.aux
rm -f "$dir"/Dict.{vo*,glob}
rm -f "$dir"/.Dict.aux
rm -f "$dir"/Skeleton.{vo*,glob}
rm -f "$dir"/.Skeleton.aux
rm -f "$dir"/Temp.*
rm -f "$dir"/.Temp.*

# user feedback
echo "  Generating skeletal semantics $module"
echo "  Creating file $outfile"
