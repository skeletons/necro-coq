(** val option_map : ('a1 -> 'a2) -> 'a1 option -> 'a2 option **)

let option_map f = function
| Some a -> Some (f a)
| None -> None

(** val nth_error : 'a1 list -> int -> 'a1 option **)

let rec nth_error l n =
  (fun zero succ n -> if n=0 then zero () else succ (n-1))
    (fun _ -> match l with
              | [] -> None
              | x :: _ -> Some x)
    (fun n0 -> match l with
               | [] -> None
               | _ :: l0 -> nth_error l0 n0)
    n

type 'a dict =
| Empty
| Add of string * 'a * 'a dict

(** val find : string -> 'a1 dict -> 'a1 option **)

let rec find x = function
| Empty -> None
| Add (y, v, d0) -> if ( = ) x y then Some v else find x d0

type necrotype =
| Var of string
| Base of string * necrotype list
| Prod of necrotype list
| Arrow of necrotype * necrotype

type typed_var =
| TVLet of string * necrotype
| TVUnspec of string * necrotype * necrotype list
| TVSpec of string * necrotype * necrotype list

type pattern =
| Pattern_var of string * necrotype
| Pattern_wildcard of necrotype
| Pattern_tuple of pattern list
| Pattern_constr of string * necrotype list * pattern
| Pattern_rec of ((string * necrotype list) * pattern) list

type term =
| Term_constructor of string * necrotype list * term
| Term_var of typed_var
| Term_tuple of term list
| Term_func of pattern * skeleton
| Term_field of term * necrotype list * string
| Term_nth of term * necrotype list * int
| Term_record of term option * ((string * necrotype list) * term) list
and skeleton =
| Skel_branch of necrotype * skeleton list
| Skel_return of term
| Skel_apply of term * term list
| Skel_letin of pattern * skeleton * skeleton
| Skel_exists of pattern * necrotype * skeleton

type skeletal_semantics = { s_base_type : string list;
                            s_constructor : string list;
                            s_field : string list;
                            s_unspec_term : string list;
                            s_spec_term : string list;
                            s_ctype : ((string list * necrotype) * string) dict;
                            s_ftype : ((string list * string) * necrotype) dict;
                            s_unspec_term_decl : (string list * necrotype) dict;
                            s_spec_term_decl : ((string list * necrotype) * term)
                                               dict }

type typing_env = (string * necrotype) list

(** val findEnv : string -> (string * 'a1) list -> 'a1 option **)

let rec findEnv s = function
| [] -> None
| x :: q -> if ( = ) (fst x) s then Some (snd x) else findEnv s q

(** val subst : necrotype -> typing_env -> necrotype **)

let rec subst ty tae =
  let subst_list = fun tyl -> List.map (fun ty0 -> subst ty0 tae) tyl in
  (match ty with
   | Var x -> (match findEnv x tae with
               | Some ty0 -> ty0
               | None -> ty)
   | Base (s, tl) -> Base (s, (subst_list tl))
   | Prod xl -> Prod (subst_list xl)
   | Arrow (s1, s2) -> Arrow ((subst s1 tae), (subst s2 tae)))

(** val subst_list_type : necrotype list -> typing_env -> necrotype list **)

let subst_list_type tl tae =
  List.map (fun ty -> subst ty tae) tl

(** val subst_pattern : pattern -> typing_env -> pattern **)

let rec subst_pattern p tae =
  match p with
  | Pattern_var (x, y) -> Pattern_var (x, (subst y tae))
  | Pattern_wildcard y -> Pattern_wildcard (subst y tae)
  | Pattern_tuple pl ->
    Pattern_tuple (List.map (fun y -> subst_pattern y tae) pl)
  | Pattern_constr (c, tyl, p0) ->
    Pattern_constr (c, (subst_list_type tyl tae), (subst_pattern p0 tae))
  | Pattern_rec pl ->
    Pattern_rec
      (List.map (fun y -> let (y0, p0) = y in (y0, (subst_pattern p0 tae)))
        pl)

(** val subst_typed_var : typed_var -> typing_env -> typed_var **)

let subst_typed_var tv tae =
  match tv with
  | TVLet (x, y) -> TVLet (x, (subst y tae))
  | TVUnspec (x, y, ta) ->
    TVUnspec (x, (subst y tae), (subst_list_type ta tae))
  | TVSpec (x, y, ta) -> TVSpec (x, (subst y tae), (subst_list_type ta tae))

(** val subst_term : term -> typing_env -> term **)

let rec subst_term t tae =
  match t with
  | Term_constructor (c, tyl, t0) ->
    Term_constructor (c, (subst_list_type tyl tae), (subst_term t0 tae))
  | Term_var tv -> Term_var (subst_typed_var tv tae)
  | Term_tuple tl -> Term_tuple (List.map (fun t0 -> subst_term t0 tae) tl)
  | Term_func (p, sk) ->
    Term_func ((subst_pattern p tae), (subst_skel sk tae))
  | Term_field (t0, tyl, f) ->
    Term_field ((subst_term t0 tae), (subst_list_type tyl tae), f)
  | Term_nth (t0, tyl, f) ->
    Term_nth ((subst_term t0 tae), (subst_list_type tyl tae), f)
  | Term_record (to0, tl) ->
    Term_record ((option_map (fun t0 -> subst_term t0 tae) to0),
      (List.map (fun y ->
        let (y0, t0) = y in
        let (f, ta) = y0 in
        ((f, (subst_list_type ta tae)), (subst_term t0 tae))) tl))

(** val subst_skel : skeleton -> typing_env -> skeleton **)

and subst_skel sk tae =
  match sk with
  | Skel_branch (tau, skl) ->
    Skel_branch ((subst tau tae),
      (List.map (fun sk0 -> subst_skel sk0 tae) skl))
  | Skel_return t -> Skel_return (subst_term t tae)
  | Skel_apply (t, tl) ->
    Skel_apply ((subst_term t tae),
      (List.map (fun t0 -> subst_term t0 tae) tl))
  | Skel_letin (p, sk1, sk2) ->
    Skel_letin ((subst_pattern p tae), (subst_skel sk1 tae),
      (subst_skel sk2 tae))
  | Skel_exists (p, ty, sk0) ->
    Skel_exists ((subst_pattern p tae), (subst ty tae), (subst_skel sk0 tae))

(** val subst_term' : term -> string list -> necrotype list -> term **)

let subst_term' t ta tyl =
  let tae = List.combine ta tyl in subst_term t tae

(*============================================================================*)
(*============================================================================*)

module type SkelSemImplemSig = sig
  val ss : skeletal_semantics
  type basevalue
  type cvalue =
    | Cval_base of basevalue
    | Cval_constructor of string * cvalue
    | Cval_tuple of cvalue list
    | Cval_closure of pattern * skeleton * (string * cvalue) list
    | Cval_record of (string * cvalue) list
    | Cval_unspec of int * string * necrotype list * cvalue list
  type unspec_function_am = necrotype list -> cvalue list -> cvalue list
  val u_am : (int * unspec_function_am) dict
end

(*============================================================================*)
(*============================================================================*)

module AM (SSI : SkelSemImplemSig) = struct
  include SSI

  type env = (string * cvalue) list

  type krt =
  | Kr_id
  | Kr_constr of string * krt
  | Kr_list of term list * env * klt
  | Kr_let of pattern * skeleton * env * krt
  | Kr_app1 of term list * env * krt
  | Kr_app3 of cvalue list * krt
  | Kr_field of string * krt
  | Kr_nth of int * krt
  | Kr_up of ((string * necrotype list) * term) list * env * krt
  | Kr_ldt of ((string * necrotype list) * term) list * env * string * kdt
  | Kr_ldp of pattern * env * ((string * necrotype list) * pattern) list
     * (string * cvalue) list * ket
  and kdt =
  | Kd_id
  | Kd_rec of krt
  | Kd_up of (string * cvalue) list * krt
  | Kd_cons of string * cvalue * kdt
  and klt =
  | Kl_id
  | Kl_tuple of krt
  | Kl_list of klt * cvalue
  | Kl_app2 of cvalue * krt
  and ket =
  | Ke_id
  | Ke_let of skeleton * krt
  | Ke_pat of pattern list * cvalue list * ket
  | Ke_app of skeleton * cvalue list * krt
  | Ke_ldp of ((string * necrotype list) * pattern) list * (string * cvalue) list
     * ket

  type fkt =
  | F_empty
  | F_sk of skeleton * env * krt * fkt
  | F_list of cvalue list * krt * fkt

  type amstate =
  | KR of krt * cvalue * fkt
  | KD of kdt * (string * cvalue) list * fkt
  | KL of klt * cvalue list * fkt
  | KE of ket * env * fkt
  | B of fkt
  | V of term * env * krt * fkt
  | UR of cvalue * ((string * necrotype list) * term) list * env * krt * fkt
  | GF of cvalue * string * krt * fkt
  | GN of cvalue * int * krt * fkt
  | GS of cvalue option * krt * fkt
  | D of ((string * necrotype list) * term) list * env * kdt * fkt
  | LK of env * string * krt * fkt
  | VL of term list * env * klt * fkt
  | SK of skeleton * env * krt * fkt
  | A of cvalue list * cvalue * krt * fkt
  | L of cvalue list * krt * fkt
  | P of pattern * cvalue * env * ket * fkt
  | PD of ((string * necrotype list) * pattern) list * (string * cvalue) list * 
     env * ket * fkt
  | PL of pattern list * cvalue list * env * ket * fkt

  (** val step : amstate -> amstate option **)

  let step = function
  | KR (k0, r, fk) ->
    (match k0 with
     | Kr_id -> None
     | Kr_constr (c, k) -> Some (KR (k, (Cval_constructor (c, r)), fk))
     | Kr_list (l, e, k) -> Some (VL (l, e, (Kl_list (k, r)), fk))
     | Kr_let (p, sk, e, k) -> Some (P (p, r, e, (Ke_let (sk, k)), fk))
     | Kr_app1 (al, e, k) -> Some (VL (al, e, (Kl_app2 (r, k)), fk))
     | Kr_app3 (rl, k) -> Some (A (rl, r, k, fk))
     | Kr_field (d, k) -> Some (GF (r, d, k, fk))
     | Kr_nth (n, k) -> Some (GN (r, n, k, fk))
     | Kr_up (ldt, e, k) -> Some (UR (r, ldt, e, k, fk))
     | Kr_ldt (ldt, e, d, k) -> Some (D (ldt, e, (Kd_cons (d, r, k)), fk))
     | Kr_ldp (p, e, ldp, ldr, k) ->
       Some (P (p, r, e, (Ke_ldp (ldp, ldr, k)), fk)))
  | KD (k0, ldr, fk) ->
    (match k0 with
     | Kd_id -> None
     | Kd_rec k -> Some (KR (k, (Cval_record ldr), fk))
     | Kd_up (ldr1, k) ->
       Some (KR (k, (Cval_record (List.append ldr ldr1)), fk))
     | Kd_cons (d, r, k) -> Some (KD (k, ((d, r) :: ldr), fk)))
  | KL (k0, rl, fk) ->
    (match k0 with
     | Kl_id -> None
     | Kl_tuple k -> Some (KR (k, (Cval_tuple rl), fk))
     | Kl_list (k, r) -> Some (KL (k, (r :: rl), fk))
     | Kl_app2 (r, k) -> Some (A (rl, r, k, fk)))
  | KE (k0, e, fk) ->
    (match k0 with
     | Ke_id -> None
     | Ke_let (sk, k) -> Some (SK (sk, e, k, fk))
     | Ke_pat (l, rl, k) -> Some (PL (l, rl, e, k, fk))
     | Ke_app (sk, al, k) -> Some (SK (sk, e, (Kr_app3 (al, k)), fk))
     | Ke_ldp (ldp, ldr, k) -> Some (PD (ldp, ldr, e, k, fk)))
  | B f ->
    (match f with
     | F_empty -> None
     | F_sk (sk, e, k, fk) -> Some (SK (sk, e, k, fk))
     | F_list (rl, k, fk) -> Some (L (rl, k, fk)))
  | V (t, e, k, fk) ->
    (match t with
     | Term_constructor (c, _, v) -> Some (V (v, e, (Kr_constr (c, k)), fk))
     | Term_var t0 ->
       (match t0 with
        | TVLet (x, _) -> Some (LK (e, x, k, fk))
        | TVUnspec (x, _, tyl) ->
          (match find x u_am with
           | Some p ->
             let (n0, itp) = p in
             ((fun zero succ n -> if n=0 then zero () else succ (n-1))
                (fun _ -> Some (L ((itp tyl []), k, fk)))
                (fun n -> Some (KR (k, (Cval_unspec (n, x, tyl, [])), fk)))
                n0)
           | None -> Some (B fk))
        | TVSpec (x, _, tyl) ->
          (match find x ss.s_spec_term_decl with
           | Some p ->
             let (p0, t1) = p in
             let (ta, _) = p0 in
             if ( = ) (List.length ta) (List.length tyl)
             then Some (V ((subst_term' t1 ta tyl), [], k, fk))
             else Some (B fk)
           | None -> Some (B fk)))
     | Term_tuple vl -> Some (VL (vl, e, (Kl_tuple k), fk))
     | Term_func (p, sk) -> Some (KR (k, (Cval_closure (p, sk, e)), fk))
     | Term_field (v, _, d) -> Some (V (v, e, (Kr_field (d, k)), fk))
     | Term_nth (v, _, n) -> Some (V (v, e, (Kr_nth (n, k)), fk))
     | Term_record (o, ldt) ->
       (match o with
        | Some v -> Some (V (v, e, (Kr_up (ldt, e, k)), fk))
        | None -> Some (D (ldt, e, (Kd_rec k), fk))))
  | UR (c, ldt, e, k, fk) ->
    (match c with
     | Cval_record ldr -> Some (D (ldt, e, (Kd_up (ldr, k)), fk))
     | _ -> Some (B fk))
  | GF (c, d, k, fk) ->
    (match c with
     | Cval_record ldr -> Some (LK (ldr, d, k, fk))
     | _ -> Some (B fk))
  | GN (c, n, k, fk) ->
    (match c with
     | Cval_tuple rl -> Some (GS ((nth_error rl n), k, fk))
     | _ -> Some (B fk))
  | GS (o, k, fk) ->
    (match o with
     | Some r -> Some (KR (k, r, fk))
     | None -> Some (B fk))
  | D (l, e, k, fk) ->
    (match l with
     | [] -> Some (KD (k, [], fk))
     | p :: ldt ->
       let (p0, v) = p in
       let (d, _) = p0 in Some (V (v, e, (Kr_ldt (ldt, e, d, k)), fk)))
  | LK (e0, x, k, fk) ->
    (match e0 with
     | [] -> Some (B fk)
     | p :: e ->
       let (y, r) = p in
       if ( = ) x y then Some (KR (k, r, fk)) else Some (LK (e, x, k, fk)))
  | VL (l0, e, k, fk) ->
    (match l0 with
     | [] -> Some (KL (k, [], fk))
     | v :: l -> Some (V (v, e, (Kr_list (l, e, k)), fk)))
  | SK (s0, e, k, fk) ->
    (match s0 with
     | Skel_branch (nt, l0) ->
       (match l0 with
        | [] -> Some (B fk)
        | sk :: l ->
          Some (SK (sk, e, k, (F_sk ((Skel_branch (nt, l)), e, k, fk)))))
     | Skel_return v -> Some (V (v, e, k, fk))
     | Skel_apply (v, vl) -> Some (V (v, e, (Kr_app1 (vl, e, k)), fk))
     | Skel_letin (p, sk1, sk2) ->
       Some (SK (sk1, e, (Kr_let (p, sk2, e, k)), fk))
     | Skel_exists (_, _, _) -> Some (B fk))
  | A (l, r, k, fk) ->
    (match l with
     | [] -> Some (KR (k, r, fk))
     | r0 :: rl ->
       (match r with
        | Cval_closure (p, sk, e) ->
          Some (P (p, r0, e, (Ke_app (sk, rl, k)), fk))
        | Cval_unspec (n0, f, ta, parg) ->
          ((fun zero succ n -> if n=0 then zero () else succ (n-1))
             (fun _ ->
             match find f u_am with
             | Some p ->
               let (_, itp) = p in
               Some (L ((itp ta (List.append parg (r0 :: []))), (Kr_app3 (rl,
               k)), fk))
             | None -> Some (B fk))
             (fun n -> Some (A (rl, (Cval_unspec (n, f, ta,
             (List.append parg (r0 :: [])))), k, fk)))
             n0)
        | _ -> Some (B fk)))
  | L (l0, k, fk) ->
    (match l0 with
     | [] -> Some (B fk)
     | r :: l -> Some (KR (k, r, (F_list (l, k, fk)))))
  | P (p0, r, e, k, fk) ->
    (match p0 with
     | Pattern_var (x, _) -> Some (KE (k, ((x, r) :: e), fk))
     | Pattern_wildcard _ -> Some (KE (k, e, fk))
     | Pattern_tuple pl ->
       (match r with
        | Cval_tuple rl -> Some (PL (pl, rl, e, k, fk))
        | _ -> Some (B fk))
     | Pattern_constr (c1, _, p) ->
       (match r with
        | Cval_constructor (c2, r0) ->
          if ( = ) c1 c2 then Some (P (p, r0, e, k, fk)) else Some (B fk)
        | _ -> Some (B fk))
     | Pattern_rec ldp ->
       (match r with
        | Cval_record ldr -> Some (PD (ldp, ldr, e, k, fk))
        | _ -> Some (B fk)))
  | PD (l, ldr, e, k, fk) ->
    (match l with
     | [] -> Some (KE (k, e, fk))
     | p0 :: ldp ->
       let (p1, p) = p0 in
       let (d, _) = p1 in Some (LK (ldr, d, (Kr_ldp (p, e, ldp, ldr, k)), fk)))
  | PL (l, l0, e, k, fk) ->
    (match l with
     | [] -> (match l0 with
              | [] -> Some (KE (k, e, fk))
              | _ :: _ -> Some (B fk))
     | p :: pl ->
       (match l0 with
        | [] -> Some (B fk)
        | r :: rl -> Some (P (p, r, e, (Ke_pat (pl, rl, k)), fk))))

  (** val inject_term : term -> env -> amstate **)

  let inject_term t e =
    V (t, e, Kr_id, F_empty)

  (** val inject_skel : skeleton -> env -> amstate **)

  let inject_skel sk e =
    SK (sk, e, Kr_id, F_empty)

  (** val evalfuel : int -> amstate -> cvalue option **)

  let rec evalfuel n a = match a with
  | KR (k, r, _) ->
    (match k with
     | Kr_id -> Some r
     | _ ->
       ((fun zero succ n -> if n=0 then zero () else succ (n-1))
          (fun _ -> None)
          (fun m ->
          match step a with
          | Some b -> evalfuel m b
          | None -> None)
          n))
  | _ ->
    ((fun zero succ n -> if n=0 then zero () else succ (n-1))
       (fun _ -> None)
       (fun m ->
       match step a with
       | Some b -> evalfuel m b
       | None -> None)
       n)

end
