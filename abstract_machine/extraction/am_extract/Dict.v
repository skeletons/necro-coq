Require Import List String.


(* TODO: use a better instantiation *)

Inductive dict (A:Type) :=
| empty
| add: string -> A -> dict A -> dict A.

Fixpoint find {A} x (d:dict A): option A :=
  match d with
  | empty _ => None
  | add _ y v d =>
      if eqb x y then Some v else find x d
  end.

Fixpoint keys {A} (d: dict A): list string :=
  match d with
  | empty _ => nil
  | add _ x _ d => x :: keys d
  end.

Lemma find_empty:
  forall A x, find x (empty A) = None.
Proof.
  reflexivity.
Qed.

Lemma find_success:
  forall A k v d, find k (add A k v d) = Some v.
Proof.
  intros; simpl. rewrite eqb_refl; reflexivity.
Qed.

Lemma find_add:
  forall A k k' v d, k <> k' -> find k (add A k' v d) = find k d.
Proof.
  intros A k k' v d neq; simpl.
  apply eqb_neq in neq; rewrite neq.
  reflexivity.
Qed.

Module DictNotations.
  Notation "∅" := (empty _).
  Notation "a + { x → y }" := (add _ x y a) (left associativity, at level 20).
End DictNotations.

Arguments empty {_}.
Arguments add [_] _ _ _.
