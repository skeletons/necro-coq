Require Import String List.
Require Import Dict Skeleton WellFormed Concrete Concrete_ndam Concrete_am.
Require Import ZArith.

Require Extraction.
Require Import ExtrOcamlBasic.
Require Import ExtrOcamlNativeString.

(* makes things hard to read, but no need for 'nat' type *)
Extract Inductive nat => "int"
  [ "0" "(fun x -> x + 1)" ]
  "(fun zero succ n -> if n=0 then zero () else succ (n-1))".

Extract Inlined Constant fst => fst.
Extract Inlined Constant snd => snd.
Extract Inlined Constant eqb => "( = )".
Extract Inlined Constant Nat.eqb => "( = )".
Extract Inlined Constant length => "List.length".
Extract Inlined Constant map => "List.map".
Extract Inlined Constant combine => "List.combine".
Extract Inlined Constant app => "List.append".

Extraction "coqextract" evalfuel inject_term inject_skel.
